﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Net.Mail;

public partial class Testing_Page : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnClick_Click(object sender, EventArgs e)
    {
        try
        {
            Label lbl = new Label();
            bool MailSent = fn_SendMail("sumit@kwebmaker.com,pranit@kwebmaker.com", "arijit.roy@aegisindia.com", "Inquiry Details for Retail LPG Inquiries", "Body", lbl);
            Response.Write(MailSent.ToString());
            Response.Write(lbl.Text);
        }
        catch (Exception ex)
        {            
            throw;
        }
       


    }
    public bool fn_SendMail(string strTo, string strFrom, string strSubject, string strBody, Label lb)
    {
        bool blnRetVal = false;
        string strWebSiteURL = @"http://www.horizondatasys.com/";
        try
        {

            StringBuilder mailbody = new StringBuilder();
            SmtpClient mailClient = null;
            MailMessage message = null;
            mailClient = new SmtpClient();
            message = new MailMessage();
            mailClient.Host = "smtp.logix.in";
            mailClient.Port = 587;

            string strMailUserName = "aegis@aegisindia.com";
            string strMailPassword = "Aegis@1234";

            System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential(strMailUserName, strMailPassword);
            mailClient.UseDefaultCredentials = false;
            mailClient.Credentials = SMTPUserInfo;
            mailClient.EnableSsl = true;
            mailClient.DeliveryMethod = SmtpDeliveryMethod.Network;

            string strFromMail = strMailUserName; //ConfigurationManager.AppSettings["FROM_ADDR"];
            MailAddress fromAddress = new MailAddress(strFrom);
            message.From = fromAddress;

            MailAddress replyTo = new MailAddress(strFrom);
            message.ReplyTo = replyTo;

            message.To.Add(strTo);
            message.Subject = strSubject;

            mailbody.AppendFormat(strBody);
            message.Body = mailbody.ToString();
            message.IsBodyHtml = true;
            message.Priority = MailPriority.High;
            mailClient.Send(message);
            message = null;
            mailClient = null;

            blnRetVal = true;

        }
        catch (Exception ex)
        {
            lb.Text = ex.Message;
            blnRetVal = false;
        }     
        return blnRetVal;
    }
}