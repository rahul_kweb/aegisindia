﻿<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true"
    CodeFile="Corporate_Governances.aspx.cs" Inherits="Corporate_Governances" Title="Aegis Logistics Limited - Investor Relations - Key Financial Data" %>

<%@ Register Src="investor_top.ascx" TagName="investor_top" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%--<!--  scroll to top  -->

<script src="js/jquery.localscroll-1.2.6-min.js" type="text/javascript"></script>
<script src="js/jquery.scrollTo-1.4.0-min.js" type="text/javascript"></script>
<script>

$(document).ready(function () {
	$.localScroll();	
});

</script>

<!--  scroll to top  -->--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:investor_top ID="investor_top1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div id="main_container" style="border-top: 1px solid #e4e4e4; padding-top: 20px;">
        <%--<div id="banner"><img src="images/banner/liquid_logistic.jpg" border="0" alt="Liquid Logistics" title="Liquid logistics" /></div>--%>
        <div class="clear">
        </div>
        <!--  Cont-left  -->
        <div id="cont_left">
            <div id="pg_head">
                Investor Relations
            </div>
            <div id="rel_box_link">
                <ul>
                    <li class="selec">Corporate Governance</li>
                    <%--<li><a href="key_financial_data.aspx">Key Financial Data</a></li>--%>
                    <li><a href="key_financial_data.aspx">Financial Information</a></li>
                    <li><a href="shares.aspx">Stock Information</a></li>
                    <li><a href="Dividend.aspx">Dividend</a></li>
                    <%--<li><a href="Debentures.aspx">Debentures</a></li>--%>
                    <li><a href="reports_filings.aspx">Stock Exchange Communication</a></li>
                    <li><a href="events_presentations.aspx">Investor Presentations</a></li>
                    <%--<li><a href="acquisitions.aspx">Acquisitions</a></li>
    <li><a href="corporate_governance.aspx">Corporate Governance</a></li>
    <li><a href="investor_contact.aspx">Investor Contact</a></li>--%>
                    <%--<li><a href="quarterly_results.aspx">Financial Results</a></li>--%>
                    <li><a href="news.aspx">News and Events</a></li>
                    <li><a href="Investor_Downloads.aspx">Investor Downloads</a></li>
                     <li><a href="InvestorContacts.aspx">Investor Contacts</a></li>
                    <%--<li><a href="live_share_info.aspx">Live Share Info</a></li>--%>
                    <%--<li><a href="unclaimedamounts.aspx">Unpaid & Unclaimed Amounts</a></li>--%>
                    <%--<li><a href="sealord_containers_limited.aspx">Sea Lord Containers Limited (Subsidiary)</a></li>--%>
                    <li><a href="Aegis_Gas_Lpg_Pvt_Ltd.aspx">Aegis Gas (LPG) Pvt. Ltd. (Subsidiary)</a></li>
                </ul>
            </div>
        </div>
        <!--  Cont-left  -->
        <div id="gray_line" style="height: 525px;">
        </div>
        <!--  Cont-right Admin/Documents/Quarterly_Results_2006-07_Pdf1.pdf" -->
        <div id="cont_right">
            <div id="cont_right_box">
                <h3>Our Policies and Codes</h3>

                <ul class="list">
                    <asp:DataList ID="dtlPdf" runat="server">
                        <ItemTemplate>
                            <li><a href='<%#Eval("Pdf_Page_Path_vcr").ToString()=="#"?"#":"/Admin/Documents/"+Eval("Pdf_Page_Path_vcr")+".pdf" %>'
                                target="_blank">
                                <%# Eval("Pdf_Title_vcr")%>
                            </a></li>
                        </ItemTemplate>
                        <%-- <li><a href="pdf/Key_Financial_Data/Financial_highlights_ANNUAL_REPORT_2011-12.pdf" target="_blank">Financial Highlights of Company &amp; Group </a></li>
    
    <li><a href="pdf/Key_Financial_Data/Balance_sheet_ANNUAL_REPORT_2011-12.pdf" target="_blank">Balance Sheet</a></a></li>
    
    <li><a href="pdf/Key_Financial_Data/Segmental_Results_New.pdf" target="_blank">Segmental Results</a></a></li>--%>
                    </asp:DataList>
                </ul>
            </div>
        </div>
        <!--  Cont-right  -->
        <div class="clear" style="height: 40px;">
        </div>
        <!--  Breadcrum  -->
        <div id="bread_box">
            <a href="index.aspx" onmouseover="home.src='images/home_ic_h.jpg'" onmouseout="home.src='images/home_ic.jpg'"
                style="padding: 7px 16px 10px 8px;">
                <img src="images/home_ic.jpg" border="0" name="home" title="Home" alt="" /></a>
            <a href="#">Investor Relations</a> <span style="margin-left: 7px; margin-right: 10px;">Corporate Governance</span>
        </div>
        <!--  Breadcrum  -->
        <%--<div style="float:right; margin-top: 9px;"><a href="#top" onmouseover="tp.src='images/top_h.jpg'" onmouseout="tp.src='images/top.jpg'"><img src="images/top.jpg" border="0" name="tp" alt="" /></a></div>--%>
    </div>
</asp:Content>
