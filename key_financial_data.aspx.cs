﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class key_financial_data : System.Web.UI.Page
{
    Class1 c = new Class1();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindDataList();
            BindOuterDataList();
        }
    }
    private void BindDataList()
    {
        try
        {
            dtlPdf.DataSource = c.Display("EXEC AddUpdateGetPdfPage 'Get_By_Master_Id',0,1");
            dtlPdf.DataBind();
        }
        catch (Exception ex)
        {
 
        }
    }

    private void BindOuterDataList()
    {
        try
        {
            dtlOuter.DataSource = c.Display("EXEC AddUpdateGetPdfMaster 'Get_Years'");
            dtlOuter.DataBind();

            foreach (DataListItem li in dtlOuter.Items)
            {
                int Id = int.Parse(((Label)li.FindControl("lblId")).Text);

                ((DataList)li.FindControl("dtlInner")).DataSource = c.Display("EXEC AddUpdateGetPdfPage 'Get_By_Master_Id',0," + Id);
                ((DataList)li.FindControl("dtlInner")).DataBind();
            }
        }
        catch (Exception ex)
        {

        }
    }
}
