﻿<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true"
    CodeFile="reports_filings_bk.aspx.cs" Inherits="reports_filings_bk" Title="Aegis Logistics Limited - Investor Relations - Reports & Filings" %>

<%@ Register Src="investor_top.ascx" TagName="investor_top" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!--hide and show-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js">
    </script>
    <script>
        $(document).ready(function () {
            $(".show").hide();
        });
    </script>
    <script type="text/javascript">
        function ShowRec() {
            $(".show").toggle('');
            $("#hide").css("background-image", 'url(./images/minus.png)');
            $("#hide").attr("onclick", "newShowRec()");
        }
    </script>
    <script type="text/javascript">
        function newShowRec() {
            $(".show").toggle('');
            $("#hide").css("background-image", 'url(./images/plus.png)');
            $("#hide").attr("onclick", "ShowRec()");
        }
    </script>
    <!--hide and show-->
    <%-- <script type="text/javascript">
        $(function () {
            $("#accordion").accordion();


        });
    </script>

    --%>
    <script type="text/javascript" src="js/mootools.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="css/screen.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:investor_top ID="investor_top1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div id="main_container" style="border-top: 1px solid #e4e4e4; padding-top: 20px;">
        <%--<div id="banner"><img src="images/banner/liquid_logistic.jpg" border="0" alt="Liquid Logistics" title="Liquid logistics" /></div>--%>
        <div class="clear">
        </div>
        <!--  Cont-left  -->
        <div id="cont_left">
            <div id="pg_head">
                Investor Relations</div>
            <div id="rel_box_link">
                <ul>
                    <li><a href="Corporate_Governances.aspx">Corporate Governance</a></li>
                    <li><a href="key_financial_data.aspx">Key Financial Data</a></li>
                    <li><a href="shares.aspx">Shares</a></li>
                    <li><a href="Debentures.aspx">Debentures</a></li>
                    <li class="selec">Reports &amp; Filings</li>
                    <li><a href="events_presentations.aspx">Presentations</a></li>
                    <%--<li><a href="acquisitions.aspx">Acquisitions</a></li>
    <li><a href="corporate_governance.aspx">Corporate Governance</a></li>
    <li><a href="investor_contact.aspx">Investor Contact</a></li>--%>
                    <li><a href="quarterly_results.aspx">Financial Results</a></li>
                    <li><a href="news.aspx">News &amp; Events</a></li>
                    <%--<li><a href="live_share_info.aspx">Live Share Info</a></li>--%>
                    <li><a href="unclaimedamounts.aspx">Unpaid & Unclaimed Amounts</a></li>
                    <li><a href="sealord_containers_limited.aspx">Sea Lord Containers Limited (Subsidiary)</a></li>
                    <li><a href="Aegis_Gas_Lpg_Pvt_Ltd.aspx">Aegis Gas (LPG) Pvt. Ltd. (Subsidiary)</a></li>
                </ul>
            </div>
        </div>
        <!--  Cont-left  -->
        <div id="gray_line" style="height: 525px;">
        </div>
        <!--  Cont-right  -->
        <div id="cont_right">
            <div id="cont_right_box">
                <div id="accordion" style="width: 685px;">
                    <!--start toggle-->
                    <h3 class="toggler atStart">
                        Annual Report</h3>
                    <div class="element atStart">
                        <ul class="list_in">
                            <asp:Repeater ID="rptrheading" runat="server" OnItemDataBound="R1_ItemDataBound">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnid" runat="server" Value='<%#Eval("HeadId") %>' />
                                    <li id="Li2" onclick="javascript:return ShowRec();"><span style="cursor: pointer;">
                                        <%#Eval("Heading")%></span>
                                        <asp:Repeater ID="rptrsubtitle" runat="server">
                                            <ItemTemplate>
                                                <ul>
                                                    <li><a href="Admin/Documents/<%#Eval("PDFFILE","{0}") %>" target="_blank">
                                                        <%#Eval("SUBTITLE")%></a></li>
                                                </ul>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
                            <%--  <li id="Li2" onclick="javascript:return ShowRec();"><span style="cursor: pointer;">Annual
                                Report 2015-2016</span>
                                <ul>
                                    <li><a href="Admin/Documents/Aegis Logistics_Full_Annual_Report_2016.pdf" target="_blank">
                                        Annual Report 2015-16</a></li>
                                </ul>
                            </li>--%>
                            <%--<li id="Li1" onclick="javascript:return ShowRec();"><span style="cursor: pointer;">Annual
                                Report 2014-15</span>
                                <ul>
                                    <li><a href="Admin/Documents/Aegis_Logistics_AR_2015_FINAL.pdf" target="_blank">Annual
                                        Report 2014-15</a></li>
                                    <li><a href="Admin/Documents/58th_Chairmans_Speech_110815_Final.pdf" target="_blank">
                                        Chairman’s Speech at 58th AGM</a></li>
                                    <li><a href="Admin/Documents/Outcome_of_voting_at_58th_Annual_General_Meeting.pdf"
                                        target="_blank">Outcome of voting at 58th Annual General Meeting</a></li>
                                </ul>
                            </li>--%>
                            <%--<li id="hide" onclick="javascript:return ShowRec();"><span style="cursor: pointer;">
                                Annual Report 2013-14</span>
                                <ul>
                                    <li><a href="Admin/Documents/Aegis_Annual%20Report%202013-14.pdf" target="_blank">Annual
                                        Report 2013-14</a></li>
                                    <li><a href="Admin/Documents/News_04_08_201439.pdf" target="_blank">Chairman’s Speech
                                        at 57th AGM</a></li>
                                    <li><a href="Admin/Documents/Outcome%20of%20voting%20at%2057th%20Annual%20General%20Meeting.pdf"
                                        target="_blank">Outcome of voting at 57th Annual General Meeting</a></li>
                                </ul>
                            </li>--%>
                            <%-- <li><a href="Admin/Documents/Reports_Annual_Pdf139.pdf" target="_blank">Annual Report
                                2012-13</a></li>
                            <li><a href="Admin/Documents/Reports_Annual_Pdf65.pdf" target="_blank">Annual Report
                                2011-12</a></li>
                            <li><a href="Admin/Documents/Reports_Annual_Pdf64.pdf" target="_blank">Annual Report
                                2010-11</a></li>
                            <li><a href="Admin/Documents/Reports_Annual_Pdf63.pdf" target="_blank">Annual Report
                                2009-10</a></li>
                            <li><a href="Admin/Documents/Reports_Annual_Pdf62.pdf" target="_blank">Annual Report
                                2008-09</a></li>
                            <li><a href="Admin/Documents/Reports_Annual_Pdf61.pdf" target="_blank">Annual Report
                                2007-08</a></li>--%>
                        </ul>
                    </div>
                    <h3 class="toggler atStart">
                        Shareholding Pattern</h3>
                    <div class="element atStart">
                        <ul class="list_in">
                            <asp:DataList ID="dtlShareHoldingPdf" runat="server">
                                <ItemTemplate>
                                    <li><a href='<%# string.Format("Admin/Documents/{0}.pdf",Eval("Pdf_Page_Path_vcr")) %>'
                                        target="_blank">
                                        <%# Eval("Pdf_Title_vcr")%>
                                    </a></li>
                                </ItemTemplate>
                            </asp:DataList>
                            <%--<li><a href="pdf/reports_filings/Shareholding_Pattern_30-06-2012.pdf" target="_blank">Shareholding Pattern as on 30.06.2012</a></li>--%>
                        </ul>
                    </div>
                    <%--<h3 class="toggler atStart">
                        Code of Conduct</h3>--%>
                    <%--<div class="element atStart">
                        <ul class="list_in">
                            <asp:DataList ID="dtlCodeOfConductPdf" runat="server">
                                <ItemTemplate>
                                    <li><a href='<%# string.Format("Admin/Documents/{0}.pdf",Eval("Pdf_Page_Path_vcr")) %>'
                                        target="_blank">
                                        <%# Eval("Pdf_Title_vcr")%>
                                    </a></li>
                                </ItemTemplate>
                            </asp:DataList>
                        </ul>
                    </div>--%>
                    <h3 class="toggler atStart">
                        Dividend</h3>
                    <div class="element atStart">
                        <ul class="list_in">
                            <asp:DataList ID="dtlDividendPdf" runat="server">
                                <ItemTemplate>
                                    <li><a href='<%# string.Format("Admin/Documents/{0}.pdf",Eval("Pdf_Page_Path_vcr")) %>'
                                        target="_blank">
                                        <%# Eval("Pdf_Title_vcr")%>
                                    </a></li>
                                </ItemTemplate>
                            </asp:DataList>
                            <%--<li><a href="pdf/reports_filings/Shareholding_Pattern_30-06-2012.pdf" target="_blank">Shareholding Pattern as on 30.06.2012</a></li>--%>
                        </ul>
                    </div>
                    <h3 class="toggler atStart">
                        Subsidiary Companies</h3>
                    <div class="element atStart">
                        <ul class="list_in">
                            <li><a href="Admin/Documents/ATPL_2014_15.pdf" target="_blank">Aegis Terminal (Pipavav)
                                Limited Annual Report 2014-15</a></li>
                            <li><a href="Admin/Documents/Aegis_LPG.pdf" target="_blank">Aegis LPG Logistics (Pipavav)
                                Limited Annual Report 2014-15</a></li>
                            <li><a href="Admin/Documents/AGI_2014_15.pdf" target="_blank">Aegis Group International
                                Pte Limited Annual Report 2014-15</a></li>
                            <li><a href="Admin/Documents/AIMS_2014_15.pdf" target="_blank">Aegis International Marine
                                Services Pte Limited Annual Report 2014-15</a></li>
                            <li><a href="Admin/Documents/HALPG_ 2014-15.pdf" target="_blank">Hindustan Aegis LPG
                                Limited Annual Report 2014-15</a></li>
                            <li><a href="Admin/Documents/EILPG_2014_15.pdf" target="_blank">Eastern India LPG Company
                                Private Limited Annual Report 2014-15</a></li>
                            <li><a href="Admin/Documents/KCPL_2014_15.pdf" target="_blank">Konkan Storage Systems
                                (Kochi) Private Limited Annual Report 2014-15</a></li>
                            <%--<li id="Li3" onclick="javascript:return ShowRec();"><span style="cursor: pointer;">
                                Annual Report 2013-14</span>
                                <ul>
                                    <%--<li><a href="Admin/Documents/Reports_Annual_Pdf173.pdf">Annual Report 2013-14</a></li>
                                    <li><a href="Admin/Documents/Reports_Annual_Pdf174.pdf">E-Voting Communication</a></li>
                                    <li><a href="Admin/Documents/Aegis_Annual%20Report%202013-14.pdf" target="_blank">Annual
                                        Report 2013-14</a></li>
                                    <%--<li><a href="Admin/Documents/Aegis_E-Voting%20Communicaton.pdf" target="_blank">E-Voting Communication</a></li>
                                    <li><a href="Admin/Documents/News_04_08_201439.pdf" target="_blank">Chairman’s Speech
                                        at 57th AGM</a></li>
                                    <li><a href="Admin/Documents/Outcome%20of%20voting%20at%2057th%20Annual%20General%20Meeting.pdf"
                                        target="_blank">Outcome of voting at 57th Annual General Meeting</a></li>
                                </ul>
                            </li>--%>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--  Cont-right  -->
        <div class="clear" style="height: 40px;">
        </div>
        <!--  Breadcrum  -->
        <div id="bread_box">
            <a href="index.aspx" onmouseover="home.src='images/home_ic_h.jpg'" onmouseout="home.src='images/home_ic.jpg'"
                style="padding: 7px 16px 10px 8px;">
                <img src="images/home_ic.jpg" border="0" name="home" title="Home" /></a> <a href="#">
                    Investor Relations</a> <span style="margin-left: 7px; margin-right: 10px;">Reports &amp;
                        Filings</span>
        </div>
    </div>
</asp:Content>
