﻿<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true" CodeFile="hse_highlights.aspx.cs" Inherits="hse_highlights" Title="Aegis - HSE - Highlights" %>

<%@ Register src="hse_top.ascx" tagname="hse_top" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<%--<!--  scroll to top  -->

<script src="js/jquery.localscroll-1.2.6-min.js" type="text/javascript"></script>
<script src="js/jquery.scrollTo-1.4.0-min.js" type="text/javascript"></script>
<script>

$(document).ready(function () {
	$.localScroll();	
});

</script>

<!--  scroll to top  -->--%>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:hse_top ID="hse_top1" runat="server" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

<div id="main_container">


	<div id="banner"><img src="images/banner/liquid_logistic.jpg" border="0" alt="Liquid Logistics" title="Liquid logistics" runat="server" /></div>
		
	<div class="clear"></div>
        
        
    <!--  Cont-left  -->  
		
	<div id="cont_left">
    
    <div id="pg_head">HSE</div>
    
    <div id="rel_box_link">
    <ul>
    <li class="selec">Highlights</li>
  	<li><a href="hse_training_development.aspx">Training  &amp; Development</a></li>
    </ul>
    </div>
    
    
    <div style="float:left; margin-top:10px;"><a href="#"><img src="images/hse/dw_hse_policy.jpg" border="0" alt="" runat="server" /></a></div>
    
    
    </div>
        
    <!--  Cont-left  -->  
    
    <div id="gray_line" style="height:400px;"></div>
    
    <!--  Cont-right  -->
    
    <div id="cont_right">
   
    
    <div id="cont_right_box">
     <asp:Label ID="lblContent" runat="server"></asp:Label>
  <%--  
    <ul class="list">
    <li>Reporting &amp; Monitoring of Potential Incidents, Near miss &amp; unusual occurrences</li>
    <li>HSE Reactive and Proactive indicators are part of top Management review</li>
    <li>Comprehensive Review of OHSE aspects undertaken by dedicated team with Remedial Action plan tracking</li>
    <li>Six Sigma Project undertaken to bring down Tanker Turnaround cycle Time (by 35%)</li>
    <li>Security assessment &amp; measures - surveillance cameras on terminal sites</li>
    <li>Operating Manuals as per Integrated Management System.</li>
    </ul>
    
    <div>
    <img src="images/hse/hse_1.jpg" width="210px" height="120px" border="0" title="HSE" style="margin-right: 20px; border: 1px solid #ddd;" />
    <img src="images/hse/hse_2.jpg" width="210px" height="120px" border="0" title="HSE" style="margin-right: 20px; border: 1px solid #ddd;" />
    <img src="images/hse/hse_3.jpg" width="210px" height="120px" border="0" title="HSE" style="border: 1px solid #ddd;" />
    </div>
    --%>
    
    </div>        
    
    </div>
    
    <!--  Cont-right  -->

<div class="clear" style="height:40px;"></div>

<!--  Breadcrum  -->
        
    <div id="bread_box">
    <a href="index.aspx" onmouseover="home.src='images/home_ic_h.jpg'" onmouseout="home.src='images/home_ic.jpg'" style="padding:7px 16px 10px 8px;"><img src="images/home_ic.jpg" border="0" name="home" title="Home" alt="" runat="server" /></a>
    <a href="#">HSE</a>
    <span style="margin-left:7px; margin-right:10px;">Highlights</span>
    </div>
        
    <!--  Breadcrum  -->

<%--<div style="float:right; margin-top: 9px;"><a href="#top" onmouseover="tp.src='images/top_h.jpg'" onmouseout="tp.src='images/top.jpg'"><img src="images/top.jpg" border="0" name="tp" alt="" runat="server" /></a></div>--%>
</div>

</asp:Content>

