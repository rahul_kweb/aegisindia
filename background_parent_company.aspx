﻿<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true" CodeFile="background_parent_company.aspx.cs" Inherits="background_parent_company" Title="Aegis - About Us - Background" %>

<%@ Register src="about_top.ascx" tagname="about_top" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<%-- <!--  scroll to top  -->

<script src="js/jquery.localscroll-1.2.6-min.js" type="text/javascript"></script>
<script src="js/jquery.scrollTo-1.4.0-min.js" type="text/javascript"></script>
<script type="text/javascript">

$(document).ready(function () {
	$.localScroll();	
});

</script>

<!--  scroll to top  --> --%>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:about_top ID="about_top1" runat="server" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

<div id="main_container">


	<div id="banner"><img src="images/banner/liquid_logistic.jpg" border="0" alt="Liquid Logistics" title="Liquid logistics" runat="server" /></div>
		
	<div class="clear"></div>
        
        
    <!--  Cont-left  -->  
		
	<div id="cont_left">
    
    <div id="pg_head"><a href="company_profile.aspx">About Us</a></div>
    
    <div id="rel_box_link">
    <ul>
    <%--<li><a href="company_profile.aspx">Company Profile</a></li>--%>
    <li class="selec">Background</li>
  	<li><a href="key_management_team.aspx">Key Management / Team</a></li>
  	<li><a href="partners.aspx">Partners</a></li>
  	<li><a href="associations_credentials.aspx">Associations &amp; Credentials</a></li>
  	<li><a href="initiatives.aspx">Initiatives</a></li>
  	<li><a href="infrastructure.aspx">Infrastructure</a></li>
  	<li><a href="awards_recognition.aspx" style="border-bottom:none;">Awards &amp; Recognition</a></li>
    </ul>
    </div>
   
    
    </div>
        
    <!--  Cont-left  -->  
    
    <div id="gray_line" style="height:450px;"></div>
    
    <!--  Cont-right  -->
    
    <div id="cont_right">
    
    <div id="cont_right_box">
    
 <%--   <ul class="list">
    <asp:Label ID="lblContent" runat="server" Text=""></asp:Label>
    <li>Leading Petroleum, Chemical and Gas Company since 1977</li>
    <li>Largest importer of LPG in the Pvt. Sector in Country</li>
    <li>Extensive experience in oil terminals and waterfront management</li>
    <li>Demonstrated excellent HSE and Quality track record</li>
    <li>Leading Pvt. Sector company in Auto Gas sector</li>
    <li>Significant Cash reserves. Capacity to invest Capex</li>
    <li>Listed on BSE & NSE with credit ratings -
    	<ul class="list_in">
        <li>P1+ by CRISIL for Working Capital</li>
        <li>“AA” by CARE for Long-Term Borrowings</li>
    	</ul>
    </li>
    </ul>--%>
    
    <asp:Label ID="lblContent" runat="server" Text=""></asp:Label>
    
    <img src="images/company_sitemap.jpg" border="0" alt="Aegis Logistics Limited" title="Aegis Logistics Limited" runat="server" />
    
    </div>        
    
    </div>
    
    <!--  Cont-right  -->

<div class="clear" style="height:40px;"></div>

<!--  Breadcrum  -->
        
    <div id="bread_box">
    <a href="index.aspx" onmouseover="home.src='images/home_ic_h.jpg'" onmouseout="home.src='images/home_ic.jpg'" style="padding:7px 16px 10px 8px;"><img src="images/home_ic.jpg" border="0" name="home" title="Home" alt="" runat="server" /></a>
    <a href="company_profile.aspx">About Us</a>
    <span style="margin-left:7px; margin-right:10px;">Background-Parent Company</span>
    </div>
        
    <!--  Breadcrum  -->

<%--<div style="float:right; margin-top: 9px;"><a href="#top" onmouseover="tp.src='images/top_h.jpg'" onmouseout="tp.src='images/top.jpg'"><img src="images/top.jpg" border="0" name="tp" alt="" runat="server" /></a></div>--%>
</div>

</asp:Content>

