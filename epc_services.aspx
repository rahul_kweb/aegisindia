﻿<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true" CodeFile="epc_services.aspx.cs" Inherits="epc_services" Title="Aegis Logistics Limited - Business Mix - EPC Services" %>

<%@ Register src="business_top.ascx" tagname="business_top" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<%--<!--  scroll to top  -->

<script src="js/jquery.localscroll-1.2.6-min.js" type="text/javascript"></script>
<script src="js/jquery.scrollTo-1.4.0-min.js" type="text/javascript"></script>
<script>

$(document).ready(function () {
	$.localScroll();	
});

</script>

<!--  scroll to top  -->--%>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:business_top ID="business_top1" runat="server" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

<div id="main_container">


	<div id="banner">
	<%--<img src="images/banner/general.jpg" border="0" alt="EPC Services" title="EPC Services" runat="server" />--%>
	<asp:Image runat="server" Visible="false"  border="0"  ID="ImgBanner" alt="EPC Services" title="EPC Services" ImageUrl='<%# string.Format("~/Admin/images/{0}",Eval("Image_vcr")) %>' />
	</div>
		
	<div class="clear"></div>
        
        
    <!--  Cont-left  -->  
		
	<div id="cont_left">
    
    <div id="pg_head">Business Mix</div>
    
   <div id="rel_box" class="rel_box liquid">
    
    <a href="liquid_logistics.aspx">Liquid Logistics</a>

    </div>
    
    <div id="rel_box" class="rel_box gas">

    <a href="gas_logistics.aspx">Gas Logistics</a>    </div>
    
    <div id="rel_box" class="rel_box epc">
   
    <a href="epc_services.aspx" class="active">EPC Services</a>    </div>
    
    <div id="rel_box" class="rel_box retail">
   
         <a href="http://www.aegisretail.in"  target="_blank">Retail LPG</a>
    </div>
    
    
      <div id="rel_box" class="rel_box marine">
 
  <a href="marine.aspx">Marine</a>    
  </div>
    
    
    </div>
        
    <!--  Cont-left  -->  
    
    <div id="gray_line" style="height:500px;"></div>
    
    <!--  Cont-right  -->
    
    <div id="cont_right">
   
    
    	<div id="cont_right_box">
    	 <asp:Label ID="lblContent" runat="server" Text=""></asp:Label>
    	<%--
          <p>As part of this division, we provide the following services for Oil &amp; Gas installations which typically 
          include, <strong>Terminals, Depots, Retail Outlets, Storage installations at manufacturing plants</strong> etc.</p>
          
          <div class="green_heading">Operation &amp; Maintenance</div>
          
          <p><img src="images/epc_services/oper_mant.jpg" title="Operation &amp; Maintenance" alt="" runat="server" style="float: right; margin:0px 0px 0px 30px; border:1px solid #c8c8c8;" />
          We provide complete manpower and technical expertise for Oil &amp; Gas installations. Our goal is to carry out end to end 
          operations and help our customer with the Risk Management of the installations.</p>
          
          <div class="clear" style="height:20px;"></div>
          
          <div class="green_heading">Build Own Operate / Build Own Operate Transfer</div>
          
          <p><img src="images/epc_services/build_own.jpg" title="Build Own Operate/ Build Own Operate Transfer" alt="" runat="server" style="float:right; margin:0px 0px 0px 30px; border:1px solid #c8c8c8;" />
          As a combination of the above two, our years of experience in building and operating Oil &amp; Gas installations helps 
          us provide the customer with the expertise required to design and build installations as per International safety 
          standards. In addition to this, we also carry out the complete operations. The capex by us also helps the customer 
          with their cash flows.</p>
          
          <div class="clear" style="height:20px;"></div>
          
          <div class="green_heading">Bina Dispatch Terminal – BORL</div>
          
          <ul class="list"><img src="images/epc_services/borl_1.jpg" title="Bina Dispatch Terminal – BORL" alt="" runat="server" style="float:right; margin:0px 0px 0px 30px; border:1px solid #c8c8c8;" />
          <li>Awarded 5 years O &amp; M contract in Dec, 2010</li>
          <li>Terminal capacity – 4,50,000 KL</li>
          <li>Assisted in successful commissioning</li>
          <li>Handling HSD, MS, ATF, SKO, Naphtha &amp; LPG</li>
          <li>Product receipt from refinery through pipeline</li>
          <li>Product despatch through tank lorries, rail wagons, Bina-Kota pipeline</li>
          <li>Round the clock close co-ordination with BORL representatives to ensure smooth and safe operations. Close to 100 Staff Deputed.</li>
          </ul>
          
          <div class="green_heading">Puthuvypeen COT - BPCL</div>
          
          <ul class="list"><img src="images/epc_services/bpcl.jpg" title="Puthuvypeen COT - BPCL" alt="" runat="server" style="float:right; margin:0px 0px 0px 30px; border:1px solid #c8c8c8;" />
          <li>Awarded the contract in October 2007</li>
          <li>Renewed in November 2008 and then in 2009</li>
          <li>Terminal capacity – 2,46,000 KL</li>
          <li>Assisted in successful commissioning</li>
          <li>Operating the complete crude oil facility (Throughput approx. 8 MMTPA)</li>
          <li>Receipt from SBM and Disptach via pipeline to the refinery</li>
          <li>Adjudged 1st prize for Contractor‟s Safety Award 2010</li>
          </ul>
          
          <div class="green_heading">New Contracts Awarded</div>
          
          <ul class="list">
          <li><strong>ONGC</strong>
          		<ul class="list_in">
                <li>Aegis has been awarded the contract for O &amp; M services at Nhava Supply Base by ONGC for the next 
                3 years.</li>
                <li>Scope of work includes handling of HFHSD and ATF facility which include bunkering of ONGC vessels and 
                drumming of ATF for fuelling at their Helibase.</li>
                </ul>
          </li>
          <li><strong>BPCL</strong>
          		<ul class="list_in">
                <li>Aegis has also been technically qualified and declared as L1 by BPCL to provide O &amp; M services at 
                their JNPT Bunker Terminal for the next 2 years.</li>
                <li>Scope of Work includes handling BPCL‟s terminal at JNPT to support their commercial bunkering activities.</li>
                </ul></li>
          </ul>
          --%>
		</div>
    </div>
    
    <!--  Cont-right  -->

<div class="clear" style="height:40px;"></div>

<!--  Breadcrum  -->
        
    <div id="bread_box">
    <a href="index.aspx" onmouseover="home.src='images/home_ic_h.jpg'" onmouseout="home.src='images/home_ic.jpg'" style="padding:7px 16px 10px 8px;"><img src="images/home_ic.jpg" border="0" name="home" title="Home" alt="" runat="server" /></a>
    <a href="#">Business Mix</a>
    <span style="margin-left:7px; margin-right:10px;">EPC Services</span>
    </div>
        
    <!--  Breadcrum  -->

<%--<div style="float:right; margin-top: 9px;"><a href="#top" onmouseover="tp.src='images/top_h.jpg'" onmouseout="tp.src='images/top.jpg'"><img src="images/top.jpg" border="0" name="tp" alt="" runat="server" /></a></div>--%>
</div>

</asp:Content>

