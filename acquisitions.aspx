﻿<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true" CodeFile="acquisitions.aspx.cs" Inherits="acquisitions" Title="Aegis - Investor Relations - Acquisitions" %>

<%@ Register src="investor_top.ascx" tagname="investor_top" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<%-- <!--  scroll to top  -->

<script src="js/jquery.localscroll-1.2.6-min.js" type="text/javascript"></script>
<script src="js/jquery.scrollTo-1.4.0-min.js" type="text/javascript"></script>
<script>

$(document).ready(function () {
	$.localScroll();	
});

</script>

<!--  scroll to top  --> --%>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:investor_top ID="investor_top1" runat="server" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

<div id="main_container">


	<div id="banner"><img src="images/banner/liquid_logistic.jpg" border="0" alt="Liquid Logistics" title="Liquid logistics" /></div>
		
	<div class="clear"></div>
        
        
    <!--  Cont-left  -->  
		
	<div id="cont_left">
    
    <div id="pg_head">Investor Relations</div>
    
    <div id="rel_box_link">
    <ul>
    <li><a href="key_financial_data.aspx">Key Financial Data</a></li>
    <li><a href="shares.aspx">Shares</a></li>
    <li><a href="reports_filings.aspx">Reports &amp; Filings</a></li>
    <li><a href="events_presentations.aspx">Presentations</a></li>
    <li class="selec">Acquisitions</li>
    <li><a href="corporate_governance.aspx">Corporate Governance</a></li>
    <li><a href="investor_contact.aspx">Investor Contact</a></li>
    <li><a href="quarterly_results.aspx">Quarterly Results</a></li>
    <li><a href="live_share_info.aspx">Live Share Info</a></li>
    </ul>
    </div>  
    
    
    </div>
        
    <!--  Cont-left  -->  
    
    <div id="gray_line" style="height:400px;"></div>
    
    <!--  Cont-right  -->
    
    <div id="cont_right">
   
    <div id="cont_right_box">
    
     <asp:Label ID="lblContent" runat="server"></asp:Label>
    
    
    <%--<table width="680px" border="0" cellpadding="12px" cellspacing="1px" bgcolor="#d6d6d6">
                <tr bgcolor="#e1e1e1">
                <td width="170px" align="center"><strong>Year of Acquisition</strong></td>
                <td width="330px" align="center"><strong >Company</strong></td>
                <td width="170px" align="center"><strong>% Shareholding</strong></td>
                </tr>
                <tr bgcolor="#fff">
                <td align="center">2006</td>
                <td>Sea Lord Containers Ltd.</td>
                <td align="center">75%</td>
                </tr> 
                <tr bgcolor="#fff">
                <td align="center">2007</td>
                <td>Konkan Storage Systems (Kochi) Pvt. Ltd.</td>
                <td align="center">100% </td>
                </tr>
                <tr bgcolor="#fff">
                <td align="center">2010</td>
                <td>Shell Gas (LPG) India Pvt. Ltd</td>
                <td align="center">100%</td>
                </tr> 
                
                </table>--%>
    
    
    </div>        
    
    </div>
    
    <!--  Cont-right  -->

<div class="clear" style="height:40px;"></div>

<!--  Breadcrum  -->
        
    <div id="bread_box">
    <a href="index.aspx" onmouseover="home.src='images/home_ic_h.jpg'" onmouseout="home.src='images/home_ic.jpg'" style="padding:7px 16px 10px 8px;"><img src="images/home_ic.jpg" border="0" name="home" title="Home" alt="" /></a>
    <a href="#">Investor Relations</a>
    <span style="margin-left:7px; margin-right:10px;">Acquisitions</span>
    </div>
        
    <!--  Breadcrum  -->

<%--<div style="float:right; margin-top: 9px;"><a href="#top" onmouseover="tp.src='images/top_h.jpg'" onmouseout="tp.src='images/top.jpg'"><img src="images/top.jpg" border="0" name="tp" alt="" /></a></div>--%>
</div>

</asp:Content>

