﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

public partial class aegis_master : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
 
        }
    }

    protected void btnSearch_Click(object sender, ImageClickEventArgs e)
    {
        string key = search_box.Value.Trim();

        if (key.Trim().Length > 0)
        {
            Response.Redirect("Search.aspx?jHu567xfgfkwpqQ3=" + key.Trim(), true);
        }
    }
}
