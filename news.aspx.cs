﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class news : System.Web.UI.Page
{
    Class1 c = new Class1();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindOuterDataList();
            //c.BindInnerPageBanner(43, ImgBanner);

        }
    }
    private void BindOuterDataList()
    {
        try
        {
            dtlOuter.DataSource = c.Display("EXEC AddUpdateGetNewsMaster 'Get_News_Year'");
            dtlOuter.DataBind();

            foreach (DataListItem li in dtlOuter.Items)
            {
                int Year = int.Parse(((Label)li.FindControl("lblYear")).Text);

                ((DataList)li.FindControl("dtlInner")).DataSource = c.Display("EXEC AddUpdateGetNewsMaster 'Get_By_Year',0," + Year);
                ((DataList)li.FindControl("dtlInner")).DataBind();
            }
        }
        catch (Exception ex)
        { }
    }
}
