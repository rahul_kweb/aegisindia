﻿<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true" CodeFile="news1.aspx.cs" Inherits="news1" Title="Aegis Logistics Limited - Investor Relations - News" %>

<%@ Register src="investor_top.ascx" tagname="investor_top" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<%--<!--  scroll to top  -->

<script src="js/jquery.localscroll-1.2.6-min.js" type="text/javascript"></script>
<script src="js/jquery.scrollTo-1.4.0-min.js" type="text/javascript"></script>
<script>

$(document).ready(function () {
	$.localScroll();	
});

</script>

<!--  scroll to top  -->--%>

    <script type="text/javascript" src="js/mootools.js"></script>    
    <link rel="stylesheet" type="text/css" media="screen" href="css/screen.css" />

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:investor_top ID="investor_top1" runat="server" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

<div id="main_container" style="border-top: 1px solid #e4e4e4; padding-top:20px;">


	<%--<div id="banner"><img src="images/banner/liquid_logistic.jpg" border="0" alt="Liquid Logistics" title="Liquid logistics" /></div>--%>
		
	<div class="clear"></div>
        
        
    <!--  Cont-left  -->  
		
	<div id="cont_left">
    
    <div id="pg_head">Investor Relations</div>
    
    <div id="rel_box_link">
    <ul>
    <li><a href="key_financial_data.aspx">Key Financial Data</a></li>
    <li><a href="shares.aspx">Shares</a></li>
    <li><a href="reports_filings.aspx">Reports &amp; Filings</a></li>
    <li><a href="events_presentations.aspx">Presentations</a></li>
    <%--<li><a href="acquisitions.aspx">Acquisitions</a></li>
    <li><a href="corporate_governance.aspx">Corporate Governance</a></li>
    <li><a href="investor_contact.aspx">Investor Contact</a></li>--%>
    <li class="selec">Quarterly Results</li>
    <li><a href="news.aspx">News &amp; Events</a></li>
    <%--<li><a href="live_share_info.aspx">Live Share Info</a></li>--%>
    </ul>
    </div>  
    
    
    </div>
        
    <!--  Cont-left  -->  
    
    <div id="gray_line" style="height:500px;"></div>
    
    <!--  Cont-right  -->
    
    <div id="cont_right">
    
    <div id="cont_right_box">
    
     <div id="accordion" style="width:950px;">
                        <!--start toggle-->
        <asp:DataList ID="dtlOuter" runat="server">
        <ItemTemplate>
            <h3 class="toggler atStart" style="border-bottom: 1px solid #eee;">
                <asp:Label ID="lblYear" runat="server" Text='<%# Eval("Year_int")%>'></asp:Label>         
            
            </h3>
             <div class="element atStart">                          
                          <ul class="list_news">  
            <asp:DataList ID="dtlInner" runat="server">
            <ItemTemplate>
                                            
                            <li><p><strong><%# Eval("News_Date_dtm")%><br />
                           <%# Eval("Title_vcr")%>
                            </strong><br />
                            <%# Eval("SubTitle_vcr")%>
                            
                            </p>
                             <p><a href='<%# string.Format("Admin/Documents/{0}.pdf",Eval("Pdf_Path_vcr")) %>' target="_blank" class="v_pdf" >PDF</a></p>
                            
                             <%--<p><a href='<%# string.Format("Admin/Documents/{0}.pdf",Eval("Pdf_Path_vcr")) %>' target="_blank"><img src="images/pdf.jpg" /></a></p>--%>
                            </li>
                          
                
            </ItemTemplate>            
            </asp:DataList>
             </ul>
           </div>
            
        </ItemTemplate>
        
        </asp:DataList>
                        
                        <%--<h3 class="toggler atStart" style="border-bottom: 1px solid #eee;">2012</h3>
                        <div class="element atStart">                          
                          <ul class="list_news">                          
                            <li><p><strong>31 Jul, 2012<br />CHAIRMAN’S SPEECH 55TH ANNUAL GENERAL MEETING AT VAPI ON TUESDAY</strong><br />It gives me immense pleasure in welcoming you to the 55th Annual General Meeting of Aegis Logistics Limited. On behalf of the Board of Directors and my colleagues at Aegis, I thank you all for your kind presence here today.</p>
                            <p><a href="news/2012/Chairmans_Speech.pdf" target="_blank"><img src="images/pdf.jpg" /></a></p>
                            </li>
                            
                            <li><p><strong>30 May, 2012 <br />PRESS RELEASE AEGIS GROUP REPORTS PROFIT RISE IN Q4 </strong><br /> For Q4FY12 Aegis Group reported Net Profit of Rs.81.47 Crs. (Rs.17.55 Crs.) largely driven by good operating performance and reversal of Mark to Market provisioning on account of strengthening of Rupee in Q4.</p>
                            <p><a href="news/2012/Press_Release_30-05-2012.pdf" target="_blank"><img src="images/pdf.jpg" /></a></p>
                            </li>
                            
                            <li><p><strong>14 Feb, 2012 <br />AEGIS RESULTS Q3FY12 </strong><br />For Q3FY12 Aegis Group reported an increase in Revenue by 50% to Rs.1102 Crs. (Rs.731 Crs.) and Operating Profit (PBIDT) of Rs.20.38 Crs. (Rs.18.60 Crs.), an increase over 9%. However, increased provision towards mark to market of Foreign Currency exposure have resulted into a net loss of Rs.51.48 Crs. (profit of Rs.16.65 Crs.). </p>
                            <p><a href="news/2012/Press_Release_140212.pdf" target="_blank"><img src="images/pdf.jpg" /></a></p>
                            </li>
                            
                            <li><p><strong>20 Jan, 2012 <br />AEGIS GROUP LAUNCHES ITS “MARINE PRODUCTS DIVISION” </strong><br />Aegis Group is launching its Marine Products Division today. The Marine Products Division aims to provide complete solution to Marine community which would include extensive range of bunker fuels, marine lubricants and technical services at various ports.</p>
                            <p><a href="news/2012/AEGIS_MARINE_PRODUCTS_LAUNCH_PRESS_RELEASE.pdf" target="_blank"><img src="images/pdf.jpg" /></a></p>
                            </li>
                            
                          </ul>                          
                        </div>                        
                        
                        <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
                        
                        <h3 class="toggler atStart" style="border-bottom: 1px solid #eee;">2011</h3>
                        <div class="element atStart">
                          <ul class="list_news">                          
                            <li><p><strong>29 Jul, 2011<br />CHAIRMAN’S SPEECH AT 54TH ANNUAL GENERAL MEETING AT VAPI ON FRIDAY, 29TH JULY, 2011 </strong><br />It gives me immense pleasure in welcoming you to the 54th Annual General Meeting of Aegis Logistics Limited. On behalf of the Board of Directors and my colleagues at Aegis, I thank you all for your kind presence here today.</p>
                            <p><a href="news/2011/ALL_54_AGM_2011_Chairmans_Speech.pdf" target="_blank"><img src="images/pdf.jpg" /></a></p>
                            </li>
                            
                            <li><p><strong>29 Jul, 2011<br />PRESS RELEASE SHARP INCREASE IN AEGIS Q1 NET PROFIT - UP BY 92% </strong><br />For Q1FY12, Aegis Group reported a net profit of Rs. 16.61 Crs. (from Rs. 8.67 Crs) — an increase of 92% on a year over year basis as a result of significant developments in its gas business.</p>
                            <p><a href="news/2011/Press_Release_290711_final.pdf" target="_blank"><img src="images/pdf.jpg" /></a></p>
                            </li>
                            
                            <li><p><strong>30 May, 2011 <br />AEGIS GROUP PROFIT RISE BY 48% IN Q4 </strong><br />PRESS RELEASE For Q4FY11 Aegis Group reported net profit of Rs.16.86 Crs (Rs.11.40 Crs.), an increase of 48% on QoQ basis largely driven by good performance of Gas business.</p>
                            <p><a href="news/2011/PressRel305.pdf" target="_blank"><img src="images/pdf.jpg" /></a></p>
                            </li>
                            
                            <li><p><strong>25 Feb, 2011<br />PRESS RELEASE AEGIS TO OFFER 6% EQUITY STAKE TO INFRASTRUCTURE INDIA HOLDINGS FUND </strong><br />Aegis Logistics Limited, a leader in Oil, Gas and Chemical Logistics today entered into a Shareholders Agreement with Infrastructure India Holdings Fund LLC (IIHF)</p>
                            <p><a href="news/2011/20110225210533365.pdf" target="_blank"><img src="images/pdf.jpg" /></a></p>
                            </li>
                            
                            <li><p><strong>12 Feb, 2011 <br />PRESS RELEASE AEGIS Q3FY11 </strong><br /> For Q3 FY11 Aegis Group reported revenue of Rs.731 crores with an increase of over 200% QoQ and PBT of Rs.16.65 crores showing an increase of 24% QoQ.</p>
                            <p><a href="news/2011/PRESS_RELEASE1_11022011.pdf" target="_blank"><img src="images/pdf.jpg" /></a></p>
                            </li>
                          </ul>
                        </div> 
                        
                        
                        <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->                       
                        
                        <h3 class="toggler atStart" style="border-bottom: 1px solid #eee;">2010</h3>
                        <div class="element atStart">
                          <ul class="list_news">                          
                            <li><p><strong>23 Nov, 2010 <br />AEGIS ENTERS INTO A MAJOR DEAL WITH APM TERMINALS </strong><br />AEGIS ENTERS INTO A MAJOR DEAL WITH APM TERMINALS PIPAVAV FOR A PORT INFRASTRUCTURE PROJECT</p>
                            <p><a href="news/2010/PRESS_RELEASE_23112010.pdf" target="_blank"><img src="images/pdf.jpg" /></a></p>
                            </li>
                            
                            <li><p><strong>30 Oct, 2010 <br />PRESS RELEASE AEGIS Q2FY11 </strong><br />For Q2FY11 Aegis Group reported Revenue of Rs. 273 Crs. </p>
                            <p><a href="news/2010/Press_Release_Q2FY11.pdf" target="_blank"><img src="images/pdf.jpg" /></a></p>
                            </li>
                            
                            <li><p><strong>17 Sep, 2010  <br />Aegis in OUTLOOK PROFIT </strong><br />Stepping on gas</p>
                            <p><a href="news/2010/OUTLOOK_PROFIT.pdf" target="_blank"><img src="images/pdf.jpg" /></a></p>
                            </li>
                            
                            <li><p><strong>01 Sep, 2010 <br />FUND RAISING & STOCK SPLIT </strong><br />AEGIS BOARD APPROVES FUND RAISING OF UPTO 100 CRORES AND A STOCK SPLIT</p>
                            <p><a href="news/2010/PRESS_RELEASE_01092010v1.pdf" target="_blank"><img src="images/pdf.jpg" /></a></p>
                            </li>
                            
                            <li><p><strong>17 Jul, 2010 <br />Chairman's Speech at 53rd annual General meeting </strong><br />Chairman's Speech at 53rd annual General meeting</p>
                            <p><a href="news/2010/CHAIRMAN_SPEECH.pdf" target="_blank"><img src="images/pdf.jpg" /></a></p>
                            </li>
                            
                            <li><p><strong>07 Jul, 2010<br />Press Release </strong><br />Press Release</p>
                            <p><a href="news/2010/PRESS_RELEASE_07072010.pdf" target="_blank"><img src="images/pdf.jpg" /></a></p>
                            </li>
                            
                            <li><p><strong>02 Jul, 2010 <br />Interview with REUTERS </strong><br />Interview with REUTERS</p>
                            <p><a href="news/2010/THOMSUNREUTERS_INTERVIEW_MD_OF_AEGIS.pdf" target="_blank"><img src="images/pdf.jpg" /></a></p>
                            </li>
                            
                            <li><p><strong>25 Jun, 2010  <br />MEDIA STATEMENT - Oil Price Deregulation </strong><br />MEDIA STATEMENT - Oil Price Deregulation</p>
                            <p><a href="news/2010/tenders.pdf" target="_blank"><img src="images/pdf.jpg" /></a></p>
                            </li>
                            
                          </ul>
                        </div>--%>
                        
                      </div>
    
    </div>        
    
    </div>
    
    <!--  Cont-right  -->

<div class="clear" style="height:40px;"></div>

<!--  Breadcrum  -->
        
    <div id="bread_box">
    <a href="index.aspx" onmouseover="home.src='images/home_ic_h.jpg'" onmouseout="home.src='images/home_ic.jpg'" style="padding:7px 16px 10px 8px;"><img src="images/home_ic.jpg" border="0" name="home" title="Home" alt="Home" /></a>
    <a href="#">Investor Relations</a>
    <span style="margin-left:7px; margin-right:10px;">News &amp; Events</span>
    </div>
        
    <!--  Breadcrum  -->

<%--<div style="float:right; margin-top: 9px;"><a href="#top" onmouseover="tp.src='images/top_h.jpg'" onmouseout="tp.src='images/top.jpg'"><img src="images/top.jpg" border="0" name="tp" alt="Top" /></a></div>--%>
</div>

</asp:Content>

