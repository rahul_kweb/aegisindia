﻿<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true" CodeFile="awards_recognition.aspx.cs" Inherits="awards_recognition" Title="Aegis - About Us - Awards & Recognition" %>

<%@ Register src="about_top.ascx" tagname="about_top" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<%--<!--  scroll to top  -->

<script src="js/jquery.localscroll-1.2.6-min.js" type="text/javascript"></script>
<script src="js/jquery.scrollTo-1.4.0-min.js" type="text/javascript"></script>
<script type="text/javascript">

$(document).ready(function () {
	$.localScroll();	
});

</script>

<!--  scroll to top  -->--%>

<!-- colorbox -->

<link media="screen" rel="stylesheet" href="colorbox/colorbox.css" />	
<script type="text/javascript" src="colorbox/jquery.colorbox.js"></script>
<script type="text/javascript" src="colorbox/id_nos.js"></script>

<!-- colorbox -->

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:about_top ID="about_top1" runat="server" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

<div id="main_container">


	<div id="banner"><img src="images/banner/liquid_logistic.jpg" border="0" alt="Liquid Logistics" title="Liquid logistics" runat="server" /></div>
		
	<div class="clear"></div>
        
        
    <!--  Cont-left  -->  
		
	<div id="cont_left">
    
    <div id="pg_head"><a href="company_profile.aspx">About Us</a></div>
    
    <div id="rel_box_link">
    <ul>
    <%--<li><a href="company_profile.aspx">Company Profile</a></li>
    <li><a href="background_parent_company.aspx">Background</a></li>--%>
    <li><a href="key_management_team.aspx">Key Management &amp; Team</a></li>
    <li><a href="partners.aspx">Partners</a></li>
    <li><a href="associations_credentials.aspx">Credentials</a></li>
    <li><a href="initiatives.aspx">Continuous Improvement</a></li>
    <%--<li><a href="infrastructure.aspx">Infrastructure</a></li>
    <li class="selec">Awards &amp; Recognition</li>--%>
    </ul>
    </div>
    
    
    </div>
        
    <!--  Cont-left  -->  
    
    <div id="gray_line" style="height:500px;"></div>
    
    <!--  Cont-right  -->
    
    <div id="cont_right">
    
    <div id="cont_right_box">
    
    <p>Aegis has been awarded <strong>1st Prize among 30 contractors by BORL</strong> in November 2011 for Safety Practices.</p>
        <asp:DataList ID="dtlImages" runat="server" RepeatColumns="3" RepeatDirection="Horizontal">
        <ItemTemplate>        
             <div class="prize_img">
                    <a runat="server" href='<%# string.Format("Admin/images/{0}",Eval("Image_Path_vcr")) %>' rel="example1">
                     <asp:Image ID="Img1"  runat="server"  ImageUrl='<%# string.Format("~/Admin/images/{0}",Eval("Image_Path_vcr")) %>' /></a>
                    <%--<img src="images/awards/safety_2000.jpg" width="200px" height="136px" border="0" />--%>
            </div>                
        </ItemTemplate>        
        </asp:DataList>
    
    <%--
    <div class="prize_img">
    <a href="images/awards/safety_2000.jpg" rel="example1"><img src="images/awards/safety_2000.jpg" width="200px" height="136px" border="0" /></a>
    </div>
    
    <div class="prize_img">
    <a href="images/awards/safety_2001.jpg" rel="example1"><img src="images/awards/safety_2001.jpg" width="200px" height="136px" border="0" /></a>
    </div>
    
    <div class="prize_img" style="margin-right:0px;">
    <a href="images/awards/safety_2002.jpg" rel="example1"><img src="images/awards/safety_2002.jpg" width="200px" height="136px" border="0" /></a>
    </div>
    
    <div class="prize_img">
    <a href="images/awards/safety_2003.jpg" rel="example1"><img src="images/awards/safety_2003.jpg" width="200px" height="136px" border="0" /></a>
    </div>
    
    <div class="prize_img">
    <a href="images/awards/safety_2009.jpg" rel="example1"><img src="images/awards/safety_2009.jpg" width="200px" height="136px" border="0" /></a>
    </div>
    
    <div class="prize_img" style="margin-right:0px;">
    <a href="images/awards/safety_2010.jpg" rel="example1"><img src="images/awards/safety_2010.jpg" width="200px" height="136px" border="0" /></a>
    </div>
    
    <div class="prize_img">
    <a href="images/awards/cert_1.jpg" rel="example1"><img src="images/awards/cert_1.jpg" width="100px" height="150px" border="0" /></a>
    </div>
    
    <div class="prize_img">
    <a href="images/awards/cert_2.jpg" rel="example1"><img src="images/awards/cert_2.jpg" width="100px" height="150px" border="0" /></a>
    </div>
    
   
    
    <div class="prize_img">
    <a href="images/awards/award_1.jpg" rel="example1"><img src="images/awards/award_1.jpg" width="100px" height="150px" border="0" /></a>
    </div>
    
    <div class="prize_img">
    <a href="images/awards/award_2.jpg" rel="example1"><img src="images/awards/award_2.jpg" width="100px" height="150px" border="0" /></a>
    </div>
    
    <div class="prize_img" style="margin-right:0px;">
    <a href="images/awards/award_3.jpg" rel="example1"><img src="images/awards/award_3.jpg" width="100px" height="150px" border="0" /></a>
    </div>--%>
    
    
    
    </div>        
    
    </div>
    
    <!--  Cont-right  -->

<div class="clear" style="height:40px;"></div>

<!--  Breadcrum  -->
        
    <div id="bread_box">
    <a href="index.aspx" onmouseover="home.src='images/home_ic_h.jpg'" onmouseout="home.src='images/home_ic.jpg'" style="padding:7px 16px 10px 8px;"><img src="images/home_ic.jpg" border="0" name="home" title="Home" /></a>
    <a href="company_profile.aspx">About Us</a>
    <span style="margin-left:7px; margin-right:10px;">Awards &amp; Recognition</span>
    </div>
        
    <!--  Breadcrum  -->

<%--<div style="float:right; margin-top: 9px;"><a href="#top" onmouseover="tp.src='images/top_h.jpg'" onmouseout="tp.src='images/top.jpg'"><img src="images/top.jpg" border="0" name="tp" /></a></div>--%>
</div>


</asp:Content>

