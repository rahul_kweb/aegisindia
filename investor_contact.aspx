﻿<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true" CodeFile="investor_contact.aspx.cs" Inherits="investor_contact" Title="Aegis - Investor Relations - Investor Contacts" %>

<%@ Register src="investor_top.ascx" tagname="investor_top" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<%--<!--  scroll to top  -->

<script src="js/jquery.localscroll-1.2.6-min.js" type="text/javascript"></script>
<script src="js/jquery.scrollTo-1.4.0-min.js" type="text/javascript"></script>
<script>

$(document).ready(function () {
	$.localScroll();	
});

</script>

<!--  scroll to top  -->--%>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:investor_top ID="investor_top1" runat="server" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

<div id="main_container">


	<div id="banner"><img src="images/banner/liquid_logistic.jpg" border="0" alt="Liquid Logistics" title="Liquid logistics" /></div>
		
	<div class="clear"></div>
        
        
    <!--  Cont-left  -->  
		
	<div id="cont_left">
    
    <div id="pg_head">Investor Relations</div>
    
    <div id="rel_box_link">
    <ul>
    <li><a href="key_financial_data.aspx">Key Financial Data</a></li>
    <li><a href="shares.aspx">Shares</a></li>
    <li><a href="reports_filings.aspx">Reports &amp; Filings</a></li>
    <li><a href="events_presentations.aspx">Presentations</a></li>
    <%--<li><a href="acquisitions.aspx">Acquisitions</a></li>
    <li><a href="corporate_governance.aspx">Corporate Governance</a></li>--%>
    <li class="selec">Investor Contact</li>
    <li><a href="quarterly_results.aspx">Quarterly Results</a></li>
    <%--<li><a href="live_share_info.aspx">Live Share Info</a></li>--%>
    </ul>
    </div>  
    
    
    </div>
        
    <!--  Cont-left  -->  
    
    <div id="gray_line" style="height:500px;"></div>
    
    <!--  Cont-right  -->
    
    <div id="cont_right">
   
    
    <div id="cont_right_box">
     <asp:Label ID="lblContent" runat="server"></asp:Label>
     
    <%--
    <p><strong>Dematerialisation of Shares :</strong> The ISIN No. is - INE 208C01017</p>
    
    <p><strong>Compliance Officer :</strong><br />
    Mr. Shivatosh Chakraborty,<br />Company Secretary &amp; General Manager - Legal<br />
    Tel : 022 66663666<br />
    Fax : 022 66663777<br />
    Email : <a href="mailto:secretarial@aegisindia.com" class="anylink">secretarial@aegisindia.com</a></p>
    
    <strong>Listing on Stock Exchanges :</strong>
    <table width="450px" border="0" cellpadding="5px" cellspacing="1px" style="background-color:#d6d6d6;">
            <tr bgcolor="#fff">
            <td valign="top">Shares Listed at :</td>
            <td>Bombay Stock Exchange<br />The National Stock Exchange of India Ltd.</td>
            </tr>
            <tr bgcolor="#fff">
            <td>Stock Code :</td>
            <td>500003 AEGISCHEM</td>
            </tr>
    </table>
    
   <div class="clear" style="height:20px;"></div>
   <strong>Shareholders Information : </strong> Registrar / Share Transfer Agents
   <table width="650px" border="0" cellpadding="0px" cellspacing="1px">
            <tr bgcolor="#fff">
            <td colspan="2"><strong>Mr. N. Narayan</strong></td>
            </tr>
            <tr bgcolor="#fff" valign="top">
            <td width="350px">M/s.Sharepro Services India P Ltd.<br />
                                Unit : Aegis Logistics Ltd.,<br />
                                13 AB, Samhita Warehousing Complex,<br />
                                2nd Floor, Sakinaka Telehpone Exchange Lane,<br />
                                Off. Andheri - Kurla Road,<br />
                                Sakinaka Andheri (E),<br />
                                Mumbai – 400 072
            </td>
            <td width="300px">M/s. Sharepro Services India Pvt. Ltd.<br />
                                Unit: Aegis Logistics Ltd.,<br />
                                912, Raheja Centre,<br />
                                Free Press Journal Road,<br />
                                Nariman Point,<br />
                                Mumbai – 400 021</td>
            </tr>
            <tr>
            <td colspan="2" height="10px"></td>
            </tr>
            <tr bgcolor="#fff">
            <td>Tel : 22-67720300, 22-67720400<br />Fax : 022-28591568, 022-28508927</td>
            <td>Tel : 022-66134700<br />Fax : 022-22825484</td>
            </tr>
            <tr>
            <td colspan="2" height="10px"></td>
            </tr>
            <tr bgcolor="#fff">
            <td>Email : <a href="mailto:sharepro@shareproservices.com" class="anylink">sharepro@shareproservices.com</a></td>
            <td>Email : <a href="mailto:sharepro@shareproservices.com" class="anylink">sharepro@shareproservices.com</a></td>
            </tr>
    </table>
    --%>
    
    </div>        
    
    </div>
    
    <!--  Cont-right  -->

<div class="clear" style="height:40px;"></div>

<!--  Breadcrum  -->
        
    <div id="bread_box">
    <a href="index.aspx" onmouseover="home.src='images/home_ic_h.jpg'" onmouseout="home.src='images/home_ic.jpg'" style="padding:7px 16px 10px 8px;"><img src="images/home_ic.jpg" border="0" name="home" title="Home" alt="Home" /></a>
    <a href="#">Investor Relations</a>
    <span style="margin-left:7px; margin-right:10px;">Investor Contacts</span>
    </div>
        
    <!--  Breadcrum  -->

<%--<div style="float:right; margin-top: 9px;"><a href="#top" onmouseover="tp.src='images/top_h.jpg'" onmouseout="tp.src='images/top.jpg'"><img src="images/top.jpg" border="0" name="tp" alt="Top" /></a></div>--%>
</div>

</asp:Content>

