﻿<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true" CodeFile="liquid_logistics.aspx.cs" Inherits="liquid_logistics" Title="Aegis Logistics Limited - Business Mix - Liquid Logistics" %>

<%@ Register src="business_top.ascx" tagname="business_top" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<!--  tabs  -->

<script type="text/javascript" src="js/tabber.js"></script>
<link rel="stylesheet" href="css/example.css" type="text/css" media="screen" />

<script type="text/javascript">

/* Optional: Temporarily hide the "tabber" class so it does not "flash"
   on the page as plain HTML. After tabber runs, the class is changed
   to "tabberlive" and it will appear. */

document.write('<style type="text/css">.tabber{display:none;}<\/style>');
</script>

<!--  tabs  -->


<%--<!--  scroll to top  -->

<script src="js/jquery.localscroll-1.2.6-min.js" type="text/javascript"></script>
<script src="js/jquery.scrollTo-1.4.0-min.js" type="text/javascript"></script>
<script>

$(document).ready(function () {
	$.localScroll();	
});

</script>

<!--  scroll to top  -->--%>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:business_top ID="business_top1" runat="server" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

<div id="main_container">


	<div id="banner">
	<%--<img src="images/banner/liquid_logistic.jpg" border="0" alt="Liquid Logistics" title="Liquid logistics" runat="server" />--%>
	<asp:Image runat="server" Visible="false"  border="0"  ID="ImgBanner" alt="Liquid Logistics" title="Liquid logistics"  ImageUrl='<%# string.Format("~/Admin/images/{0}",Eval("Image_vcr")) %>' />
	</div>
		
	<div class="clear"></div>
        
        
    <!--  Cont-left  -->  
		
	<div id="cont_left">
    
    <div id="pg_head">Business Mix</div>
    

    
    <div id="rel_box" class="rel_box liquid">
    
    <a href="liquid_logistics.aspx" class="active">Liquid Logistics</a>
    <!--<a href="gas_logistics.aspx" onmouseover="m2.src='images/rel_imgs/gas_h.jpg'" onmouseout="m2.src='images/rel_imgs/gas.jpg'">
    <img src="images/rel_imgs/gas.jpg" border="0" name="m2" title="Gas Logistics" alt="" runat="server" /></a>-->
    </div>
    
    <div id="rel_box" class="rel_box gas">
    <!--<a href="epc_services.aspx" onmouseover="m3.src='images/rel_imgs/epc_h.jpg'" onmouseout="m3.src='images/rel_imgs/epc.jpg'">
    <img src="images/rel_imgs/epc.jpg" border="0" name="m3" title="EPC Services" alt="" runat="server" /></a>-->
    <a href="gas_logistics.aspx">Gas Logistics</a>    </div>
    
    <div id="rel_box" class="rel_box epc">
    <!--<a href="retail_lpg.aspx" onmouseover="m4.src='images/rel_imgs/retail_h.jpg'" onmouseout="m4.src='images/rel_imgs/retail.jpg'">
    <img src="images/rel_imgs/retail.jpg" border="0" name="m4" title="Retail LPG" alt="" runat="server" /></a>-->
    <a href="epc_services.aspx">EPC Services</a>    </div>
    
    <div id="rel_box" class="rel_box retail">
    <!--<a href="marine.aspx" onmouseover="m5.src='images/rel_imgs/marine_h.jpg'" onmouseout="m5.src='images/rel_imgs/marine.jpg'">
    <img src="images/rel_imgs/marine.jpg" border="0" name="m5" title="Marine" alt="" runat="server" /></a>-->
    
   <%-- <a href="retail_lpg.aspx">Retail LPG</a> --%> 
         <a href="http://www.aegisretail.in"  target="_blank">Retail LPG</a>
    </div>
    
    
      <div id="rel_box" class="rel_box marine">
    
    
  <!--  <img src="images/rel_imgs/liquid_h.jpg" border="0" name="m1" title="Liquid Logistics" alt="" runat="server" />--> 
  <a href="marine.aspx">Marine</a>    </div>
    
    
    
    </div>
        
    <!--  Cont-left  -->  
    
    <div id="gray_line" style="height:500px;"></div>
    
    <!--  Cont-right  -->
    
    <div id="cont_right">
    
    <asp:Label ID="lblContent" runat="server" Text=""></asp:Label>
    
    
    
        <%--<div class="tabber">
               
        
        <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
    
    
         <div class="tabbertab">
          <h2>Overview</h2>
          
          <p>Aegis Group owns and operates a network of shore based tank farm installations for the
receipt and handling of bulk liquids. These terminals are located in Mumbai, Kochi,
Pipavav and Haldia, and are connected by pipelines to various berths for handling the
export and import of hazardous chemicals, petroleum products, and petrochemicals.
The terminals are certified to ISO9001:2008, ISO14001:2004 and OHAS 18001:2007,
have an excellent record of safe operations, and are fully equipped with modern fire
fighting and safety equipment conforming to all Indian regulations.</p>

<p>Clients include top tier firms such as Bharat Petroleum, Hindustan Petroleum, Reliance
Industries, Caltex, Supreme Industries, as well as leading chemical firms, such as
Jubilant, Bombay Dyeing and Lakshmi Organics.</p>

<p>Facilities are offered on a long term contract or spot contract basis, and other services
such as customs bonding, inventory management, just in time delivery and on site
product quality testing are also available.</p>
          
          <div class="clear"></div>
          
         </div>
        
        
        
        <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
    
         <div class="tabbertab">
          <h2>Mumbai</h2>
          
          <div class="green_heading">Aegis Liquid Terminal</div>
          
          <ul class="list"><img src="images/liquid_logistic/liquid_img1.jpg" alt="" runat="server" style="float:right; margin-left:10px;" class="img_bdr" />
              <li>Providing Import, Export, Storage and logistics services to Industry handling Class A, B & C products and all types of chemicals</li>
              <li>Total Capacity 1,65,000 KL, 37 tanks which include MS, SS , epoxy coated, IFR tanks and specialty tanks (Heated)</li>
              <li>26 Tank Lorry filling bays</li>
              <li>4x12” jetty pipelines including one SS pipeline</li>
              <li>Connected to 5 berths</li>
              <li>300 Road Tankers per day</li>
              <li>Connected to HPCL and BPCL Refineries as well as Oil Installations in Sewree and Wadala via a network of pipelines</li>
              <li>Integrated Management System</li>
              <li>Providing service to Oil Companies, major consumers and chemical traders viz. BPCL, HPCL, Reliance, Bombay Dyeing etc.</li>
          </ul>
          
          <div class="green_heading">Sealord Terminal</div>
          
          <ul class="list"><img src="images/liquid_logistic/liquid_img2.jpg" alt="" runat="server" style="float:right; margin-left:10px;" class="img_bdr" />
              <li>Providing Import, Export, Storage and logistics services to Industry handling Class A, B & C products</li>
              <li>Total Capacity 75,000 KL, 10 tanks which include MS and IFR tanks</li>
              <li>10 Tank Lorry filling bays</li>
              <li>4x12” jetty pipelines including one SS pipeline</li>
              <li>Connected to 5 berths</li>
              <li>Connected to HPFR, BPFR, HPCL Wadala Depot and IOC sewree depot via a network of pipelines</li>
              <li>Integrated Management System</li>
          </ul>
          
          <div class="green_heading">Terminal Details</div>
          
          <div id="table_box">
          
          		<table width="680px" border="0" cellpadding="12px" cellspacing="1px" bgcolor="#d6d6d6">
                <tr bgcolor="#e1e1e1" align="center">
                <td width="150px"><strong>Terminal</strong></td>
                <td width="150px"><strong >Total Capacity (KL)</strong></td>
                <td width="300px"><strong>Products</strong></td>
                </tr>
                <tr bgcolor="#ffffff" align="center">
                <td>Mumbai <br />(Aegis &amp; Sealord)</td>
                <td>2,72,000</td>
                <td>A, B, C class Chemicals, petroleum products and liquefied gases.</td>
                </tr>
                </table>
          
          		
          
          </div>
          <div id="table_bot"></div>
         
         <div id="table_box">
         <table width="680px" border="0" cellpadding="12px" cellspacing="1px" bgcolor="#d6d6d6">
                <tr bgcolor="#e1e1e1" align="center">
                <td width="180px"><strong>Berth Details</strong></td>
                <td width="120px" align="center"><strong>Max. LOA (m)</strong></td>
                <td width="120px" align="center"><strong>Max. Draft (m)</strong></td>
                <td width="180px" align="center"><strong>Max. Displacement (MT)</strong></td>
                </tr>
                <tr bgcolor="#ffffff" align="center">
                <td>New Pir Pau</td>
                <td align="center">197</td>
                <td align="center">10.5</td>
                <td align="center">40,000</td>
                </tr>
                <tr bgcolor="#ffffff" align="center">
                <td>Old Pir Pau</td>
                <td align="center">173</td>
                <td align="center">7.0</td>
                <td align="center">12,000</td>
                </tr>
                <tr bgcolor="#ffffff" align="center">
                <td>Jawahar Dweep Oil Jetty 1</td>
                <td align="center">228.6</td>
                <td align="center">11.2</td>
                <td align="center">70,000</td>
                </tr>
                <tr bgcolor="#ffffff" align="center">
                <td>Jawahar Dweep Oil Jetty 2</td>
                <td align="center">198</td>
                <td align="center">11.5</td>
                <td align="center">48,000</td>
                </tr>
                <tr bgcolor="#ffffff" align="center">
                <td>Jawahar Dweep Oil Jetty 3</td>
                <td align="center">228.6</td>
                <td align="center">11.2</td>
                <td align="center">70,000</td>
                </tr>
                </table> 
          </div>
          <div id="table_bot"></div>
          
          <div class="green_heading">Terminalling Facilities</div>
          
          <ul class="list">
          <li>Licensed for handling all Class ‘A’, ‘B’ & ‘C’ cargoes.</li>
          <li>Facilities in operation for last 30 years.</li>
          <li>Internal floating roof tanks for volatile products, Stainless Steel , Epoxy Coated tanks for sensitive chemicals and Acids.</li>
          <li>Cryogenic storage facility for LPG and Propane.</li>
          <li>Connected to 2 nos. berths of Pir Pau and 3 nos. berths of Jawahar Dweep via MbPT manifold.</li>
          <li>Jetty Pipelines (2 – Carbon Steel, 1 – Stainless Steel & 1 – LTCS) of 12" each for liquid cargo and one 12" LTCS and one 6" LTCS for liquefied petroleum gases.</li>
          <li>High vessel discharge and loading rates.</li>
          <li>High capacity export pumps.</li>
          <li>Laboratory at site.</li>
          <li>Nitrogen blanketing facility in the tanks.</li>
          <li>Electronic Flow meters.</li>
          <li>(3 + 2) electronic weighbridges with maximum capacity of 50 MT.</li>
          <li>Real time computerised RDBMS based Tank Farm Management System.</li>
          <li>Connectivity with BPCL and HPCL refineries and various depots of IOC,BPCL & HPCL via a network of pipelines.</li>
          </ul>
          
          <div class="green_heading">Safety at Terminal</div>
          
          <ul class="list">
          <li>Most sophisticated fire fighting facilities comprising of both electrical and diesel driven fire water pumps.</li>
          <li>Water spray system in loading bay & valve manifolds, Foam cum water spray system for transfer pump house, Foam injection and water sprinkler system for storage tanks.</li>
          <li>Manual call points, fire detectors & smoke detectors are provided at various location of the plant.</li>
          </ul>
          
          <div class="green_heading">Certifications</div>
          
          <ul class="list">
          <li>First independent terminal to have obtained ISO 9001:2000 certification.</li>
          <li>Only independent integrated Oil & Gas terminal in the country to have obtained ISO 14001 and OHSAS 18001 certification.</li>
          <li>One of the seven companies in the country accredited with Responsible Care logo.</li>
          </ul>
          
          
          <div class="clear"></div>
         </div>
        
        <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
    
    
         <div class="tabbertab">
          <h2>Kochi</h2>
          
          <div class="green_heading">Konkan Storage Terminal</div>
                    
          <ul class="list"><img src="images/liquid_logistic/liquid_img3.jpg" alt="" runat="server" style="float:right; margin-left:10px;" class="img_bdr" />
              <li>Providing Import, Export, Storage and logistics services to Industry handling Class A, B & C products</li>
              <li>Total Capacity 51,000 KL, 20 tanks which include MS and IFR tanks</li>
              <li>10 Tank Lorry filling bays</li>
              <li>2x12” and 1x10” jetty pipelines</li>
              <li>Connected to South Coal berth</li>
              <li>Ethanol Blending System</li>
              <li>Awarded by Govt. Of Kerala for safety</li>
              <li>Catering to chemical traders, oil companies</li>
          </ul>
          
          <div class="green_heading">Terminal Details</div>
          
          <div id="table_box">
          		<table width="680px" border="0" cellpadding="12px" cellspacing="1px" bgcolor="#d6d6d6">
                <tr bgcolor="#e1e1e1" align="center">
                <td width="150px"><strong>Terminal</strong></td>
                <td width="150px"><strong >Total Capacity (KL)</strong></td>
                <td width="300px"><strong>Products</strong></td>
                </tr>
                <tr bgcolor="#ffffff" align="center">
                <td>Konkan Storage Systems (Kochi) Pvt. Ltd.</td>
                <td>51,000</td>
                <td>A, B, C class Chemicals, petroleum products.</td>
                </tr>
                </table>
                
          </div>
          <div id="table_bot"></div>
          
          <div id="table_box">
          
           		<table width="680px" border="0" cellpadding="12px" cellspacing="1px" bgcolor="#d6d6d6">
                <tr bgcolor="#e1e1e1" align="center">
                <td width="150px"><strong>Berth Details</strong></td>
                <td width="130px" align="center"><strong>Max. LOA (m)</strong></td>
                <td width="130px" align="center"><strong>Max. Draft (m)</strong></td>
                <td width="190px" align="center"><strong>Max. Displacement (MT)</strong></td>
                </tr>
                <tr bgcolor="#ffffff" align="center">
                <td>South Coal Berth</td>
                <td align="center">170</td>
                <td align="center">9.0</td>
                <td align="center">22,000</td>
                </tr>
                </table>
                
          </div>
          <div id="table_bot"></div>
          
          <div class="green_heading">Terminalling Facilities</div>
          
          <ul class="list">
          <li>Licensed for handling all Class ‘A’, ‘B’ & ‘C’ cargoes.</li>
          <li>Internal floating roof tank for volatile products.</li>
          <li>2 Nos. 12” pipelines and 1 Nos. 10” Jetty pipelines.</li>
          <li>One electronic weighbridge with maximum capacity of 40 MT.</li>
          <li>Real time computerised RDBMS based Tank Farm Management System.</li>
          </ul>
          
          <div class="green_heading">Safety at Terminal</div>
          
          <ul class="list">
          <li>Fire fighting facilities comprising of both electrical and diesel driven fire water pumps. Hydrants, monitors, deluge operated sprinkler systems, portable fire extinguishers portable DCP extinguishers, siren/announcement system.</li>
          </ul>
          
          <div class="green_heading">Operations And Maintenance Contracts</div>
          
          <ul class="list">
          <li>Presently carrying out operation and maintenance of MRPL’s first petroleum depot at Kasargode (Kerela), handling MS, HSD, SKO, FO, Bulk and packedn Bitumen.</li>
          <li>Presently carrying out operation of BPCL (Kochi)’s first Shore Tank Farm at Puthuvypeen (Kerela) handling Crude oil.</li>
          </ul>
          
          <div class="clear"></div>
          
         </div>
        
        <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
    
    
         <div class="tabbertab">
          <h2>Haldia</h2>
          
          <p>Coming Soon ...</p>
          
          <div class="clear"></div>
          
         </div>
        
        <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
    
    
         <div class="tabbertab">
          <h2>Pipavav</h2>
          
          <p>Coming Soon ...</p>
          
          <div class="clear"></div>
          
         </div>
        
        <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
    
        </div>--%>
    
    </div>
    
    <!--  Cont-right  -->

<div class="clear" style="height:40px;"></div>

<!--  Breadcrum  -->
        
    <div id="bread_box">
    <a href="index.aspx" onmouseover="home.src='images/home_ic_h.jpg'" onmouseout="home.src='images/home_ic.jpg'" style="padding:7px 16px 10px 8px;"><img src="images/home_ic.jpg" border="0" name="home" title="Home" alt="" runat="server" /></a>
    <a href="#">Business Mix</a>
    <span style="margin-left:7px; margin-right:10px;">Liquid Logistics</span>
    </div>
        
    <!--  Breadcrum  -->

<%--<div style="float:right; margin-top: 9px;"><a href="#top" onmouseover="tp.src='images/top_h.jpg'" onmouseout="tp.src='images/top.jpg'"><img src="images/top.jpg" border="0" name="tp" alt="" runat="server" /></a></div>--%>
</div>

</asp:Content>

