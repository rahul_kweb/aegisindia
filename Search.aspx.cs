﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Search : System.Web.UI.Page
{
    Class1 c = new Class1();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            int cnt = Request.QueryString.Count;
            if (cnt > 0)
            {
                string ky = Request.QueryString.Keys[0];
                if (ky.Equals("jHu567xfgfkwpqQ3"))
                {
                    SearchBox(Request.QueryString[0].ToString());
                    lblSearch.Text = Request.QueryString[0].ToString();
                }
            }
            else
            {

            }
        }
    }
    private void SearchBox(string key)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = c.Display("EXEC AddUpdategetCMS 'Get_For_Search',0,0,'','" + key + "'");

            if (dt.Rows.Count < 1)
            {
                lblMsg.Visible = true;
            }
            else
            {
                lblMsg.Visible = false;
                DataList1.DataSource = dt;
                DataList1.DataBind();

                foreach (DataListItem dl in DataList1.Items)
                {
                    string txt = ((Label)dl.FindControl("lblDetails")).Text;
                    
                    if (txt.Length > 80)
                    {
                        if (txt.Contains("<img"))
                        {
                            ((Label)dl.FindControl("lblDetails")).Text = txt.Remove(400).Replace("class", "").Replace("<img", "<img style='display:none;'").Replace("<strong>", "").Replace("</strong>", "").Replace("<h2>", "").Replace("</h2>", "").Replace("<ul>", "").Replace("</ul>", "").Replace("<li>", "").Replace("</li>", "").Replace("<span style=\"font-weight: bold\">", "").Replace("</span>", "") + ".....";
                        }
                        else
                        {
                            ((Label)dl.FindControl("lblDetails")).Text = txt.Remove(400).Replace("class", "").Replace("<strong>", "").Replace("</strong>", "").Replace("<h2>", "").Replace("</h2>", "").Replace("<ul>", "").Replace("</ul>", "").Replace("<li>", "").Replace("</li>", "").Replace("<span style=\"font-weight: bold\">", "").Replace("</span>", "") + ".....";
                        }                        
                    }
                }
            }
        }
        catch (Exception ex)
        {

        }
    }

}
