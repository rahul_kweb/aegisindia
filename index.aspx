﻿<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true"
    CodeFile="index.aspx.cs" Inherits="index" Title="Aegis Logistics Limited" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!-- Slider -->
    <link rel="stylesheet" href="css/global.css" />

    <script type="text/javascript" src="js/slides.min.jquery.js"></script>
    <script type="text/javascript">
        $(function () {
            $('#slides').slides({
                preload: true,
                preloadImage: 'img/loading.gif',
                play: 9000,
                pause: 9000,
                hoverPause: true,
                animationStart: function (current) {
                    $('.caption').animate({
                        bottom: -35
                    }, 100);
                    if (window.console && console.log) {
                        // example return of current slide number
                        console.log('animationStart on slide: ', current);
                    };
                },
                animationComplete: function (current) {
                    $('.caption').animate({
                        bottom: 0
                    }, 200);
                    if (window.console && console.log) {
                        // example return of current slide number
                        console.log('animationComplete on slide: ', current);
                    };
                },
                slidesLoaded: function () {
                    $('.caption').animate({
                        bottom: 0
                    }, 200);
                }
            });
        });
    </script>
    <!-- Slider -->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="smoothmenu1" class="ddsmoothmenu">
        <ul>
            <li class="selc"><a href="index.aspx">Home</a></li>
            <li><a href="liquid_logistics.aspx">Business Mix</a>
                <ul>
                    <li><a href="liquid_logistics.aspx">Liquid Logistics</a></li>
                    <li><a href="gas_logistics.aspx">Gas Logistics</a></li>
                    <li><a href="epc_services.aspx">EPC Services</a></li>
                 <%--   <li><a href="retail_lpg.aspx">Retail LPG</a></li>--%>
                     <li><a href="http://www.aegisretail.in" class="active" target="_blank">Retail LPG</a></li>
                    <li><a href="marine.aspx">Marine</a></li>
                </ul>
            </li>
            <li><a href="hse.aspx">HSE</a>
                <ul>
                    <%--<li><a href="vision_commitment.aspx">Vision &amp; Commitment</a></li>--%>
                    <%--<li><a href="hse_performance.aspx">Performance</a></li>
  	        <li><a href="hse_management_systems.aspx">Management Systems</a></li>
  	        <li><a href="hse_development_trainings.aspx">Development &amp; Training</a></li>
  	        <li><a href="hse_environment_programs.aspx">Environment Programs</a></li>
  	        <li><a href="hse_events_pictures.aspx">Events &amp; Pictures</a></li>--%>
                    <%--<li><a href="hse_highlights.aspx">Highlights</a></li>
          <li><a href="hse_training_development.aspx">Training  &amp; Development</a></li>--%>
                </ul>
            </li>
            <li><a href="company_profile.aspx">About Us</a>
                <ul>
                    <li><a href="key_management_team.aspx">Key Management &amp; Team</a></li>
                    <li><a href="partners.aspx">Industry Partners</a></li>
                    <li><a href="associations_credentials.aspx">Credentials</a></li>
                    <li><a href="initiatives.aspx">Continuous Improvement</a></li>
                    <%--<li><a href="infrastructure.aspx">Infrastructure</a></li>
            <li><a href="awards_recognition.aspx">Awards &amp; Recognition</a></li>--%>
                </ul>
            </li>
            <li><a href="Corporate_Governances.aspx">Investor Relations</a>
                <ul>
                    <li><a href="Corporate_Governances.aspx">Corporate Governance</a></li>
                    <li><a href="key_financial_data.aspx">Financial Information</a></li>
                    <li><a href="shares.aspx">Stock Information</a></li>
                    <li><a href="Dividend.aspx">Dividend</a></li>
                    <%--<li><a href="Debentures.aspx">Debentures</a></li>--%>
                    <li><a href="reports_filings.aspx">Stock Exchange Communication</a></li>
                    <li><a href="events_presentations.aspx">Investor Presentations</a></li>
                    <%--<li><a href="acquisitions.aspx">Acquisitions</a></li>
            <li><a href="corporate_governance.aspx">Corporate Governance</a></li>
            <li><a href="investor_contact.aspx">Investor Contact</a></li>--%>
                    <%--<li><a href="quarterly_results.aspx">Financial Results</a></li>--%>
                    <li><a href="news.aspx">News and Events</a></li>
                    <li><a href="Investor_Downloads.aspx">Investor Downloads</a></li>
                     <li><a href="InvestorContacts.aspx">Investor Contacts</a></li>
                    <%--<li><a href="live_share_info.aspx">Live Share Info</a></li>--%>
                    <%--<li><a href="unclaimedamounts.aspx">Unpaid & Unclaimed Amounts</a></li>--%>
                    <%--<li><a href="sealord_containers_limited.aspx">Sea Lord Containers Limited (Subsidiary)</a></li>--%>
                 <%--   <li><a href="Aegis_Gas_Lpg_Pvt_Ltd.aspx">Aegis Gas (LPG) Pvt. Ltd. (Subsidiary)</a></li>--%>
                </ul>
            </li>
            <li><a href="csr.aspx">CSR</a></li>
            <li><a href="contactus.aspx">Contact</a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div id="main_container">
        <!-- Slider -->
        <div id="example">
            <div id="slides">
                <div class="slides_container">
                    <asp:Repeater ID="Repeater1" runat="server">
                        <ItemTemplate>
                            <div class="slide">
                                <asp:Image ID="imgbanner" runat="server" ImageUrl='<%# string.Format("~/Admin/Banner/{0}",Eval("Banner_vcr")) %>' />
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                    <%--
					<div class="slide">
						<a href="#" title=""><img src="images/slider/AegisSlide1.jpg" border="0" alt="" runat="server" /></a>
					</div>
					
					<div class="slide">
						<a href="#" title=""><img src="images/slider/AegisSlide2.jpg" border="0" alt="" runat="server" /></a>
					</div>
                    
					<div class="slide">
						<a href="#" title=""><img src="images/slider/AegisSlide3.jpg" border="0" alt="" runat="server" /></a>						
					</div>	
                    
					<div class="slide">
						<a href="#" title=""><img src="images/slider/AegisSlide4.jpg" border="0" alt="" runat="server" /></a>						
					</div>--%>
                </div>
                <a href="#" class="prev">
                    <img src="images/slider/arrow_l.png" width="50" height="50" border="0" alt="Arrow Prev" /></a>
                <a href="#" class="next">
                    <img src="images/slider/arrow_r.png" width="50" height="50" border="0" alt="Arrow Next" /></a>
            </div>
        </div>
        <!-- Slider -->
        <!-- Scroll -->
        <div id="scroll_box">
            <div class="scroll_each">
                <a href="liquid_logistics.aspx" class="sc_1">
                    <img src="images/scroll_img/liquid_logistic.png" border="0" runat="server" title="Liquid Logistics"
                        alt="Liquid Logistics"></a>
            </div>
            <div class="scroll_each">
                <a href="gas_logistics.aspx" class="sc_2">
                    <img src="images/scroll_img/gas_logistic.png" border="0" runat="server" title="Gas Logistics"
                        alt="Gas Logistics"></a>
            </div>
            <div class="scroll_each">
                <a href="epc_services.aspx" class="sc_3">
                    <img src="images/scroll_img/epc_service.png" border="0" runat="server" title="EPC Services"
                        alt="EPC Services"></a>
            </div>
            <div class="scroll_each">
                <%--<a href="retail_lpg.aspx" class="sc_4">
                    <img src="images/scroll_img/retail_lpg.png" border="0" runat="server" title="Retail LPG"
                        alt="Retail LPG"></a>--%>
                <a href="http://www.aegisretail.in/" class="sc_4">
                    <img id="Img1" src="images/scroll_img/retail_lpg.png" border="0" runat="server" title="Retail LPG"
                        alt="Retail LPG"></a>
            </div>
            <div class="scroll_each" style="margin-right: 0px; float: right;">
                <a href="marine.aspx" class="sc_5">
                    <img src="images/scroll_img/marine.png" border="0" runat="server" title="Marine"
                        alt="Marine"></a>
            </div>
        </div>
        <!-- Scroll -->
        <div style="width: 1000px; height: 218px; margin-top: 20px;">
            <!-- Welcome -->
            <div class="index_box">
                <div id="welcome_box">
                    <div class="index_box_title">
                        Vision
                    </div>
                    <div style="font-size: 12px; text-align: left; margin-right: 7px;">
                        <%--   <p>
                            Aegis Group plays a key role in Indias downstream oil and gas sector, and its flagship
                            company, Aegis Logistics Limited, is Indias leading oil, gas, and chemical logistics
                            company. Our vision is to be the industry leader in our business
                            segments by delivering superior customer service with a focus on quality, safety,
                            and environmental standards.<br />--%>
                        <p>
                            "Our vision is to be Indias leading provider of logistics services to the oil,
                            gas, and chemical industry. We aspire to be the industry leader in our business
                            segments by delivering superior customer service and a focus on quality, safety,
                            and environmental standards."<br />
                            <a href="company_profile.aspx" class="anylink" style="float: right;">more...</a>
                            <%--Aegis Logistics Limited, is a leading bulk liquid POL, Chemical products Logistics Company in the Country 
            serving Petrochemical, Oil &amp; Gas Industry. Aegis offers sevices such as sourcing of raw materials, tank 
            storage and terminalling, arranging transport through road, pipeline or ships and onsite management of 
            materials at the customer's site.--%>
                        </p>
                        <%--<p><a href="company_profile.aspx" class="anylink">more...</a></p>--%>
                    </div>
                </div>
            </div>
            <!-- Aegis group -->
            <div class="index_box">
                <div id="group_box">
                    <div class="index_box_title">
                        Aegis Group
                    </div>
                    <ul>
                        <li>Aegis Logistics Limited</li>
                        <li>Sealord Containers Limited</li>
                        <li>Konkan Storage Systems (Koch) Pvt. Limited</li>
                        <li>Aegis Gas (LPG) Pvt. Limited</li>
                        <li>Hindustan Aegis LPG Limited</li>
                        <li>Aegis Group International PTE Limited</li>
                        <li>Aegis International Marine Services PTE Limited</li>
                        <%--<li>Eastern India LPG Company Limited</li>--%>
                    </ul>
                </div>
            </div>
            <!-- Credentials -->
            <div class="index_box">
                <div class="index_box_title" style="margin: 15px 15px 10px;">
                    Credentials
                </div>
                <div id="credential_box">
                    <div class="cred_each">
                        <img src="images/ohsas.jpg" border="0" alt="" runat="server" />
                        <strong style="color: #717171;">OHSAS 18001:2007</strong><br />
                        Occupation Health and Safety Certification
                    </div>
                    <div class="cred_each">
                        <img src="images/iso_9001.jpg" border="0" alt="" runat="server" />
                        <strong style="color: #717171;">ISO 9001:2008</strong><br />
                        Quality Management Certification
                    </div>
                    <div class="cred_each">
                        <img src="images/iso_14001.jpg" border="0" alt="" runat="server" />
                        <strong style="color: #717171;">ISO 14001:2004</strong><br />
                        Environment Management System Certification
                    </div>
                    <div class="cred_each">
                        <img src="images/rccc.jpg" border="0" alt="" runat="server" />
                        Responsible Care Committed Company
                    </div>
                </div>
            </div>
        </div>
        <div class="clear">
        </div>
    </div>
</asp:Content>
