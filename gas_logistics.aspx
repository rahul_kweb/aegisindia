﻿<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true" CodeFile="gas_logistics.aspx.cs" Inherits="gas_logistics" Title="Aegis Logistics Limited - Business Mix - Gas Logistics" %>

<%@ Register src="business_top.ascx" tagname="business_top" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<%--<!--  scroll to top  -->

<script src="js/jquery.localscroll-1.2.6-min.js" type="text/javascript"></script>
<script src="js/jquery.scrollTo-1.4.0-min.js" type="text/javascript"></script>
<script>

$(document).ready(function () {
	$.localScroll();	
});

</script>

<!--  scroll to top  -->--%>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:business_top ID="business_top1" runat="server" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

<div id="main_container">


	<div id="banner">
	<%--<img src="images/banner/gas_logistics.jpg" border="0" alt="Gas Logistics" title="Gas logistics" runat="server" />--%>
	<asp:Image runat="server" Visible="false"  border="0"  ID="ImgBanner" alt="Gas Logistics" title="Gas logistics"  ImageUrl='<%# string.Format("~/Admin/images/{0}",Eval("Image_vcr")) %>' />
	
	</div>
		
	<div class="clear"></div>
        
        
    <!--  Cont-left  -->  
		
	<div id="cont_left">
    
    <div id="pg_head">Business Mix</div>
    
      <div id="rel_box" class="rel_box liquid">
    
    <a href="liquid_logistics.aspx">Liquid Logistics</a>

    </div>
    
    <div id="rel_box" class="rel_box gas">

    <a href="gas_logistics.aspx" class="active">Gas Logistics</a>    </div>
    
    <div id="rel_box" class="rel_box epc">
   
    <a href="epc_services.aspx">EPC Services</a>    </div>
    
    <div id="rel_box" class="rel_box retail">
    <a href="retail_lpg.aspx">Retail LPG</a>    </div>
    
    
      <div id="rel_box" class="rel_box marine">
 
  <a href="marine.aspx">Marine</a>    
  </div>
    
    
    
    </div>
        
    <!--  Cont-left  -->  
    
    <div id="gray_line" style="height:400px;"></div>
    
    <!--  Cont-right  -->
    
    
    <div id="cont_right">
    
    
    	<div id="cont_right_box">
    	<asp:Label ID="lblContent" runat="server" Text=""></asp:Label>
    	
         <%-- <p><img src="images/gas_logistic/gl_img1.jpg" alt="" runat="server" style="float:right; margin:0px 0px 0px 30px; border:1px solid #c8c8c8;" />
          Aegis owns and operates a 20,000 MT fully refrigerated LPG import terminal which was commissioned in 1997. The terminal 
          has state-of-the-art safety features such as full containment design LPG storage tanks, remote control water curtain, 
          gas detectors, automatic shut off systems and remote control valves. The terminal is connected to Pir Pau Jetty by 12'' 
          low temperature steel pipelines.</p>
          
          <p>Aegis offers gas logistics services to major customers on a contractual or spot basis. Such services include 
          storage of gas and delivery to the customer's site either by pipeline or road tanker.</p>
          
          <div class="green_heading">Industrial Gas Distribution</div>
          
          <p>Aegis imports, markets and distributes bulk LPG and Propane to a variety of industrial customers in sectors 
          such as steel, automotive, ceramics, glass and pharmaceutical in the western region and is one of the largest 
          private suppliers in India.</p>--%>
                    
          
		</div>
    </div>
    
    <!--  Cont-right  -->

<div class="clear" style="height:40px;"></div>

<!--  Breadcrum  -->
        
    <div id="bread_box">
    <a href="index.aspx" onmouseover="home.src='images/home_ic_h.jpg'" onmouseout="home.src='images/home_ic.jpg'" style="padding:7px 16px 10px 8px;"><img src="images/home_ic.jpg" border="0" name="home" title="Home" alt="" runat="server" /></a>
    <a href="#">Business Mix</a>
    <span style="margin-left:7px; margin-right:10px;">Gas Logistics</span>
    </div>
        
    <!--  Breadcrum  -->

<%--<div style="float:right; margin-top: 9px;"><a href="#top" onmouseover="tp.src='images/top_h.jpg'" onmouseout="tp.src='images/top.jpg'"><img src="images/top.jpg" border="0" name="tp" alt="" runat="server" /></a></div>--%>
</div>

</asp:Content>

