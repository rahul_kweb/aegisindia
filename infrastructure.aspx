﻿<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true" CodeFile="infrastructure.aspx.cs" Inherits="infrastructure" Title="Aegis - About Us - Infrastructure" %>

<%@ Register src="about_top.ascx" tagname="about_top" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:about_top ID="about_top1" runat="server" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

<div id="main_container">


	<div id="banner"><img src="images/banner/liquid_logistic.jpg" border="0" alt="Liquid Logistics" title="Liquid logistics" runat="server" /></div>
		
	<div class="clear"></div>
        
        
    <!--  Cont-left  -->  
		
	<div id="cont_left">
    
    <div id="pg_head"><a href="company_profile.aspx">About Us</a></div>
    
    <div id="rel_box_link">
    <ul>
    <%--<li><a href="company_profile.aspx">Company Profile</a></li>
    <li><a href="background_parent_company.aspx">Background</a></li>--%>
    <li><a href="key_management_team.aspx">Key Management &amp; Team</a></li>
    <li><a href="partners.aspx">Industry Partners</a></li>
    <li><a href="associations_credentials.aspx">Credentials</a></li>
    <li><a href="initiatives.aspx">Continuous Improvement</a></li>
    <%--<li class="selec">Infrastructure</li>
  	<li><a href="awards_recognition.aspx">Awards &amp; Recognition</a></li>--%>
    </ul>
    </div>
    
    </div>
        
    <!--  Cont-left  -->  
    
    <div id="gray_line" style="height:500px;"></div>
    
    <!--  Cont-right  -->
    
    <div id="cont_right">
    
    
    <div id="cont_right_box">
    <asp:Label ID="lblContent" runat="server"></asp:Label>
   <%-- <p>Coming Soon...</p>--%>
    
    
    </div>        
    
    </div>
    
    <!--  Cont-right  -->

<div class="clear" style="height:40px;"></div>

<!--  Breadcrum  -->
        
    <div id="bread_box">
    <a href="index.aspx" onmouseover="home.src='images/home_ic_h.jpg'" onmouseout="home.src='images/home_ic.jpg'" style="padding:7px 16px 10px 8px;"><img src="images/home_ic.jpg" border="0" name="home" title="Home" alt="" runat="server" /></a>
    <a href="company_profile.aspx">About Us</a>
    <span style="margin-left:7px; margin-right:10px;">Infrastructure</span>
    </div>
        
    <!--  Breadcrum  -->

<%--<div style="float:right; margin-top: 9px;"><a href="#top" onmouseover="tp.src='images/top_h.jpg'" onmouseout="tp.src='images/top.jpg'"><img src="images/top.jpg" border="0" name="tp" alt="" runat="server" /></a></div>--%>
</div>

</asp:Content>

