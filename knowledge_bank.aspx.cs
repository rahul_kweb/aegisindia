﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class knowledge_bank : System.Web.UI.Page
{
    Class1 c = new Class1();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["EmployeeName"].Equals("") || Session["EmployeeLoginId"].Equals(""))
        {
            Response.Redirect("employee_login.aspx");
        }
        else
        {
            if (!Page.IsPostBack)
            {
                lblEmpName.Text = "Welcome, " + Session["EmployeeName"].ToString();
                getDetatils();
                c.BindInnerPageBanner(44, ImgBanner);
            }
        }
    }
    private void getDetatils()
    {
        try
        {
            DataTable dt = new DataTable();
            dt = c.Display("EXEC AddUpdateGetDocumentMaster 'Get'");
            dtlDocument.DataSource = dt;
            dtlDocument.DataBind();

        }
        catch (Exception ex)
        {
            Msg.ShowError("Some error occurred.");

        }

    }
    protected void lnkLogOut_Click(object sender, EventArgs e)
    {
        Session["EmployeeName"] = "";
        Session["EmployeeLoginId"] = "";
        Response.Redirect("employee_login.aspx", true);

    }

    //private void Download(string fileName)
    //{
    //    try
    //    {
    //        // string filename = "Connectivity.doc";
    //        if (fileName != "")
    //        {
    //            string strPath = Server.MapPath("Admin\\Documents\\" + fileName);

    //            Response.Clear();
    //            Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
    //            Response.Charset = "";
    //            Response.Cache.SetCacheability(HttpCacheability.NoCache);
    //            //Response.ContentType = "image/Jpeg";
    //            Response.ContentType = "Application/pdf";
    //            //Response.AppendHeader("Content-Disposition", "attachment; filename=" + strFileName);
    //            Response.TransmitFile(strPath);
    //            Response.End();
                
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        Msg.ShowError("Some Error Occurred.");
    //    }
    //}
    //protected void ImgDownload_Click(object sender, ImageClickEventArgs e)
    //{
    //    try
    //    {
    //        ImageButton btn = (ImageButton)sender;
    //        string FileName = btn.CommandArgument;
    //        Download(FileName);
    //    }
    //    catch (Exception ex)
    //    {
    //        Msg.ShowError("Can not Download File, Some Error Occurred.");
    //    }
    //}
}
