
		$(document).ready(function(){
			//Examples of how to assign the ColorBox event to elements
			$("a[rel='example1']").colorbox();
			$("a[rel='example2']").colorbox();
			$("a[rel='example3']").colorbox();
			$("a[rel='example4']").colorbox();
			$("a[rel='example5']").colorbox();
			$("a[rel='example6']").colorbox();
			$("a[rel='example7']").colorbox();
			$("a[rel='example8']").colorbox();
			$("a[rel='example9']").colorbox();
			$("a[rel='example10']").colorbox();
			$("a[rel='example11']").colorbox();
			$("a[rel='example12']").colorbox();
			$("a[rel='example13']").colorbox();
			$("a[rel='example14']").colorbox();
			$("a[rel='example15']").colorbox();
			$("a[rel='example16']").colorbox();
			$("a[rel='example17']").colorbox();
			$("a[rel='example18']").colorbox();
			$("a[rel='example19']").colorbox();
			$("a[rel='example20']").colorbox();
			$("a[rel='example21']").colorbox();
			$("a[rel='example22']").colorbox();
			$("a[rel='example23']").colorbox();
			$("a[rel='example24']").colorbox();
			$("a[rel='example25']").colorbox();
			
			//Example of preserving a JavaScript event for inline calls.
			$("#click").click(function(){ 
				$('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("Open this window again and this message will still be here.");
				return false;
			});
		});
