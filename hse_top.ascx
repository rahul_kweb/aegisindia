﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="hse_top.ascx.cs" Inherits="hse_top" %>
<div id="smoothmenu1" class="ddsmoothmenu">
    <ul>
        <li><a href="index.aspx">Home</a></li>
        <li><a href="liquid_logistics.aspx">Business Mix</a>
            <ul>
                <li><a href="liquid_logistics.aspx">Liquid Logistics</a></li>
                <li><a href="gas_logistics.aspx">Gas Logistics</a></li>
                <li><a href="epc_services.aspx">EPC Services</a></li>
              <%--  <li><a href="retail_lpg.aspx">Retail LPG</a></li>--%>
                 <li><a href="http://www.aegisretail.in" class="active" target="_blank">Retail LPG</a></li>
                <li><a href="marine.aspx">Marine</a></li>
            </ul>
        </li>
        <li class="selc"><a href="hse.aspx">HSE</a>
            <ul>
                <%--<li><a href="vision_commitment.aspx">Vision &amp; Commitment</a></li>--%>
                <%--<li><a href="hse_performance.aspx">Performance</a></li>
  	        <li><a href="hse_management_systems.aspx">Management Systems</a></li>
  	        <li><a href="hse_development_trainings.aspx">Development &amp; Training</a></li>
  	        <li><a href="hse_environment_programs.aspx">Environment Programs</a></li>
  	        <li><a href="hse_events_pictures.aspx">Events &amp; Pictures</a></li>--%>
                <%--<li><a href="hse_highlights.aspx">Highlights</a></li>
          <li><a href="hse_training_development.aspx">Training  &amp; Development</a></li>--%>
            </ul>
        </li>
        <li><a href="company_profile.aspx">About Us</a>
            <ul>
                <li><a href="key_management_team.aspx">Key Management &amp; Team</a></li>
                <li><a href="partners.aspx">Industry Partners</a></li>
                <li><a href="associations_credentials.aspx">Credentials</a></li>
                <li><a href="initiatives.aspx">Continuous Improvement</a></li>
                <%--<li><a href="infrastructure.aspx">Infrastructure</a></li>
          <li><a href="awards_recognition.aspx">Awards &amp; Recognition</a></li>--%>
            </ul>
        </li>
        <li><a href="Corporate_Governances.aspx">Investor Relations</a>
            <ul>
                <li><a href="Corporate_Governances.aspx">Corporate Governance</a></li>
                <li><a href="key_financial_data.aspx">Financial Information</a></li>
                <li><a href="shares.aspx">Stock Information</a></li>
                <li><a href="Dividend.aspx">Dividend</a></li>
                <%--<li><a href="Debentures.aspx">Debentures</a></li>--%>
                <li><a href="reports_filings.aspx">Stock Exchange Communication</a></li>
                <li><a href="events_presentations.aspx">Investor Presentations</a></li>
                <%--<li><a href="acquisitions.aspx">Acquisitions</a></li>
            <li><a href="corporate_governance.aspx">Corporate Governance</a></li>
            <li><a href="investor_contact.aspx">Investor Contact</a></li>--%>
                <%--<li><a href="quarterly_results.aspx">Financial Results</a></li>--%>
                <li><a href="news.aspx">News and Events</a></li>
                <li><a href="Investor_Downloads.aspx">Investor Downloads</a></li>
                 <li><a href="InvestorContacts.aspx">Investor Contacts</a></li>
                <%--<li><a href="live_share_info.aspx">Live Share Info</a></li>--%>
                <%--<li><a href="unclaimedamounts.aspx">Unpaid & Unclaimed Amounts</a></li>--%>
                <%--<li><a href="sealord_containers_limited.aspx">Sea Lord Containers Limited (Subsidiary)</a></li>--%>
           <%--     <li><a href="Aegis_Gas_Lpg_Pvt_Ltd.aspx">Aegis Gas (LPG) Pvt. Ltd. (Subsidiary)</a></li>--%>
            </ul>
        </li>
        <li><a href="csr.aspx">CSR</a></li>
        <li><a href="contactus.aspx">Contact</a></li>
    </ul>
</div>
