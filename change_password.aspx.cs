﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class change_password : System.Web.UI.Page
{
    Class1 c = new Class1();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["EmployeeName"].Equals("") || Session["EmployeeLoginId"].Equals(""))
        {
            Response.Redirect("employee_login.aspx");
        }
        else
        {
            if (!Page.IsPostBack)
            {
                lblEmpName.Text = "Welcome, " + Session["EmployeeName"].ToString();
                c.BindInnerPageBanner(44, ImgBanner);
                // getDetatils();
            }
        }
    }
    protected void lnkLogOut_Click(object sender, EventArgs e)
    {
        Session["EmployeeName"] = "";
        Session["EmployeeLoginId"] = "";
        Response.Redirect("employee_login.aspx", true);
    }

    private string CheckBlank()
    {
        string blank = "";
        if (txtOldPwd.Text.Trim().Equals(""))
        {
            blank = "Old Password,";
        }
        if (txtNewPwd.Text.Trim().Equals(""))
        {
            blank = "New Password,";
        }
        if (txtConfPwd.Text.Trim().Equals(""))
        {
            blank = "Conform Password,";
        }
        if (blank != "")
        {
            blank = blank.Remove(blank.Length - 1);
        }

        return blank;
    }


    private int Change()
    {
        int a = -1;
        try
        {
            using (SqlCommand cmd = new SqlCommand("AddUpdateGetEmployeeMaster", c.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para_vcr", "Change_Password");
                cmd.Parameters.AddWithValue("@user_Id_vcr", Session["EmployeeUserName"].ToString());
                cmd.Parameters.AddWithValue("@pwd_vcr", txtOldPwd.Text.Trim());
                cmd.Parameters.AddWithValue("@NewPassword_vcr", txtNewPwd.Text.Trim());
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                a = int.Parse(dt.Rows[0].ItemArray[0].ToString());

            }

        }
        catch (Exception ex)
        {
            Msg.ShowError("Some Error Occuurred,Can Not Change Password.");

        }
        return a;
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string blank = CheckBlank();
        if (blank.Length < 0)
        {
            Msg.ShowError("Please fill <br> " + blank);
        }
        else
        {
            int a = Change();
            if (a.Equals(0))
            {
                Msg.ShowError("Old Password Does Not Match.");
            }
            else if (a.Equals(1))
            {
                Msg.ShowSuccess("Password Changed Successfully.");
            }
            else
            {
                txtOldPwd.Focus();
            }
        }

    }
}
