﻿<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true" CodeFile="hse.aspx.cs" Inherits="hse" Title="Aegis Logistics Limited - HSE" %>

<%@ Register src="hse_top.ascx" tagname="hse_top" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:hse_top ID="hse_top1" runat="server" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

<div id="main_container">

	<div id="banner">
	<%--<img src="images/banner/hse.jpg" border="0" alt="HSE" title="HSE" runat="server" />--%>
	<asp:Image runat="server" Visible="false"  border="0"  ID="ImgBanner" alt="HSE" title="HSE"  ImageUrl='<%# string.Format("~/Admin/images/{0}",Eval("Image_vcr")) %>' />
	
	</div>
		
	<div class="clear"></div>
        
        
    <!--  Cont-left  -->  
		
	<div id="cont_left">
    
    <div id="pg_head"><a href="vision_commitment.aspx">HSE</a></div>
    
    <%--<div id="rel_box_link">
    <ul>
  	<li class="selec">Performance</li>
  	<li><a href="hse_management_systems.aspx">Management Systems</a></li>
  	<li><a href="hse_development_trainings.aspx">Development &amp; Training</a></li>
  	<li><a href="hse_environment_programs.aspx">Environment Programs</a></li>
  	<li><a href="hse_events_pictures.aspx">Events &amp; Pictures</a></li>
    </ul>
    </div>--%>
    
    
    <div style="float:left; margin-top:10px;">
    <a href="" target="_blank" id="link" runat="server" >
    <img src="images/hse/dw_hse_policy.jpg" border="0" alt="" runat="server" />
    </a></div>
    
    
    </div>
        
    <!--  Cont-left  -->  
    
    <div id="gray_line" style="height:400px;"></div>
    
    <!--  Cont-right  -->
    
    <div id="cont_right">
    
    <div id="cont_right_box">
    <asp:Label ID="lblContent" runat="server" Text=""></asp:Label>
    <%--<p>Meeting Health, Safety and Environment (HSE) standards, while delivering superior customer service is a key part 
    of the mission of Aegis Group and is designed into the management processes.</p>

    <p>HSE responsibility is designated as a line responsibility, with a corporate HSE group responsible for defining 
    and setting standards, monitoring and reviewing performance, and identifying the best industry practices for 
    implementation.</p>

    <p>Aegis has established a Learning Centre in Mumbai, where regular training is carried out, using both audio visual 
    aids, as well as practical, on the job training in the area of HSE.</p>

    <p>Aegis celebrates World Environment Day every year on June 5 through various events, including planting trees. 
    Aegis is also an active user of rainwater harvesting and recycling of water, as well as initiating several 
    recycling programmes with vendors of industrial supplies.</p>

    <p>Aegis is accredited with the ISO 1400:2004 and OSHAS 18001:2007 certifications, and is a proud member of 
    Responsible Care.</p>--%>

        
    </div> 
        
       
    
    </div>
    
    <!--  Cont-right  -->

<div class="clear" style="height:40px;"></div>

<!--  Breadcrum  -->
        
    <div id="bread_box">
    <a href="index.aspx" onmouseover="home.src='images/home_ic_h.jpg'" onmouseout="home.src='images/home_ic.jpg'" style="padding:7px 16px 10px 8px;"><img src="images/home_ic.jpg" border="0" name="home" title="Home" alt="" runat="server" /></a>
    <%--<a href="vision_commitment.aspx">HSE</a>--%>
    <span style="margin-left:7px; margin-right:10px;">HSE</span>
    </div>
        
    <!--  Breadcrum  -->

<%--<div style="float:right; margin-top: 9px;"><a href="#top" onmouseover="tp.src='images/top_h.jpg'" onmouseout="tp.src='images/top.jpg'"><img src="images/top.jpg" border="0" name="tp" alt="" runat="server" /></a></div>--%>
</div>

</asp:Content>

