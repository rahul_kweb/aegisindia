﻿<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true" CodeFile="hse_development_trainings.aspx.cs" Inherits="hse_development_trainings" Title="Aegis - HSE - Development & Training" %>

<%@ Register src="hse_top.ascx" tagname="hse_top" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:hse_top ID="hse_top1" runat="server" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

<div id="main_container">

	<div id="banner"><img src="images/banner/liquid_logistic.jpg" border="0" alt="Liquid Logistics" title="Liquid logistics" runat="server" /></div>
		
	<div class="clear"></div>
        
        
    <!--  Cont-left  -->  
		
	<div id="cont_left">
    
    <div id="pg_head"><a href="vision_commitment.aspx">HSE</a></div>
    
    <div id="rel_box_link">
    <ul>
    <%--<li><a href="vision_commitment.aspx">Vision &amp; Commitment</a></li>--%>
  	<li><a href="hse_performance.aspx">Performance</a></li>
  	<li><a href="hse_management_systems.aspx">Management Systems</a></li>
  	<li class="selec">Development &amp; Training</li>
  	<li><a href="hse_environment_programs.aspx">Environment Programs</a></li>
  	<li><a href="hse_events_pictures.aspx">Events &amp; Pictures</a></li>
    </ul>
    </div>
    
    
    <div style="float:left; margin-top:10px;"><a href="pdf/Aegis_Quality_OHS_Policy.pdf" target="_blank" ><img src="images/hse/dw_hse_policy.jpg" border="0" alt="" runat="server" /></a></div>
    
    
    </div>
        
    <!--  Cont-left  -->  
    
    <div id="gray_line" style="height:400px;"></div>
    
    <!--  Cont-right  -->
    
    <div id="cont_right">
    
    <div id="cont_right_box">
        
    <%--<ul class="list">
    <li>Aegis management believes that people are important resource of enterprise and hence behavior of all staff is 
    critical to Aegis success.</li>
    <li>The Learning center in Mumbai provides good platform for building the knowledge and skill sets before new joiners 
    can take the critical responsibilities at various sites in India.</li>
    </ul>  --%> 
    
    </div>     
    
    <asp:Label ID="lblContent" runat="server" Text=""></asp:Label> 
    
    </div>
    
    <!--  Cont-right  -->

<div class="clear" style="height:40px;"></div>

<!--  Breadcrum  -->
        
    <div id="bread_box">
    <a href="index.aspx" onmouseover="home.src='images/home_ic_h.jpg'" onmouseout="home.src='images/home_ic.jpg'" style="padding:7px 16px 10px 8px;"><img src="images/home_ic.jpg" border="0" name="home" title="Home" alt="" runat="server" /></a>
    <a href="vision_commitment.aspx">HSE</a>
    <span style="margin-left:7px; margin-right:10px;">Development &amp; Trainings</span>
    </div>
        
    <!--  Breadcrum  -->

<%--<div style="float:right; margin-top: 9px;"><a href="#top" onmouseover="tp.src='images/top_h.jpg'" onmouseout="tp.src='images/top.jpg'"><img src="images/top.jpg" border="0" name="tp" alt="" runat="server" /></a></div>--%>
</div>

</asp:Content>

