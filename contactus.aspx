﻿<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true"
    CodeFile="contactus.aspx.cs" Inherits="contactus" Title="Aegis Logistics Limited - Contact Us" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!--  tabs  -->
    <script type="text/javascript" src="Admin/Js/util.js"></script>
    <script type="text/javascript" src="js/tabber.js"></script>
    <link rel="stylesheet" href="css/example.css" type="text/css" media="screen" />
    <script type="text/javascript">

        /* Optional: Temporarily hide the "tabber" class so it does not "flash"
        on the page as plain HTML. After tabber runs, the class is changed
        to "tabberlive" and it will appear. */

        document.write('<style type="text/css">.tabber{display:none;}<\/style>');
    </script>
    <!--  tabs  -->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="smoothmenu1" class="ddsmoothmenu">
        <ul>
            <li><a href="index.aspx">Home</a></li>
            <li><a href="liquid_logistics.aspx">Business Mix</a>
                <ul>
                    <li><a href="liquid_logistics.aspx">Liquid Logistics</a></li>
                    <li><a href="gas_logistics.aspx">Gas Logistics</a></li>
                    <li><a href="epc_services.aspx">EPC Services</a></li>
                    <%--<li><a href="retail_lpg.aspx">Retail LPG</a></li>--%>
                    <li><a href="http://www.aegisretail.in" class="active" target="_blank">Retail LPG</a></li>
                    <li><a href="marine.aspx">Marine</a></li>
                </ul>
            </li>
            <li><a href="hse.aspx">HSE</a>
                <ul>
                    <%--<li><a href="vision_commitment.aspx">Vision &amp; Commitment</a></li>--%>
                    <%--<li><a href="hse_performance.aspx">Performance</a></li>
  	        <li><a href="hse_management_systems.aspx">Management Systems</a></li>
  	        <li><a href="hse_development_trainings.aspx">Development &amp; Training</a></li>
  	        <li><a href="hse_environment_programs.aspx">Environment Programs</a></li>
  	        <li><a href="hse_events_pictures.aspx">Events &amp; Pictures</a></li>--%>
                    <%--<li><a href="hse_highlights.aspx">Highlights</a></li>
          <li><a href="hse_training_development.aspx">Training  &amp; Development</a></li>--%>
                </ul>
            </li>
            <li><a href="company_profile.aspx">About Us</a>
                <ul>
                    <li><a href="key_management_team.aspx">Key Management &amp Team</a></li>
                    <li><a href="partners.aspx">Industry Partners</a></li>
                    <li><a href="associations_credentials.aspx">Credentials</a></li>
                    <li><a href="initiatives.aspx">Continuous Improvement</a></li>
                    <%--<li><a href="infrastructure.aspx">Infrastructure</a></li>
          <li><a href="awards_recognition.aspx">Awards &amp; Recognition</a></li>--%>
                </ul>
            </li>
            <li><a href="Corporate_Governances.aspx">Investor Relations</a>
                <ul>
                    <li><a href="Corporate_Governances.aspx">Corporate Governance</a></li>
                    <li><a href="key_financial_data.aspx">Financial Information</a></li>
                    <li><a href="shares.aspx">Stock Information</a></li>
                    <li><a href="Dividend.aspx">Dividend</a></li>
                    <%--<li><a href="Debentures.aspx">Debentures</a></li>--%>
                    <li><a href="reports_filings.aspx">Stock Exchange Communication</a></li>
                    <li><a href="events_presentations.aspx">Investor Presentations</a></li>
                    <%--<li><a href="acquisitions.aspx">Acquisitions</a></li>
            <li><a href="corporate_governance.aspx">Corporate Governance</a></li>
            <li><a href="investor_contact.aspx">Investor Contact</a></li>--%>
                    <%--<li><a href="quarterly_results.aspx">Quarterly Results</a></li>--%>
                    <li><a href="news.aspx">News and Events</a></li>
                    <li><a href="Investor_Downloads.aspx">Investor Downloads</a></li>
                     <li><a href="InvestorContacts.aspx">Investor Contacts</a></li>
                    <%--<li><a href="live_share_info.aspx">Live Share Info</a></li>--%>
                    <%--<li><a href="unclaimedamounts.aspx">Unpaid & Unclaimed Amounts</a></li>--%>
                    <%--<li><a href="sealord_containers_limited.aspx">Sea Lord Containers Limited (Subsidiary)</a></li>--%>
                <%--    <li><a href="Aegis_Gas_Lpg_Pvt_Ltd.aspx">Aegis Gas (LPG) Pvt. Ltd. (Subsidiary)</a></li>--%>
                </ul>
            </li>
            <li><a href="csr.aspx">CSR</a></li>
            <li class="selc"><a href="contactus.aspx">Contact</a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div id="main_container" style="border-top: 1px solid #e4e4e4; padding-top: 20px;">
        <%--<div id="banner"><img src="images/banner/liquid_logistic.jpg" border="0" alt="Liquid Logistics" title="Liquid logistics" /></div>--%>
        <div class="clear">
        </div>
        <!--  Cont-right  -->
        <div id="cont_full">
            <div id="pg_head">
                Contact Us
            </div>
            <div id="contact_left_box">
                <div class="tabber">
                    <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
                    <div class="tabbertab">
                        <h2>Corporate Office</h2>
                        <div class="contact_map_box">
                            <%--<iframe width="400" height="300" allowtransparency="true" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.in/maps/ms?msa=0&amp;msid=213443207167211261652.0004c75c659e1ec64daa7&amp;hl=en&amp;ie=UTF8&amp;t=m&amp;ll=19.0052,72.818027&amp;spn=0.012173,0.017123&amp;z=15&amp;output=embed"></iframe>--%>
                            <%--<iframe src="https://mapsengine.google.com/map/embed?mid=zw1GH6EAoxQ0.kKcfoRJrtIEI"  width="400" height="300" allowtransparency="true" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>--%>
                            <iframe src="https://mapsengine.google.com/map/embed?mid=zw1GH6EAoxQ0.kSz21xjwsnE0"
                                width="400" height="300" allowtransparency="true" frameborder="0" scrolling="no"
                                marginheight="0" marginwidth="0"></iframe>
                        </div>
                        <%-- <p>403, Peninsula Chambers,<br />
            Peninsula Corporate Park,<br />
            G.K.Marg, Lower Parel(W),<br />
            Mumbai - 400 013<br /><br />
             
            Tel  : 022-6666 3666<br />
            Fax : 022-6666 3777</p>--%>
                        <p>
                            <asp:Label ID="lblCorpOffice" runat="server" Text="Under Construction ..."></asp:Label>
                            <br />
                            <asp:Label ID="lblCorpPhone" runat="server" Text=""></asp:Label>
                            <br />
                            <asp:Label ID="lblCorpFax" runat="server" Text=""></asp:Label>
                        </p>
                        <div class="clear">
                        </div>
                    </div>
                    <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
                    <div class="tabbertab">
                        <h2>Registered Office</h2>
                        <div class="contact_map_box">
                            <iframe width="400" height="300" allowtransparency="true" frameborder="0" scrolling="no"
                                marginheight="0" marginwidth="0" src="https://maps.google.co.in/maps/ms?msa=0&amp;msid=213443207167211261652.0004c75bc4411dacef737&amp;hl=en&amp;ie=UTF8&amp;t=m&amp;ll=20.376533,72.898793&amp;spn=0.012069,0.017123&amp;z=15&amp;output=embed"></iframe>
                        </div>
                        <%--	<p>502, 5th Floor,<br />
                Skylon, G.I.D.C.,<br />
                Char Rasta,<br />
                Vapi - 396195,<br />
                Dist. Valsad,<br />
                Gujarat</p>
                        --%>
                        <p>
                            <asp:Label ID="lblRegOffice" runat="server" Text="Under Construction ..."></asp:Label>
                            <br />
                            <asp:Label ID="lblRegPhone" runat="server" Text=""></asp:Label>
                            <br />
                            <asp:Label ID="lblRegFax" runat="server" Text=""></asp:Label>
                        </p>
                        <div class="clear">
                        </div>
                    </div>
                    <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
                    <%-- <div class="tabbertab">
          <h2>Terminal</h2>
          
          <div class="contact_map_box">
          
          <iframe width="400" height="300" allowtransparency="true" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.in/maps/ms?msa=0&amp;msid=213443207167211261652.0004c75c76180104b0ab7&amp;hl=en&amp;ie=UTF8&amp;t=m&amp;ll=19.045568,72.897034&amp;spn=0.048679,0.068493&amp;z=13&amp;output=embed"></iframe>
          
          </div>
          
        
                    <asp:Label ID="lblTerminalOffice" runat="server" Text="Under Construction ..."></asp:Label>
                    <br />
                    <asp:Label ID="lblTerminalPhone" runat="server" Text=""></asp:Label>
                    <br />                    
                    <asp:Label ID="lblTerminalFax" runat="server" Text=""></asp:Label>
                    
                </p>
          
          
          <div class="clear"></div>
          
         </div>
                    --%>
                    <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
                    <%--<div class="tabbertab" style="display:none;">
                        <h2>
                            Investor Contact</h2>
                        <p>
                            <asp:Label ID="lblContent" runat="server" Text=""></asp:Label></p>
                       
                        <div class="clear">
                        </div>
                    </div>--%>
                    <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
                </div>
            </div>
            <div id="contact_right_box">
                <div class="green_heading">
                    Inquiry
                </div>
                <div>
                    <h5>
                        <asp:Label ID="lblError" runat="server" Text=""></asp:Label></h5>
                </div>
                <div class="form_box">
                    <input class="input" runat="server" id="txtName" value="Name" onfocus="if(this.value=='Name')value='';"
                        onblur="if(this.value=='')value='Name';" />
                    <span style="color: #FF0000; padding-left: 2px;">*</span>
                </div>
                <div class="form_box">
                    <input class="input" runat="server" id="txtEmail" value="Email" onfocus="if(this.value=='Email')value='';"
                        onblur="if(this.value=='')value='Email';" />
                    <span style="color: #FF0000; padding-left: 2px;">*</span>
                </div>
                <div class="form_box">
                    <input class="input" value="Company Name" runat="server" id="txtCompanyName" onfocus="if(this.value=='Company Name')value='';"
                        onblur="if(this.value=='')value='Company Name';" />
                    <span style="color: #FF0000; padding-left: 2px;">*</span>
                </div>
                <div class="form_box">
                    <asp:DropDownList ID="ddlcity" Style="width: 238px; height: 25px; float: left; border: 1px solid #cecece; padding: 2px 3px 2px 0px; color: #4b4b4b; outline: none;"
                        runat="server">
                        <asp:ListItem Value="Mumbai" Selected="True">Mumbai</asp:ListItem>
                        <asp:ListItem Value="Delhi">Delhi</asp:ListItem>
                        <asp:ListItem Value="Bangalore">Bangalore</asp:ListItem>
                        <asp:ListItem Value="Hyderabad">Hyderabad</asp:ListItem>
                        <asp:ListItem Value="Ahmedabad">Ahmedabad</asp:ListItem>
                        <asp:ListItem Value="Chennai">Chennai</asp:ListItem>
                        <asp:ListItem Value="Kolkata">Kolkata</asp:ListItem>
                        <asp:ListItem Value="Surat">Surat</asp:ListItem>
                        <asp:ListItem Value="Pune">Pune</asp:ListItem>
                        <asp:ListItem Value="Jaipur">Jaipur</asp:ListItem>
                        <asp:ListItem Value="Lucknow">Lucknow</asp:ListItem>
                        <asp:ListItem Value="Kanpur">Kanpur</asp:ListItem>
                        <asp:ListItem Value="Nagpur">Nagpur</asp:ListItem>
                        <asp:ListItem Value="Indore">Indore</asp:ListItem>
                        <asp:ListItem Value="Thane">Thane</asp:ListItem>
                        <asp:ListItem Value="Bhopal">Bhopal</asp:ListItem>
                        <asp:ListItem Value="Visakhapatnam">Visakhapatnam</asp:ListItem>
                        <asp:ListItem Value="Patna">Patna</asp:ListItem>
                        <asp:ListItem Value="Vadodara">Vadodara</asp:ListItem>
                        <asp:ListItem Value="Ghaziabad">Ghaziabad</asp:ListItem>
                        <asp:ListItem Value="Ludhiana">Ludhiana</asp:ListItem>
                        <asp:ListItem Value="Agra">Agra</asp:ListItem>
                        <asp:ListItem Value="Nashik">Nashik</asp:ListItem>
                        <asp:ListItem Value="Faridabad">Faridabad</asp:ListItem>
                        <asp:ListItem Value="Meerut">Meerut</asp:ListItem>
                        <asp:ListItem Value="Rajkot">Rajkot</asp:ListItem>
                        <asp:ListItem Value="Kalyan-Dombivali">Kalyan-Dombivali</asp:ListItem>
                        <asp:ListItem Value="Vasai-Virar">Vasai-Virar</asp:ListItem>
                        <asp:ListItem Value="Varanasi">Varanasi</asp:ListItem>
                        <asp:ListItem Value="Srinagar">Srinagar</asp:ListItem>
                        <asp:ListItem Value="Aurangabad">Aurangabad</asp:ListItem>
                        <asp:ListItem Value="Dhanbad">Dhanbad</asp:ListItem>
                        <asp:ListItem Value="Amritsar">Amritsar</asp:ListItem>
                        <asp:ListItem Value="Navi Mumbai">Navi Mumbai</asp:ListItem>
                        <asp:ListItem Value="Allahabad">Allahabad</asp:ListItem>
                        <asp:ListItem Value="Ranchi">Ranchi</asp:ListItem>
                        <asp:ListItem Value="Howrah">Howrah</asp:ListItem>
                        <asp:ListItem Value="Coimbatore">Coimbatore</asp:ListItem>
                        <asp:ListItem Value="Jabalpur">Jabalpur</asp:ListItem>
                        <asp:ListItem Value="Gwalior">Gwalior</asp:ListItem>
                        <asp:ListItem Value="Vijayawada">Vijayawada</asp:ListItem>
                        <asp:ListItem Value="Jodhpur">Jodhpur</asp:ListItem>
                        <asp:ListItem Value="Madurai">Madurai</asp:ListItem>
                        <asp:ListItem Value="Raipur">Raipur</asp:ListItem>
                        <asp:ListItem Value="Kota">Kota</asp:ListItem>
                        <asp:ListItem Value="Guwahati">Guwahati</asp:ListItem>
                        <asp:ListItem Value="Chandigarh">Chandigarh</asp:ListItem>
                        <asp:ListItem Value="Solapur">Solapur</asp:ListItem>
                        <asp:ListItem Value="Hubballi-Dharwad">Hubballi-Dharwad</asp:ListItem>
                        <asp:ListItem Value="Bareilly">Bareilly</asp:ListItem>
                        <asp:ListItem Value="Moradabad">Moradabad</asp:ListItem>
                        <asp:ListItem Value="Mysore">Mysore</asp:ListItem>
                        <asp:ListItem Value="Gurgaon">Gurgaon</asp:ListItem>
                        <asp:ListItem Value="Aligarh">Aligarh</asp:ListItem>
                        <asp:ListItem Value="Jalandhar">Jalandhar</asp:ListItem>
                        <asp:ListItem Value="Tiruchirappalli">Tiruchirappalli</asp:ListItem>
                        <asp:ListItem Value="Bhubaneswar">Bhubaneswar</asp:ListItem>
                        <asp:ListItem Value="Salem">Salem</asp:ListItem>
                        <asp:ListItem Value="Mira-Bhayandar">Mira-Bhayandar</asp:ListItem>
                        <asp:ListItem Value="Thiruvananthapuram">Thiruvananthapuram</asp:ListItem>
                        <asp:ListItem Value="Bhiwandi">Bhiwandi</asp:ListItem>
                        <asp:ListItem Value="Saharanpur">Saharanpur</asp:ListItem>
                        <asp:ListItem Value="Gorakhpur">Gorakhpur</asp:ListItem>
                        <asp:ListItem Value="Guntur">Guntur</asp:ListItem>
                        <asp:ListItem Value="Bikaner">Bikaner</asp:ListItem>
                        <asp:ListItem Value="Amravati">Amravati</asp:ListItem>
                        <asp:ListItem Value="Noida">Noida</asp:ListItem>
                        <asp:ListItem Value="Jamshedpur">Jamshedpur</asp:ListItem>
                        <asp:ListItem Value="Bhilai">Bhilai</asp:ListItem>
                        <asp:ListItem Value="Warangal">Warangal</asp:ListItem>
                        <asp:ListItem Value="Cuttack">Cuttack</asp:ListItem>
                        <asp:ListItem Value="Firozabad">Firozabad</asp:ListItem>
                        <asp:ListItem Value="Kochi">Kochi</asp:ListItem>
                        <asp:ListItem Value="Bhavnagar">Bhavnagar</asp:ListItem>
                        <asp:ListItem Value="Dehradun">Dehradun</asp:ListItem>
                        <asp:ListItem Value="Durgapur">Durgapur</asp:ListItem>
                        <asp:ListItem Value="Asansol">Asansol</asp:ListItem>
                        <asp:ListItem Value="Nanded">Nanded</asp:ListItem>
                        <asp:ListItem Value="Kolhapur">Kolhapur</asp:ListItem>
                        <asp:ListItem Value="Ajmer">Ajmer</asp:ListItem>
                        <asp:ListItem Value="Gulbarga">Gulbarga</asp:ListItem>
                        <asp:ListItem Value="Jamnagar">Jamnagar</asp:ListItem>
                        <asp:ListItem Value="Ujjain">Ujjain</asp:ListItem>
                        <asp:ListItem Value="Loni">Loni</asp:ListItem>
                        <asp:ListItem Value="Siliguri">Siliguri</asp:ListItem>
                        <asp:ListItem Value="Jhansi">Jhansi</asp:ListItem>
                        <asp:ListItem Value="Ulhasnagar">Ulhasnagar</asp:ListItem>
                        <asp:ListItem Value="Nellore">Nellore</asp:ListItem>
                        <asp:ListItem Value="Jammu">Jammu</asp:ListItem>
                        <asp:ListItem Value="Sangli-Miraj & Kupwad">Sangli-Miraj & Kupwad</asp:ListItem>
                        <asp:ListItem Value="Belgaum">Belgaum</asp:ListItem>
                        <asp:ListItem Value="Mangalore">Mangalore</asp:ListItem>
                        <asp:ListItem Value="Ambattur">Ambattur</asp:ListItem>
                        <asp:ListItem Value="Tirunelveli">Tirunelveli</asp:ListItem>
                        <asp:ListItem Value="Malegaon">Malegaon</asp:ListItem>
                        <asp:ListItem Value="Gaya">Gaya</asp:ListItem>
                        <asp:ListItem Value="Jalgaon">Jalgaon</asp:ListItem>
                        <asp:ListItem Value="Udaipur">Udaipur</asp:ListItem>
                        <asp:ListItem Value="Maheshtala">Maheshtala</asp:ListItem>
                        <asp:ListItem Value="Tirupur">Tirupur</asp:ListItem>
                        <asp:ListItem Value="Davanagere">Davanagere</asp:ListItem>
                        <asp:ListItem Value="Kozhikode">Kozhikode</asp:ListItem>
                        <asp:ListItem Value="Akola">Akola</asp:ListItem>
                        <asp:ListItem Value="Kurnool">Kurnool</asp:ListItem>
                        <asp:ListItem Value="Rajpur Sonarpur">Rajpur Sonarpur</asp:ListItem>
                        <asp:ListItem Value="Bokaro">Bokaro</asp:ListItem>
                        <asp:ListItem Value="South Dumdum">South Dumdum</asp:ListItem>
                        <asp:ListItem Value="Bellary">Bellary</asp:ListItem>
                        <asp:ListItem Value="Patiala">Patiala</asp:ListItem>
                        <asp:ListItem Value="Gopalpur">Gopalpur</asp:ListItem>
                        <asp:ListItem Value="Agartala">Agartala</asp:ListItem>
                        <asp:ListItem Value="Bhagalpur">Bhagalpur</asp:ListItem>
                        <asp:ListItem Value="Muzaffarnagar">Muzaffarnagar</asp:ListItem>
                        <asp:ListItem Value="Bhatpara">Bhatpara</asp:ListItem>
                        <asp:ListItem Value="Panihati">Panihati</asp:ListItem>
                        <asp:ListItem Value="Latur">Latur</asp:ListItem>
                        <asp:ListItem Value="Dhule">Dhule</asp:ListItem>
                        <asp:ListItem Value="Rohtak">Rohtak</asp:ListItem>
                        <asp:ListItem Value="Korba">Korba</asp:ListItem>
                        <asp:ListItem Value="Bhilwara">Bhilwara</asp:ListItem>
                        <asp:ListItem Value="Brahmapur">Brahmapur</asp:ListItem>
                        <asp:ListItem Value="Muzaffarpur">Muzaffarpur</asp:ListItem>
                        <asp:ListItem Value="Ahmednagar">Ahmednagar</asp:ListItem>
                        <asp:ListItem Value="Mathura">Mathura</asp:ListItem>
                        <asp:ListItem Value="Kollam">Kollam</asp:ListItem>
                        <asp:ListItem Value="Avadi">Avadi</asp:ListItem>
                        <asp:ListItem Value="Rajahmundry">Rajahmundry</asp:ListItem>
                        <asp:ListItem Value="Kadapa">Kadapa</asp:ListItem>
                        <asp:ListItem Value="Kamarhati">Kamarhati</asp:ListItem>
                        <asp:ListItem Value="Bilaspur">Bilaspur</asp:ListItem>
                        <asp:ListItem Value="Shahjahanpur">Shahjahanpur</asp:ListItem>
                        <asp:ListItem Value="Bijapur">Bijapur</asp:ListItem>
                        <asp:ListItem Value="Rampur">Rampur</asp:ListItem>
                        <asp:ListItem Value="Shivamogga">Shivamogga(Shimoga)</asp:ListItem>
                        <asp:ListItem Value="Chandrapur">Chandrapur</asp:ListItem>
                        <asp:ListItem Value="Junagadh">Junagadh</asp:ListItem>
                        <asp:ListItem Value="Thrissur">Thrissur</asp:ListItem>
                        <asp:ListItem Value="Alwar">Alwar</asp:ListItem>
                        <asp:ListItem Value="Bardhaman">Bardhaman</asp:ListItem>
                        <asp:ListItem Value="Kulti">Kulti</asp:ListItem>
                        <asp:ListItem Value="Kakinada">Kakinada</asp:ListItem>
                        <asp:ListItem Value="Nizamabad">Nizamabad</asp:ListItem>
                        <asp:ListItem Value="Parbhani">Parbhani</asp:ListItem>
                        <asp:ListItem Value="Tumkur">Tumkur</asp:ListItem>
                        <asp:ListItem Value="Hisar">Hisar</asp:ListItem>
                        <asp:ListItem Value="Ozhukarai">Ozhukarai</asp:ListItem>
                        <asp:ListItem Value="Bihar Sharif">Bihar Sharif</asp:ListItem>
                        <asp:ListItem Value="Panipat">Panipat</asp:ListItem>
                        <asp:ListItem Value="Darbhanga">Darbhanga</asp:ListItem>
                        <asp:ListItem Value="Bally">Bally</asp:ListItem>
                        <asp:ListItem Value="Aizawl">Aizawl</asp:ListItem>
                        <asp:ListItem Value="Dewas">Dewas</asp:ListItem>
                        <asp:ListItem Value="Ichalkaranji">Ichalkaranji</asp:ListItem>
                        <asp:ListItem Value="Tirupati">Tirupati</asp:ListItem>
                        <asp:ListItem Value="Karnal">Karnal</asp:ListItem>
                        <asp:ListItem Value="Bathinda">Bathinda</asp:ListItem>
                        <asp:ListItem Value="Jalna">Jalna</asp:ListItem>
                        <asp:ListItem Value="Barasat">Barasat</asp:ListItem>
                        <asp:ListItem Value="Kirari Suleman Nagar">Kirari Suleman Nagar</asp:ListItem>
                        <asp:ListItem Value="Purnia">Purnia</asp:ListItem>
                        <asp:ListItem Value="Satna">Satna</asp:ListItem>
                        <asp:ListItem Value="Mau">Mau</asp:ListItem>
                        <asp:ListItem Value="Sonipat">Sonipat</asp:ListItem>
                        <asp:ListItem Value="Farrukhabad">Farrukhabad</asp:ListItem>
                        <asp:ListItem Value="Sagar">Sagar</asp:ListItem>
                        <asp:ListItem Value="Rourkela">Rourkela</asp:ListItem>
                        <asp:ListItem Value="Durg">Durg</asp:ListItem>
                        <asp:ListItem Value="Imphal">Imphal</asp:ListItem>
                        <asp:ListItem Value="Ratlam">Ratlam</asp:ListItem>
                        <asp:ListItem Value="Hapur">Hapur</asp:ListItem>
                        <asp:ListItem Value="Anantapur">Anantapur</asp:ListItem>
                        <asp:ListItem Value="Arrah">Arrah</asp:ListItem>
                        <asp:ListItem Value="Karimnagar">Karimnagar</asp:ListItem>
                        <asp:ListItem Value="Etawah">Etawah</asp:ListItem>
                        <asp:ListItem Value="Ambernath">Ambernath</asp:ListItem>
                        <asp:ListItem Value="North Dumdum">North Dumdum</asp:ListItem>
                        <asp:ListItem Value="Bharatpur">Bharatpur</asp:ListItem>
                        <asp:ListItem Value="Begusarai">Begusarai</asp:ListItem>
                        <asp:ListItem Value="New Delhi">New Delhi</asp:ListItem>
                        <asp:ListItem Value="Gandhidham">Gandhidham</asp:ListItem>
                        <asp:ListItem Value="Baranagar">Baranagar</asp:ListItem>
                        <asp:ListItem Value="Tiruvottiyur">Tiruvottiyur</asp:ListItem>
                        <asp:ListItem Value="Puducherry">Puducherry</asp:ListItem>
                        <asp:ListItem Value="Sikar">Sikar</asp:ListItem>
                        <asp:ListItem Value="Thoothukudi">Thoothukudi</asp:ListItem>
                        <asp:ListItem Value="Rewa">Rewa</asp:ListItem>
                        <asp:ListItem Value="Mirzapur">Mirzapur</asp:ListItem>
                        <asp:ListItem Value="Raichur">Raichur</asp:ListItem>
                        <asp:ListItem Value="Pali">Pali</asp:ListItem>
                        <asp:ListItem Value="Ramagundam">Ramagundam</asp:ListItem>
                        <asp:ListItem Value="Vizianagaram">Vizianagaram</asp:ListItem>
                        <asp:ListItem Value="Katihar">Katihar</asp:ListItem>
                        <asp:ListItem Value="Haridwar">Haridwar</asp:ListItem>
                        <asp:ListItem Value="Sri Ganganagar">Sri Ganganagar</asp:ListItem>
                        <asp:ListItem Value="Karawal Naga">Karawal Nagar</asp:ListItem>
                        <asp:ListItem Value="Nagercoil">Nagercoil</asp:ListItem>
                        <asp:ListItem Value="Mango">Mango</asp:ListItem>
                        <asp:ListItem Value="Bulandshahr">Bulandshahr</asp:ListItem>
                        <asp:ListItem Value="Thanjavur">Thanjavur</asp:ListItem>
                    </asp:DropDownList>
                    <span style="color: #FF0000; padding-left: 2px;">*</span>
                </div>
                <div class="form_box">
                    <input class="input" value="Telephone" runat="server" id="txtTelephone" onfocus="if(this.value=='Telephone')value='';"
                        onblur="if(this.value=='')value='Telephone';" onkeypress="return onlyNumbers();"
                        maxlength="10" />
                    <span style="color: #FF0000; padding-left: 2px;">*</span>
                </div>
                <div class="form_box">
                    <asp:DropDownList ID="ddlType" Style="width: 238px; height: 25px; float: left; border: 1px solid #cecece; padding: 2px 3px 2px 0px; color: #4b4b4b; outline: none;"
                        runat="server">
                        <asp:ListItem Value="1">General Inquiries</asp:ListItem>
                        <asp:ListItem Value="2">Liquid Logistics Inquiries</asp:ListItem>
                        <asp:ListItem Value="3">Gas Logistics Inquiries</asp:ListItem>
                        <asp:ListItem Value="4">EPC Services Inquiries</asp:ListItem>
                        <asp:ListItem Value="5">Retail LPG Inquiries</asp:ListItem>
                        <asp:ListItem Value="6">Marine Inquiries</asp:ListItem>
                    </asp:DropDownList>
                    <%--  <select  style="width:238px; height: 25px; float:left; border: 1px solid #cecece; padding: 2px 3px 2px 0px; color: #4b4b4b; outline:none;">
    <option>General Inquiries </option>
    <option>LTD Marketing Department </option>
    <option>GTD Marketing Department</option>
    <option>Autogas Marketing Department</option>
    </select>--%>
                    <span style="color: #FF0000; padding-left: 2px;">*</span>
                </div>
                <div class="form_box">
                    <asp:TextBox ID="txtMessage" TextMode="MultiLine" CssClass="comment_box" Text="Message"
                        runat="server" onfocus="if(this.value=='Message')value='';" onblur="if(this.value=='')value='Message';"></asp:TextBox>
                    <%--    <textarea class="comment_box" runat="server" id="txtMessage" value="Message" onfocus="if(this.value=='Message')value='';" onblur="if(this.value=='')value='Message';" >Message</textarea>--%>
                    <span style="color: #FF0000; padding-left: 2px;">*</span>
                </div>
                <div class="clear" style="height: 20px;">
                </div>
                <div class="form_box">
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="log_but" OnClick="btnSubmit_Click" />
                    <%-- <input type="submit" value="Submit" style="width: 60px; height: 25px; border:1px solid #cecece; background-color:#eeeeee; cursor:pointer;"/> --%>
                </div>
            </div>
            <div id="gray_line" style="height: 400px; float: right;">
            </div>
        </div>
        <!--  Cont-right  -->
        <div class="clear" style="height: 40px;">
        </div>
        <!--  Breadcrum  -->
        <div id="bread_box">
            <a href="index.aspx" onmouseover="home.src='images/home_ic_h.jpg'" onmouseout="home.src='images/home_ic.jpg'"
                style="padding: 7px 16px 10px 8px;">
                <img src="images/home_ic.jpg" border="0" name="home" title="Home" alt="Home" /></a>
            <span style="margin-left: 7px; margin-right: 10px;">Contact Us</span>
        </div>
        <!--  Breadcrum  -->
        <!--<div style="float:right; margin-top: 9px;"><a href="#top" onmouseover="tp.src='images/top_h.jpg'" onmouseout="tp.src='images/top.jpg'"><img src="images/top.jpg" border="0" name="tp" /></a></div>-->
    </div>
</asp:Content>
