﻿<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true" CodeFile="marine.aspx.cs" Inherits="marine" Title="Aegis Logistics Limited - Business Mix - Marine" %>

<%@ Register src="business_top.ascx" tagname="business_top" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<%--<!--  scroll to top  -->

<script src="js/jquery.localscroll-1.2.6-min.js" type="text/javascript"></script>
<script src="js/jquery.scrollTo-1.4.0-min.js" type="text/javascript"></script>
<script>

$(document).ready(function () {
	$.localScroll();	
});

</script>

<!--  scroll to top  -->--%>


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:business_top ID="business_top1" runat="server" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

<div id="main_container">


	<div id="banner">
	<%--<img src="images/banner/marine.jpg" border="0" alt="Marine" title="Marine" runat="server" />--%>
	<asp:Image runat="server" Visible="false"  border="0"  ID="ImgBanner" alt="Marine" title="Marine"  ImageUrl='<%# string.Format("~/Admin/images/{0}",Eval("Image_vcr")) %>' />
	</div>
		
	<div class="clear"></div>
        
        
    <!--  Cont-left  -->  
		
	<div id="cont_left">
    
    <div id="pg_head">Business Mix</div>
    
         <div id="rel_box" class="rel_box liquid">
    
    <a href="liquid_logistics.aspx">Liquid Logistics</a>

    </div>
    
    <div id="rel_box" class="rel_box gas">

    <a href="gas_logistics.aspx">Gas Logistics</a>    </div>
    
    <div id="rel_box" class="rel_box epc">
   
    <a href="epc_services.aspx">EPC Services</a>    </div>
    
    <div id="rel_box" class="rel_box retail">
    <%--<a href="retail_lpg.aspx">Retail LPG</a> --%>  
         <a href="http://www.aegisretail.in" target="_blank">Retail LPG</a>
    </div>
    
    
      <div id="rel_box" class="rel_box marine">
 
  <a href="marine.aspx" class="active">Marine</a>    
  </div>
    
    
    </div>
        
    <!--  Cont-left  -->  
    
    <div id="gray_line" style="height:400px;"></div>
    
    <!--  Cont-right  -->
    
    <div id="cont_right">
   
    	<div id="cont_right_box">
    	 <asp:Label ID="lblContent" runat="server"></asp:Label>
        <%--  <p>Coming Soon...</p>--%>
                    
		</div>
        
    </div>
    
    <!--  Cont-right  -->

<div class="clear" style="height:40px;"></div>

<!--  Breadcrum  -->
        
    <div id="bread_box">
    <a href="index.aspx" onmouseover="home.src='images/home_ic_h.jpg'" onmouseout="home.src='images/home_ic.jpg'" style="padding:7px 16px 10px 8px;"><img src="images/home_ic.jpg" border="0" name="home" title="Home" alt="" runat="server" /></a>
    <a href="#">Business Mix</a>
    <span style="margin-left:7px; margin-right:10px;">Marine</span>
    </div>
        
    <!--  Breadcrum  -->

<%--<div style="float:right; margin-top: 9px;"><a href="#top" onmouseover="tp.src='images/top_h.jpg'" onmouseout="tp.src='images/top.jpg'"><img src="images/top.jpg" border="0" name="tp" /></a></div>--%>
</div>

</asp:Content>

