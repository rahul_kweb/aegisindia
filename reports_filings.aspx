﻿<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true"
    CodeFile="reports_filings.aspx.cs" Inherits="reports_filings" Title="Aegis Logistics Limited - Investor Relations - Reports & Filings" %>

<%@ Register Src="investor_top.ascx" TagName="investor_top" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!--hide and show-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js">
    </script>
    <script>
        $(document).ready(function () {
            $(".show").hide();
        });
    </script>
    <script type="text/javascript">
        function ShowRec() {
            $(".show").toggle('');
            $("#hide").css("background-image", 'url(./images/minus.png)');
            $("#hide").attr("onclick", "newShowRec()");
        }
    </script>
    <script type="text/javascript">
        function newShowRec() {
            $(".show").toggle('');
            $("#hide").css("background-image", 'url(./images/plus.png)');
            $("#hide").attr("onclick", "ShowRec()");
        }
    </script>
    <!--hide and show-->
    <%-- <script type="text/javascript">
        $(function () {
            $("#accordion").accordion();


        });
    </script>

    --%>
    <script type="text/javascript" src="js/mootools.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="css/screen.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:investor_top ID="investor_top1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div id="main_container" style="border-top: 1px solid #e4e4e4; padding-top: 20px;">
        <%--<div id="banner"><img src="images/banner/liquid_logistic.jpg" border="0" alt="Liquid Logistics" title="Liquid logistics" /></div>--%>
        <div class="clear">
        </div>
        <!--  Cont-left  -->
        <div id="cont_left">
            <div id="pg_head">
                Investor Relations
            </div>
            <div id="rel_box_link">
                <ul>
                    <li><a href="Corporate_Governances.aspx">Corporate Governance</a></li>
                    <li><a href="key_financial_data.aspx">Financial Information</a></li>
                    <%--<li><a href="key_financial_data.aspx">Key Financial Data</a></li>--%>
                    <li><a href="shares.aspx">Stock Information</a></li>
                    <li><a href="Dividend.aspx">Dividend</a></li>
                    <%--<li><a href="Debentures.aspx">Debentures</a></li>--%>
                    <li class="selec">Stock Exchange Communication</li>
                    <li><a href="events_presentations.aspx">Investor Presentations</a></li>
                    <%--<li><a href="acquisitions.aspx">Acquisitions</a></li>
    <li><a href="corporate_governance.aspx">Corporate Governance</a></li>
    <li><a href="investor_contact.aspx">Investor Contact</a></li>--%>
                    <%--<li><a href="quarterly_results.aspx">Financial Results</a></li>--%>
                    <li><a href="news.aspx">News and Events</a></li>
                    <li><a href="Investor_Downloads.aspx">Investor Downloads</a></li>
                    <li><a href="InvestorContacts.aspx">Investor Contacts</a></li>
                    <%--<li><a href="live_share_info.aspx">Live Share Info</a></li>--%>
                    <%--<li><a href="unclaimedamounts.aspx">Unpaid & Unclaimed Amounts</a></li>--%>
                    <%--<li><a href="sealord_containers_limited.aspx">Sea Lord Containers Limited (Subsidiary)</a></li>--%>
                    <li><a href="Aegis_Gas_Lpg_Pvt_Ltd.aspx">Aegis Gas (LPG) Pvt. Ltd. (Subsidiary)</a></li>
                </ul>
            </div>
        </div>
        <!--  Cont-left  -->
        <div id="gray_line" style="height: 525px;">
        </div>


        <!--  Cont-right  -->
        <div id="cont_right">
            <div id="cont_right_box">
                <div>
                    <%--<h3><a href="Admin/Documents/ALL_Postal_Ballot_Results_10012019.pdf" target="_blank" style="color:#4b4b4b">Aegis Logistics – Postal Ballot Result</a></h3>--%>

                    <h3><a href="Admin/Documents/ALL_Postal_Ballot_Notice_ESPP.pdf" target="_blank" style="color: #4b4b4b">Aegis Logistics – Postal Ballot Notice</a></h3>
                    <h3><a href="Admin/Documents/ALL_Postal_Ballot_form_ESPP.pdf" target="_blank" style="color: #4b4b4b">Aegis Logistics – Postal Ballot Form</a></h3>
                </div>


                <div id="accordion" style="width: 685px;">
                    <!--start toggle-->

                    <h3 class="toggler atStart">Stock Exchange Intimations</h3>
                    <div class="element atStart">
                        <ul class="list_in">
                              <li id="Li98" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/ALL_REG_30_23042021.pdf" target="_blank">Analyst / Institutional Investor Meet – 22.04.2021</a></span>
                            </li>
                              <li id="Li97" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Disclosure_to_Stock_Exchange_20210406.pdf" target="_blank">Share Purchase Agreement</a></span>
                            </li>
                             <li id="Li96" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Disclosure_under_Regulation_30_St_Ex_05_03_2021.pdf" target="_blank">ITOCHU to exercise to acquire an additional stake in Subsidiary Company</a></span>
                            </li>
                             <li id="Li95" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Outcome_of_BM_28012021.pdf" target="_blank">Outcome of Board Meeting – 28-01-2021</a></span>
                            </li>
                            <li id="Li94" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Board_Meeting_Intimation_20_01_2021.pdf" target="_blank">Notice of Board Meeting – 28/01/2021 and Closure of Trading Window</a></span>
                            </li>
                            <li id="Li93" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Change_in_Directorate_03122020.pdf" target="_blank">Change in Directorate – 03.12.2020</a></span>
                            </li>
                            <li id="Li92" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Analyst_Institutional_Investor_Meet_25112020.pdf" target="_blank">Analyst / Institutional Investor Meet – 26.11.2020</a></span>
                            </li>
                            <li id="Li91" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/ALL_RPT_23_3_30092020.pdf" target="_blank">Disclosure Of Related Party Transactions Pursuant To Regulation 23(9) Of SEBI LODR_30.09.2020</a></span>
                            </li>
                            <li id="Li90" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Outcome_of_BM_29102020.pdf" target="_blank">Outcome of Board Meeting – 29-10-2020</a></span>
                            </li>
                             <li id="Li89" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Board_Meeting_Intimation_21_10_2020.pdf" target="_blank">Notice of Board Meeting – 29/10/2020 and Closure of Trading Window</a></span>
                            </li>                            
                              <li id="Li88" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/AGM_Proceedings_23092020.pdf" target="_blank">Proceeding of 63rd Annual General Meeting</a></span>
                            </li>
                              <li id="Li87" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Analyst_Institutional_Investor_Meet_23092020.pdf" target="_blank">Analyst / Institutional Investor Meet – September 2020</a></span>
                            </li>
                             <li id="Li86" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/IEPF_4_Advt_22092020.pdf" target="_blank">Advertisement pursuant to IEPF Rules</a></span>
                            </li>
                            <li id="Li85" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Advertisement_Reg_47_SEBI_31082020.pdf" target="_blank">Advertisement under Regulation 47 of SEBI LODR – 31-08-2020</a></span>
                            </li>
                            <li id="Li84" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/63rd_Annual_Report_Reg_34_1_SEBI_29082020.pdf" target="_blank">63rd Annual Report – Regulation 34 (1) of SEBI LODR</a></span>
                            </li>
                            <li id="Li83" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/63rd_AGM_Book_Closure_Evoting_29082020.pdf" target="_blank">63rd AGM - Book Closure - E-voting</a></span>
                            </li>
                            <li id="Li82" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Advertisement_Reg_47_SEBI_28082020.pdf" target="_blank">Advertisement under Regulation 47 of SEBI LODR</a></span>
                            </li>
                            <li id="Li79" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Regulation_30_SEBI_15082020.pdf" target="_blank">Disclosure under Regulation 30 of SEBI (LODR)</a></span>
                            </li>
                            <li id="Li78" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Outcome_of_BM_30072020.pdf" target="_blank">Outcome of Board Meeting – 30-07-2020</a></span>
                            </li>
                            <li id="Li77" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Board_Meeting_Intimation_24_07_2020.pdf" target="_blank">Notice of Board Meeting – 30/07/2020 and Closure of Trading Window</a></span>
                            </li>
                            <li id="Li81" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Email_Bank_Details_Advt_10072020.pdf" target="_blank">Advertisement for updation of E-mail ID & Bank Details</a></span>
                            </li>
                            <li id="Li76" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/BM_Outcome_22_06_2020.pdf" target="_blank">Outcome of Board Meeting – 22.06.2020</a></span>
                            </li>
                            <li id="Li75" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Board_Meeting_Intimation_18_06_2020.pdf" target="_blank">Notice of Board Meeting – 22/06/2020 and Closure of Trading Window</a></span>
                            </li>
                            <li id="Li80" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/IEPF_4_Advt_30052020.pdf" target="_blank">Advertisement pursuant to IEPF Rules</a></span>
                            </li>
                            <li id="Li74" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Closure_of_Trading_Window_31032020_Q4.pdf" target="_blank">Intimation for Closure of Trading Window</a></span>
                            </li>
                            <li id="Li73" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Reg_30_6_SEBI_27_03_2020.pdf" target="_blank">Intimation under Regulation 30 of SEBI LODR</a></span>
                            </li>
                            <li id="Li72" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/ALL_Analyst_Institutional_Investor_Meet_05_03_2020.pdf" target="_blank">Analyst / Institutional Investor Meet – 05.03.2020</a></span>
                            </li>
                            <li id="Li71" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/BM_Outcome_31_01_2020.pdf" target="_blank">Outcome of Board Meeting - 31.01.2020</a></span>
                            </li>
                            <li id="Li70" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Board_Meeting_Intimation_23_01.pdf" target="_blank">Notice of Board Meeting – 31/01/2020 and Closure of Trading Window</a></span>
                            </li>
                            <li id="Li69" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Closure_of_Trading_Window_31122019_Q3.pdf" target="_blank">Intimation for Closure of Trading Window</a></span>
                            </li>
                            <li id="Li68" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/ALL_Analyst_Institutional_Investor_Meet_20_11_2019.pdf" target="_blank">Analyst / Institutional Investor Meet – 20.11.2019</a></span>
                            </li>
                            <li id="Li67" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/BM_Outcome_14_11_2019.pdf" target="_blank">Outcome of Board Meeting - 14.11.2019</a></span>
                            </li>
                            <li id="Li66" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Board_Meeting_Intimation_06_11_2019.pdf" target="_blank">Notice of Board Meeting – 14/11/2019 and Closure of Trading Window</a></span>
                            </li>
                            <li id="Li65" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Closure_of_Trading_Window_30092019_Q2.pdf" target="_blank">Intimation for Closure of Trading Window</a></span>
                            </li>
                            <li id="Li64" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/ALL_Analyst_Institutional_Investor_Meet_28_08_2019.pdf" target="_blank">Analyst / Institutional Investor Meet – 29.08.2019</a></span>
                            </li>
                            <li id="Li63" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/BM_Outcome_30_07_2019.pdf" target="_blank">Outcome of Board Meeting - 30.07.2019</a></span>
                            </li>
                            <li id="Li62" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/ALL_AGM_Proceedings_30072019.pdf" target="_blank">Proceeding of 62nd Annual General Meeting</a></span>
                            </li>
                            <li id="Li61" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Board_Meeting_Intimation_22_07_2019.pdf" target="_blank">Notice of Board Meeting – 30/07/2019 and Closure of Trading Window</a></span>
                            </li>
                            <li id="Li60" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/ALL_AGM_Notice_Advt_16072019.pdf" target="_blank">62nd AGM - News Paper Advertisement</a></span>
                            </li>
                            <li id="Li59" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/62nd_AGM_Notice_Book_Closure_Evoting_03_07_2019.pdf" target="_blank">62nd AGM - Notice - Book Closure - E-voting</a></span>
                            </li>
                            <li id="Li58" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/62nd_Annual_Report_Reg_34_1_SEBI_03_07_2019.pdf" target="_blank">62nd Annual Report – Regulation 34 (1) of SEBI LODR</a></span>
                            </li>
                            <li id="Li57" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Closure_of_Trading_Window_28062019_Q1.pdf" target="_blank">Intimation for Closure of Trading Window</a></span>
                            </li>
                            <li id="Li56" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/ALL_Analyst_Institutional_Investor_Meet_06_06_2019.pdf" target="_blank">Analyst / Institutional Investor Meet – 06.06.2019</a></span>
                            </li>
                            <li id="Li55" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Outcome_of_BM_28052019.pdf" target="_blank">Outcome of Board Meeting - 28/05/2019</a></span>
                            </li>
                            <li id="Li54" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Board_Meeting_Intimation_20_05_2019.pdf" target="_blank">Notice of Board Meeting – 28/05/2019 and Closure of Trading Window</a></span>
                            </li>
                            <li id="Li53" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/ALLpostalBallotResult10052019.pdf" target="_blank">Aegis Logistics – Postal Ballot Result_10.05.2019</a></span>
                            </li>
                            <li id="Li52" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/ALL_ESPP_PB_Notice_form_Advt_09042019.pdf" target="_blank">Dispatch of Postal Ballot Notice-ESPP (April 2019)</a></span>
                            </li>
                            <li id="Li51" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Closure_of_Trading_Window_03042019.pdf" target="_blank">Intimation for Closure of Trading Window</a></span>
                            </li>
                            <li id="Li50" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Outcome_of_BM_18032019.pdf" target="_blank">Outcome of Board Meeting - 18.03.2019</a></span>
                            </li>
                            <li id="Li49" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/ALL_BM_intimation_11032019.pdf" target="_blank">Notice of Board Meeting – 18/03/2019 and Closure of Trading Window</a></span>
                            </li>
                            <li id="Li48" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/ALL_ Analyst_Institutional_Investor_Meet_19_02_2019.pdf" target="_blank">Analyst / Institutional Investor Meet – 20.02.2019</a></span>
                            </li>
                            <li id="Li47" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/ALL_Analyst_Institutional_Investor_Meet.pdf" target="_blank">Analyst / Institutional Investor Meet –11.02.2019 & 13.02.2019</a></span>
                            </li>

                            <li id="Li46" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Outcome_of_BM_31012019.pdf" target="_blank">Outcome of Board Meeting - 31.01.2019</a></span>
                            </li>

                            <li id="Li45" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Board_Meeting_Intimation_23012019.pdf" target="_blank">Notice of Board Meeting – 31/01/2019 and Closure of Trading Window</a></span>
                            </li>

                            <li id="Li44" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/St_Ex_Reg_30_and_47_Lett_10-12-2018.pdf" target="_blank">Dispatch of Postal Ballot Notice</a></span>
                            </li>

                            <li id="Li43" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/ALL_investormeet_03122018.pdf" target="_blank">Analyst / Institutional Investor Meet – 04.12.2018</a></span>
                            </li>

                            <li id="Li42" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Outcome_of_BM_05-11-2018.pdf" target="_blank">Outcome of Board Meeting - 05.11.2018</a></span>

                            </li>
                            <li id="Li41" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/ALL_Change_in_Auditor_01112018.pdf" target="_blank">Change in Statutory Auditor</a></span>

                            </li>

                            <li id="Li40" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Board_Meeting_Intimation_26-10-2018.pdf" target="_blank">Notice of Board Meeting - 05/11/2018 and Closure of Trading Window</a></span>

                            </li>

                            <li id="Li39" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/AGM_Proceeding_09082018.pdf" target="_blank">Proceeding of 61st Annual General Meeting</a></span>

                            </li>

                            <li id="Li38" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/ALL_BM09082018.pdf" target="_blank">Outcome of Board Meeting - 09.08.2018</a></span>
                            </li>

                            <li id="Li37" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Board_Meeting_Intimation_01-08-2018.pdf" target="_blank">Notice of Board Meeting - 09/08/2018 and Closure of Trading Window</a></span>
                            </li>

                            <li id="Li36" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/61st AGM - Notice - Book Closure - E-voting.pdf" target="_blank">61st AGM - Notice - Book Closure - E-voting</a></span>
                            </li>

                            <li id="Li35" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Outcome_of_BM_30_05_2018.pdf" target="_blank">Outcome of Board Meeting - 30.05.2018</a></span>
                            </li>

                            <li id="Li34" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Board_Meeting_Intimation_22-05-2018.pdf" target="_blank">Notice of Board Meeting – 30-05-2018 and closure of Trading Window</a></span>
                            </li>

                            <li id="Li32" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Appointment_of_Woman_Director_30-03-2018.pdf" target="_blank">Appointment of Woman Director (Independent)</a></span>
                            </li>

                            <li id="Li31" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Analyst_Institutional_Investor_Meeting _05.02.2018.pdf" target="_blank">Analyst / Institutional Investor Meeting - 05.02.2018</a></span>
                            </li>

                            <li id="Li30" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/ALL_Q3_Result_31122017.pdf" target="_blank">Outcome of Board Meeting -02.02.2018</a></span>
                            </li>

                            <li id="Li29" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/BM_Intimation_25-01-2018.pdf" target="blank">Board Meeting – Results & Interim Dividend</a></span>
                            </li>

                            <li id="Li28" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Disclosure_under_Regulation_30_St_Ex_05-01-2018.pdf" target="_blank">Shareholder's Agreement with Itochu</a></span>
                            </li>
                            <li id="Li27" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/ALL_Outcome_of_BM_Standalone_and_Consolidated_08_12_17.pdf" target="_blank">Outcome of Board Meeting - 08.12.2017</a></span>
                            </li>
                            <li id="Li26" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/St_Ex_Change_in_Directorship_05-12-17.pdf" target="_blank">Demise of Professor Poonam Kumar – Independent Director</a></span>
                            </li>
                            <li id="Li25" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/ALL_Intimation_BM_30_11_2017.pdf" target="_blank">Notice of Board Meeting - 08/12/2017 and Closure of Trading Window</a></span>
                            </li>
                            <li id="Li24" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Outcome_of_Board_Meeting-05.09.2017.pdf" target="_blank">Outcome of Board Meeting - 05.09.2017</a></span>
                            </li>
                            <li id="Li23" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/BM_Notice-29-08-2017.pdf" target="_blank">Notice of Board Meeting - 05.09.2017 and Closure of Trading Window</a></span>
                            </li>
                            <li id="Li22" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Proceedings_of_60th_AGM_110817.pdf" target="_blank">Proceedings of 60th Annual General Meeting</a></span>
                            </li>
                            <li id="Li21" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/AGM_Notice_BC_Evoting_13-07-2017_190717.pdf" target="_blank">60th AGM - Notice - Book Closure - E-voting</a></span>
                            </li>
                            <li id="Li20" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Disclosure-06-06-2017.pdf" target="_blank">Share Subscription Agreement with Itochu</a></span>
                            </li>
                            <li id="Li19" onclick="javascript:return ShowRec();" style="display: none"><span style="cursor: pointer;"><a href="Admin/Documents/ALL_BM_Newspaper_Advt.22052017.pdf" target="_blank">Board Meeting notice_24052017 (Newspaper Advt.)</a></span>
                            </li>
                            <li id="Li18" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/ALL_Board_Meeting_intimation_22052017.pdf" target="_blank">Notice of the Board Meeting_30.05.2017 And Closure Of Trading Window</a></span>
                            </li>
                            <li id="Li17" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/ALL_IEPF_Newspaper_Adv_05052017.pdf" target="_blank">Notice for transfer of shares to IEPF (News Paper Advt.)</a></span>
                            </li>
                            <li id="Li33" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Appointment_of_Independent_director_Mr._Jaideep_Khimasia.pdf" target="_blank">Appointment of Mr. Jaideep Khimasia as Independent Director</a></span>
                            </li>
                            <li id="Li4" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/ALL_intimation_of_DJK_resignation_04052017.pdf" target="_blank">Resignation of Mr. Dineshchandra J. Khimasia as Independent Director</a></span>
                            </li>
                            <li id="Li3" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/ALL_Change_in_Address_of_RTA.pdf" target="_blank">Change of office address of RTA</a></span>
                            </li>
                            <li id="Li2" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Stock_Exchange_Intimation_EdelweissInves_or_Conference08_02_17.pdf" target="_blank">Edelweiss Investor Conference</a></span>
                            </li>
                            <li id="Li1" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Stock_Exhange_ALL_BM_Intimationtostockexchangesason25012017.pdf" target="_blank">Second Interim Dividend 2016-17</a></span>
                            </li>
                            <li id="Li16" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/Interim_dividend_2016-17.pdf" target="_blank">Interim dividend 2016-17</a></span>
                                <%--<ul>
                                    <li><a href="Admin/Documents/Aegis Logistics_Full_Annual_Report_2016.pdf" target="_blank">Annual Report 2015-16</a></li>
                                    <li><a href="Admin/Documents/Chairmans_Speech_at_59th_AGM.pdf" target="_blank">Chairman’s Speech at 59th AGM</a></li>
                                    <li><a href="Admin/Documents/Outcome_of_voting_at _59th_Annual_GeneralMeeting.pdf" target="_blank">Outcome of voting at 59th Annual General Meeting</a></li>
                                </ul>--%>
                            </li>
                            <li id="Li15" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/ALL_resignation_Mr_Rajnikant_Karavadia_08112016.pdf" target="_blank">Resignation of Mr. Rajnikant J. Karavadia</a></span>
                                <%--<ul>
                                    <li><a href="Admin/Documents/Aegis Logistics_Full_Annual_Report_2016.pdf" target="_blank">Annual Report 2015-16</a></li>
                                    <li><a href="Admin/Documents/Chairmans_Speech_at_59th_AGM.pdf" target="_blank">Chairman’s Speech at 59th AGM</a></li>
                                    <li><a href="Admin/Documents/Outcome_of_voting_at _59th_Annual_GeneralMeeting.pdf" target="_blank">Outcome of voting at 59th Annual General Meeting</a></li>
                                </ul>--%>
                            </li>
                            <li id="Li5" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/27.09.2016_Demise_of_chairman.pdf" target="_blank">Demise of Chairman</a></span>
                                <%--<ul>
                                    <li><a href="Admin/Documents/Aegis Logistics_Full_Annual_Report_2016.pdf" target="_blank">Annual Report 2015-16</a></li>
                                    <li><a href="Admin/Documents/Chairmans_Speech_at_59th_AGM.pdf" target="_blank">Chairman’s Speech at 59th AGM</a></li>
                                    <li><a href="Admin/Documents/Outcome_of_voting_at _59th_Annual_GeneralMeeting.pdf" target="_blank">Outcome of voting at 59th Annual General Meeting</a></li>
                                </ul>--%>
                            </li>
                            <li id="Li6" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/06.09.2016_IIHF_reduced_stake.pdf" target="_blank">Updates on Infrastructure India Holding Fund LLC ("IIHF")</a></span>
                                <%--<ul>
                                    <li><a href="Admin/Documents/Aegis Logistics_Full_Annual_Report_2016.pdf" target="_blank">Annual Report 2015-16</a></li>
                                    <li><a href="Admin/Documents/Chairmans_Speech_at_59th_AGM.pdf" target="_blank">Chairman’s Speech at 59th AGM</a></li>
                                    <li><a href="Admin/Documents/Outcome_of_voting_at _59th_Annual_GeneralMeeting.pdf" target="_blank">Outcome of voting at 59th Annual General Meeting</a></li>
                                </ul>--%>
                            </li>
                            <li id="Li7" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/05.07.2016_submission_of_Audit_report_in_the_matter_of_RTA.pdf" target="_blank">Audit Report in the matter of Sharepro Services (I) Pvt. Ltd.</a></span>
                                <%--<ul>
                                    <li><a href="Admin/Documents/Aegis Logistics_Full_Annual_Report_2016.pdf" target="_blank">Annual Report 2015-16</a></li>
                                    <li><a href="Admin/Documents/Chairmans_Speech_at_59th_AGM.pdf" target="_blank">Chairman’s Speech at 59th AGM</a></li>
                                    <li><a href="Admin/Documents/Outcome_of_voting_at _59th_Annual_GeneralMeeting.pdf" target="_blank">Outcome of voting at 59th Annual General Meeting</a></li>
                                </ul>--%>
                            </li>
                            <li id="Li8" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/28.06.2016_Acq_of_shares_of_Subsidiary_Sea_Lord.pdf" target="_blank">Increase in stake of Sea Lord Containers Ltd (Subsidiary of the Company)</a></span>
                                <%--<ul>
                                    <li><a href="Admin/Documents/Aegis Logistics_Full_Annual_Report_2016.pdf" target="_blank">Annual Report 2015-16</a></li>
                                    <li><a href="Admin/Documents/Chairmans_Speech_at_59th_AGM.pdf" target="_blank">Chairman’s Speech at 59th AGM</a></li>
                                    <li><a href="Admin/Documents/Outcome_of_voting_at _59th_Annual_GeneralMeeting.pdf" target="_blank">Outcome of voting at 59th Annual General Meeting</a></li>
                                </ul>--%>
                            </li>
                            <li id="Li9" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/24.06.2016_Subsidiary_Sea lord_Unlisted.pdf" target="_blank">Updates on Subsidiary</a></span>
                                <%--<ul>
                                    <li><a href="Admin/Documents/Aegis Logistics_Full_Annual_Report_2016.pdf" target="_blank">Annual Report 2015-16</a></li>
                                    <li><a href="Admin/Documents/Chairmans_Speech_at_59th_AGM.pdf" target="_blank">Chairman’s Speech at 59th AGM</a></li>
                                    <li><a href="Admin/Documents/Outcome_of_voting_at _59th_Annual_GeneralMeeting.pdf" target="_blank">Outcome of voting at 59th Annual General Meeting</a></li>
                                </ul>--%>
                            </li>
                            <li id="Li10" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/11.05.2016_change_in_RTA.pdf" target="_blank">Change in the Registrar and Share Transfer Agent of the Company</a></span>
                                <%--<ul>
                                    <li><a href="Admin/Documents/Aegis Logistics_Full_Annual_Report_2016.pdf" target="_blank">Annual Report 2015-16</a></li>
                                    <li><a href="Admin/Documents/Chairmans_Speech_at_59th_AGM.pdf" target="_blank">Chairman’s Speech at 59th AGM</a></li>
                                    <li><a href="Admin/Documents/Outcome_of_voting_at _59th_Annual_GeneralMeeting.pdf" target="_blank">Outcome of voting at 59th Annual General Meeting</a></li>
                                </ul>--%>
                            </li>
                            <li id="Li11" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/10.03.2016_Outcome_of_BM_3rd_Dividend_declared.pdf" target="_blank">Third Interim Dividend</a></span>
                                <%--<ul>
                                    <li><a href="Admin/Documents/Aegis Logistics_Full_Annual_Report_2016.pdf" target="_blank">Annual Report 2015-16</a></li>
                                    <li><a href="Admin/Documents/Chairmans_Speech_at_59th_AGM.pdf" target="_blank">Chairman’s Speech at 59th AGM</a></li>
                                    <li><a href="Admin/Documents/Outcome_of_voting_at _59th_Annual_GeneralMeeting.pdf" target="_blank">Outcome of voting at 59th Annual General Meeting</a></li>
                                </ul>--%>
                            </li>
                            <li id="Li12" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/28.01.2016_Outcome_of_BM_2nd_Dividend_declared.pdf" target="_blank">Second Interim Dividend</a></span>
                                <%--<ul>
                                    <li><a href="Admin/Documents/Aegis Logistics_Full_Annual_Report_2016.pdf" target="_blank">Annual Report 2015-16</a></li>
                                    <li><a href="Admin/Documents/Chairmans_Speech_at_59th_AGM.pdf" target="_blank">Chairman’s Speech at 59th AGM</a></li>
                                    <li><a href="Admin/Documents/Outcome_of_voting_at _59th_Annual_GeneralMeeting.pdf" target="_blank">Outcome of voting at 59th Annual General Meeting</a></li>
                                </ul>--%>
                            </li>
                            <li id="Li13" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/01.12.2015_Appointment_of_KMP_for_determing_materiality.pdf" target="_blank">Authorised KMPs to determine materiality of events/information</a></span>
                                <%--<ul>
                                    <li><a href="Admin/Documents/Aegis Logistics_Full_Annual_Report_2016.pdf" target="_blank">Annual Report 2015-16</a></li>
                                    <li><a href="Admin/Documents/Chairmans_Speech_at_59th_AGM.pdf" target="_blank">Chairman’s Speech at 59th AGM</a></li>
                                    <li><a href="Admin/Documents/Outcome_of_voting_at _59th_Annual_GeneralMeeting.pdf" target="_blank">Outcome of voting at 59th Annual General Meeting</a></li>
                                </ul>--%>
                            </li>
                            <li id="Li14" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a href="Admin/Documents/03.11.2015_outcome of BM_1st_Dividend_declared.pdf" target="_blank">1st Interim dividend </a></span>
                                <%--<ul>
                                    <li><a href="Admin/Documents/Aegis Logistics_Full_Annual_Report_2016.pdf" target="_blank">Annual Report 2015-16</a></li>
                                    <li><a href="Admin/Documents/Chairmans_Speech_at_59th_AGM.pdf" target="_blank">Chairman’s Speech at 59th AGM</a></li>
                                    <li><a href="Admin/Documents/Outcome_of_voting_at _59th_Annual_GeneralMeeting.pdf" target="_blank">Outcome of voting at 59th Annual General Meeting</a></li>
                                </ul>--%>
                            </li>





                        </ul>




                    </div>

                    <h3 class="toggler atStart">Shareholding Pattern</h3>
                    <div class="element atStart">
                        <ul class="list_in">
                            <asp:DataList ID="dtlShareHoldingPdf" runat="server">
                                <ItemTemplate>
                                    <li><a href='<%# string.Format("Admin/Documents/{0}.pdf",Eval("Pdf_Page_Path_vcr")) %>'
                                        target="_blank">
                                        <%# Eval("Pdf_Title_vcr")%>
                                    </a></li>
                                </ItemTemplate>
                            </asp:DataList>
                            <%--<li><a href="pdf/reports_filings/Shareholding_Pattern_30-06-2012.pdf" target="_blank">Shareholding Pattern as on 30.06.2012</a></li>--%>
                        </ul>
                    </div>
                    <%--<h3 class="toggler atStart">
                        Code of Conduct</h3>--%>
                    <%--<div class="element atStart">
                        <ul class="list_in">
                            <asp:DataList ID="dtlCodeOfConductPdf" runat="server">
                                <ItemTemplate>
                                    <li><a href='<%# string.Format("Admin/Documents/{0}.pdf",Eval("Pdf_Page_Path_vcr")) %>'
                                        target="_blank">
                                        <%# Eval("Pdf_Title_vcr")%>
                                    </a></li>
                                </ItemTemplate>
                            </asp:DataList>
                        </ul>
                    </div>--%>
                    <%--<h3 class="toggler atStart">Dividend</h3>
                    <div class="element atStart">
                        <ul class="list_in">
                            <asp:DataList ID="dtlDividendPdf" runat="server">
                                <ItemTemplate>
                                    <li><a href='<%# string.Format("Admin/Documents/{0}.pdf",Eval("Pdf_Page_Path_vcr")) %>'
                                        target="_blank">
                                        <%# Eval("Pdf_Title_vcr")%>
                                    </a></li>
                                </ItemTemplate>
                            </asp:DataList>                            
                        </ul>
                    </div>--%>
                    <%--<h3 class="toggler atStart">Subsidiary Companies</h3>
                    <div class="element atStart">
                        <ul class="list_in">

                            <li id="Li4" onclick="javascript:return ShowRec();"><span style="cursor: pointer;">Annual
                                Report 2015-2016</span>
                                <ul>

                                    <li><a href="Admin/Documents/Aegis_Group_International_Pte._Limited_Annual_Report_2015-16.pdf"
                                        target="_blank">Aegis Group International Pte. Limited - Annual Report 2015-16 </a></li>

                                    <li><a href="Admin/Documents/Aegis_International_Marine_Services_Pte._Limited-Annual_Report_2015- 2016.pdf"
                                        target="_blank">Aegis International Marine Services Pte. Limited - Annual Report 2015- 2016</a></li>

                                    <li><a href="Admin/Documents/Aegis_LPG_Logistics_(Pipavav)_Limited_Annual_Report_2015-16.pdf"
                                        target="_blank">Aegis LPG Logistics (Pipavav) Limited - Annual Report 2015-16</a></li>

                                    <li><a href="Admin/Documents/Aegis_Terminal_(Pipavav)_Limited-Annual_Report_2015-16.pdf"
                                        target="_blank">Aegis Terminal (Pipavav) Limited - Annual Report 2015-16
                                    </a></li>

                                    <li><a href="Admin/Documents/Eastern_India_LPG_Company_Private_Limited-Annual_Report_2015-16_New.pdf"
                                        target="_blank">Eastern India LPG Company Private Limited - Annual Report 2015-16 </a></li>

                                    <li><a href="Admin/Documents/Hindustan_Aegis_LPG_Limited-Annual_Report_2015-16_New.pdf"
                                        target="_blank">Hindustan Aegis LPG Limited - Annual Report 2015-16 </a></li>

                                    <li><a href="Admin/Documents/Konkan_Storage_Systems_(Kochi)_Private_Limited-Annual_Report_2015-16.pdf"
                                        target="_blank">Konkan Storage Systems (Kochi) Private Limited - Annual Report 2015-16
                                    </a></li>
                                    <li><a href="Admin/Documents/Sea_Lord_Containers_Ltd-Annual_Report_2015-16.pdf"
                                        target="_blank">Sea Lord Containers Ltd - Annual Report 2015-16 </a></li>
                                </ul>
                            </li>










                            <li id="Li3" onclick="javascript:return ShowRec();"><span style="cursor: pointer;">Annual Report 2014-15</span>
                                <ul>
                                    <li><a href="Admin/Documents/Sea_Lord_Containers_Ltd_Annual_Report_2014_15_11_7_16.pdf"
                                        target="_blank">Sea Lord Containers Ltd. Annual Report 2014-15</a></li>
                                    <li><a href="Admin/Documents/ATPL_2014_15.pdf" target="_blank">Aegis Terminal (Pipavav)
                                Limited Annual Report 2014-15</a></li>
                                    <li><a href="Admin/Documents/Aegis_LPG.pdf" target="_blank">Aegis LPG Logistics (Pipavav)
                                Limited Annual Report 2014-15</a></li>
                                    <li><a href="Admin/Documents/AGI_2014_15.pdf" target="_blank">Aegis Group International
                                Pte Limited Annual Report 2014-15</a></li>
                                    <li><a href="Admin/Documents/AIMS_2014_15.pdf" target="_blank">Aegis International Marine
                                Services Pte Limited Annual Report 2014-15</a></li>
                                    <li><a href="Admin/Documents/HALPG_ 2014-15.pdf" target="_blank">Hindustan Aegis LPG
                                Limited Annual Report 2014-15</a></li>
                                    <li><a href="Admin/Documents/EILPG_2014_15.pdf" target="_blank">Eastern India LPG Company
                                Private Limited Annual Report 2014-15</a></li>
                                    <li><a href="Admin/Documents/KCPL_2014_15.pdf" target="_blank">Konkan Storage Systems
                                (Kochi) Private Limited Annual Report 2014-15</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>--%>
                </div>
            </div>
        </div>
        <!--  Cont-right  -->
        <div class="clear" style="height: 40px;">
        </div>
        <!--  Breadcrum  -->
        <div id="bread_box">
            <a href="index.aspx" onmouseover="home.src='images/home_ic_h.jpg'" onmouseout="home.src='images/home_ic.jpg'"
                style="padding: 7px 16px 10px 8px;">
                <img src="images/home_ic.jpg" border="0" name="home" title="Home" /></a> <a href="#">Investor Relations</a> <span style="margin-left: 7px; margin-right: 10px;">Stock Exchange Communication</span>
        </div>
    </div>
</asp:Content>
