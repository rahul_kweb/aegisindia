﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class demo_test :System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        StringBuilder sbPerformaInvoice = new StringBuilder();


        sbPerformaInvoice.Append("<body style='font-family:'Roboto Condensed',sans-serif;margin:0;padding:0;box-sizing:border-box;text-decoration:none;'>");
        sbPerformaInvoice.Append("<div style='width:1100px;margin:0 auto;'>");
        sbPerformaInvoice.Append("<div style='padding:20px 0;background-color:#000;'>");
        sbPerformaInvoice.Append("<div style='padding:0 30px;'>");
        sbPerformaInvoice.Append("<div><img src='http://www.astaguru.com/images/logo.png' alt='logo'></div>");
        sbPerformaInvoice.Append("</div>");
        sbPerformaInvoice.Append("</div>");
        sbPerformaInvoice.Append("<div style='padding:0 30px;'>");
        sbPerformaInvoice.Append("<div style='padding-top:10px;font-size:22px;font-weight:bold;float:left;'>Div. of Safset Agencies Pvt. Ltd.</div>");
        sbPerformaInvoice.Append("<div style='float:right;font-weight:bold;font-size:24px;padding:15px 0 34px;'>PROFORMA INVOICE</div>");
        sbPerformaInvoice.Append("<div style='clear:both;'></div>");
        sbPerformaInvoice.Append("<div style='float:right;border:solid 1px #000;padding:10px;font-size:16px;'");
        sbPerformaInvoice.Append("<div><span style='font-weight:bold;width:140px;display:inline-block;margin-bottom:10px;'>Invoice Number:</span>invoiceno</div>");
        sbPerformaInvoice.Append("<div><span style='font-weight:bold;width:140px;display:inline-block;margin-bottom:10px;'>Date:</span>date</div>");
        sbPerformaInvoice.Append("</div>");
        sbPerformaInvoice.Append("<div style='clear:both;'></div>");
        sbPerformaInvoice.Append("<div style='width:70%;margin:40px 0px 70px;'>");
        sbPerformaInvoice.Append("<table style='border-spacing:0;width:100%;border:1px solid black;border-collapse:collapse;text-align:center;'width='100%' align='center'>");
        sbPerformaInvoice.Append("<tr>");
        sbPerformaInvoice.Append("<th style='border:1px solid black;border-collapse:collapse;font-size:14px;color:#000;font-weight:bold;padding:10px;vertical-align:top;text-align:left;' valign='top' align='left'>Customer Name</th>");
        sbPerformaInvoice.Append("<th style='border:1px solid black;border-collapse:collapse;font-size:14px;color:#000;font-weight:bold;padding:10px;vertical-align:top;text-align:left;' valign='top' align='left'>Address</th>");
        sbPerformaInvoice.Append("</tr>");
        sbPerformaInvoice.Append("<tr>");
        sbPerformaInvoice.Append("<td style='border:1px solid black;border-collapse:collapse;font-size:14px;color:#000;font-weight:bold;padding:10px;vertical-align:top;text-align:left;' valign='top' align='left'>" + objauc.username + "</td>");
        sbPerformaInvoice.Append("<td style='border:1px solid black;border-collapse:collapse;font-size:14px;color:#000;font-weight:bold;padding:10px;vertical-align:top;text-align:left;' valign='top' align='left'>" + objauc.fullAddress + "</td>");
        sbPerformaInvoice.Append("</tr>");
        sbPerformaInvoice.Append("</table>");
        sbPerformaInvoice.Append("</div>");
        sbPerformaInvoice.Append("<div style='margin-bottom:15px;'>");
        sbPerformaInvoice.Append("<table style='border-collapse:collapse;border-spacing:0;width:100%;' width='100%'>");
        sbPerformaInvoice.Append("<tr>");
        sbPerformaInvoice.Append("<td style='border:1px solid black;border-collapse:collapse;text-transform:none;font-weight:700;font-size:14px;padding:5px;text-align:center;' align='center'>Port of Loading and Country of Origin</td>");
        sbPerformaInvoice.Append("<td style='border:1px solid black;border-collapse:collapse;font-size:14px;padding:5px;text-align:center;' align='center'>MUMBAI MAHARASHTRA INDIA</td>");
        sbPerformaInvoice.Append("<td style='border:1px solid black;border-collapse:collapse;text-transform:none;font-weight:700;font-size:14px;padding:5px;text-align:center;' align='center'>Delivery</td>");
        sbPerformaInvoice.Append("<td style='border:1px solid black;border-collapse:collapse;font-size:14px;padding:5px;text-align:center;'align='center'>" + objauc.city + " " + objauc.state + "  " + objauc.country + "</td>");
        sbPerformaInvoice.Append("</tr>");
        sbPerformaInvoice.Append("</table>");
        sbPerformaInvoice.Append("</div>");
        sbPerformaInvoice.Append("<div style='width:100%;margin:0px 0px 20px;'>");
        sbPerformaInvoice.Append("<table style='border-spacing:0;border:1px solid black;border-collapse:collapse;'>");
        sbPerformaInvoice.Append("<tr>");
        sbPerformaInvoice.Append("<th style='text-align:center;border:1px solid black;border-collapse:collapse;font-size:16px;color:#000;font-weight:bold;padding:5px 10px;text-transform:capitalize;' align='center'>Image</th>");
        sbPerformaInvoice.Append("<th style='text-align:center;border:1px solid black;border-collapse:collapse;font-size:16px;color:#000;font-weight:bold;padding:5px 10px;text-transform:capitalize;' align='center'>Description of Goods</th>");
        sbPerformaInvoice.Append("<th style='text-align:center;border:1px solid black;border-collapse:collapse;font-size:16px;color:#000;font-weight:bold;padding:5px 10px;text-transform:capitalize;' align='center'>Quantity</th>");
        sbPerformaInvoice.Append("<th style='text-align:center;border:1px solid black;border-collapse:collapse;font-size:16px;color:#000;font-weight:bold;padding:5px 10px;text-transform:capitalize;' align='center'>Product Price<br></th>");
        sbPerformaInvoice.Append("</tr>");
        sbPerformaInvoice.Append("<tr>");
        sbPerformaInvoice.Append("<td style='text-align:center;border:1px solid black;border-collapse:collapse;vertical-align:top;text-transform:none;padding:5px 10px;width:30%;' width='30%' align='center' valign='top'><img src='http://www.astaguru.com/" + objauc.image + "' alt=''></td>");
        sbPerformaInvoice.Append("<td  style='border:1px solid black;border-collapse:collapse;vertical-align:top;text-transform:none;padding:5px 10px;width:55%;text-align:left;' width='55%' valign='top' align='left'");
        sbPerformaInvoice.Append("Value of Book including 15% margin<br />");
        sbPerformaInvoice.Append("<ul style='font-size:13px;margin-top:50px;padding-left:0;padding-left:none;'>");
        sbPerformaInvoice.Append("<li style='margin-bottom:5px;'><span style='width:70px;display:inline-block;'><span>Lot. No.</span>" + objauc.reference + "</li>");
        sbPerformaInvoice.Append("<li style='margin-bottom:5px;'><span style='width:70px;display:inline-block;'>" + objauc.title + "</li>");
        sbPerformaInvoice.Append("<li style='margin-bottom:5px;'><span style='width:70px;display:inline-block;'>Size</span>" + objauc.productsize + "</li");
        sbPerformaInvoice.Append("</ul>");
        sbPerformaInvoice.Append("</td>");
        sbPerformaInvoice.Append("<td style='text-align:center;border:1px solid black;border-collapse:collapse;text-transform:none;padding:5px 10px;vertical-align:middle;' valign='middle' align='center'>1</td>");
        sbPerformaInvoice.Append("<td style='border:1px solid black;border-collapse:collapse;text-transform:none;padding:5px 10px;width:15%;text-align:right;font-size:14px;vertical-align:middle;' width='15%' valign='middle' align='right'>" + Total_Amount + "</td>");
        sbPerformaInvoice.Append("</tr>");
        sbPerformaInvoice.Append("<tr>");
        sbPerformaInvoice.Append("<td style='text-align:center;border:1px solid black;border-collapse:collapse;border-right:0;vertical-align:top;text-transform:none;padding:5px 10px;' align='center' valign='top'></td>");
        sbPerformaInvoice.Append("<td  colspan='2' style='border:1px solid black;border-collapse:collapse;text-align:left;border-left:0;font-weight:700;vertical-align:top;text-transform:none;padding:5px 10px;' align='left' valign='top'>Sub Total</td>");
        sbPerformaInvoice.Append("<td style='border:1px solid black;border-collapse:collapse;vertical-align:top;text-transform:none;padding:5px 10px;width:15%;text-align:right;font-size:14px;' width='15%' valign='top' align='right'>" + Total_Amount + "</td>");
        sbPerformaInvoice.Append("</tr>");
        sbPerformaInvoice.Append("<tr>");
        sbPerformaInvoice.Append("<td style='text-align:center;border:1px solid black;border-collapse:collapse;border-right:0;vertical-align:top;text-transform:none;padding:5px 10px;' align='center' valign='top'></td>");
        sbPerformaInvoice.Append("<td colspan='2' style='border:1px solid black;border-collapse:collapse;text-align:left;border-left:0;vertical-align:top;text-transform:none;padding:5px 10px;'align='left' valign='top'><span style='font-size:15px;margin-right:10px;width:269px;display:inline-block;'> " + objauc.PrVat + "% on lot</span> </td>");
        sbPerformaInvoice.Append("<td style='border:1px solid black;border-collapse:collapse;vertical-align:top;text-transform:none;padding:5px 10px;width:15%;text-align:right;font-size:14px;' width='15%' valign='top' align='right'>" + gstvalue + "</td>");
        sbPerformaInvoice.Append("</tr>");
        sbPerformaInvoice.Append("<td style='text-align:center;border:1px solid black;border-collapse:collapse;border-right:0;vertical-align:top;text-transform:none;padding:5px 10px;' align='center' valign='top'></td>");
        sbPerformaInvoice.Append("<td colspan='2' style='border:1px solid black;border-collapse:collapse;text-align:left;border-left:0;vertical-align:top;text-transform:none;padding:5px 10px;' align='left' valign='top'><span style='font-size:15px;margin-right:10px;width:269px;display:inline-block;'>CGST " + CGST + "% on lot</span> </td>");
        sbPerformaInvoice.Append("<td style='border:1px solid black;border-collapse:collapse;vertical-align:top;text-transform:none;padding:5px 10px;width:15%;text-align:right;font-size:14px;' width='15%' valign='top' align='right'>" + CGST_value + "</td>");
        sbPerformaInvoice.Append("</tr>");
        sbPerformaInvoice.Append("<tr>");
        sbPerformaInvoice.Append("<td style='text-align:center;border:1px solid black;border-collapse:collapse;border-right:0;vertical-align:top;text-transform:none;padding:5px 10px;' align='center' valign='top'></td>");
        sbPerformaInvoice.Append("<td colspan='2' style='border:1px solid black;border-collapse:collapse;text-align:left;border-left:0;vertical-align:top;text-transform:none;padding:5px 10px;' align='left' valign='top'><span style='font-size:15px;margin-right:10px;width:269px;display:inline-block;'>SGST " + SGST + "% on lot</span> </td>");
        sbPerformaInvoice.Append("<td style='border:1px solid black;border-collapse:collapse;vertical-align:top;text-transform:none;padding:5px 10px;width:15%;text-align:right;font-size:14px;' width='15%' valign='top' align='right'>" + SGST_value + "</td>");
        sbPerformaInvoice.Append("<tr>");
        sbPerformaInvoice.Append("<td style='text-align:center;border:1px solid black;border-collapse:collapse;border-right:0;vertical-align:top;text-transform:none;padding:5px 10px;' align='center' valign='top'></td>");
        sbPerformaInvoice.Append("<td colspan='2' style='border:1px solid black;border-collapse:collapse;text-align:left;border-left:0;vertical-align:top;text-transform:none;padding:5px 10px;' align='left' valign='top'><span style='font-weight:700;font-size:15px;margin-right:10px;width:269px;display:inline-block;'>Total Invoice Amount</span></td>");
        sbPerformaInvoice.Append("<td style='border:1px solid black;border-collapse:collapse;vertical-align:top;text-transform:none;padding:5px 10px;width:15%;text-align:right;font-size:14px;' width='15%' valign='top' align='right'>" + Final_Total + "</td>");
        sbPerformaInvoice.Append("</tr>");
        sbPerformaInvoice.Append("</table>");
        sbPerformaInvoice.Append("</div>");
        //sbPerformaInvoice.Append("<div style="font-weight:bold;font-size:16px;margin-bottom:10px;"><span style="font-weight:bold;font-size:16px;margin-right:20px;">Amount (IN WORDS) </span>{amtinwords}</div>");
        sbPerformaInvoice.Append("<div style='font-size:14px;margin:20px 0;'><span style='font-weight:bold;'>Note:</span>Duties,Taxes and any other charges, wherever applicable at the shipping destination, must be paid directly by the buyer to the respective authorities.</div>");
        sbPerformaInvoice.Append("<div style='font-size:14px;margin:30px 0 5px;'>THIS IS A COMPUTER GENERATED INVOICE AND DOES NOT REQUIRE ANY SIGNATURE</div>");
        sbPerformaInvoice.Append("<div></div>");
        sbPerformaInvoice.Append("<div style='border:solid 1px #000;padding:20px;margin-bottom:10px;'>");
        sbPerformaInvoice.Append("<div style='width:37%;float:left;text-align:left;'>");
        sbPerformaInvoice.Append("<div style='font-weight:700;'>Payment Infomation:</div>");
        sbPerformaInvoice.Append("<div style='margin:10px 0 5px;font-size:18px;'>Payment by Cheque</div>");
        sbPerformaInvoice.Append("<div style='font-size:16px;font-weight:bold;'>Please make your cheque in favour of :<span style='display:block;font-weight:bold;font-size:20px;'>Astaguru.com</span></div>");
        sbPerformaInvoice.Append("</div>");
        sbPerformaInvoice.Append("<div style='width:60%;float:right;text-align:left;'>");
        sbPerformaInvoice.Append("<div>Payment by Direct Wire Transfer</div>");
        sbPerformaInvoice.Append("<ul style='margin-top:15px;padding-left:0;list-style:none;'>");
        sbPerformaInvoice.Append("<li style='font-weight:700;font-size:14px;margin-bottom:8px;'><span style='display:inline-block;width:178px;font-weight:normal;'>Name of the Benficiary:</span>Astaguru.com</li>");
        sbPerformaInvoice.Append("<li style='font-weight:700;font-size:14px;margin-bottom:8px;'><span style='display:inline-block;width:178px;font-weight:normal;'>Bank Name :</span>ICICI Bank,</li>");
        sbPerformaInvoice.Append("<li style='font-size:14px;margin-bottom:8px;'><span style='display:inline-block;width:178px;font-weight:normal;'>Bank Address:</span>240 Navsari Building, D. N. Road, Fort, Mumbai-400001. India.</li>");
        sbPerformaInvoice.Append("<li style='font-weight:700;font-size:14px;margin-bottom:8px;'><span style='display:inline-block;width:178px;font-weight:normal;'>Account No:</span>623505385049</li>");
        sbPerformaInvoice.Append("<li style='font-size:14px;margin-bottom:8px;'><span style='display:inline-block;width:178px;font-weight:normal;'>Swift Code:</span>ICICINBBCTS</li>");
        sbPerformaInvoice.Append("<li style='font-size:14px;margin-bottom:8px;'><span style='display:inline-block;width:178px;font-weight:normal;'>RTGS/NEFT IFSC Code:</span>ICIC0006235</li>");
        sbPerformaInvoice.Append("<li style='font-size:14px;margin-bottom:8px;'><span style='display:inline-block;width:178px;font-weight:normal;'>IBAN Number :</span>DE92501108006231605970 (For Euro Payments)</li>");
        sbPerformaInvoice.Append("<li style='font-size:14px;margin-bottom:8px;'><span style='display:inline-block;width:178px;font-weight:normal;'>IBAN Number :</span>ABA FED No. 021000021 (For USD Payments)</li>");
        sbPerformaInvoice.Append("</ul>");
        sbPerformaInvoice.Append("</div>");
        sbPerformaInvoice.Append("<div style='clear:both;'></div>");
        sbPerformaInvoice.Append("</div>");
        sbPerformaInvoice.Append("<div style='padding:10px 0;border-top:solid 1px #000;'>");
        sbPerformaInvoice.Append("<span style='width:250px;font-weight:bold;font-size:14px;display:inline-block;text-align:left;'>GSTTIN No. :27AABCS9352H1Z4</span");
        sbPerformaInvoice.Append("<span style='float:right;width:250px;font-size:14px;display:inline-block;font-weight:normal;text-align:left;'>With effect from 1st July 2017</span>");
        sbPerformaInvoice.Append("</div>");
        sbPerformaInvoice.Append("<div style='padding:10px 0;border-bottom:solid 1px #000;border-top:solid 1px #000;'>");
        sbPerformaInvoice.Append("<span style='width:250px;font-weight:bold;font-size:14px;display:inline-block;text-align:left;'>IEC Code:307004121</span>");
        sbPerformaInvoice.Append("<span style='margin-left:100px;width:250px;font-weight:bold;font-size:14px;display:inline-block;text-align:left;'>PAN No:AABCS9352H</span>");
        sbPerformaInvoice.Append("<span style='margin-left:100px;width:250px;font-weight:bold;font-size:14px;display:inline-block;text-align:left;'>CIN:U51209MH1966PTCO013575</span>");
        sbPerformaInvoice.Append("</div>");
        sbPerformaInvoice.Append("<div style='margin:10px 0 20px;'>");
        sbPerformaInvoice.Append("<p style='margin-bottom:2px;font-size:13px;color:#000;text-align:center;margin-top:3px;'>FGP Centre, Commercial Union House, 3rd Floor, 9, Wallace Street, Fort, Mumbai-400 001, India.</p>");
        sbPerformaInvoice.Append("<p style='margin-bottom:2px;font-size:13px;color:#000;text-align:center;margin-top:3px;'>Tel:91-22-2204 8138 / 39 Fax:91-22-2204 8140 Email:contact@astaguru.com</p>");
        sbPerformaInvoice.Append("</div>");
        sbPerformaInvoice.Append("</div>");
        sbPerformaInvoice.Append("</div>");
        sbPerformaInvoice.Append("</body>");




    }
}