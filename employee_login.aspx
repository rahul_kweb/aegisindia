﻿<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true" CodeFile="employee_login.aspx.cs" Inherits="employee_login" Title="Aegis Logistics Limited - Employee Login" %>
<%@ Register Src="~/Admin/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="Admin/StyleSheet.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   
  <div id="smoothmenu1" class="ddsmoothmenu">
    
        <ul>
        <li><a href="index.aspx">Home</a></li>
        <li><a href="liquid_logistics.aspx">Business Mix</a>
          <ul>
          <li><a href="liquid_logistics.aspx">Liquid Logistics</a></li>
          <li><a href="gas_logistics.aspx">Gas Logistics</a></li>
          <li><a href="epc_services.aspx">EPC Services</a></li>
          <%--<li><a href="retail_lpg.aspx">Retail LPG</a></li>--%>
              <li><a href="http://www.aegisretail.in"  target="_blank">Retail LPG</a></li>
          <li><a href="marine.aspx">Marine</a></li> 
          </ul>
        </li>
        <li><a href="hse.aspx">HSE</a>
          <ul>
            <%--<li><a href="vision_commitment.aspx">Vision &amp; Commitment</a></li>--%>
  	        <%--<li><a href="hse_performance.aspx">Performance</a></li>
  	        <li><a href="hse_management_systems.aspx">Management Systems</a></li>
  	        <li><a href="hse_development_trainings.aspx">Development &amp; Training</a></li>
  	        <li><a href="hse_environment_programs.aspx">Environment Programs</a></li>
  	        <li><a href="hse_events_pictures.aspx">Events &amp; Pictures</a></li>--%>
          <%--<li><a href="hse_highlights.aspx">Highlights</a></li>
          <li><a href="hse_training_development.aspx">Training  &amp; Development</a></li>--%>
          </ul>
        </li>
        <li><a href="company_profile.aspx">About Us</a>
          <ul>
          <li><a href="key_management_team.aspx">Key Management &amp; Team</a></li>
          <li><a href="partners.aspx">Industry Partners</a></li>
          <li><a href="associations_credentials.aspx">Credentials</a></li>
          <li><a href="initiatives.aspx">Continuous Improvement</a></li>
          <%--<li><a href="infrastructure.aspx">Infrastructure</a></li>
          <li><a href="awards_recognition.aspx">Awards &amp; Recognition</a></li>--%>
          </ul>
        </li>
        <li><a href="key_financial_data.aspx">Investor Relations</a>
          <ul>
          	<li><a href="key_financial_data.aspx">Key Financial Data</a></li>
            <li><a href="shares.aspx">Shares</a></li>
            <li><a href="reports_filings.aspx">Reports &amp; Filings</a></li>
            <li><a href="events_presentations.aspx">Presentations</a></li>
            <%--<li><a href="acquisitions.aspx">Acquisitions</a></li>
            <li><a href="corporate_governance.aspx">Corporate Governance</a></li>
            <li><a href="investor_contact.aspx">Investor Contact</a></li>--%>
            <li><a href="quarterly_results.aspx">Quarterly Results</a></li>
            <%--<li><a href="live_share_info.aspx">Live Share Info</a></li>--%>
          </ul>        
        </li>
        <li><a href="csr.aspx">CSR</a></li>
        <li><a href="contactus.aspx">Contact</a></li>
        </ul>

	</div>  
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

<div id="main_container" style="height:450px; border-top: 1px solid #e4e4e4;">

<div id="cont_full">
		
        <div id="login_box">
        
        <div id="pg_head">Employee Login</div>
        <div class="clear"></div>
        
        <div class="log_box">               
        Username &nbsp;&nbsp;
            <asp:TextBox ID="txtUsername" CssClass="input_log" runat="server"></asp:TextBox>
       	    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                ErrorMessage="*" ControlToValidate="txtUsername"></asp:RequiredFieldValidator>
       	</div>
        <div class="clear"></div>
        
        <div class="log_box">               
        Password &nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="txtPassword" CssClass="input_log" runat="server" 
                TextMode="Password"></asp:TextBox>
       	    <asp:RequiredFieldValidator ID="ReqPwd" runat="server" 
                ErrorMessage="*" ControlToValidate="txtPassword"></asp:RequiredFieldValidator>
       	</div>
        <div class="clear" style="height:10px;"></div>
        
        <div class="log_box">
            <asp:Button ID="btnLogin" class="log_but" runat="server" Text="Login" onclick="btnLogin_Click" />
       	</div>
        <div class="clear"></div>
        
        <div class="log_box" style="color:#e9242d; font-size:12px; font-weight:bold;">
            
            <asp:Label ID="lblMsg" runat="server" Visible="false" Text=""></asp:Label>
        
            <%--<uc1:MyMessageBox ID="Msg" runat="server" ShowCloseButton="true" Visible="False" />--%>
        
        </div>
        
		</div>

</div>

<div class="clear"></div>
</div>

</asp:Content>

