﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class key_management_team : System.Web.UI.Page
{
    Class1 c = new Class1();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
           
            c.BindInnerPageBanner(41, ImgBanner);
            c.BindPageTalent(1,lblContent);
            //BindList();
            BindChairman();
        }
    }
    //private void BindList()
    //{
    //    try
    //    {
    //        DataTable dt = new DataTable();
    //        dt = c.Display("EXEC AddUpdateGetChairmanMaster 'Get_By_TypeOther'");
            
    //        dtlImages.DataSource = dt;
    //        dtlImages.DataBind();
    //    }
    //    catch (Exception ex)
    //    {

    //    }
    //}
    private void BindChairman()
    {
        //chariman
        DataTable dt = new DataTable();
        dt = c.Display("EXEC AddUpdateGetChairmanMaster 'Get'");
        int i = 0;
        int removerow = 0;

        foreach (DataRow dr in dt.Rows)
        {
            string post;
            post = dr["Desg_vcr"].ToString();
            //if (post.ToLower().Equals("founding chairman"))
            if (post.ToLower().Equals("chairman & managing director"))
            {
                imgChairman.ImageUrl = "~/Admin/images/" + dr["Image_vcr"].ToString();
                imgChairman.ToolTip = dr["Name_vcr"].ToString();
                lblName.Text = dr["Name_vcr"].ToString();
                lblDesg.Text = dr["Desg_vcr"].ToString();
                removerow = i;

            }
            
            i++;
        } 
        dt.Rows.RemoveAt(removerow);
        dt.AcceptChanges(); 

        dtlImages.DataSource = dt;
        dtlImages.DataBind();
     
    }

}
