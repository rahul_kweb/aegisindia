﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class partners : System.Web.UI.Page
{
    Class1 c = new Class1();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindList();
             c.BindInnerPageBanner(4, ImgBanner);
        }
    }
    private void BindList()
    {
        try
        {
            dtlImages.DataSource = c.Display("EXEC AddUpdateGetImageMaster 'Get_By_Type_Id',0,'','',0,1");
            dtlImages.DataBind();
        }
        catch (Exception ex)
        {
 
        }
    }
}
