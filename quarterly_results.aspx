﻿<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true"
    CodeFile="quarterly_results.aspx.cs" Inherits="quarterly_results" Title="Aegis Logistics Limited - Investor Relations - Quarterly Results" %>

<%@ Register Src="investor_top.ascx" TagName="investor_top" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%--<!--  scroll to top  -->

<script src="js/jquery.localscroll-1.2.6-min.js" type="text/javascript"></script>
<script src="js/jquery.scrollTo-1.4.0-min.js" type="text/javascript"></script>
<script>

$(document).ready(function () {
	$.localScroll();	
});

</script>

<!--  scroll to top  -->--%>
    <script type="text/javascript" src="js/mootools.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="css/screen.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:investor_top ID="investor_top1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div id="main_container" style="border-top: 1px solid #e4e4e4; padding-top: 20px;">
        <%--<div id="banner"><img src="images/banner/liquid_logistic.jpg" border="0" alt="Liquid Logistics" title="Liquid logistics" /></div>--%>
        <div class="clear">
        </div>
        <!--  Cont-left  -->
        <div id="cont_left">
            <div id="pg_head">
                Investor Relations</div>
            <div id="rel_box_link">
                <ul>
                    <li><a href="Corporate_Governances.aspx">Corporate Governance</a></li>
                    <%--<li><a href="key_financial_data.aspx">Key Financial Data</a></li>--%>
                    <li><a href="key_financial_data.aspx">Financial Information</a></li>
                    <li><a href="shares.aspx">Shares</a></li>
                    <li><a href="Debentures.aspx">Debentures</a></li>
                    <li><a href="reports_filings.aspx">Reports &amp; Filings</a></li>
                    <li><a href="events_presentations.aspx">Presentations</a></li>
                    <%--<li><a href="acquisitions.aspx">Acquisitions</a></li>
    <li><a href="corporate_governance.aspx">Corporate Governance</a></li>
    <li><a href="investor_contact.aspx">Investor Contact</a></li>--%>
                    <li class="selec">Financial Results</li>
                    <li><a href="news.aspx">News &amp; Events</a></li>
                    <%--<li><a href="live_share_info.aspx">Live Share Info</a></li>--%>
                    <li><a href="unclaimedamounts.aspx">Unpaid & Unclaimed Amounts</a></li>
                    <%--<li><a href="sealord_containers_limited.aspx">Sea Lord Containers Limited (Subsidiary)</a></li>--%>
                    <li><a href="Aegis_Gas_Lpg_Pvt_Ltd.aspx">Aegis Gas (LPG) Pvt. Ltd. (Subsidiary)</a></li>
                </ul>
            </div>
        </div>
        <!--  Cont-left  -->
        <div id="gray_line" style="height: 525px;">
        </div>
        <!--  Cont-right  -->
        <div id="cont_right">
            <div id="cont_right_box">
                <div id="accordion" style="width: 685px;">
                    <!--start toggle-->
                    <asp:DataList ID="dtlOuter" runat="server">
                        <ItemTemplate>
                            <h3 class="toggler atStart">
                                <%# Eval("Pdf_Master_Name_vcr") %>
                            </h3>
                            <div class="element atStart">
                                <asp:Label ID="lblId" runat="server" Visible="false" Text='<%# Eval("Pdf_Master_Id_int") %>'></asp:Label>
                                <ul class="list_in">
                                    <asp:DataList ID="dtlInner" runat="server">
                                        <ItemTemplate>
                                            <li><a href='<%# string.Format("Admin/Documents/{0}.pdf",Eval("Pdf_Page_Path_vcr")) %>'
                                                target="_blank">
                                                <%# Eval("Pdf_Title_vcr")%>
                                            </a></li>
                                        </ItemTemplate>
                                    </asp:DataList>
                                    <%--<li><a href="pdf/quarterly_results/2011-12/Consolidated.pdf" target="_blank">FY12 Consolidated Audited Results</a></li>
                          
                          <li><a href="pdf/quarterly_results/2011-12/Standalone.pdf" target="_blank">FY12 Standalone Audited Results</a></li>
                          
                          <li><a href="pdf/quarterly_results/2011-12/Q1_FY12_Standalone.pdf" target="_blank">Q1 FY12 Standalone</a></li>
                          
                          <li><a href="pdf/quarterly_results/2011-12/Q1_FY12_RESULTS.pdf" target="_blank">Q1_FY12 Consolidated</a></li>
                          
                          <li><a href="pdf/quarterly_results/2011-12/ConsolideteResults.pdf" target="_blank">Q2 FY12 Consolidated Result</a></li>
                          
                          <li><a href="pdf/quarterly_results/2011-12/Standalone_Results.pdf" target="_blank">Q2 FY12 Standalone Results</a></li>
                          
                          <li><a href="pdf/quarterly_results/2011-12/Q3 FY12 Consolidated Results new.pdf" target="_blank">Q3 FY12 Consolidated Results</a></li>
                          
                          <li><a href="pdf/quarterly_results/2011-12/Q3 FY12 Standalone Results new.pdf" target="_blank">Q3 FY12 Standalone Results</a></li>--%>
                                </ul>
                            </div>
                        </ItemTemplate>
                    </asp:DataList>
                    <%-- <h3 class="toggler atStart">2011 - 12</h3>
                        <div class="element atStart">
                          
                          <ul class="list_in"> 
                          
                          <li><a href="pdf/quarterly_results/2011-12/Consolidated.pdf" target="_blank">FY12 Consolidated Audited Results</a></li>
                          
                          <li><a href="pdf/quarterly_results/2011-12/Standalone.pdf" target="_blank">FY12 Standalone Audited Results</a></li>
                          
                          <li><a href="pdf/quarterly_results/2011-12/Q1_FY12_Standalone.pdf" target="_blank">Q1 FY12 Standalone</a></li>
                          
                          <li><a href="pdf/quarterly_results/2011-12/Q1_FY12_RESULTS.pdf" target="_blank">Q1_FY12 Consolidated</a></li>
                          
                          <li><a href="pdf/quarterly_results/2011-12/ConsolideteResults.pdf" target="_blank">Q2 FY12 Consolidated Result</a></li>
                          
                          <li><a href="pdf/quarterly_results/2011-12/Standalone_Results.pdf" target="_blank">Q2 FY12 Standalone Results</a></li>
                          
                          <li><a href="pdf/quarterly_results/2011-12/Q3 FY12 Consolidated Results new.pdf" target="_blank">Q3 FY12 Consolidated Results</a></li>
                          
                          <li><a href="pdf/quarterly_results/2011-12/Q3 FY12 Standalone Results new.pdf" target="_blank">Q3 FY12 Standalone Results</a></li>

                          </ul>
                          
                        </div>                        
                        
                        <h3 class="toggler atStart">2010 - 11</h3>
                        <div class="element atStart">
                          <ul class="list_in"> 
                          
                          <li><a href="pdf/quarterly_results/2010-11/CONSOLIDATED305.pdf" target="_blank">FY11 Consolidated Audited Results</a></li>
                          
                          <li><a href="pdf/quarterly_results/2010-11/STANDALONE305.pdf" target="_blank">FY11 Standalone Audited Results</a></li>
                          
                          <li><a href="pdf/quarterly_results/2010-11/Aegis Q1 2010-11.pdf" target="_blank">Q1 June</a></li>
                          
                          <li><a href="pdf/quarterly_results/2010-11/Aegis Q2 2010-11.pdf" target="_blank">Q2 Consolidated</a></li>
                          
                          <li><a href="pdf/quarterly_results/2010-11/Q2F11 STANDALONE.pdf" target="_blank">Q2 STANDALONE</a></li>
                          
                          <li><a href="pdf/quarterly_results/2010-11/Q3 UNAUDITEDAegis Consol.pdf" target="_blank">Q3 Consolidated</a></li>
                          
                          <li><a href="pdf/quarterly_results/2010-11/Q3 UNAUDITEDAegis stanalone.pdf" target="_blank">Q3 STANDALONE</a></li>

                          </ul>
                        </div>                        
                        
                        <h3 class="toggler atStart">2009 - 10</h3>
                        <div class="element atStart">
                         <ul class="list_in"> 
                          
                          <li><a href="pdf/quarterly_results/2009-10/AUDITED Consol.pdf" target="_blank">AUDITED Consolidated</a></li>
                          
                          <li><a href="pdf/quarterly_results/2009-10/AUDITED standalone.pdf" target="_blank">AUDITED Standalone</a></li>
                          
                          <li><a href="pdf/quarterly_results/2009-10/Q1 Jun09.pdf" target="_blank">Q1 June</a></li>
                          
                          <li><a href="pdf/quarterly_results/2009-10/Aegis Results.pdf" target="_blank">Q2</a></li>
                          
                          <li><a href="pdf/quarterly_results/2009-10/UnauditedFinancialResults.pdf" target="_blank">Q2 Unaudited Financial Results</a></li>
                          
                          <li><a href="pdf/quarterly_results/2009-10/Q3 UNAUDITED RESULTn.pdf" target="_blank">Q3 Unaudited Financial Results</a></li>

                          </ul>
                        </div>                        
                        
                        <h3 class="toggler atStart">2008 - 09</h3>
                        <div class="element atStart">
                         <ul class="list_in"> 
                          
                          <li><a href="#">Q1</a></li>
                          
                          <li><a href="pdf/quarterly_results/2008-09/NOTES_AEGISjune08[1].pdf" target="_blank">Q1 Notes</a></li>
                          
                          <li><a href="pdf/quarterly_results/2008-09/AEZ2.pdf" target="_blank">Q2</a></li>
                          
                          <li><a href="pdf/quarterly_results/2008-09/NOTES_AEGISDeptember08.pdf" target="_blank">Q3 Notes</a></li>
                          
                          <li><a href="pdf/quarterly_results/2008-09/UFR April 2009.pdf" target="_blank">Q4-08-09</a></li>

                          </ul>
                        </div>                       
                        
                        <h3 class="toggler atStart">2007 - 08</h3>
                        <div class="element atStart">
                         <ul class="list_in"> 
                          
                          <li><a href="pdf/quarterly_results/2007-08/Sept07.pdf" target="_blank">Q2</a></li>
                          
                          <li><a href="pdf/quarterly_results/2007-08/Sept07_Notes.pdf" target="_blank">Q2 Notes</a></li>
                          
                          <li><a href="pdf/quarterly_results/2007-08/March08_Notes.pdf" target="_blank">Q4 Notes</a></li>

                          </ul>
                        </div>                      
                        
                        <h3 class="toggler atStart">2006 - 07</h3>
                        <div class="element atStart">
                         <ul class="list_in"> 
                          
                          <li><a href="pdf/quarterly_results/2006-07/June06.pdf" target="_blank">Q1</a></li>

                          </ul>
                        </div>--%>
                </div>
            </div>
        </div>
        <!--  Cont-right  -->
        <div class="clear" style="height: 40px;">
        </div>
        <!--  Breadcrum  -->
        <div id="bread_box">
            <a href="index.aspx" onmouseover="home.src='images/home_ic_h.jpg'" onmouseout="home.src='images/home_ic.jpg'"
                style="padding: 7px 16px 10px 8px;">
                <img src="images/home_ic.jpg" border="0" name="home" title="Home" alt="Home" /></a>
            <a href="#">Investor Relations</a> <span style="margin-left: 7px; margin-right: 10px;">
                Quarterly Results</span>
        </div>
        <!--  Breadcrum  -->
        <%--<div style="float:right; margin-top: 9px;"><a href="#top" onmouseover="tp.src='images/top_h.jpg'" onmouseout="tp.src='images/top.jpg'"><img src="images/top.jpg" border="0" name="tp" alt="Top" /></a></div>--%>
    </div>
</asp:Content>
