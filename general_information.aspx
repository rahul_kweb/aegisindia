﻿<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true" CodeFile="general_information.aspx.cs" Inherits="general_information" Title="Aegis Logistics Limited - Employee Login - General Information" %>


<%@ Register src="employee_login_top.ascx" tagname="employee_login_top" tagprefix="uc1" %>
<%@ Register Src="~/Admin/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<%--<!--  scroll to top  -->

<script src="js/jquery.localscroll-1.2.6-min.js" type="text/javascript"></script>
<script src="js/jquery.scrollTo-1.4.0-min.js" type="text/javascript"></script>
<script>

$(document).ready(function () {
	$.localScroll();	
});

</script>

<!--  scroll to top  -->--%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:employee_login_top ID="employee_login_top1" runat="server" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

<div id="main_container">


	<div id="banner">
	<%--<img src="images/banner/liquid_logistic.jpg" border="0" alt="Liquid Logistics" title="Liquid logistics" />--%>
	<asp:Image runat="server" Visible="false"  border="0"  ID="ImgBanner" alt="General Inforamtion" title="General Inforamtion" ImageUrl='<%# string.Format("~/Admin/images/{0}",Eval("Image_vcr")) %>' />
	</div>
		
	<div class="clear"></div>
        
        
    <!--  Cont-left  -->  
		
	<div id="cont_left">
    
    <div id="pg_head">Employee Section</div>
    
    <div id="rel_box_link">
    <ul>
    <li class="selec">General Information</li>
    <li><a href="knowledge_bank.aspx">Knowledge Bank</a></li>
    <li><a href="change_password.aspx">Change Password</a></li>
    </ul>
    </div>  
    
    
    </div>
        
    <!--  Cont-left  -->  
    
    <div id="gray_line" style="height:500px;"></div>
    
    <!--  Cont-right  -->
    
    <div id="cont_right">
    <uc1:MyMessageBox ID="Msg" runat="server" ShowCloseButton="true" 
                    Visible="False" />
    <div id="log_welcome"><asp:Label ID="lblEmpName" runat="server" Text="Welcome"></asp:Label> 
     &nbsp;&nbsp;|&nbsp;&nbsp; 
        
        <asp:LinkButton ID="lnkLogOut"  runat="server" 
            onclick="lnkLogOut_Click" CausesValidation="False">Logout</asp:LinkButton>
            </div>
    
    <div id="log_right_box">
    
    <ul class="list">
        <asp:DataList ID="dtlNotice" runat="server">
        <ItemTemplate>
                <asp:Label ID="lblContent" runat="server" Text='<%# Eval("Notice_ntxt") %>'></asp:Label> 
        </ItemTemplate>        
        </asp:DataList>
       
    <%--<li>Aegis Logistics Limited, is a leading bulk liquid POL, Chemical products Logistics Company in the Country serving 
    Petrochemical, Oil & Gas Industry. Aegis offers sevices such as sourcing of raw materials, tank storage and terminalling,
     arranging transport through road, pipeline or ships and onsite management of materials at the customer's site.</li>
    
    <li>Aegis Logistics Limited, is a leading bulk liquid POL, Chemical products Logistics Company in the Country serving 
    Petrochemical, Oil & Gas Industry. Aegis offers sevices such as sourcing of raw materials, tank storage and terminalling,
     arranging transport through road, pipeline or ships and onsite management of materials at the customer's site.</li>
    
    <li>Aegis Logistics Limited, is a leading bulk liquid POL, Chemical products Logistics Company in the Country serving 
    Petrochemical, Oil & Gas Industry. Aegis offers sevices such as sourcing of raw materials, tank storage and terminalling,
     arranging transport through road, pipeline or ships and onsite management of materials at the customer's site.</li>
    
    <li>Aegis Logistics Limited, is a leading bulk liquid POL, Chemical products Logistics Company in the Country serving 
    Petrochemical, Oil & Gas Industry. Aegis offers sevices such as sourcing of raw materials, tank storage and terminalling,
     arranging transport through road, pipeline or ships and onsite management of materials at the customer's site.</li>
    
    <li>Aegis Logistics Limited, is a leading bulk liquid POL, Chemical products Logistics Company in the Country serving 
    Petrochemical, Oil & Gas Industry. Aegis offers sevices such as sourcing of raw materials, tank storage and terminalling,
     arranging transport through road, pipeline or ships and onsite management of materials at the customer's site.</li>
    
    <li>Aegis Logistics Limited, is a leading bulk liquid POL, Chemical products Logistics Company in the Country serving 
    Petrochemical, Oil & Gas Industry. Aegis offers sevices such as sourcing of raw materials, tank storage and terminalling,
     arranging transport through road, pipeline or ships and onsite management of materials at the customer's site.</li>
    --%>
    </ul>
    
    
    </div>        
    
    </div>
    
    <!--  Cont-right  -->

<div class="clear" style="height:40px;"></div>

<!--  Breadcrum  -->
        
    <div id="bread_box">
    <a href="index.aspx" onmouseover="home.src='images/home_ic_h.jpg'" onmouseout="home.src='images/home_ic.jpg'" style="padding:7px 16px 10px 8px;"><img src="images/home_ic.jpg" border="0" name="home" title="Home" alt="" /></a>
    <a href="#">Employee Login</a>
    <span style="margin-left:7px; margin-right:10px;">General Information</span>
    </div>
        
    <!--  Breadcrum  -->

<%--<div style="float:right; margin-top: 9px;"><a href="#top" onmouseover="tp.src='images/top_h.jpg'" onmouseout="tp.src='images/top.jpg'"><img src="images/top.jpg" border="0" name="tp" alt="" /></a></div>--%>
</div>

</asp:Content>

