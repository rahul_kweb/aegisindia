﻿<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true" CodeFile="associations_credentials.aspx.cs" Inherits="associations_credentials" Title="Aegis Logistics Limited - About Us - Credentials" %>

<%@ Register src="about_top.ascx" tagname="about_top" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<%-- <!--  scroll to top  -->

<script src="js/jquery.localscroll-1.2.6-min.js" type="text/javascript"></script>
<script src="js/jquery.scrollTo-1.4.0-min.js" type="text/javascript"></script>
<script type="text/javascript">

$(document).ready(function () {
	$.localScroll();	
});

</script>

<!--  scroll to top  --> --%>

<!-- colorbox -->

<link media="screen" rel="stylesheet" href="colorbox/colorbox.css" />	
<script type="text/javascript" src="colorbox/jquery.colorbox.js"></script>
<script type="text/javascript" src="colorbox/id_nos.js"></script>

<!-- colorbox -->

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<uc1:about_top ID="about_top1" runat="server" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

<div id="main_container">


	<div id="banner">
	<%--<img src="images/banner/general.jpg" border="0" alt="Credentials" title="Credentials" runat="server" />--%>
	<asp:Image runat="server" Visible="false"  border="0"  ID="ImgBanner" alt="Credentials" title="Credentials" ImageUrl='<%# string.Format("~/Admin/images/{0}",Eval("Image_vcr")) %>' />
	</div>
		
	<div class="clear"></div>
        
        
    <!--  Cont-left  -->  
		
	<div id="cont_left">
    
    <div id="pg_head"><a href="company_profile.aspx">About Us</a></div>
    
    <div id="rel_box_link">
    <ul>
    <%--<li><a href="company_profile.aspx">Company Profile</a></li>
    <li><a href="background_parent_company.aspx">Background</a></li>--%>
    <li><a href="key_management_team.aspx">Key Management &amp; Team</a></li>
    <li><a href="partners.aspx">Industry Partners</a></li>
    <li class="selec">Credentials</li>
  	<li><a href="initiatives.aspx">Continuous Improvement</a></li>
  	<%--<li><a href="infrastructure.aspx">Infrastructure</a></li>
  	<li><a href="awards_recognition.aspx">Awards &amp; Recognition</a></li>--%>
    </ul>
    </div>
    
    </div>
        
    <!--  Cont-left  -->  
    
    <div id="gray_line" style="height:500px;"></div>
    
    <!--  Cont-right  -->
    
    <div id="cont_right">
   
    
    <div id="cont_right_box">
    
     <asp:Label ID="lblContent" runat="server" Text=""></asp:Label>
     
    <%--<ul class="list">
    <li>P1+ by CRISIL for Working Capital</li>
    <li>“AA” by CARE for Long-Term Borrowings</li>
    <li>ISO 9001:2008, Quality Management Systems - <a href="images/credentials/iso_9001.jpg" class="anylink2" rel="example1">Click here to view</a></li>
    <li>ISO 14001:2004, Environmental Management Systems - <a href="images/credentials/iso_14001.jpg" class="anylink2" rel="example2">Click here to view</a></li>
    <li>OHSAS 18001:2007, Occupational, Health & Safety - <a href="images/credentials/ohsas.jpg" class="anylink2" rel="example3">Click here to view</a></li>
    <li>Rating 1 under LPG Regulations & Supply and Distribution Order by CARE</li>
    <li>Rating 1 under LPG (Regulation of use in Motor Vehicles for Auto Business) by CARE</li>
    <li>Member of -
    	<ul class="list_in">
        <li>SIGTTO (Society of International Gas Tankers & Terminal Operators) - <a href="images/credentials/sigtto.jpg" class="anylink2" rel="example4">Click here to view</a></li>
        <li>NFPA (National Fire Prevention Association – USA)</li>
        <li>NSC (National Safety Council)</li>
        <li>MARG (Mutual Aid Response Group)</li>
        <li>ICC (Indian Chemical Council)</li>
        <li>BCCI (Bombay Chamber of Commerce & Industry)</li>
        </ul>
     </li>
     </ul>
    
    
    <div class="clear"></div>--%>
    
    </div> 
    
    
    </div>
    
    <!--  Cont-right  -->

<div class="clear" style="height:40px;"></div>

<!--  Breadcrum  -->
        
    <div id="bread_box">
    <a href="index.aspx" onmouseover="home.src='images/home_ic_h.jpg'" onmouseout="home.src='images/home_ic.jpg'" style="padding:7px 16px 10px 8px;"><img src="images/home_ic.jpg" border="0" name="home" title="Home" alt="" runat="server" /></a>
    <a href="company_profile.aspx">About Us</a>
    <span style="margin-left:7px; margin-right:10px;">Credentials</span>
    </div>
        
    <!--  Breadcrum  -->

<%--<div style="float:right; margin-top: 9px;"><a href="#top" onmouseover="tp.src='images/top_h.jpg'" onmouseout="tp.src='images/top.jpg'"><img src="images/top.jpg" border="0" name="tp" alt="" runat="server" /></a></div>--%>
</div>

</asp:Content>

