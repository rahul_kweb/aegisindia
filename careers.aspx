﻿<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true" CodeFile="careers.aspx.cs" Inherits="careers" Title="Aegis Logistics Limited - Careers" %>

<%@ Register src="general_top.ascx" tagname="general_top" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" src="Admin/Js/util.js"></script>
    <%--  <script type="text/javascript">
    function CheckBlank()
    {
        var str='';
        var a=false;
        if($('#<%txtName.ClientID %>').val()= 'Name')
        {
            str='Name';
            a=true;
        }
        else
        {
            a=false;
        }
        if($('#<%txtEmail.ClientID %>').val()= 'Email')
        {
            str='Email Id';
            a=true;
        }
        else
        {
            a=false;
        }
        if($('#<%txtContact.ClientID %>').val()== 'Contact')
        {
            str='Contact No.';
            a=true;
        }
        else
        {
            a=false;
        }
        if(a==true)
        {     str =str.substring(0, str.length-1);
              $('#<%=lblMsg.ClientID %>').html('Please Fill Following Fields :- <br /> '+str);
              $('#<%=lblMsg.ClientID %>').show();
              return false;
        }
        else
        {
            $('#<%=lblMsg.ClientID %>').hide();
            if($('#<%=txtEmail.ClientID %>').val().length>0)
            {
                var atpos=$('#<%=txtEmail.ClientID %>').val().indexOf("@");
                var dotpos=$('#<%=txtEmail.ClientID %>').val().lastIndexOf(".");
                if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
                  {
                    $('#<%=lblMsg.ClientID %>').html('Email id is not valid.'+str);
                    $('#<%=lblMsg.ClientID %>').show();
                     
                     return false;
                  }
                
            }
            else
            {
                return true;
            }
        }
        
    }
    
    </script>--%>

   
   <%-- <script type="text/javascript">
      
//         function ChangeDropDown()
//         {
//            var e = document.getElementById("<%=ddlJobType.ClientID %>");    
//            var type = e.options[e.selectedIndex].text;
//            if(type == 'Other')
//            {
//                $('#<%txtJobType.ClientID %>').show();
//            }
//            else
//            {
//                $('#<%txtJobType.ClientID %>').hide();
//            }
//         }
    </script>--%>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <uc1:general_top ID="general_top1" runat="server" />

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

    <div id="main_container">


	<div id="banner">
<%--	<img src="images/banner/general.jpg" border="0" alt="Careers" title="Careers" />--%>
	<asp:Image runat="server" Visible="false"  border="0"  ID="ImgBanner" alt="Careers" title="Careers"  ImageUrl='<%# string.Format("~/Admin/images/{0}",Eval("Image_vcr")) %>' />
	</div>
		
	<div class="clear"></div>
        
    
    <div id="cont_full">
    
    
    
    <div id="pg_head">Careers</div>
    
    <div id="contact_left_box">
    
    <div class="green_heading">Current Openings:</div>
    
    <asp:DataList ID="dtlJob" runat="server">
       <ItemTemplate> 
          
        <div class="career_box">
    	<p><strong>Job Title:</strong> <%# Eval("Title_vcr")%> </p>
    	<p><strong>Primary Skill:</strong> <%# Eval("Desc_vcr")%></p>
    	<p><strong>Location: </strong> <%# Eval("Location_vcr")%> </p>
    	
        </div>
        
         <br />
         <br />
     <%--    <a href='ApplyForJob.aspx?sdf45dfg=<%# Eval("Title_vcr")%>'>
            <img src="images/apply.jpg" border="0" /></a>    --%>     
         
          
         <br />
         
         <br />
         <br />
        </ItemTemplate>
        <FooterTemplate>
       
        <p id="P1" runat="server" visible='<%#bool.Parse((dtlJob.Items.Count==0).ToString()) %>'>
            We are not hiring right now.
        </p>
        </FooterTemplate>
       </asp:DataList>
       
   <%-- <div class="career_box">
    	<p><strong>Job Title : ABCD </strong></p>
    	<p><strong>Primary Skill : </strong></p>
    	<p><strong>Location : </strong>Mumbai</p>
    	<p><img src="images/apply.jpg" border="0" alt="" /></p>
    </div>--%>
    
    </div>
    
    
    
    <div id="contact_right_box">
    
    <div class="green_heading">Apply</div>
    <div class="form_box" style="min-height:0px;height:auto; color:Red;" id="divMsg">
        <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>  
    </div>
    
    <div class="form_box">
        <asp:TextBox ID="txtName" Text="Name" CssClass="input" runat="server" onfocus="if(this.value=='Name')value='';" onblur="if(this.value=='')value='Name';" ></asp:TextBox>
    <%--<input class="input" value="Name" onfocus="if(this.value=='Name')value='';" onblur="if(this.value=='')value='Name';" />--%>
    <span style="color:#FF0000; padding-left:2px;">*</span>
    </div>
    
    <div class="form_box">
        <asp:TextBox ID="txtEmail" Text="Email" CssClass="input" runat="server" onfocus="if(this.value=='Email')value='';" onblur="if(this.value=='')value='Email';"></asp:TextBox>
    <%--<input class="input" value="Email" onfocus="if(this.value=='Email')value='';" onblur="if(this.value=='')value='Email';" />--%>
    <span style="color:#FF0000; padding-left:2px;">*</span>
    </div>
    
    <div class="form_box">
        <asp:TextBox ID="txtContact" Text="Contact no." CssClass="input" runat="server" onfocus="if(this.value=='Contact no.')value='';" onblur="if(this.value=='')value='Contact no.';" onKeyPress="return onlyNumbers();"   MaxLength="10" ></asp:TextBox>
    <%--<input class="input" value="Contact no." onfocus="if(this.value=='Contact no.')value='';" onblur="if(this.value=='')value='Contact no.';" />--%>
    <span style="color:#FF0000; padding-left:2px;">*</span>
    </div>
    
    <div class="form_box">
        <asp:DropDownList CssClass="input" Width="236px" Height="25px" ID="ddlJobType" 
            runat="server" AutoPostBack="True" 
            onselectedindexchanged="ddlJobType_SelectedIndexChanged">
        </asp:DropDownList>
  <%--  <input class="input" value="Applying for" onfocus="if(this.value=='Applying for')value='';" onblur="if(this.value=='')value='Applying for';" />--%>
    <span style="color:#FF0000; padding-left:2px;">*</span>
    </div>
     <div class="form_box" runat="server" id="divOther" visible="false">
     
       <asp:TextBox ID="txtJobType"  Text="Applying for" CssClass="input" runat="server" onfocus="if(this.value=='Applying for')value='';" onblur="if(this.value=='')value='Applying for';" ></asp:TextBox>
    
    <span style="color:#FF0000; padding-left:2px;">*</span>
    </div>
    <div class="form_box">
        <asp:FileUpload CssClass="input" ID="FileUpload1" runat="server" />
    <%--<input class="input" value="Upload Resume" type="file" /> --%>
    
    <span style="color:#FF0000; padding-left:2px;">*</span>
    </div>
    
  
    
    <div class="clear" style="height:10px;"></div>
    
    <div class="form_box">
        <asp:Button ID="btnApply" CssClass="log_but" runat="server" Text="Submit" 
            onclick="btnApply_Click" />
    <%--<input type="submit" value="Submit" class="log_but" /> --%>
    </div>
    
    </div>
        
    <div id="gray_line" style="height:400px; float:right;"></div>
    
         
    
    </div>
    
    

<div class="clear" style="height:40px;"></div>

<!--  Breadcrum  -->
        
    <div id="bread_box">
    <a href="index.aspx" onmouseover="home.src='images/home_ic_h.jpg'" onmouseout="home.src='images/home_ic.jpg'" style="padding:7px 16px 10px 8px;"><img src="images/home_ic.jpg" border="0" name="home" title="Home" /></a>
    <span style="margin-left:7px; margin-right:10px;">Careers</span>
    </div>
        
    <!--  Breadcrum  -->

</div>


</asp:Content>

