<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true" CodeFile="key_management_team.aspx.cs" Inherits="key_management_team" Title="Aegis Logistics Limited - About Us - Key Management & Team" %>

<%@ Register src="about_top.ascx" tagname="about_top" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">


<meta http-equiv="Keywords" content="Chairman,Chandaria,Kapoor,Kapoor M. Chandaria ,Raj Chandaria,Anish Chandaria,Vice Chairman, Managing Director,CEO,Key Management & Team" />


<%-- <!--  scroll to top  -->

<script src="js/jquery.localscroll-1.2.6-min.js" type="text/javascript"></script>
<script src="js/jquery.scrollTo-1.4.0-min.js" type="text/javascript"></script>
<script type="text/javascript">

$(document).ready(function () {
	$.localScroll();	
});

</script>

<!--  scroll to top  --> --%>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:about_top ID="about_top1" runat="server" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

<div id="main_container">


	<div id="banner">
	<%--<img src="images/banner/general.jpg" border="0" alt="Key Management & Team" title="Key Management & Team" runat="server" />--%>
	<asp:Image runat="server" Visible="false"  border="0"  ID="ImgBanner" alt="Key Management & Team" title="Key Management & Team"  ImageUrl='<%# string.Format("~/Admin/images/{0}",Eval("Image_vcr")) %>' />
	
	</div>
		
	<div class="clear"></div>
        
        
    <!--  Cont-left  -->  
		
	<div id="cont_left">
    
    <div id="pg_head"><a href="company_profile.aspx">About Us</a></div>
    
    <div id="rel_box_link">
    <ul>
    <%--<li><a href="company_profile.aspx">Company Profile</a></li>
    <li><a href="background_parent_company.aspx">Background</a></li>--%>
    <li class="selec">Key Management &amp; Team</li>
  	<li><a href="partners.aspx">Industry Partners</a></li>
  	<li><a href="associations_credentials.aspx">Credentials</a></li>
  	<li><a href="initiatives.aspx">Continuous Improvement</a></li>
  	<%--<li><a href="infrastructure.aspx">Infrastructure</a></li>
  	<li><a href="awards_recognition.aspx">Awards &amp; Recognition</a></li>--%>
    </ul>
    </div>
    
    </div>
        
    <!--  Cont-left  -->  
    
    <div id="gray_line" style="height:500px;"></div>
    
    <!--  Cont-right  -->
     <div id="cont_right">
    
    <div id="cont_right_box">
    <div class="team_box" style="margin:0px auto 20px; float:none;">
    
    <div class="img">
                <asp:Image ID="imgChairman" Width="100px" Height="120px"  runat="server" ToolTip='<%#Eval("Image_vcr") %>' style="border:0;" />
            </div>
            <div class="name">
<strong>
    <asp:Label ID="lblName" runat="server" Text=""></asp:Label></strong>
    <br />
                <asp:Label ID="lblDesg" runat="server" Text=""></asp:Label>
</div></div>

   <%--  <asp:DataList ID="d" runat="server" Width="685px" >
        <ItemTemplate>
    
    </ItemTemplate>
    </asp:DataList>--%>
    
 <%--   <div class="team_box" style="margin:0px auto 20px; float:none;">
    <div class="img">
                     
        <asp:Image ID="imgChairman" runat="server" 
         />
    
  
    <div class="name"><strong>
        <asp:Label ID="lblName" runat="server" Text=""></asp:Label></strong><br />
        <asp:Label ID="lblPost" runat="server" Text=""></asp:Label>
        </div>
    </div>
    --%>
    
    
    
    
    
    
    <div class="clear"></div>
        
 <%--   <div class="team_box">
    <div class="img">
    <img src="images/unknown.jpg" border="0" alt="" runat="server" /></div>
    <div class="name">
    <strong>Raj K. Chandaria</strong><br />Vice Chairman &amp;<br />Managing Director</div>
    </div>--%>

    
          <asp:DataList ID="dtlImages" runat="server" RepeatColumns="2"  RepeatDirection="Horizontal">
        <ItemTemplate>        
            <div class="team_box">
            <div class="img">
                <asp:Image ID="Img1" Width="100px" Height="120px"  runat="server" ToolTip='<%#Eval("Name_vcr") %>' style="border:0;" ImageUrl='<%# string.Format("~/Admin/images/{0}",Eval("Image_vcr")) %>' />
            </div>
            <div class="name">
<strong><%#Eval("Name_vcr") %></strong><br /><%#Eval("Desg_vcr")%>
</div></div>
        </ItemTemplate>
        
        </asp:DataList>
       
    
    
   <%-- <div class="team_box">
    <div class="img"><img src="images/unknown.jpg" border="0" alt="" runat="server" /></div>
    <div class="name"><strong>Anish K. Chandaria</strong><br />CEO &amp;<br />Managing Director</div>
    </div>
    
    <div class="team_box">
    <div class="img"><img src="images/unknown.jpg" border="0" alt="" runat="server" /></div>
    <div class="name"><strong>Sudhir Malhotra</strong><br />President</div>
    </div>
    
    <div class="team_box">
    <div class="img"><img src="images/unknown.jpg" border="0" alt="" runat="server" /></div>
    <div class="name"><strong>Rajiv Chohan</strong><br />President (Business Development)</div>
    </div>
    
    <div class="team_box">
    <div class="img"><img src="images/unknown.jpg" border="0" alt="" runat="server" /></div>
    <div class="name"><strong>K . S. Sawant</strong><br />President (Ops &amp; Projects)</div>
    </div>
    
    <div class="team_box">
    <div class="img"><img src="images/unknown.jpg" border="0" alt="" runat="server" /></div>
    <div class="name"><strong>B. I . Gosalia</strong><br />Chief Financial Officer</div>
    </div>--%>
    
    <div class="clear" style="height:15px;"></div>  
    
    <div class="green_heading">Team</div>     
    
  <%--  <ul class="list">
    <li>200 years of Oil Industry Leadership level Experience.</li>
    <li>Leadership team with experience from BPCL, HPCL, Shell, Reliance, ENOC, BORL and other major industries.</li>
    <li>Total Number of employees : 359</li>
    <li>Talented pool of Engineers, MBAs, CAs and Diploma Engineers.</li>
    </ul>--%>
        <asp:Label ID="lblContent" runat="server" Text=""></asp:Label>
    
   </div> 
    
    
    </div>
    
    <!--  Cont-right  -->

<div class="clear" style="height:40px;"></div>

<!--  Breadcrum  -->
        
    <div id="bread_box">
    <a href="index.aspx" onmouseover="home.src='images/home_ic_h.jpg'" onmouseout="home.src='images/home_ic.jpg'" style="padding:7px 16px 10px 8px;"><img src="images/home_ic.jpg" border="0" name="home" title="Home" alt="" runat="server" /></a>
    <a href="company_profile.aspx">About Us</a>
    <span style="margin-left:7px; margin-right:10px;">Key Management &amp; Team</span>
    </div>
        
    <!--  Breadcrum  -->

<%--<div style="float:right; margin-top: 9px;"><a href="#top" onmouseover="tp.src='images/top_h.jpg'" onmouseout="tp.src='images/top.jpg'"><img src="images/top.jpg" border="0" name="tp" alt="" runat="server" /></a></div>--%>
</div>

</asp:Content>

