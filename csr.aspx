﻿<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true"
    CodeFile="csr.aspx.cs" Inherits="csr" Title="Aegis Logistics Limited - CSR" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%--<!--  scroll to top  -->

<script src="js/jquery.localscroll-1.2.6-min.js" type="text/javascript"></script>
<script src="js/jquery.scrollTo-1.4.0-min.js" type="text/javascript"></script>
<script type="text/javascript">

$(document).ready(function () {
	$.localScroll();	
});

</script>

<!--  scroll to top  -->--%>
    <!-- colorbox -->
    <link media="screen" rel="stylesheet" href="colorbox/colorbox.css" />
    <script type="text/javascript" src="colorbox/jquery.colorbox.js"></script>
    <script type="text/javascript" src="colorbox/id_nos.js"></script>
    <!-- colorbox -->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="smoothmenu1" class="ddsmoothmenu">
        <ul>
            <li><a href="index.aspx">Home</a></li>
            <li><a href="liquid_logistics.aspx">Business Mix</a>
                <ul>
                    <li><a href="liquid_logistics.aspx">Liquid Logistics</a></li>
                    <li><a href="gas_logistics.aspx">Gas Logistics</a></li>
                    <li><a href="epc_services.aspx">EPC Services</a></li>
                   <%-- <li><a href="retail_lpg.aspx">Retail LPG</a></li>--%>
                    <li><a href="http://www.aegisretail.in" class="active" target="_blank">Retail LPG</a></li>
                    <li><a href="marine.aspx">Marine</a></li>
                </ul>
            </li>
            <li><a href="hse.aspx">HSE</a>
                <ul>
                    <%--<li><a href="hse_performance.aspx">Performance</a></li>
          <li><a href="hse_management_systems.aspx">Management Systems</a></li>
          <li><a href="hse_development_trainings.aspx">Development &amp; Training</a></li>
          <li><a href="hse_environment_programs.aspx">Environment Programs</a></li>
          <li><a href="hse_events_pictures.aspx">Events &amp; Pictures</a></li>--%>
                </ul>
            </li>
            <li><a href="company_profile.aspx">About Us</a>
                <ul>
                    <li><a href="key_management_team.aspx">Key Management &amp; Team</a></li>
                    <li><a href="partners.aspx">Industry Partners</a></li>
                    <li><a href="associations_credentials.aspx">Credentials</a></li>
                    <li><a href="initiatives.aspx">Continuous Improvement</a></li>
                    <%--<li><a href="infrastructure.aspx">Infrastructure</a></li>
          <li><a href="awards_recognition.aspx">Awards &amp; Recognition</a></li>--%>
                </ul>
            </li>
            <li><a href="Corporate_Governances.aspx">Investor Relations</a>
                <ul>
                    <li><a href="Corporate_Governances.aspx">Corporate Governance</a></li>
                    <li><a href="key_financial_data.aspx">Financial Information</a></li>
                    <li><a href="shares.aspx">Stock Information</a></li>
                    <li><a href="Dividend.aspx">Dividend</a></li>
                    <%--<li><a href="Debentures.aspx">Debentures</a></li>--%>
                    <li><a href="reports_filings.aspx">Stock Exchange Communication</a></li>
                    <li><a href="events_presentations.aspx">Investor Presentations</a></li>
                    <%--<li><a href="acquisitions.aspx">Acquisitions</a></li>
          <li><a href="corporate_governance.aspx">Corporate Governance</a></li>
          <li><a href="investor_contact.aspx">Investor Contact</a></li>--%>
                    <%--<li><a href="quarterly_results.aspx">Quarterly Results</a></li>--%>
                    <li><a href="news.aspx">News and Events</a></li>
                    <li><a href="Investor_Downloads.aspx">Investor Downloads</a></li>
                     <li><a href="InvestorContacts.aspx">Investor Contacts</a></li>
                    <%--<li><a href="live_share_info.aspx">Live Share Info</a></li>--%>
                    <%--<li><a href="unclaimedamounts.aspx">Unpaid & Unclaimed Amounts</a></li>--%>
                    <%--<li><a href="sealord_containers_limited.aspx">Sea Lord Containers Limited (Subsidiary)</a></li>--%>
                  <%--  <li><a href="Aegis_Gas_Lpg_Pvt_Ltd.aspx">Aegis Gas (LPG) Pvt. Ltd. (Subsidiary)</a></li>--%>
                </ul>
            </li>
            <li class="selc"><a href="csr.aspx">CSR</a></li>
            <li><a href="contactus.aspx">Contact</a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div id="main_container">
        <div id="banner">
            <%--<img src="images/banner/csr.jpg" border="0" alt="CSR" title="CSR" />--%>
            <asp:Image runat="server" Visible="false" border="0" ID="ImgBanner" alt="CSR" title="CSR"
                ImageUrl='<%# string.Format("~/Admin/images/{0}",Eval("Image_vcr")) %>' />
        </div>
        <div class="clear">
        </div>
        <div id="cont_full">
            <asp:Label ID="lblContent" runat="server"></asp:Label>
            <%-- <div id="pg_head">Corporate Social Responsibility</div>
    
    <p>ANaRDe FOUNDATION was set up in 1979 with a mission to eradicate poverty through an integrated rural development approach addressing the multifaceted complexity of rural development. Constant approach in improving the quality of life of the villagers by providing water at the door step in villages and alleviating poverty are the major projects.</p>
    
    <div class="green_heading">Activities :</div>
    
    <ul class="list">
    <li>Economic Programmes</li>
    <li>Education</li>
    <li>Afforestation</li>
    <li>Health &amp; Sanitation</li>
    <li>Social &amp; Cultural</li>
    <li>Community Needs</li>
    </ul>
            --%>
            <asp:DataList ID="dtlImages" runat="server" RepeatColumns="4" RepeatDirection="Horizontal">
                <ItemTemplate>
                    <div class="csr_img">
                        <a id="A2" runat="server" href='<%# string.Format("Admin/images/{0}",Eval("Image_Path_vcr")) %>'
                            rel="example1" title='<%#Eval("Title_vcr") %>'>
                            <asp:Image ID="Image1" runat="server" Style="border: 0;" Height="130px" Width="200px"
                                ImageUrl='<%# string.Format("~/Admin/images/{0}",Eval("Image_Path_vcr")) %>' />
                        </a>
                        <br />
                        <%#Eval("Title_vcr") %>
                    </div>
                </ItemTemplate>
            </asp:DataList>
            <%--<div class="csr_img">
    <a href="images/csr/img_1.jpg" rel="example1" title="Pure Water Station for Rural People">
    <img src="images/csr/img_1.jpg" width="200px" height="130px" border="0" alt="" /></a>
    <br />Pure Water Station for Rural People
    </div>
    
    <div class="csr_img">
    <a href="images/csr/img_2.jpg" rel="example2" title="Skill Development Programme">
    <img src="images/csr/img_2.jpg" width="200px" height="130px" border="0" alt="" /></a>
    <br />Skill Development Programme
    </div>
    
    <div class="csr_img">
    <a href="images/csr/img_3.jpg" rel="example3" title="Check Dams in Rural Area">
    <img src="images/csr/img_3.jpg" width="200px" height="130px" border="0" alt="" /></a>
    <br />Check Dams in Rural Area
    </div>
    
    <div class="csr_img">
    <a href="images/csr/img_4.jpg" rel="example4" title="Computer Training Programmes">
    <img src="images/csr/img_4.jpg" width="200px" height="130px" border="0" alt="" /></a>
    <br />Computer Training Programmes
    </div>--%>
        </div>
        <div class="clear" style="height: 40px;">
        </div>
        <!--  Breadcrum  -->
        <div id="bread_box">
            <a href="index.aspx" onmouseover="home.src='images/home_ic_h.jpg'" onmouseout="home.src='images/home_ic.jpg'"
                style="padding: 7px 16px 10px 8px;">
                <img src="images/home_ic.jpg" border="0" name="home" title="Home" alt="Home" /></a>
            <!--<a href="#">About Us</a>-->
            <span style="margin-left: 7px; margin-right: 10px;">CSR</span>
        </div>
        <!--  Breadcrum  -->
        <%--<div style="float:right; margin-top: 9px;"><a href="#top" onmouseover="tp.src='images/top_h.jpg'" onmouseout="tp.src='images/top.jpg'"><img src="images/top.jpg" border="0" name="tp" alt="Top" /></a></div>--%>
    </div>
</asp:Content>
