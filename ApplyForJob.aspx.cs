﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class ApplyForJob : System.Web.UI.Page
{
    Class1 c = new Class1();
    string FilePath = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.QueryString.Count > 0)
            {
                if (Request.QueryString.Keys[0].ToString().Equals("sdf45dfg"))
                {
                    lblTitle.Text = Request.QueryString[0].ToString();
                }
                else
                {
                    lblTitle.Text = "";
                    Response.Redirect("careers.aspx");
                }
            }
            else
            {
                Response.Redirect("careers.aspx");
            }
        }
    }
   
    private int AddUpdate()
    {
        int a = 0;
        try
        {
            string ext = "";
            int Id = 0;
            using (SqlCommand cmd = new SqlCommand("AddUpdateResumeMaster", c.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para_vcr", "Add");
                cmd.Parameters.AddWithValue("@Name_vcr", txtName.Text.Trim());
                cmd.Parameters.AddWithValue("@Email_vcr", txtEmail.Text.Trim());
                cmd.Parameters.AddWithValue("@File_Name_vcr", "UploadedResume");
                cmd.Parameters.AddWithValue("@JOb_Title", lblTitle.Text.Trim());


                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                Id = int.Parse(dt.Rows[0][0].ToString());
                string fileName = dt.Rows[0][1].ToString();

                ext = fileName + SaveFile(fileName);

            }
            using (SqlCommand cmd1 = new SqlCommand("AddUpdateResumeMaster", c.con))
            {
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.AddWithValue("@Para_vcr", "UpdateResumeExtension");
                cmd1.Parameters.AddWithValue("@File_Name_vcr", ext);
                cmd1.Parameters.AddWithValue("@Resume_Id_bint", Id);
                if (c.con.State == ConnectionState.Closed)
                {
                    c.con.Open();
                }
                cmd1.ExecuteNonQuery();

            }
            string body = "Job Application For The Post Of " + lblTitle.Text;
           // int Mail = c.SendEMail("Lilavati Hospital,Job Application", body, "Resume", FilePath);
            a = 1;
        }
        catch (Exception ex)
        {
            a = 0;
            lblMsg.Visible = true;
            lblMsg.Text = "Some error occurred,could not apply for the job.";
        }
        return a;
    }
    private void Reset()
    {
        txtName.Text = "";
        txtEmail.Text = "";
        lblMsg.Text = "";
        txtName.Focus();
    }
    private string SaveFile(string fl)
    {
        string extensionI1 = System.IO.Path.GetExtension(FileUpload1.FileName);

        //string prthumnail = string.Empty;
        //if (extensionI1 != "")
        //{
        //    prthumnail = "Exponents_Prothum_" + txtpcode.Text + "_" + productid + "" + extensionI1;
        //}
        //else
        //{
        //    prthumnail = "";
        //}

        if (extensionI1 != "")
        {
            FileUpload1.SaveAs(Server.MapPath("Admin\\Resume\\" + fl + extensionI1 + ""));
            FilePath = Server.MapPath("Admin\\Resume\\" + fl + extensionI1);
        }
        return extensionI1;

    }


    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string blank = "";
        if (txtName.Text.Trim().Equals(""))
        {
            blank = "Name,";
        }
        if (txtEmail.Text.Trim().Equals(""))
        {
            blank += "Email,";
        }
        if (!FileUpload1.HasFile)
        {
            blank += "Resume,";
        }

        if (blank != "")
        {
            blank = blank.Remove(blank.Length - 1);
            lblMsg.Visible = true;
            lblMsg.Text = "Please Fill Following Fields.</br>" + blank;
        }
        else
        {
            int a = AddUpdate();
            if (a.Equals(1))
            {
                Reset();
                lblMsg.Visible = true;
                lblMsg.Text = "Your Application has been sent.";
            }
        }
    }
}
