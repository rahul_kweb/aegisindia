﻿<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true"
    CodeFile="key_financial_data.aspx.cs" Inherits="key_financial_data" Title="Aegis Logistics Limited - Investor Relations - Key Financial Data" %>

<%@ Register Src="investor_top.ascx" TagName="investor_top" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%--<!--  scroll to top  -->

<script src="js/jquery.localscroll-1.2.6-min.js" type="text/javascript"></script>
<script src="js/jquery.scrollTo-1.4.0-min.js" type="text/javascript"></script>
<script>

$(document).ready(function () {
	$.localScroll();	
});

</script>

<!--  scroll to top  -->--%>


    <script type="text/javascript">
        function ShowRec() {
            $(".show").toggle('');
            $("#hide").css("background-image", 'url(./images/minus.png)');
            $("#hide").attr("onclick", "newShowRec()");
        }
    </script>
    <script type="text/javascript">
        function newShowRec() {
            $(".show").toggle('');
            $("#hide").css("background-image", 'url(./images/plus.png)');
            $("#hide").attr("onclick", "ShowRec()");
        }

        jQuery(document).ready(function () {
            jQuery('.customeToggleContent').eq(0).show();


            jQuery('.customeToggle').click(function (event) {
                jQuery('.customeToggleContent').slideUp();
                jQuery(this).next('.customeToggleContent').slideDown();
                jQuery('.customeToggleContent1').slideUp();
                jQuery('.customeToggleContent1').eq(0).slideDown();
            })

            jQuery('.customeToggleContent1').eq(0).show();

            jQuery('.customeToggle1').click(function (event) {
                jQuery('.customeToggleContent1').slideUp();
                jQuery(this).next('.customeToggleContent1').slideDown();
            })
        })

    </script>


    <script type="text/javascript" src="js/mootools.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="css/screen.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:investor_top ID="investor_top1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div id="main_container" style="border-top: 1px solid #e4e4e4; padding-top: 20px;">
        <%--<div id="banner"><img src="images/banner/liquid_logistic.jpg" border="0" alt="Liquid Logistics" title="Liquid logistics" /></div>--%>
        <div class="clear">
        </div>
        <!--  Cont-left  -->
        <div id="cont_left">
            <div id="pg_head">
                Investor Relations
            </div>
            <div id="rel_box_link">
                <ul>
                    <li><a href="Corporate_Governances.aspx">Corporate Governance</a></li>
                    <li class="selec">Financial Information</li>
                    <%--<li class="selec">Key Financial Data</li>--%>
                    <li><a href="shares.aspx">Stock Information</a></li>
                    <li><a href="Dividend.aspx">Dividend</a></li>
                    <%--<li><a href="Debentures.aspx">Debentures</a></li>--%>
                    <li><a href="reports_filings.aspx">Stock Exchange Communication</a></li>
                    <li><a href="events_presentations.aspx">Investor Presentations</a></li>
                    <%--<li><a href="acquisitions.aspx">Acquisitions</a></li>
    <li><a href="corporate_governance.aspx">Corporate Governance</a></li>
    <li><a href="investor_contact.aspx">Investor Contact</a></li>--%>
                    <%--<li><a href="quarterly_results.aspx">Financial Results</a></li>--%>
                    <li><a href="news.aspx">News and Events</a></li>
                    <li><a href="Investor_Downloads.aspx">Investor Downloads</a></li>
                    <li><a href="InvestorContacts.aspx">Investor Contacts</a></li>
                    <%--<li><a href="live_share_info.aspx">Live Share Info</a></li>--%>
                    <%--<li><a href="unclaimedamounts.aspx">Unpaid & Unclaimed Amounts</a></li>--%>
                    <%--<li><a href="sealord_containers_limited.aspx">Sea Lord Containers Limited (Subsidiary)</a></li>--%>
                    <li><a href="Aegis_Gas_Lpg_Pvt_Ltd.aspx">Aegis Gas (LPG) Pvt. Ltd. (Subsidiary)</a></li>
                </ul>
            </div>
        </div>
        <!--  Cont-left  -->
        <div id="gray_line" style="height: 525px;">
        </div>
        <!--  Cont-right Admin/Documents/Quarterly_Results_2006-07_Pdf1.pdf" -->
        <div id="cont_right">
            <div id="cont_right_box">
                <div id="accordion" style="width: 685px;">
                    <ul class="list">
                        <asp:DataList ID="dtlPdf" runat="server">
                            <ItemTemplate>
                                <li><a href='<%# string.Format("Admin/Documents/{0}.pdf",Eval("Pdf_Page_Path_vcr")) %>'
                                    target="_blank">
                                    <%# Eval("Pdf_Title_vcr")%>
                                </a></li>
                            </ItemTemplate>
                            <%-- <li><a href="pdf/Key_Financial_Data/Financial_highlights_ANNUAL_REPORT_2011-12.pdf" target="_blank">Financial Highlights of Company &amp; Group </a></li>
    
    <li><a href="pdf/Key_Financial_Data/Balance_sheet_ANNUAL_REPORT_2011-12.pdf" target="_blank">Balance Sheet</a></a></li>
    
    <li><a href="pdf/Key_Financial_Data/Segmental_Results_New.pdf" target="_blank">Segmental Results</a></a></li>--%>
                        </asp:DataList>
                    </ul>

                    <h3 class="customeToggle">Annual Report</h3>
                    <div class="customeToggleContent">
                        <ul class="list_in">

                            <li id="Li8" onclick="javascript:return ShowRec();"><span style="cursor: pointer;">Annual Report 2019-20</span>
                                <ul>
                                     <li><a href="Admin/Documents/Regulation_44_23092020.pdf" target="_blank">Outcome of voting at 63rd Annual General Meeting</a></li>
                                    <li><a href="Admin/Documents/Chairmans_Speech_23092020.pdf" target="_blank">Chairman's Speech at 63rd AGM</a></li>
                                    <li><a href="Admin/Documents/AGM Notice of Aegis - 22-09-2020.pdf" target="_blank">Notice of AGM on 22-09-2020</a></li>
                                    <li><a href="Admin/Documents/AEGIS AR 2019-20_Cover to Cover_Final 26.08.2020.pdf" target="_blank">Annual Report 2019-20</a></li>
                                    <li><a href="Admin/Documents/Instructions for Speaker - for website.pdf" target="_blank">Instructions for Speaker</a></li>
                                </ul>
                            </li>
                            <li id="Li7" onclick="javascript:return ShowRec();"><span style="cursor: pointer;">Annual Report 2018-19</span>
                                <ul>
                                    <li><a href="Admin/Documents/ALL_AGM_reg44_voting_30072019.pdf" target="_blank">Outcome of voting at 62nd Annual General Meeting</a></li>
                                    <li><a href="Admin/Documents/ALL_ChairmansSpeechat62ndAGM.pdf" target="_blank">Chairman’s Speech at 62nd AGM</a></li>
                                    <li><a href="Admin/Documents/MGT-9_Directors Report_extract of annual return 2018-19_new_03.10.2019.pdf" target="_blank">MGT-9 – Annual Report 2018-19</a></li>
                                    <li><a href="Admin/Documents/AEGIS AR 2018-19 (FINAL).pdf" target="_blank">Annual Report 2018-19</a></li>
                                </ul>
                            </li>
                            <li id="Li6" onclick="javascript:return ShowRec();"><span style="cursor: pointer;">Annual Report 2017-18</span>
                                <ul>
                                    <li><a href="Admin/Documents/Chairmans_Speech_09_08_2018.pdf" target="_blank">Chairman’s Speech at 61st AGM</a></li>
                                    <li><a href="Admin/Documents/Voting_Result_Regulation_44_09082018.pdf" target="_blank">Outcome of voting at 61st Annual General Meeting</a></li>
                                    <li><a href="Admin/Documents/AEGIS_Annual Report_2018.pdf" target="_blank">Annual Report 2017-18</a></li>
                                </ul>
                            </li>

                            <li id="Li5" onclick="javascript:return ShowRec();"><span style="cursor: pointer;">Annual
                                Report 2016-17</span>
                                <ul>
                                    <li><a href="Admin/Documents/Aegis_AnnualReport12072017.pdf" target="_blank">Annual Report 2016-17</a></li>
                                    <li><a href="Admin/Documents/Chairmans_Speech_at_60th_AGM-110817.pdf" target="_blank">Chairman’s Speech at 60th AGM </a></li>
                                    <li><a href="Admin/Documents/Outcome_of_voting_at_60th_Annual_General_Meeting110817.pdf" target="_blank">Outcome of voting at 60th Annual General Meeting</a></li>
                                </ul>
                            </li>
                            <li id="Li2" onclick="javascript:return ShowRec();"><span style="cursor: pointer;">Annual
                                Report 2015-16</span>
                                <ul>
                                    <li><a href="Admin/Documents/Aegis Logistics_Full_Annual_Report_2016.pdf" target="_blank">Annual Report 2015-16</a></li>
                                    <li><a href="Admin/Documents/Chairmans_Speech_at_59th_AGM.pdf" target="_blank">Chairman’s Speech at 59th AGM</a></li>
                                    <li><a href="Admin/Documents/Outcome_of_voting_at _59th_Annual_GeneralMeeting.pdf" target="_blank">Outcome of voting at 59th Annual General Meeting</a></li>
                                </ul>
                            </li>
                            <li id="Li1" onclick="javascript:return ShowRec();"><span style="cursor: pointer;">Annual
                                Report 2014-15</span>
                                <ul>
                                    <%--<li><a href="Admin/Documents/Reports_Annual_Pdf173.pdf">Annual Report 2013-14</a></li>
                                    <li><a href="Admin/Documents/Reports_Annual_Pdf174.pdf">E-Voting Communication</a></li>--%>
                                    <li><a href="Admin/Documents/Aegis_Logistics_AR_2015_FINAL.pdf" target="_blank">Annual
                                        Report 2014-15</a></li>
                                    <li><a href="Admin/Documents/58th_Chairmans_Speech_110815_Final.pdf" target="_blank">Chairman’s Speech at 58th AGM</a></li>
                                    <li><a href="Admin/Documents/Outcome_of_voting_at_58th_Annual_General_Meeting.pdf"
                                        target="_blank">Outcome of voting at 58th Annual General Meeting</a></li>
                                </ul>
                            </li>
                            <li id="hide" onclick="javascript:return ShowRec();"><span style="cursor: pointer;">Annual Report 2013-14</span>
                                <ul>
                                    <%--<li><a href="Admin/Documents/Reports_Annual_Pdf173.pdf">Annual Report 2013-14</a></li>
                                    <li><a href="Admin/Documents/Reports_Annual_Pdf174.pdf">E-Voting Communication</a></li>--%>
                                    <li><a href="Admin/Documents/Aegis_Annual%20Report%202013-14.pdf" target="_blank">Annual
                                        Report 2013-14</a></li>
                                    <%--<li><a href="Admin/Documents/Aegis_E-Voting%20Communicaton.pdf" target="_blank">E-Voting Communication</a></li>--%>
                                    <li><a href="Admin/Documents/News_04_08_201439.pdf" target="_blank">Chairman’s Speech
                                        at 57th AGM</a></li>
                                    <li><a href="Admin/Documents/Outcome%20of%20voting%20at%2057th%20Annual%20General%20Meeting.pdf"
                                        target="_blank">Outcome of voting at 57th Annual General Meeting</a></li>
                                </ul>
                            </li>
                            <li><a href="Admin/Documents/Reports_Annual_Pdf139.pdf" target="_blank">Annual Report
                                2012-13</a></li>
                            <li><a href="Admin/Documents/Reports_Annual_Pdf65.pdf" target="_blank">Annual Report
                                2011-12</a></li>
                            <li><a href="Admin/Documents/Reports_Annual_Pdf64.pdf" target="_blank">Annual Report
                                2010-11</a></li>
                            <li><a href="Admin/Documents/Reports_Annual_Pdf63.pdf" target="_blank">Annual Report
                                2009-10</a></li>
                            <li><a href="Admin/Documents/Reports_Annual_Pdf62.pdf" target="_blank">Annual Report
                                2008-09</a></li>
                            <li><a href="Admin/Documents/Reports_Annual_Pdf61.pdf" target="_blank">Annual Report
                                2007-08</a></li>
                        </ul>
                    </div>

                    <h3 class="customeToggle">Financial Results </h3>
                    <div class="customeToggleContent">
                        <ul class="list_in">
                            <asp:DataList ID="dtlOuter" runat="server">
                                <ItemTemplate>
                                    <h3 class="customeToggle1">
                                        <%# Eval("Pdf_Master_Name_vcr") %>
                                    </h3>
                                    <div class="customeToggleContent1">
                                        <asp:Label ID="lblId" runat="server" Visible="false" Text='<%# Eval("Pdf_Master_Id_int") %>'></asp:Label>
                                        <ul class="list_in">
                                            <asp:DataList ID="dtlInner" runat="server">
                                                <ItemTemplate>
                                                    <li><a href='<%# string.Format("Admin/Documents/{0}.pdf",Eval("Pdf_Page_Path_vcr")) %>'
                                                        target="_blank">
                                                        <%# Eval("Pdf_Title_vcr")%>
                                                    </a></li>
                                                </ItemTemplate>
                                            </asp:DataList>
                                        </ul>
                                    </div>
                                </ItemTemplate>
                            </asp:DataList>
                        </ul>
                    </div>

                    <h3 class="customeToggle">Subsidiary Companies</h3>
                    <div class="customeToggleContent">
                        <ul class="list_in">
                            <li id="Li9" onclick="javascript:return ShowRec();"><span style="cursor: pointer;">Annual Report 2019-20</span>
                                <ul>
                                    <li><a href="Admin/Documents/SEALORD_Annual Report 2019-2020.pdf" target="_blank">Sea Lord Containers Limited- Annual Report 2019-20</a></li>
                                    <li><a href="Admin/Documents/AGI_2019-20.pdf" target="_blank">Aegis Group International Pte. Limited - 2019-20</a></li>
                                    <li><a href="Admin/Documents/AIMS_2019-20.pdf" target="_blank">Aegis International Marine Services Pte. Limited - 2019-20</a></li>
                                    <li><a href="Admin/Documents/EILPG_2019-20.pdf" target="_blank">Eastern India LPG Company Private Limited –2019-20</a></li>
                                    <li><a href="Admin/Documents/ALLPL_2019-20.pdf" target="_blank">Aegis LPG Logistics (Pipavav) Limited - 2019-20</a></li>
                                    <li><a href="Admin/Documents/ATPL_2019-20.pdf" target="_blank">Aegis Terminal (Pipavav) Limited - 2019-20</a></li>
                                    <li><a href="Admin/Documents/KCPL_2019-20.pdf" target="_blank">Konkan Storage Systems (Kochi) Private Limited - 2019-20</a></li>
                                    <li><a href="Admin/Documents/HALPG_2019-20.pdf" target="_blank">Hindustan Aegis LPG Limited - 2019-20</a></li>
                                    <li><a href="Admin/Documents/AGPL_2019-20.pdf" target="_blank">Aegis Gas (LPG) Private Limited - 2019-20</a></li>
                                </ul>
                            </li>
                            <li id="Li8" onclick="javascript:return ShowRec();"><span style="cursor: pointer;">Annual Report 2018-19</span>
                                <ul>
                                    <li><a href="Admin/Documents/SCL-Annual Report 2018-2019.pdf" target="_blank">Sea Lord Containers Limited - Annual Report 2018-19</a></li>
                                    <li><a href="Admin/Documents/Aegis Gas (LPG) Private Limited-Annual Report 2018-19.pdf" target="_blank">Aegis Gas (LPG) Private Limited - Annual Report 2018-19</a></li>
                                    <li><a href="Admin/Documents/Hindustan Aegis LPG Limited -Annual Report 2018-19.pdf" target="_blank">Hindustan Aegis LPG Limited - Annual Report 2018-19</a></li>
                                    <li><a href="Admin/Documents/KCPL-ANNUAL REPORT-2018-2019.pdf" target="_blank">Konkan Storage Systems (Kochi) Private Limited - Annual Report 2018-19</a></li>
                                    <li><a href="Admin/Documents/Eastern India LPG Company Private Limited - Annual Report 2018-19.pdf" target="_blank">Eastern India LPG Company Private Limited - Annual Report 2018-19</a></li>
                                    <li><a href="Admin/Documents/Aegis Terminal (Pipavav) Limited - Annual Report 2018-19.pdf" target="_blank">Aegis Terminal (Pipavav) Limited - Annual Report 2018-19</a></li>
                                    <li><a href="Admin/Documents/Aegis LPG Logistics (Pipavav) Limited - Annual Report 2018-19.pdf" target="_blank">Aegis LPG Logistics (Pipavav) Limited - Annual Report 2018-19</a></li>
                                    <li><a href="Admin/Documents/Aegis International Marine Services Pte. Limited - Audited Accounts 2018-19.pdf" target="_blank">Aegis International Marine Services Pte. Limited - Audited Accounts 2018-19</a></li>
                                    <li><a href="Admin/Documents/Aegis Group International Pte. Limited - Audited Accounts 2018-19.pdf" target="_blank">Aegis Group International Pte. Limited - Audited Accounts 2018-19</a></li>
                                </ul>
                            </li>
                            <li id="Li7" onclick="javascript:return ShowRec();"><span style="cursor: pointer;">Annual Report 2017-18</span>
                                <ul>
                                    <li><a href="Admin/Documents/Sea Lord Annual Report_2018.pdf" target="_blank">Sea Lord Containers Limited - Annual Report 2017-18</a></li>
                                    <li><a href="Admin/Documents/AGPL_2017-18.pdf" target="_blank">Aegis Gas (LPG) Private Limited - Annual Report 2017-18</a></li>
                                    <li><a href="Admin/Documents/HALPG_2017-18.pdf" target="_blank">Hindustan Aegis LPG Limited - Annual Report 2017-18 </a></li>
                                    <li><a href="Admin/Documents/KCPL_ANNUAL REPORT 2017-2018.pdf" target="_blank">Konkan Storage Systems (Kochi) Private Limited - Annual Report 2017-18 </a></li>
                                    <li><a href="Admin/Documents/EILPG_2017-18.pdf" target="_blank">Eastern India LPG Company Private Limited - Annual Report 2017-18 </a></li>
                                    <li><a href="Admin/Documents/ATPL-Annual report  2017-18.pdf" target="_blank">Aegis Terminal (Pipavav) Limited - Annual Report 2017-18 </a></li>
                                    <li><a href="Admin/Documents/ALLPL-ANNUAL REPORT 2017-18.pdf" target="_blank">Aegis LPG Logistics (Pipavav) Limited - Annual Report 2017-18 </a></li>
                                    <li><a href="Admin/Documents/AGI - Annual Report - 2017-18.pdf" target="_blank">Aegis Group International Pte. Limited - Audited Accounts 2017-18 </a></li>
                                    <li><a href="Admin/Documents/AIMS - Annual Report - 2017-18.pdf" target="_blank">Aegis International Marine Services Pte. Limited - Audited Accounts 2017-18</a></li>
                                </ul>
                            </li>

                            <li id="Li6" onclick="javascript:return ShowRec();"><span style="cursor: pointer;">Annual Report 2016-17</span>
                                <ul>

                                    <li><a href="Admin/Documents/AGPL-2016-17_New1.pdf"
                                        target="_blank">Aegis Gas (LPG) Private Limited Annual Report 2016-17</a></li>

                                    <li><a href="Admin/Documents/Sea_Lord_Containers_Ltd_Annual_Report2016-17_190719.pdf"
                                        target="_blank">Sea Lord Containers Ltd. - Annual Report 2016-17</a></li>

                                    <li><a href="Admin/Documents/Hindustan_Aegis_LPG_Limited-Annual_Report_2016-17_030817.pdf"
                                        target="_blank">Hindustan Aegis LPG Limited - Annual Report 2016-17</a></li>

                                    <li><a href="Admin/Documents/Konkan_Storage_Systems(Kochi)Private_Limited-Annual_Report_2016-17_030817.pdf"
                                        target="_blank">Konkan Storage Systems (Kochi) Private Limited - Annual Report 2016-17 </a></li>

                                    <li><a href="Admin/Documents/Eastern_India_LPG_Company_Private_Limited-Annual_Report_2016-17_030817.pdf"
                                        target="_blank">Eastern India LPG Company Private Limited - Annual Report 2016-17 </a></li>

                                    <li><a href="Admin/Documents/Aegis_Terminal(Pipavav)_Limited_Annual_Report_2016-17_030817.pdf"
                                        target="_blank">Aegis Terminal (Pipavav) Limited - Annual Report 2016-17 </a></li>

                                    <li><a href="Admin/Documents/Aegis_LPG_Logistics(Pipavav)_Limited-Annual_Report_2016-17_030817.pdf"
                                        target="_blank">Aegis LPG Logistics (Pipavav) Limited - Annual Report 2016-17 </a></li>

                                    <li><a href="Admin/Documents/Aegis_Group_International_Pte_Limited_Audited_Accounts_2016-17_190717.pdf"
                                        target="_blank">Aegis Group International Pte. Limited - Audited Accounts 2016-17 </a></li>

                                    <li><a href="Admin/Documents/Aegis_International_Marine_Services_Pte_Limited_Audited_Accounts2016-17_190717.pdf"
                                        target="_blank">Aegis International Marine Services Pte. Limited - Audited Accounts 2016-17 </a></li>
                                </ul>
                            </li>

                            <li id="Li4" onclick="javascript:return ShowRec();"><span style="cursor: pointer;">Annual
                                Report 2015-2016</span>
                                <ul>
                                    <li><a href="Admin/Documents/AGPL_2015_16_New1.pdf"
                                        target="_blank">Aegis Gas (LPG) Private Limited Annual Report 2015-16</a></li>

                                    <li><a href="Admin/Documents/Aegis_Group_International_Pte._Limited_Annual_Report_2015-16.pdf"
                                        target="_blank">Aegis Group International Pte. Limited - Annual Report 2015-16 </a></li>

                                    <li><a href="Admin/Documents/Aegis_International_Marine_Services_Pte._Limited-Annual_Report_2015- 2016.pdf"
                                        target="_blank">Aegis International Marine Services Pte. Limited - Annual Report 2015- 2016</a></li>

                                    <li><a href="Admin/Documents/Aegis_LPG_Logistics_(Pipavav)_Limited_Annual_Report_2015-16.pdf"
                                        target="_blank">Aegis LPG Logistics (Pipavav) Limited - Annual Report 2015-16</a></li>

                                    <li><a href="Admin/Documents/Aegis_Terminal_(Pipavav)_Limited-Annual_Report_2015-16.pdf"
                                        target="_blank">Aegis Terminal (Pipavav) Limited - Annual Report 2015-16
                                    </a></li>

                                    <li><a href="Admin/Documents/Eastern_India_LPG_Company_Private_Limited-Annual_Report_2015-16_New.pdf"
                                        target="_blank">Eastern India LPG Company Private Limited - Annual Report 2015-16 </a></li>

                                    <li><a href="Admin/Documents/Hindustan_Aegis_LPG_Limited-Annual_Report_2015-16_New.pdf"
                                        target="_blank">Hindustan Aegis LPG Limited - Annual Report 2015-16 </a></li>

                                    <li><a href="Admin/Documents/Konkan_Storage_Systems_(Kochi)_Private_Limited-Annual_Report_2015-16.pdf"
                                        target="_blank">Konkan Storage Systems (Kochi) Private Limited - Annual Report 2015-16
                                    </a></li>
                                    <li><a href="Admin/Documents/Sea_Lord_Containers_Ltd-Annual_Report_2015-16.pdf"
                                        target="_blank">Sea Lord Containers Ltd - Annual Report 2015-16 </a></li>
                                </ul>
                            </li>

                            <li id="Li3" onclick="javascript:return ShowRec();"><span style="cursor: pointer;">Annual Report 2014-15</span>
                                <ul>

                                    <li><a href="Admin/Documents/AGPL_2014_15_New1.pdf"
                                        target="_blank">Aegis Gas (LPG) Private Limited Annual Report 2014-15</a></li>

                                    <li><a href="Admin/Documents/Sea_Lord_Containers_Ltd_Annual_Report_2014_15_11_7_16.pdf"
                                        target="_blank">Sea Lord Containers Ltd. Annual Report 2014-15</a></li>
                                    <li><a href="Admin/Documents/ATPL_2014_15.pdf" target="_blank">Aegis Terminal (Pipavav)
                                Limited Annual Report 2014-15</a></li>
                                    <li><a href="Admin/Documents/Aegis_LPG.pdf" target="_blank">Aegis LPG Logistics (Pipavav)
                                Limited Annual Report 2014-15</a></li>
                                    <li><a href="Admin/Documents/AGI_2014_15.pdf" target="_blank">Aegis Group International
                                Pte Limited Annual Report 2014-15</a></li>
                                    <li><a href="Admin/Documents/AIMS_2014_15.pdf" target="_blank">Aegis International Marine
                                Services Pte Limited Annual Report 2014-15</a></li>
                                    <li><a href="Admin/Documents/HALPG_ 2014-15.pdf" target="_blank">Hindustan Aegis LPG
                                Limited Annual Report 2014-15</a></li>
                                    <li><a href="Admin/Documents/EILPG_2014_15.pdf" target="_blank">Eastern India LPG Company
                                Private Limited Annual Report 2014-15</a></li>
                                    <li><a href="Admin/Documents/KCPL_2014_15.pdf" target="_blank">Konkan Storage Systems
                                (Kochi) Private Limited Annual Report 2014-15</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
        <!--  Cont-right  -->
        <div class="clear" style="height: 40px;">
        </div>
        <!--  Breadcrum  -->
        <div id="bread_box">
            <a href="index.aspx" onmouseover="home.src='images/home_ic_h.jpg'" onmouseout="home.src='images/home_ic.jpg'"
                style="padding: 7px 16px 10px 8px;">
                <img src="images/home_ic.jpg" border="0" name="home" title="Home" alt="" /></a>
            <a href="#">Investor Relations</a> <span style="margin-left: 7px; margin-right: 10px;">Financial Information</span>
        </div>
        <!--  Breadcrum  -->
        <%--<div style="float:right; margin-top: 9px;"><a href="#top" onmouseover="tp.src='images/top_h.jpg'" onmouseout="tp.src='images/top.jpg'"><img src="images/top.jpg" border="0" name="tp" alt="" /></a></div>--%>
    </div>

    <style>
        .customeToggle, .customeToggle1 {
            margin-bottom: 5px !important;
        }

        .customeToggleContent, .customeToggleContent1 {
            display: none;
        }
    </style>
</asp:Content>
