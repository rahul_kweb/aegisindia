﻿<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true" CodeFile="corporate_governance.aspx.cs" Inherits="corporate_governance" Title="Aegis - Investor Relations - Corporate Governance" %>

<%@ Register src="investor_top.ascx" tagname="investor_top" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<%--<!--  scroll to top  -->

<script src="js/jquery.localscroll-1.2.6-min.js" type="text/javascript"></script>
<script src="js/jquery.scrollTo-1.4.0-min.js" type="text/javascript"></script>
<script>

$(document).ready(function () {
	$.localScroll();	
});

</script>

<!--  scroll to top  -->--%>

    <script type="text/javascript" src="js/mootools.js"></script>    
    <link rel="stylesheet" type="text/css" media="screen" href="css/screen.css" />

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:investor_top ID="investor_top1" runat="server" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

<div id="main_container">


	<div id="banner"><img src="images/banner/liquid_logistic.jpg" border="0" alt="Liquid Logistics" title="Liquid logistics" /></div>
		
	<div class="clear"></div>
        
        
    <!--  Cont-left  -->  
		
	<div id="cont_left">
    
    <div id="pg_head">Investor Relations</div>
    
    <div id="rel_box_link">
    <ul>
    <li><a href="key_financial_data.aspx">Key Financial Data</a></li>
    <li><a href="shares.aspx">Stock Information</a></li>
    <li><a href="reports_filings.aspx">Reports &amp; Filings</a></li>
    <li><a href="events_presentations.aspx">Presentations</a></li>
    <li><a href="acquisitions.aspx">Acquisitions</a></li>
    <li class="selec">Corporate Governance</li>
    <li><a href="investor_contact.aspx">Investor Contact</a></li>
    <li><a href="quarterly_results.aspx">Quarterly Results</a></li>
    <li><a href="live_share_info.aspx">Live Share Info</a></li>
    </ul>
    </div>  
    
    
    </div>
        
    <!--  Cont-left  -->  
    
    <div id="gray_line" style="height:500px;"></div>
    
    <!--  Cont-right  -->
    
    <div id="cont_right">
    
    <div id="cont_right_box">
    
    <div id="accordion" style="width:685px;">
                        <!--start toggle-->
                        <h3 class="toggler atStart">Board Composition</h3>
                        <div class="element atStart">
                          
                          <table width="450px" border="0" cellpadding="8px" cellspacing="1px" style="background-color:#d6d6d6;">
                            <tr bgcolor="#e1e1e1">
                            <td width="200px"><strong>Name</strong></td>
                            <td width="250px"><strong >Designation</strong></td>
                            </tr>
                            <tr bgcolor="#fff">
                            <td>Mr. K. M.Chandaria</td>
                            <td>Chairman</td>
                            </tr> 
                            <tr bgcolor="#fff">
                            <td>Mr. R. K. Chandaria</td>
                            <td>Vice Chairman &amp; Mananging Director</td>
                            </tr>
                            <tr bgcolor="#fff">
                            <td>Mr.A. K. Chandaria</td>
                            <td>Managing Director</td>
                            </tr> 
                            <tr bgcolor="#fff">
                            <td>Mr. R. P. Chandaria</td>
                            <td rowspan="6" style="background: url(images/curl.jpg) no-repeat 3px center; padding-left: 20px;">Directors</td>
                            </tr> 
                            <tr bgcolor="#fff">
                            <td>Mr. A. M. Chandaria</td>
                            </tr> 
                            <tr bgcolor="#fff">
                            <td>Mr. D. J. Khimasia</td>
                            </tr> 
                            <tr bgcolor="#fff">
                            <td>Mr. R. J. Karavadia</td>
                            </tr> 
                            <tr bgcolor="#fff">
                            <td>Mr. K. S. Nagpal</td>
                            </tr> 
                            <tr bgcolor="#fff">
                            <td>Mr. V. H. Pandya</td>
                            </tr>
                
                		</table>
                          
                        </div>                        
                        
                        <h3 class="toggler atStart">Board Meetings</h3>
                        <div class="element atStart">
                        
                        	
                          <table width="450px" border="0" cellpadding="8px" cellspacing="1px" style="background-color:#d6d6d6;">
                          	<tr bgcolor="#FFFFFF">
                            <td colspan="2">BOARD MEETINGS HELD DURING FINANCIAL YEAR 2011-12</td>
                            </tr>
                            <tr bgcolor="#e1e1e1">
                            <td width="100px"><strong>Date</strong></td>
                            <td width="350px"><strong >Purpose</strong></td>
                            </tr>                            
                            <tr bgcolor="#fff">
                            <td>30 - 05 - 2011</td>
                            <td>Financial Results </td>
                            </tr>
                            <tr bgcolor="#fff">
                            <td>29 - 07 - 2011</td>
                            <td>Quarterly Results</td>
                            </tr>
                            <tr bgcolor="#fff">
                            <td>14 - 11 - 2011</td>
                            <td>Quarterly Results </td>
                            </tr>
                            <tr bgcolor="#fff">
                            <td>27 - 12 - 2011</td>
                            <td>General</td>
                            </tr>
                            <tr bgcolor="#fff">
                            <td>14 - 02 - 2012</td>
                            <td>Quarterly Results</td>
                            </tr>
                
                		</table>
          </div>                        
                        
                        <h3 class="toggler atStart">Code of Conduct</h3>
                        <div class="element atStart">
                          <ul class="list_in"> 
                           <asp:DataList ID="dtlCoadPdf" runat="server">
                            <ItemTemplate>     
                                <li><a href='<%# string.Format("Admin/Documents/{0}.pdf",Eval("Pdf_Page_Path_vcr")) %>' target="_blank"><%# Eval("Pdf_Title_vcr")%> </a></li>     
                            </ItemTemplate>                         
                            </asp:DataList>   
                                                   
                          	<%--<li><a href="pdf/corporate_governance/Coad Of Conduct.pdf" target="_blank">Download Pdf</a></li>--%>
                       
                          </ul>
                        </div>                     
                        
                        <h3 class="toggler atStart">Management Review &amp; Analysis</h3>
                        <div class="element atStart">
                          <ul class="list_in">  
                          <asp:DataList ID="dtlMngPdf" runat="server">
                            <ItemTemplate>     
                                <li><a href='<%# string.Format("Admin/Documents/{0}.pdf",Eval("Pdf_Page_Path_vcr")) %>' target="_blank"><%# Eval("Pdf_Title_vcr")%> </a></li>     
                            </ItemTemplate>                         
                            </asp:DataList>
                                                  
                          <%--	<li><a href="pdf/corporate_governance/MDAR ANNUAL_REPORT_2011-12.pdf" target="_blank">Download Pdf</a></li>--%>
                          </ul>
                        </div>
                        
        </div>    
    
    </div>        
    
    </div>
    
    <!--  Cont-right  -->

<div class="clear" style="height:40px;"></div>

<!--  Breadcrum  -->
        
    <div id="bread_box">
    <a href="index.aspx" onmouseover="home.src='images/home_ic_h.jpg'" onmouseout="home.src='images/home_ic.jpg'" style="padding:7px 16px 10px 8px;"><img src="images/home_ic.jpg" border="0" name="home" title="Home" alt="Home" /></a>
    <a href="#">Investor Relations</a>
    <span style="margin-left:7px; margin-right:10px;">Corporate Governance</span>
    </div>
        
    <!--  Breadcrum  -->

<%--<div style="float:right; margin-top: 9px;"><a href="#top" onmouseover="tp.src='images/top_h.jpg'" onmouseout="tp.src='images/top.jpg'"><img src="images/top.jpg" border="0" name="tp" alt="Top" /></a></div>--%>
</div>

</asp:Content>

