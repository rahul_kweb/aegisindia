﻿<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true" CodeFile="change_password.aspx.cs" Inherits="change_password" Title="Aegis Logistics Limited - Employee Login - Change Password" %>

<%@ Register src="employee_login_top.ascx" tagname="employee_login_top" tagprefix="uc1" %>
<%@ Register Src="~/Admin/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="Admin/StyleSheet.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:employee_login_top ID="employee_login_top1" runat="server" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

<div id="main_container">


	<div id="banner">
	<%--<img src="images/banner/liquid_logistic.jpg" border="0" alt="Liquid Logistics" title="Liquid logistics" />--%>
	<asp:Image runat="server" Visible="false"  border="0"  ID="ImgBanner" alt="Change Password" title="Change Password" ImageUrl='<%# string.Format("~/Admin/images/{0}",Eval("Image_vcr")) %>' />
	</div>
		
	<div class="clear"></div>
        
        
    <!--  Cont-left  -->  
		
	<div id="cont_left">
    
    <div id="pg_head">Employee Section</div>
    
    <div id="rel_box_link">
    <ul>
    <li><a href="general_information.aspx">General Information</a></li>
    <li><a href="knowledge_bank.aspx">Knowledge Bank</a></li>
    <li class="selec">Change Password</li>
    </ul>
    </div>  
    
    
    </div>
        
    <!--  Cont-left  -->  
    
    <div id="gray_line" style="height:400px;"></div>
    
    <!--  Cont-right  -->
    
    <div id="cont_right">
    <uc1:MyMessageBox ID="Msg" runat="server" ShowCloseButton="true" 
                    Visible="False" />
    
    <div id="log_welcome"><asp:Label ID="lblEmpName" runat="server" Text="Welcome"></asp:Label> 
     &nbsp;&nbsp;|&nbsp;&nbsp; 
     
     
        <asp:LinkButton ID="lnkLogOut"  runat="server" 
            onclick="lnkLogOut_Click" CausesValidation="False">Logout</asp:LinkButton>
            </div>
    
    <div id="log_right_box">
    
    <div class="chg_box">
    <div class="chg_particular">Old Password :</div>
    <asp:TextBox ID="txtOldPwd" CssClass="chg_input" runat="server" 
            TextMode="Password"></asp:TextBox>
    <asp:RequiredFieldValidator ID="ReqOld" runat="server" 
            ErrorMessage="Please Type Old password" ControlToValidate="txtOldPwd" SetFocusOnError="True"></asp:RequiredFieldValidator>
    </div>
          <div class="clear"></div>
    
    <div class="chg_box">
    <div class="chg_particular">New Password :</div>
                
        <asp:TextBox ID="txtNewPwd" CssClass="chg_input" runat="server" 
            TextMode="Password"></asp:TextBox>
        <asp:RequiredFieldValidator ID="ReqNew" runat="server" 
            ErrorMessage="Please Type New Password" ControlToValidate="txtNewPwd" SetFocusOnError="True"></asp:RequiredFieldValidator>
    </div>
          <div class="clear"></div>
    
    <div class="chg_box">
    <div class="chg_particular">Confirm Password :</div>
    <asp:TextBox ID="txtConfPwd" CssClass="chg_input" runat="server" 
            TextMode="Password"></asp:TextBox>
  
        <asp:RequiredFieldValidator ID="ReqConf" runat="server" 
            ErrorMessage="Please Type Confirm Password" ControlToValidate="txtConfPwd" 
            SetFocusOnError="True"></asp:RequiredFieldValidator>
        <asp:CompareValidator ID="CompareValidator1" runat="server" 
            ControlToCompare="txtNewPwd" ControlToValidate="txtConfPwd" 
            ErrorMessage="Please Check Confirm Password" SetFocusOnError="True"></asp:CompareValidator>
    </div>
          <div class="clear"></div>
    
    <div class="chg_box">
    <div class="chg_particular"></div>
     <asp:Button ID="btnSubmit" 
           CssClass="log_but" style="width:125px"
            runat="server" Text="Change Password" onclick="btnSubmit_Click"  />
    </div>
          <div class="clear"></div>
    
    
    
        
        
    </div>        
    
    </div>
    
    <!--  Cont-right  -->

<div class="clear" style="height:40px;"></div>

<!--  Breadcrum  -->
        
    <div id="bread_box">
    <a href="index.aspx" onmouseover="home.src='images/home_ic_h.jpg'" onmouseout="home.src='images/home_ic.jpg'" style="padding:7px 16px 10px 8px;"><img src="images/home_ic.jpg" border="0" name="home" title="Home" /></a>
    <a href="#">Employee Login</a>
    <span style="margin-left:7px; margin-right:10px;">Change Password</span>
    </div>
        
    <!--  Breadcrum  -->

</div>

</asp:Content>

