﻿<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true" CodeFile="company_profile.aspx.cs" Inherits="company_profile" Title="Aegis Logistics Limited - About Us" %>

<%@ Register src="about_top.ascx" tagname="about_top" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<%-- <!--  scroll to top  -->

<script src="js/jquery.localscroll-1.2.6-min.js" type="text/javascript"></script>
<script src="js/jquery.scrollTo-1.4.0-min.js" type="text/javascript"></script>
<script type="text/javascript">

$(document).ready(function () {
	$.localScroll();	
});

</script>

 <!--  scroll to top  --> --%>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">



    <uc1:about_top ID="about_top1" runat="server" />



</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

<div id="main_container">


	<div id="banner">
	<%--<img src="images/banner/general.jpg" border="0" alt="About Us" title="About Us" runat="server" />--%>
	<asp:Image runat="server" Visible="false"  border="0"  ID="ImgBanner" alt="About Us" title="About Us" ImageUrl='<%# string.Format("~/Admin/images/{0}",Eval("Image_vcr")) %>' />
	</div>
		
	<div class="clear"></div>
        
        
    <!--  Cont-left  -->  
		
	<div id="cont_left">
    
    <div id="pg_head">About Us</div>
    
    <div id="rel_box_link">
    <ul>
    <%--<li class="selec">Company Profile</li>
    <li><a href="background_parent_company.aspx">Background</a></li>--%>
  	<li><a href="key_management_team.aspx">Key Management &amp; Team</a></li>
  	<li><a href="partners.aspx">Industry Partners</a></li>
  	<li><a href="associations_credentials.aspx">Credentials</a></li>
  	<li><a href="initiatives.aspx">Continuous Improvement</a></li>
  	<%--<li><a href="infrastructure.aspx">Infrastructure</a></li>
  	<li><a href="awards_recognition.aspx">Awards &amp; Recognition</a></li>--%>
    </ul>
    </div>
    
    
    </div>
        
    <!--  Cont-left  -->  
    
    <div id="gray_line" style="height:500px;"></div>
    
    <!--  Cont-right  -->
    
    <div id="cont_right">
    
    <div id="cont_right_box">
    
   <%-- <p>Aegis Logistics Limited, is a leading bulk liquid POL, Chemical products Logistics Company in the Country serving 
    Petrochemical, Oil &amp; Gas Industry. Aegis offers sevices such as sourcing of raw materials, tank storage and 
    terminalling, arranging transport through road, pipeline or ships and onsite management of materials at the customer's 
    site.</p>
    
    <p>Aegis believes in providing its customers in the oil, gas and chemical industry with end to end solutions, managing 
    every activity in the supply chain to enable reliable, continuous and competitive supplies of raw materials.</p>
    
    <p>Aegis is listed on the Bombay Stock Exchange and National Stock Exchange.</p>
    --%>
    
     <asp:Label ID="lblContent" runat="server" Text=""></asp:Label>  
    
    
    </div>       
     
    
    </div>
    
    <!--  Cont-right  -->

<div class="clear" style="height:40px;"></div>

<!--  Breadcrum  -->
        
    <div id="bread_box">
    <a href="index.aspx" onmouseover="home.src='images/home_ic_h.jpg'" onmouseout="home.src='images/home_ic.jpg'" style="padding:7px 16px 10px 8px;"><img src="images/home_ic.jpg" border="0" name="home" title="Home" alt="" runat="server" /></a>
    <a href="company_profile.aspx">About Us</a>
    <span style="margin-left:7px; margin-right:10px;">Company Profile</span>
    </div>
        
    <!--  Breadcrum  -->

<%--<div style="float:right; margin-top: 9px;"><a href="#top" onmouseover="tp.src='images/top_h.jpg'" onmouseout="tp.src='images/top.jpg'"><img src="images/top.jpg" border="0" name="tp" alt="" runat="server" /></a></div>--%>
</div>

</asp:Content>

