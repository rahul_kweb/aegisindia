﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Text;

/// <summary>
/// Summary description for Class1
/// </summary>
public class Class1
{
    public SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ConnectionString);

    public DataTable Display(string sql)
    {
        SqlDataAdapter da = new SqlDataAdapter(sql, con);
        DataTable dt = new DataTable();
        da.Fill(dt);
        return dt;
    }
    public bool IsNumeric(string strToCheck)
    {
        return Regex.IsMatch(strToCheck, "^\\d+(\\.\\d+)?$");
    }
    public void BindDropDownList(DropDownList ddl, string sql, string value, string text)
    {
        ddl.DataSource = Display(sql);
        ddl.DataTextField = text;
        ddl.DataValueField = value;
        ddl.DataBind();
    }
    public void BindPageContent(int Id, Label lblContent)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = Display("EXEC AddUpdategetCMS 'Get_By_Id'," + Id);
            lblContent.Text = dt.Rows[0]["Desc_vcr"].ToString();
        }
        catch (Exception ex)
        {

        }
    }
    public void BindPageTalent(int Id, Label lblContent)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = Display("EXEC AddUpdategetTalentMaster 'Get_By_Id'," + Id);
            lblContent.Text = dt.Rows[0]["Desc_vcr"].ToString();
        }
        catch (Exception ex)
        {

        }
    }
    public void BindInnerPageBanner(int Id, Image img)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = Display("EXEC AddUpdateGetInnerBannerMaster 'Get_By_Id',0,'',0,"+Id);
           
            if (dt.Rows.Count > 0)
            {
                string photo = dt.Rows[0]["Image_vcr"].ToString();
                img.Visible = true;
                img.ImageUrl = "~/Admin/images/" + photo;
            }
            else
            {
                img.Visible = false;
            }
        }
        catch (Exception ex)
        {

        }
    }
    public bool ShowHideAdminMenu(string Permission, Page pg, string Cntrl, string ToCheck)
    {
        bool valid = true;

        if ((Permission.Contains(ToCheck) || Permission.Contains("ADMIN")))
        {
            pg.Master.FindControl(Cntrl).Visible = true;

        }
        else
        {
            valid = false;
        }
        return valid;
    }

    public string SendEMail(string Subject, string Body, string toemail,string fromemailid)
    {
        string s = "";
        bool blnRetVal = false;
        int sent = 0;
        try
        {

           // SmtpClient mailClient = null;
           // MailMessage message = null;
           // mailClient = new SmtpClient();
           // message = new MailMessage();

           // mailClient.Host = "smtp.rediffmailpro.com";
           // mailClient.Port = 25;


           ////string strMailUserName = "smtpusr";
           // string strMailUserName = "smtpusr@aegisindia.com";
           // string strMailPassword = "01012005";

           // //mailClient.Host = "smtp.gmail.com";
           // //mailClient.Port = 587;

           // //string strMailUserName = "salestickets@horizondatasys.com";
           // //string strMailPassword = "salestickets123";


           // System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential(strMailUserName, strMailPassword);
           // mailClient.UseDefaultCredentials = false;
           // mailClient.Credentials = SMTPUserInfo;
           // mailClient.EnableSsl = false;
           // mailClient.DeliveryMethod = SmtpDeliveryMethod.Network;

           // string strFromMail = strMailUserName; //ConfigurationManager.AppSettings["FROM_ADDR"];
           // MailAddress fromAddress = new MailAddress(fromemailid);
           // message.From = fromAddress;
           // message.To.Add(toemail);
           // //message.Bcc.Add("saloni@kwebmaker.com");
           // message.Priority = System.Net.Mail.MailPriority.High;

           // message.Subject = Subject;


           // message.Body = Body;
           // message.IsBodyHtml = true;
           // mailClient.Send(message);
           // message = null;
           // mailClient = null;



            MailMessage message = new MailMessage();
            message.From = new MailAddress(fromemailid);
            message.To.Add(toemail);
            //message.Bcc.Add("saloni@kwebmaker.com");
            message.Subject = Subject;
            message.Body = Body;
            message.BodyEncoding = System.Text.Encoding.ASCII;
            message.IsBodyHtml = true;
            message.Priority = MailPriority.High;


          SmtpClient smtp = new SmtpClient("relay-hosting.secureserver.net", 25);

            //smtp.Send(message);
          //  SmtpClient smtp = new SmtpClient();
          //  smtp.Host = "smtp.rediffmailpro.com";
          //  smtp.Port = 110;
          //  smtp.UseDefaultCredentials = false;
            
          //    //smtp.Credentials = new System.Net.NetworkCredential("noreply@bossindia.com", "boss-456");
          //smtp.Credentials = new System.Net.NetworkCredential("smtpusr@aegisindia.com", "01012005");
           
          //  smtp.EnableSsl = true;
            smtp.Send(message);
            sent = 1;
            s = "";

        }
             
        catch (Exception ex)
        {
           sent = 0;
           s=ex.Message;
           
        }
      
        return s;
    }

    public bool fn_SendMail(string strTo, string strFrom, string strSubject, string strBody,Label lb)
    {
        bool blnRetVal = false;
        string strWebSiteURL = @"http://www.horizondatasys.com/";
        try
        {

            StringBuilder mailbody = new StringBuilder();
            SmtpClient mailClient = null;
            MailMessage message = null;
            mailClient = new SmtpClient();
            message = new MailMessage();
            mailClient.Host = "smtp.logix.in";
            mailClient.Port = 587;

            string strMailUserName = "aegis@aegisindia.com";
            string strMailPassword = "Aegis@1234";

            System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential(strMailUserName, strMailPassword);
            mailClient.UseDefaultCredentials = false;
            mailClient.Credentials = SMTPUserInfo;
            mailClient.EnableSsl = true;
            mailClient.DeliveryMethod = SmtpDeliveryMethod.Network;

            string strFromMail = strMailUserName; //ConfigurationManager.AppSettings["FROM_ADDR"];
            MailAddress fromAddress = new MailAddress(strFrom);
            message.From = fromAddress;

            MailAddress replyTo = new MailAddress(strFrom);
            message.ReplyTo = replyTo;

            message.To.Add(strTo);
            message.Subject = strSubject;

            mailbody.AppendFormat(strBody);
            message.Body = mailbody.ToString();
            message.IsBodyHtml = true;
            message.Priority = MailPriority.High;
            mailClient.Send(message);
            message = null;
            mailClient = null;

            blnRetVal = true;

        }
        catch (Exception ex)
        {
            lb.Text = ex.Message;
            blnRetVal = false;
        }
        catch
        {
            blnRetVal = false;
        }
        return blnRetVal;
    }
}
