﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class careers : System.Web.UI.Page
{
    Class1 c = new Class1();
    string FilePath = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GetJob();
            c.BindInnerPageBanner(42, ImgBanner);
        }
    }
    private void GetJob()
    {
        try
        {

            DataTable dt = new DataTable();
            dt = c.Display("EXEC AddUpdateJobMaster 'Get_For_Display'");
            dtlJob.DataSource = dt;
            dtlJob.DataBind();

            ddlJobType.DataSource = dt;
            ddlJobType.DataTextField = "Title_vcr";
            ddlJobType.DataValueField = "Job_Id_bint";
            ddlJobType.DataBind();

            ddlJobType.Items.Insert(0, "Applying For");
            int LastIndex = ddlJobType.Items.Count;
            ddlJobType.Items.Insert(LastIndex, "Other");

        }
        catch (Exception ex)
        {

        }
    }
    private string CheckBlank()
    {
        string blank = "";
        if (txtName.Text.Trim().Equals("Name") || txtName.Text.Trim().Equals(""))
        {
            blank = "Address,";
        }
        if (txtEmail.Text.Trim().Equals("Email")||txtEmail.Text.Trim().Equals(""))
        {
            blank += "Email Id,";
        }
        if (txtContact.Text.Trim().Equals("Contact no.") || txtContact.Text.Trim().Equals(""))
        {
            blank += "Contact,";
        }
        if (ddlJobType.SelectedIndex.Equals(0))
        {
            blank += "Applying For,";
        }
        else if(ddlJobType.SelectedItem.Text.Equals("Other"))
        {
            if (txtJobType.Text.Trim().Equals("Applying for") || txtJobType.Text.Trim().Equals(""))
            {
                blank += "Applying For,";
            }
        }

        if (!FileUpload1.HasFile)
        {
            blank += "Resume,";
        }
        if (blank != "")
        {
            blank = blank.Remove(blank.Length - 1);
        }
        return blank;
    }
    protected void btnApply_Click(object sender, EventArgs e)
    {
        string blank = CheckBlank();
        if (!blank.Equals(""))
        {
            lblMsg.Visible = true;
            lblMsg.Text = "Please Fill <br/> " + blank;
        }
        else
        {
            int a = AddUpdate();
            if (a.Equals(1))
            {
                Reset();
                lblMsg.Visible = true;
                lblMsg.Text = "Your Application has been sent.";
            }
        }
    }
    private int AddUpdate()
    {
        int a = 0;
        try
        {
            string ext = "";
            int Id = 0;
            using (SqlCommand cmd = new SqlCommand("AddUpdateResumeMaster", c.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para_vcr", "Add");
                cmd.Parameters.AddWithValue("@Name_vcr", txtName.Text.Trim());
                cmd.Parameters.AddWithValue("@Email_vcr", txtEmail.Text.Trim());
                cmd.Parameters.AddWithValue("@File_Name_vcr", "UploadedResume");
                if (divOther.Visible)
                {
                    cmd.Parameters.AddWithValue("@JOb_Title", txtJobType.Text.Trim());
                }
                else
                {
                    //cmd.Parameters.AddWithValue("@JOb_Title", ddlJobType.Text);
                    cmd.Parameters.AddWithValue("@JOb_Title", ddlJobType.SelectedItem.Text);

                }


                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                Id = int.Parse(dt.Rows[0][0].ToString());
                string fileName = dt.Rows[0][1].ToString();

                ext = fileName + SaveFile(fileName);

            }
            using (SqlCommand cmd1 = new SqlCommand("AddUpdateResumeMaster", c.con))
            {
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.AddWithValue("@Para_vcr", "UpdateResumeExtension");
                cmd1.Parameters.AddWithValue("@File_Name_vcr", ext);
                cmd1.Parameters.AddWithValue("@Resume_Id_bint", Id);
                if (c.con.State == ConnectionState.Closed)
                {
                    c.con.Open();
                }
                cmd1.ExecuteNonQuery();

            }
            string body = "Job Application For The Post Of " + ddlJobType.Text;
            // int Mail = c.SendEMail("Lilavati Hospital,Job Application", body, "Resume", FilePath);
            a = 1;
        }
        catch (Exception ex)
        {
            a = 0;
            lblMsg.Visible = true;
            lblMsg.Text = "Some error occurred,could not apply for the job.";
        }
        return a;
    }
    private void Reset()
    {
        txtName.Text = "Name";
        txtEmail.Text = "Email";
        lblMsg.Text = "";
        txtJobType.Text = "";
        divOther.Visible = false;
        txtContact.Text = "Contact no.";
        ddlJobType.SelectedIndex = -1;
        txtName.Focus();
    }
    private string SaveFile(string fl)
    {
        string extensionI1 = System.IO.Path.GetExtension(FileUpload1.FileName);

        if (extensionI1 != "")
        {
            FileUpload1.SaveAs(Server.MapPath("Admin\\Resume\\" + fl + extensionI1 + ""));
            FilePath = Server.MapPath("Admin\\Resume\\" + fl + extensionI1);
        }
        return extensionI1;

    }

    protected void ddlJobType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlJobType.SelectedItem.Text.Equals("Other"))
        {
            divOther.Visible = true;
            txtJobType.Focus();
        }
        else
        {
            divOther.Visible = false;
        }
    }
}
