﻿<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true"
    CodeFile="sealord_containers_limited.aspx.cs" Inherits="sealord_containers_limited"
    Title="Aegis Logistics Limited - Investor Relations - Sealord Containers Limited (Subsidiary)" %>

<%@ Register Src="investor_top.ascx" TagName="investor_top" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" type="text/javascript">
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".show").hide();
        });
    </script>
    <script type="text/javascript">
        function ShowRec() {
            $(".show").toggle('');
            $("#hide").css("background-image", 'url(./images/minus.png)');
            $("#hide").attr("onclick", "newShowRec()");
        }
    </script>
    <script type="text/javascript">
        function newShowRec() {
            $(".show").toggle('');
            $("#hide").css("background-image", 'url(./images/plus.png)');
            $("#hide").attr("onclick", "ShowRec()");
        }
    </script>
    <%--<!--  scroll to top  -->

<script src="js/jquery.localscroll-1.2.6-min.js" type="text/javascript"></script>
<script src="js/jquery.scrollTo-1.4.0-min.js" type="text/javascript"></script>
<script>

$(document).ready(function () {
	$.localScroll();	
});

</script>

<!--  scroll to top  -->--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:investor_top ID="investor_top1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div id="main_container" style="border-top: 1px solid #e4e4e4; padding-top: 20px;">
        <%--<div id="banner"><img src="images/banner/liquid_logistic.jpg" border="0" alt="Liquid Logistics" title="Liquid logistics" /></div>--%>
        <div class="clear">
        </div>
        <!--  Cont-left  -->
        <div id="cont_left">
            <div id="pg_head">
                Investor Relations</div>
            <div id="rel_box_link">
                <ul>
                    <li><a href="Corporate_Governances.aspx">Corporate Governance</a></li>
                    <li><a href="key_financial_data.aspx">Financial Information</a></li>
                    <%--<li><a href="key_financial_data.aspx">Key Financial Data</a> </li>--%>
                    <li><a href="shares.aspx">Shares</a></li>
                    <li><a href="Debentures.aspx">Debentures</a></li>
                    <li><a href="reports_filings.aspx">Reports &amp; Filings</a></li>
                    <li><a href="events_presentations.aspx">Presentations</a></li>
                    <%--<li><a href="acquisitions.aspx">Acquisitions</a></li>
    <li><a href="corporate_governance.aspx">Corporate Governance</a></li>
    <li><a href="investor_contact.aspx">Investor Contact</a></li>--%>
                    <li><a href="quarterly_results.aspx">Financial Results</a></li>
                    <li><a href="news.aspx">News &amp; Events</a></li>
                    <%--<li><a href="live_share_info.aspx">Live Share Info</a></li>--%>
                    <li><a href="unclaimedamounts.aspx">Unpaid & Unclaimed Amounts</a></li>
                    <li class="selec">Sea Lord Containers Limited (Subsidiary)</li>
                    <li><a href="Aegis_Gas_Lpg_Pvt_Ltd.aspx">Aegis Gas (LPG) Pvt. Ltd. (Subsidiary)</a></li>
                </ul>
            </div>
        </div>
        <!--  Cont-left  -->
        <div id="gray_line" style="height: 525px;">
        </div>
        <!--  Cont-right Admin/Documents/Quarterly_Results_2006-07_Pdf1.pdf" -->
        <div id="cont_right">
            <div id="cont_right_box">
                <div id="accordion" style="width: 685px;">
                    <!--start toggle-->
                    <div class="element atStart">
                        <ul class="list_in">
                        <li><a href="Admin/Documents/Postal_Ballot_Results.pdf" target="_blank" style="color: #4B4B50;">
                                Postal Ballot Results</a></li>
                            <li><a href="Admin/Documents/SCL_Postal_Ballot_Notice.pdf" target="_blank" style="color: #4B4B50;">
                                Postal Ballot Notice</a></li>
                            <li><a href="Admin/Documents/SCL_Postal_Ballot_Form.pdf" target="_blank" style="color: #4B4B50;">
                                Postal Ballot Form</a></li>
                            <li><a href="Admin/Documents/Sea_Lord_Containers_Ltd_Annual_Report_2014_15.pdf" target="_blank"
                                style="color: #4B4B50;">Sea Lord Annual Report 2014-15</a></li>
                            <%--<li><a href="Admin/Documents/AGPL_2014_15_new.pdf" target="_blank"
                                style="color: #4B4B50;">Sea Lord Annual Report 2014-15</a></li>--%>                            
                        </ul>
                    </div>
                    <div class="element atStart">
                        <ul class="list_in">
                            <%--<li><a href="Admin/Documents/Postal Ballot Result.pdf">Postal Ballot Result</a></li>--%>
                            <li id="hide" onclick="javascript:return ShowRec();"><span style="cursor: pointer;">
                                Sea Lord Annual Report 2013-14</span>
                                <ul>
                                    <li><a href="Admin/Documents/Sea%20Lord_Annual%20Report%202013-14.pdf" target="_blank">
                                        Sea Lord Annual Report 2013-14</a></li>
                                    <%--<li><a href="Admin/Documents/Sea%20Lord_E-Voting%20Communicaton.pdf" target="_blank">
                                        Sea Lord E-Voting Communicaton</a></li>--%>
                                    <%--<li><a href="Admin/Documents/Outcome%20of%20voting%20at%2034th%20Annual%20General%20Meeting.pdf">Outcome of voting at 34th Annual General Meeting</a></li>--%>
                                </ul>
                            </li>
                            <%-- <li><a href="Admin/Documents/Sealord_Ltd172.pdf">Postal Ballot Form (Specimen)</a></li>
                            <li><a href="Admin/Documents/Sealord_Ltd170.pdf">Postal Ballot Notice dated 29.05.2014</a></li>--%>
                        </ul>
                    </div>
                    <!-- second div start from here -->
                    <div class="element atStart">
                        <ul class="list_in">
                            <%--<li><a href="Admin/Documents/Postal Ballot Result.pdf">Postal Ballot Result</a></li>--%>
                            <li id="Li1" onclick="javascript:return ShowRec();"><span style="cursor: pointer;">Sea
                                Lord_Corporate Governance</span>
                                <ul>
                                    <li><a href="Admin/Documents/Code_of_Conduct_(For_20Directors_20&_20Officials)_2001-04-2015.pdf"
                                        target="_blank">Sea Lord_Code of Conduct</a></li>
                                    <li><a href="Admin/Documents/Corporate_Social_Responsibility_Policy-30-01-2015.pdf"
                                        target="_blank">Sea Lord_Corporate Social responsibility Policy</a></li>
                                    <li><a href="Admin/Documents/Policy_for_Determining_Material_Subsidiaries-30-01-2015.pdf"
                                        target="_blank">Sea Lord_Policy for determining Material Subsidiaries</a></li>
                                    <li><a href="Admin/Documents/Policy_on_Related_Party_Transactions-30-01-2015.pdf"
                                        target="_blank">Sea Lord_Policy on Related Party transactions</a></li>
                                    <li><a href="Admin/Documents/Terms_&_conditions_of_appointment_of_Independent_Director.pdf"
                                        target="_blank">Sea Lord_Terms & conditions of appointment of Independent Directors</a></li>
                                    <li><a href="Admin/Documents/Vigil_Mechanism_Policy-30-01-2015.pdf" target="_blank">
                                        Sea Lord_Vigil Mechanism Policy</a></li>
                                </ul>
                            </li>
                            <%-- <li><a href="Admin/Documents/Sealord_Ltd172.pdf">Postal Ballot Form (Specimen)</a></li>
                            <li><a href="Admin/Documents/Sealord_Ltd170.pdf">Postal Ballot Notice dated 29.05.2014</a></li>--%>
                        </ul>
                    </div>
                    <div class="element atStart">
                        <ul class="list_in">
                            <%--<li><a href="Admin/Documents/Postal Ballot Result.pdf">Postal Ballot Result</a></li>--%>
                            <li id="Li2" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a
                                href="Admin/Documents/Sea_Lord-Code_of_Fair_Disclosures_of_UPSI_15.05.2015.pdf"
                                target="_blank">Sea Lord_Code of Fair Disclosures of UPSI</a></span>
                                <%--<ul>
                                    <li><a href="Admin/Documents/Sea_Lord-Code_of_Fair_Disclosures_of_UPSI_15.05.2015.pdf"
                                        target="_blank">Sea Lord- Code of Fair Disclosures of UPSI</a></li>
                                </ul>--%>
                            </li>
                            <%-- <li><a href="Admin/Documents/Sealord_Ltd172.pdf">Postal Ballot Form (Specimen)</a></li>
                            <li><a href="Admin/Documents/Sealord_Ltd170.pdf">Postal Ballot Notice dated 29.05.2014</a></li>--%>
                        </ul>
                    </div>
                    <%--    <ul class="list">
        <asp:DataList ID="dtlPdf" runat="server">
        <ItemTemplate>
        
         <li><a href='<%# string.Format("Admin/Documents/{0}.pdf",Eval("Pdf_Page_Path_vcr")) %>' target="_blank"><%# Eval("Pdf_Title_vcr")%> </a></li>

        </ItemTemplate>
        
   <%-- <li><a href="pdf/Key_Financial_Data/Financial_highlights_ANNUAL_REPORT_2011-12.pdf" target="_blank">Financial Highlights of Company &amp; Group </a></li>
    
    <li><a href="pdf/Key_Financial_Data/Balance_sheet_ANNUAL_REPORT_2011-12.pdf" target="_blank">Balance Sheet</a></a></li>
    
    <li><a href="pdf/Key_Financial_Data/Segmental_Results_New.pdf" target="_blank">Segmental Results</a></a></li>--%>
                </div>
            </div>
        </div>
        <!--  Cont-right  -->
        <div class="clear" style="height: 40px;">
        </div>
        <!--  Breadcrum  -->
        <div id="bread_box">
            <a href="index.aspx" onmouseover="home.src='images/home_ic_h.jpg'" onmouseout="home.src='images/home_ic.jpg'"
                style="padding: 7px 16px 10px 8px;">
                <img src="images/home_ic.jpg" border="0" name="home" title="Home" alt="" /></a>
            <a href="#">Investor Relations</a> <span style="margin-left: 7px; margin-right: 10px;">
                Sealord Containers Limited (Subsidiary)</span>
        </div>
        <!--  Breadcrum  -->
        <%--<div style="float:right; margin-top: 9px;"><a href="#top" onmouseover="tp.src='images/top_h.jpg'" onmouseout="tp.src='images/top.jpg'"><img src="images/top.jpg" border="0" name="tp" alt="" /></a></div>--%>
    </div>
</asp:Content>
