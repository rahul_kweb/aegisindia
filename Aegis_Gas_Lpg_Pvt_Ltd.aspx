﻿<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true"
    CodeFile="Aegis_Gas_Lpg_Pvt_Ltd.aspx.cs" Inherits="Aegis_Gas_Lpg_Pvt_Ltd" Title="Aegis Logistics Limited - Investor Relations - Aegis Gas (LPG) Pvt. Ltd. (Subsidiary)" %>

<%@ Register Src="investor_top.ascx" TagName="investor_top" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" type="text/javascript">
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".show").hide();
        });
    </script>
    <script type="text/javascript">
        function ShowRec() {
            $(".show").toggle('');
            $("#hide").css("background-image", 'url(./images/minus.png)');
            $("#hide").attr("onclick", "newShowRec()");
        }
    </script>
    <script type="text/javascript">
        function newShowRec() {
            $(".show").toggle('');
            $("#hide").css("background-image", 'url(./images/plus.png)');
            $("#hide").attr("onclick", "ShowRec()");
        }
    </script>
    <%--<!--  scroll to top  -->

<script src="js/jquery.localscroll-1.2.6-min.js" type="text/javascript"></script>
<script src="js/jquery.scrollTo-1.4.0-min.js" type="text/javascript"></script>
<script>

$(document).ready(function () {
	$.localScroll();	
});

</script>

<!--  scroll to top  -->--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:investor_top ID="investor_top1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div id="main_container" style="border-top: 1px solid #e4e4e4; padding-top: 20px;">
        <%--<div id="banner"><img src="images/banner/liquid_logistic.jpg" border="0" alt="Liquid Logistics" title="Liquid logistics" /></div>--%>
        <div class="clear">
        </div>
        <!--  Cont-left  -->
        <div id="cont_left">
            <div id="pg_head">
                Investor Relations
            </div>
            <div id="rel_box_link">
                <ul>
                    <li><a href="Corporate_Governances.aspx">Corporate Governance</a></li>
                    <li><a href="key_financial_data.aspx">Financial Information</a></li>
                    <%--<li><a href="key_financial_data.aspx">Key Financial Data</a> </li>--%>
                    <li><a href="shares.aspx">Stock Information</a></li>
                    <li><a href="Dividend.aspx">Dividend</a></li>
                    <%--<li><a href="Debentures.aspx">Debentures</a></li>--%>
                    <li><a href="reports_filings.aspx">Stock Exchange Communication</a></li>
                    <li><a href="events_presentations.aspx">Investor Presentations</a></li>
                    <%--<li><a href="acquisitions.aspx">Acquisitions</a></li>
    <li><a href="corporate_governance.aspx">Corporate Governance</a></li>
    <li><a href="investor_contact.aspx">Investor Contact</a></li>--%>
                    <%--<li><a href="quarterly_results.aspx">Financial Results</a></li>--%>
                    <li><a href="news.aspx">News and Events</a></li>
                    <li><a href="Investor_Downloads.aspx">Investor Downloads</a></li>
                    <li><a href="InvestorContacts.aspx">Investor Contacts</a></li>
                    <%--<li><a href="live_share_info.aspx">Live Share Info</a></li>--%>
                    <%--<li><a href="unclaimedamounts.aspx">Unpaid & Unclaimed Amounts</a></li>--%>
                    <%--<li><a href="sealord_containers_limited.aspx">Sea Lord Containers Limited (Subsidiary)</a></li>--%>
                    <li class="selec">Aegis Gas (LPG) Pvt. Ltd. (Subsidiary)</li>
                </ul>
            </div>
        </div>
        <!--  Cont-left  -->
        <div id="gray_line" style="height: 525px;">
        </div>
        <!--  Cont-right Admin/Documents/Quarterly_Results_2006-07_Pdf1.pdf" -->
        <div id="cont_right">
            <div id="cont_right_box">
                <div id="accordion" style="width: 685px;">
                    <!--start toggle-->
                    <%--<div class="element atStart">
                        <ul class="list_in">
                            <li id="hide" onclick="javascript:return ShowRec();"><span style="cursor: pointer;">
                                Sea Lord_Annual Report 2013-14</span>
                                <ul>
                                    <li><a href="Admin/Documents/Sea%20Lord_Annual%20Report%202013-14.pdf">Sea Lord_Annual
                                        Report 2013-14</a></li>
                                    <li><a href="Admin/Documents/Sea%20Lord_E-Voting%20Communicaton.pdf">Sea Lord_E-Voting
                                        Communicaton</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>--%>
                    <!-- second div start from here -->
                    <%--<div class="element atStart">
                        <ul class="list_in">
                            <li id="Li1" onclick="javascript:return ShowRec();"><span style="cursor: pointer;">Sea
                                Lord_Corporate Governance</span>
                                <ul>
                                    <li><a href="Admin/Documents/Code_of_Conduct_(For Directors & Officials)- 01-04-2015.pdf"
                                        target="_blank">Sea Lord_Code of Conduct</a></li>
                                    <li><a href="Admin/Documents/Corporate_Social_Responsibility_Policy-30-01-2015.pdf"
                                        target="_blank">Sea Lord_Corporate Social responsibility Policy</a></li>
                                    <li><a href="Admin/Documents/Policy_for_Determining_Material_Subsidiaries-30-01-2015.pdf"
                                        target="_blank">Sea Lord_Policy for determining Material Subsidiaries</a></li>
                                    <li><a href="Admin/Documents/Policy_on_Related_Party_Transactions-30-01-2015.pdf"
                                        target="_blank">Sea Lord_Policy on Related Party transactions</a></li>
                                    <li><a href="Admin/Documents/Terms_&_conditions_of_appointment_of_Independent_Director.pdf"
                                        target="_blank">Sea Lord_Terms & conditions of appointment of Independent Directors</a></li>
                                    <li><a href="Admin/Documents/Vigil_Mechanism_Policy-30-01-2015.pdf" target="_blank">
                                        Sea Lord_Vigil Mechanism Policy</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>--%>
                    <div class="element atStart">


                         <ul class="list_in">
                            <li id="Li9" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a
                                href="Admin/Documents/AGPL_Unaudited_Results_11-12-2017.pdf" target="_blank">Unaudited
Results Q2 FY 2017-18.</a></span>
                            </li>
                        </ul>

                        <ul class="list_in">
                            <li id="Li8" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a
                                href="Admin/Documents/AGPL-2016-17-301017.pdf" target="_blank">Annual Report 2016-17.</a></span>
                            </li>
                        </ul>
                        <%--<ul class="list_in">
                            <li id="Li8" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a
                                href="Admin/Documents/Audited_Accounts_(Standalone)_2016-17.pdf" target="_blank">Audited Accounts (Standalone) 2016-17.</a></span>
                            </li>
                        </ul>
                        <ul class="list_in">
                            <li id="Li9" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a
                                href="Admin/Documents/Audited_Accounts_(Consolidated)_2016-17.pdf" target="_blank">Audited Accounts (Consolidated) 2016-17.</a></span>
                            </li>
                        </ul>--%>
                        <ul class="list_in">
                            <li id="Li7" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a
                                href="Admin/Documents/AGPL-Audited_Results_2017_050617.pdf" target="_blank">Audited Financial Results 2017.</a></span>
                            </li>
                        </ul>

                        <ul class="list_in">
                            <li id="Li6" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a
                                href="Admin/Documents/Unaudited_Results_Q2_FY2016-17.pdf" target="_blank">Unaudited Results Q2 FY 2016-17.</a></span>
                            </li>
                        </ul>

                        <ul class="list_in">
                            <li id="Li5" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a
                                href="Admin/Documents/AGPL_2015_16_FINAL_BALSHEET_New.pdf" target="_blank">Annual Report 2015-16.</a></span>
                            </li>
                        </ul>

                        <ul class="list_in">
                            <li id="Li1" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a
                                href="Admin/Documents/AGPL_Result_31.03.2016_.pdf" target="_blank">Audited Financial results 2016.</a></span>
                            </li>
                        </ul>

                        <ul class="list_in">
                            <li id="Li3" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a
                                href="Admin/Documents/AGPL_clause41sept.pdf" target="_blank">Unaudited Results Q2 FY 2015-16.</a></span>
                            </li>
                        </ul>
                        <ul class="list_in">
                            <%--<li id="Li1" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a
                                href="Admin/Documents/AGPL_2014_15.pdf" target="_blank">Annual Report 2014-15</a></span>
                            </li>--%>
                            <li id="Li4" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a
                                href="Admin/Documents/AGPL_2014_15_new.pdf" target="_blank">Annual Report 2014-15.</a></span>
                            </li>
                            <%--<li><a href="Admin/Documents/AGPL_2014_15_new.pdf" target="_blank"
                                style="color: #4B4B50;">Sea Lord Annual Report 2014-15</a></li>--%>
                        </ul>
                        <ul class="list_in">
                            <li id="Li2" onclick="javascript:return ShowRec();"><span style="cursor: pointer;"><a
                                href="Admin/Documents/AGPL-Code_of_Fair_Disclosures_of_UPSI_15-05-2015.pdf" target="_blank">Code for Practice and Procedures for Fair Disclosure of UPSI.</a></span> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--  Cont-right  -->
        <div class="clear" style="height: 40px;">
        </div>
        <!--  Breadcrum  -->
        <div id="bread_box">
            <a href="index.aspx" onmouseover="home.src='images/home_ic_h.jpg'" onmouseout="home.src='images/home_ic.jpg'"
                style="padding: 7px 16px 10px 8px;">
                <img src="images/home_ic.jpg" border="0" name="home" title="Home" alt="" /></a>
            <a href="#">Investor Relations</a> <span style="margin-left: 7px; margin-right: 10px;">Aegis Gas (LPG) Pvt. Ltd. (Subsidiary)</span>
        </div>
        <!--  Breadcrum  -->
        <%--<div style="float:right; margin-top: 9px;"><a href="#top" onmouseover="tp.src='images/top_h.jpg'" onmouseout="tp.src='images/top.jpg'"><img src="images/top.jpg" border="0" name="tp" alt="" /></a></div>--%>
    </div>
</asp:Content>
