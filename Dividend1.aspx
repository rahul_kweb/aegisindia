﻿<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true"
    CodeFile="Dividend.aspx.cs" Inherits="key_financial_data" Title="Aegis Logistics Limited - Investor Relations - Divident" %>

<%@ Register Src="investor_top.ascx" TagName="investor_top" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function ShowRec() {
            $(".show").toggle('');
            $("#hide").css("background-image", 'url(./images/minus.png)');
            $("#hide").attr("onclick", "newShowRec()");
        }
    </script>
    <script type="text/javascript">
        function newShowRec() {
            $(".show").toggle('');
            $("#hide").css("background-image", 'url(./images/plus.png)');
            $("#hide").attr("onclick", "ShowRec()");
        }

        jQuery(document).ready(function () {
            jQuery('.customeToggleContent').eq(0).show();


            jQuery('.customeToggle').click(function (event) {
                jQuery('.customeToggleContent').slideUp();
                jQuery(this).next('.customeToggleContent').slideDown();
                jQuery('.customeToggleContent1').slideUp();
                jQuery('.customeToggleContent1').eq(0).slideDown();
            })

            jQuery('.customeToggleContent1').eq(0).show();

            jQuery('.customeToggle1').click(function (event) {
                jQuery('.customeToggleContent1').slideUp();
                jQuery(this).next('.customeToggleContent1').slideDown();
            })
        })

    </script>


    <script type="text/javascript" src="js/mootools.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="css/screen.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:investor_top ID="investor_top1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div id="main_container" style="border-top: 1px solid #e4e4e4; padding-top: 20px;">
        <%--<div id="banner"><img src="images/banner/liquid_logistic.jpg" border="0" alt="Liquid Logistics" title="Liquid logistics" /></div>--%>
        <div class="clear">
        </div>
        <!--  Cont-left  -->
        <div id="cont_left">
            <div id="pg_head">
                Investor Relations
            </div>
            <div id="rel_box_link">
                <ul>
                    <li><a href="Corporate_Governances.aspx">Corporate Governance</a></li>
                    <li><a href="key_financial_data.aspx">Financial Information</a></li>
                    <li><a href="shares.aspx">Stock Information</a></li>
                    <li class="selec">Dividend</li>
                    <%--<li><a href="Debentures.aspx">Debentures</a></li>--%>
                    <li><a href="reports_filings.aspx">Stock Exchange Communication</a></li>
                    <li><a href="events_presentations.aspx">Investor Presentations</a></li>
                    <li><a href="news.aspx">News and Events</a></li>
                    <li><a href="Investor_Downloads.aspx">Investor Downloads</a></li>
                    <li><a href="InvestorContacts.aspx">Investor Contacts</a></li>
                    <%-- <li><a href="unclaimedamounts.aspx">Unpaid & Unclaimed Amounts</a></li>--%>
                    <li><a href="Aegis_Gas_Lpg_Pvt_Ltd.aspx">Aegis Gas (LPG) Pvt. Ltd. (Subsidiary)</a></li>
                </ul>
            </div>
        </div>
        <!--  Cont-left  -->
        <div id="gray_line" style="height: 525px;">
        </div>
        <!--  Cont-right Admin/Documents/Quarterly_Results_2006-07_Pdf1.pdf" -->
        <div id="cont_right">
            <div id="cont_right_box">
                <div id="accordion" style="width: 685px;">

                    <h3 class="customeToggle">Share Transfer to IEPF</h3>
                    <div class="customeToggleContent">
                        <ul class="list_in">
                            <li><a href="Admin/Documents/Divident/IEPF_List_of_Shareholders_31-05-2017.pdf" target="_blank">IEPF – List of Shareholders – 31-05-2017</a></li>
                        </ul>
                    </div>

                    <h3 class="customeToggle">History</h3>
                    <div class="customeToggleContent">
                        <ul class="list_in">
                            <li><a href="Admin/Documents/Divident/DividendHistory130717.pdf" target="_blank">Dividend History</a></li>
                        </ul>
                    </div>

                    <h3 class="customeToggle">Notice</h3>
                    <div class="customeToggleContent">
                        <ul class="list_in">
                            <asp:DataList ID="dtlDividendPdf" runat="server">
                                <ItemTemplate>
                                    <li><a href='<%# string.Format("Admin/Documents/{0}.pdf",Eval("Pdf_Page_Path_vcr")) %>'
                                        target="_blank">
                                        <%# Eval("Pdf_Title_vcr")%>
                                    </a></li>
                                </ItemTemplate>
                            </asp:DataList>
                        </ul>
                    </div>

                    <h3 class="customeToggle">Unclaimed and Unpaid Amounts</h3>
                    <div class="customeToggleContent">
                        <ul class="list_in">
                            <li><a href="Admin/documents/Divident/Dividend-29-11-17.pdf" target="_blank">Unclaimed Dividend from 2009-10 (Final) to 2016-17 (2nd Interim) – Status as on 10-08-2017</a></li>
                            <li><a href="Admin/documents/Divident/Bonus Fraction._22_11_17.pdf" target="_blank">Unclaimed Bonus Fraction Amount – Status as on 10-08-2017</a></li>
                            <li><a href="Admin/documents/Divident/FD-29-11-17.pdf" target="_blank">Unclaimed Interest on matured Deposits and unclaimed amount of matured Deposits – Status as on 10-08-2017</a></li>

                            <!-- 29-11-2017 -->
                            <%--   <li><a href="Admin/documents/Divident/Dividend22_11_17.pdf" target="_blank">Unclaimed Dividend from 2009-10 (Interim) to 2015-16 (3rd Interim) – Status as on 05-08-2016</a></li>
                            <li><a href="Admin/documents/Divident/Bonus Fraction._22_11_17.pdf" target="_blank">Unclaimed Bonus Fraction Amount – Status as on 05-08-2016</a></li>
                            <li><a href="Admin/documents/Divident/FD.22_11_17.pdf" target="_blank">Unclaimed interest on matured Deposits and unclaimed amount of matured Deposits – Status as on 05-08-2016</a></li>
                            --%>
                            <%-- <li><a href="Admin/unclaimedamounts/Unclaimed_Dividend_from_2009-10(Interim)-to-2015-16-(3rd Interim)-As-on-5-08-2016.pdf" target="_blank">Unclaimed Dividend from 2009-10 (Interim) to 2015-16 (3rd Interim) – Status as on 05-08-2016</a></li>--%>
                            <%-- <li><a href="Admin/unclaimedamounts/Unclaimed_Bonus_Fraction_Amount-As-on-05-08-2016.pdf" target="_blank">Unclaimed Bonus Fraction Amount – Status as on 05-08-2016</a></li>--%>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
        <!--  Cont-right  -->
        <div class="clear" style="height: 40px;">
        </div>
        <!--  Breadcrum  -->
        <div id="bread_box">
            <a href="index.aspx" onmouseover="home.src='images/home_ic_h.jpg'" onmouseout="home.src='images/home_ic.jpg'"
                style="padding: 7px 16px 10px 8px;">
                <img src="images/home_ic.jpg" border="0" name="home" title="Home" alt="" /></a>
            <a href="#">Investor Relations</a> <span style="margin-left: 7px; margin-right: 10px;">Dividend</span>
        </div>
        <!--  Breadcrum  -->
        <%--<div style="float:right; margin-top: 9px;"><a href="#top" onmouseover="tp.src='images/top_h.jpg'" onmouseout="tp.src='images/top.jpg'"><img src="images/top.jpg" border="0" name="tp" alt="" /></a></div>--%>
    </div>

    <style>
        .customeToggle, .customeToggle1 {
            margin-bottom: 5px !important;
        }

        .customeToggleContent, .customeToggleContent1 {
            display: none;
        }
    </style>
</asp:Content>
