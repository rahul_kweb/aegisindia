﻿<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true"
    CodeFile="retail_lpg.aspx.cs" Inherits="retail_lpg" Title="Aegis Logistics Limited - Business Mix - Retail LPG" %>

<%@ Register Src="business_top.ascx" TagName="business_top" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!--  tabs  -->
    <script type="text/javascript" src="js/tabber.js"></script>
    <link rel="stylesheet" href="css/example.css" type="text/css" media="screen" />
    <script type="text/javascript">

        /* Optional: Temporarily hide the "tabber" class so it does not "flash"
        on the page as plain HTML. After tabber runs, the class is changed
        to "tabberlive" and it will appear. */

        document.write('<style type="text/css">.tabber{display:none;}<\/style>');
    </script>
    <!--  tabs  -->
    <%--<!--  scroll to top  -->

<script src="js/jquery.localscroll-1.2.6-min.js" type="text/javascript"></script>
<script src="js/jquery.scrollTo-1.4.0-min.js" type="text/javascript"></script>
<script>

$(document).ready(function () {
	$.localScroll();	
});

</script>

<!--  scroll to top  -->--%>
    <!-- Slider -->
    <link rel="stylesheet" href="css/global.css" />
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
    <script type="text/javascript" src="js/slides.min.jquery.js"></script>
    <script>
        $(function () {
            $('#slides').slides({
                preload: true,
                preloadImage: 'img/loading.gif',
                play: 3000,
                pause: 3000,
                hoverPause: true,
                animationStart: function (current) {
                    $('.caption').animate({
                        bottom: -35
                    }, 100);
                    if (window.console && console.log) {
                        // example return of current slide number
                        console.log('animationStart on slide: ', current);
                    };
                },
                animationComplete: function (current) {
                    $('.caption').animate({
                        bottom: 0
                    }, 2000);
                    if (window.console && console.log) {
                        // example return of current slide number
                        console.log('animationComplete on slide: ', current);
                    };
                },
                slidesLoaded: function () {
                    $('.caption').animate({
                        bottom: 0
                    }, 2000);
                }
            });
        });
    </script>
    <!-- Slider -->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:business_top ID="business_top1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div id="main_container">
        <div id="banner">
            <div id="example">
                <div id="slides">
                    <div class="slides_container">
                        <div class="slide">
                            <a href="#" title="">
                                <img id="Img3" src="images/slider/retail_lpg3.jpg" border="0" alt="" runat="server" /></a>
                        </div>
                        <div class="slide">
                            <a href="#" title="">
                                <img id="Img2" src="images/slider/retail_lpg2.jpg" border="0" alt="" runat="server" /></a>
                        </div>
                        <div class="slide">
                            <a href="#" title="">
                                <img id="Img1" src="images/slider/retail_lpg1.jpg" border="0" alt="" runat="server" /></a>
                        </div>
                        <div class="slide">
                            <a href="#" title="">
                                <img id="Img4" src="images/slider/retail_lpg4.jpg" border="0" alt="" runat="server" /></a>
                        </div>
                    </div>
                    <a href="#" class="prev">
                        <img src="images/slider/arrow_l.png" width="50" height="50" border="0" alt="Arrow Prev" /></a>
                    <a href="#" class="next">
                        <img src="images/slider/arrow_r.png" width="50" height="50" border="0" alt="Arrow Next" /></a>
                </div>
            </div>
            <%--	<img src="images/banner/retail_LPG.jpg" border="0" alt="Retail LPG" title="Retail LPG" runat="server" />--%>
            <%--<asp:Image runat="server" Visible="false" border="0" ID="ImgBanner" alt="Retail LPG"
                title="Retail LPG" ImageUrl='<%# string.Format("~/Admin/images/{0}",Eval("Image_vcr")) %>' />--%>
        </div>
        <div class="clear">
        </div>
        <!--  Cont-left  -->
        <div id="cont_left">
            <div id="pg_head">
                Business Mix</div>
            <div id="rel_box" class="rel_box liquid">
                <a href="liquid_logistics.aspx">Liquid Logistics</a>
            </div>
            <div id="rel_box" class="rel_box gas">
                <a href="gas_logistics.aspx">Gas Logistics</a>
            </div>
            <div id="rel_box" class="rel_box epc">
                <a href="epc_services.aspx">EPC Services</a>
            </div>
            <div id="rel_box" class="rel_box retail">
                <%--<a href="retail_lpg.aspx" class="active">Retail LPG</a>--%>
                <a href="http://www.aegisretail.in" class="active" target="_blank">Retail LPG</a>
            </div>
            <div id="rel_box" class="rel_box marine">
                <a href="marine.aspx">Marine</a>
            </div>
        </div>
        <!--  Cont-left  -->
        <div id="gray_line" style="height: 400px;">
        </div>
        <!--  Cont-right  -->
        <div id="cont_right">
            <%-- <div class="tabber">--%>
            <asp:Label ID="lblContent" runat="server" Text=""></asp:Label>
            <%--  <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
    
         <div class="tabbertab">
          <h2>Overview</h2>
          
          <p>Liquified Petroleum Gas(LPG) is an excellent energy source because of its availability,
portability, environmental benefits and versatility. Aegis is an important participant in two
segments of the Indian retail LPG market: Transportation and Commercial Cylinders.</p>

<p>Auto LPG has emerged as an alternative fuel to petrol over the past 10 years in India
since the liberalization of this application by the Government of India in 2001. Aegis
opened its first Auto LPG station in 2005 and has expanded to a network of 84 stations
all over western and southern India under the “Aegis Autogas” brand. These stations
now supply thousands of cars, autorickshaws and motorcycles with a high quality,
economical and environmentally friendly alternative to petrol.</p>

<p>Aegis also markets LPG packed in cylinders which are widely used for domestic,
commercial and industrial applications due to their handling convenience and cleanliness. Aegis operates an extensive 
network of filling plants, distribution points and
dealers under “Aegis Puregas” brand.</p>          
          
          <div class="clear"></div>
         </div>
          
       <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
    
         <div class="tabbertab">
          <h2>Auto LPG Retailing</h2>
          
          <p>With high oil prices and supply concerns globally, the world is exploring the use of alternate clean fuels 
          instead of petrol. In India, CNG and Auto LPG are the two gas-based alternatives to petrol. Worldwide, Auto LPG 
          is generally preferred to CNG because Auto LPG vehicles can run three times the distance on a full tank, the 
          conversion kits cost 50% less, require less maintenance and are safer because of lower tank pressure.</p>
          
          <p>Aegis has taken a pioneering role in the development of an extensive retail network of Auto LPG stations in 
          India under the brand name Aegis Autogas. Currently the network is spread over nine states (Maharashtra, Goa, 
          Gujarat, Andhra Pradesh, Karnataka, Tamilnadu, Kerala, Rajasthan and Madhya Pradesh).</p>
          
          <div style="text-align: center;">
          <img src="images/retail_lpg/gl_img2.jpg" title="Auto LPG Retailing" alt="" runat="server" class="img_bdr" style="margin:0px 30px 20px 0px;" />
          <img src="images/retail_lpg/gl_img3.jpg" title="Auto LPG Retailing" alt="" runat="server" class="img_bdr" style="margin:0px 0px 20px 0px;" />
          <img src="images/retail_lpg/gl_img4.jpg" title="Auto LPG Retailing" alt="" runat="server" class="img_bdr" style="margin:0px 30px 20px 0px;" />
          <img src="images/retail_lpg/gl_img5.jpg" title="Auto LPG Retailing" alt="" runat="server" class="img_bdr" style="margin:0px 0px 20px 0px;" />
          </div>          
          
          <div class="clear"></div>
         </div>
        
        <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
    
    
         <div class="tabbertab">
          <h2>Packed LPG Retailing</h2>
          
          <p>Aegis supplies LPG in cylinders for industrial, commercial and domestic requirements as well, under the brand 
          name 'Aegis Puregas'. Aegis has wide LPG cylinder distribution network comprising of LPG cylinder filling plants, 
          distributors and dealers and assures its customers right quality and quantity at the right time and helps 
          customers to reduce operating costs.</p>
          
          <ul class="list">
          <li>Packed LPG S &amp; M to Industrial, Commercial and Domestic sectors under the brand name “Aegis PureGas”</li>
          <li>Bottling carried out at self owned facility at Kheda, Gujarat and through various other contracted sites</li>
          <li>Present throughput of around 1,000 MT per Month through various sites</li>
          <li>Currently handling of 12, 15 &amp; 21 kg capacity cylinders</li>
          <li>Distribution in Maharashtra, Gujarat and Karnataka</li>
          </ul>
          
          <div style="text-align: center;">
          <img src="images/retail_lpg/lpg_1.jpg" title="Packed LPG Retailing" alt="" class="img_bdr" style="margin:0px 30px 20px 0px;" />
          <img src="images/retail_lpg/lpg_2.jpg" title="Packed LPG Retailing" alt="" class="img_bdr" style="margin:0px 0px 20px 0px;" />
          <img src="images/retail_lpg/lpg_3.jpg" title="Packed LPG Retailing" alt="" class="img_bdr" style="margin:0px 30px 20px 0px;" />
          </div>
                    
          
          
          <div class="clear"></div>
          
         </div>
        
        <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->--%>
            <%-- </div>--%>
        </div>
        <!--  Cont-right  -->
        <div class="clear" style="height: 40px;">
        </div>
        <!--  Breadcrum  -->
        <div id="bread_box">
            <a href="index.aspx" onmouseover="home.src='images/home_ic_h.jpg'" onmouseout="home.src='images/home_ic.jpg'"
                style="padding: 7px 16px 10px 8px;">
                <img src="images/home_ic.jpg" border="0" name="home" title="Home" alt="" runat="server" /></a>
            <a href="#">Business Mix</a> <span style="margin-left: 7px; margin-right: 10px;">Retail
                LPG</span>
        </div>
        <!--  Breadcrum  -->
        <%--<div style="float:right; margin-top: 9px;"><a href="#top" onmouseover="tp.src='images/top_h.jpg'" onmouseout="tp.src='images/top.jpg'"><img src="images/top.jpg" border="0" name="tp" alt="" runat="server" /></a></div>--%>
    </div>
</asp:Content>
