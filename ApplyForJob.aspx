﻿<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true" CodeFile="ApplyForJob.aspx.cs" Inherits="ApplyForJob" Title="Aegis - Apply For Job" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script src="Admin/jscripts/jquery-1.4.1.min.js" type="text/javascript"></script>
 <script type="text/javascript">
    function CheckBlank()
    {
        var a=false;
        
        var str='';
        
          
        if($('#<%=txtName.ClientID %>').val().length<1)
        {
            a=true;
            str=str+'Name,';
        }
        else
        {
            a=false;
        }
        
        if($('#<%=txtEmail.ClientID %>').val().length<1)
        {
            a=true;
            str= str+'Email Id,';
        }
        else
        {
            a=false;
        }
       
        if(a==true)
        {     str =str.substring(0, str.length-1);
              $('#<%=lblMsg.ClientID %>').html('Please Fill Following Fields :- <br /> '+str);
              $('#<%=lblMsg.ClientID %>').show();
              return false;
        }
        else
        {
            $('#<%=lblMsg.ClientID %>').hide();
            if($('#<%=txtEmail.ClientID %>').val().length>0)
            {
                var atpos=$('#<%=txtEmail.ClientID %>').val().indexOf("@");
                var dotpos=$('#<%=txtEmail.ClientID %>').val().lastIndexOf(".");
                if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
                  {
                    $('#<%=lblMsg.ClientID %>').html('Email id is not valid.'+str);
                    $('#<%=lblMsg.ClientID %>').show();
                     
                     return false;
                  }
                
            }
            else
            {
                return true;
            }
        }
    }
   </script>
<style type="text/css">
.tbl
{
    background-color:#E5F4F2;
}
    .style1
    {
        height: 31px;
    }
    .Req
    {
    	color:Red;
    	font-size:10pt;
    	
   	}
    .style2
    {
        height: 29px;
    }
    .style3
    {
        height: 32px;
    }
</style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div id="smoothmenu1" class="ddsmoothmenu">
    
        <ul>
        <li><a href="index.aspx">Home</a></li>
        <li><a href="company_profile.aspx">About Us</a>
            <ul>
            <%--<li><a href="company_profile.aspx">Company Profile</a></li>--%>
            <li><a href="background_parent_company.aspx">Background</a></li>
            <li><a href="key_management_team.aspx">Key Management / Team</a></li>
            <li><a href="partners.aspx">Partners</a></li>
            <li><a href="associations_credentials.aspx">Associations &amp; Credentials</a></li>
            <li><a href="initiatives.aspx">Initiatives</a></li>
            <li><a href="infrastructure.aspx">Infrastructure</a></li>
            <li><a href="awards_recognition.aspx">Awards &amp; Recognition</a></li>
          	</ul>
        </li>
        <li><a href="liquid_logistics.aspx">Business Mix</a>
          <ul>
          <li><a href="liquid_logistics.aspx">Liquid Logistic</a></li>
          <li><a href="gas_logistics.aspx">Gas Logistic</a></li>
          <li><a href="epc_services.aspx">EPC Services</a></li>
         <%-- <li><a href="retail_lpg.aspx">Retail LPG</a></li>--%>
               <li><a href="http://www.aegisretail.in" class="active" target="_blank">Retail LPG</a></li>
          <li><a href="marine.aspx">Marine</a></li> 
          </ul>
        </li>
        <li><a href="hse_highlights.aspx">HSE</a>
          <ul>
          <li><a href="hse_highlights.aspx">Highlights</a></li>
          <li><a href="hse_training_development.aspx">Training  &amp; Development</a></li>
          </ul>
        </li>
        <li><a href="key_financial_data.aspx">Investor Relations</a>
          <ul>
          	<li><a href="key_financial_data.aspx">Key Financial Data</a></li>
            <li><a href="shares.aspx">Shares</a></li>
            <li><a href="reports_filings.aspx">Reports &amp; Filings</a></li>
            <li><a href="events_presentations.aspx">Presentations</a></li>
            <li><a href="acquisitions.aspx">Acquisitions</a></li>
            <li><a href="corporate_governance.aspx">Corporate Governance</a></li>
            <li><a href="investor_contact.aspx">Investor Contact</a></li>
            <li><a href="quarterly_results.aspx">Quarterly Results</a></li>
            <li><a href="live_share_info.aspx">Live Share Info</a></li>
          </ul>
        </li>
        <li><a href="csr.aspx">CSR</a></li>
        <li><a href="employee_login.aspx">Employee Login</a></li>
        </ul>

	</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

<div id="main_container">


	<div id="banner"><img src="images/banner/liquid_logistic.jpg" border="0" alt="Liquid Logistics" title="Liquid logistics" /></div>
		
	<div class="clear"></div>
        
    
    <div id="cont_full">
    
    
    
    <div id="pg_head">Apply For Job</div>
    
    <div id="contact_left_box">
    
    
   <%-- <div class="career_box">
    	<p><strong>Job Title : ABCD </strong></p>
    	<p><strong>Primary Skill : </strong></p>
    	<p><strong>Location : </strong>Mumbai</p>
    	<p><img src="images/apply.jpg" border="0" alt="" /></p>
    </div>--%>
    <table class="tbl">
            <tr>
                <td class="style1" colspan="2">
                    <asp:Label ID="lblMsg" runat="server" style="display:none;" ></asp:Label>
                    </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:Label ID="lblTitle" runat="server" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    <asp:Label ID="Label1" runat="server" Text="Name "></asp:Label>
                    <span class="Req" style="vertical-align:top;">*</span>
                </td>
                <td class="style2">
                    <asp:TextBox ID="txtName" runat="server" Width="203px" CssClass="input_d"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    <asp:Label ID="Label2" runat="server" Text="Email"></asp:Label><span class="Req" style="vertical-align:top;">*</span>
                </td>
                <td class="style2">
                    <asp:TextBox ID="txtEmail" runat="server" Width="203px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    <asp:Label ID="Label3" runat="server" Text="Resume"></asp:Label><span class="Req" style="vertical-align:top;">*</span>
                </td>
                <td class="style2">
                    <asp:FileUpload ID="FileUpload1" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="style3">
                    &nbsp;</td>
                <td class="style2">
                    &nbsp;</td>
            </tr>
            <tr>
                <td  class="style3">
                    &nbsp;</td>
                <td>
                    <asp:Button ID="btnSubmit" style="width: 60px; height: 25px; border:1px solid #cecece; background-color:#eeeeee;" OnClick="btnSubmit_Click" OnClientClick="javascript:return CheckBlank();" runat="server" Text="Submit" />
               
                </td>
            </tr>
        </table>
    
    </div>
    
    
    
    <div id="gray_line" style="height:400px; float:right;"></div>
    
         
    
    </div>
    
    

<div class="clear" style="height:40px;"></div>

<!--  Breadcrum  -->
        
    <div id="bread_box">
    <a href="index.aspx" onmouseover="home.src='images/home_ic_h.jpg'" onmouseout="home.src='images/home_ic.jpg'" style="padding:7px 16px 10px 8px;"><img src="images/home_ic.jpg" border="0" name="home" title="Home" /></a>
    <span style="margin-left:7px; margin-right:10px;">Careers</span>
    </div>
        
    <!--  Breadcrum  -->

</div>


</asp:Content>

