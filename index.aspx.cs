﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class index : System.Web.UI.Page
{
    Class1 c = new Class1();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
           
            HtmlMeta metaKeys = new HtmlMeta();

            metaKeys.Name = "keywords";
            metaKeys.Content = "Aegis,Aegis India,Liquid Logistics,Gas Logistics,EPC Services,Marine";
            this.Header.Controls.Add(metaKeys);

            HtmlMeta metaDesc = new HtmlMeta();
            metaDesc.Name = "description";
            metaDesc.Content = "Aegis Group is a key player in India’s downstream oil and gas sector, and its flagship company, Aegis Logistics Limited, is India’s leading oil, gas, and chemical logistics company. Our vision is to be the industry leader in each of our chosen business segments by delivering superior customer service and a focus on quality, safety, and environmental standards.";
           
            this.Header.Controls.Add(metaDesc);
            bindpage();
        }
        
    }

    public void bindpage()
    {
        Repeater1.DataSource = c.Display("EXEC AddUpdateGetHomePageBanner 'Get_Home_Banner'");
        Repeater1.DataBind();
    }
}
