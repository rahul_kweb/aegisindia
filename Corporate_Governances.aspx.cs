﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Corporate_Governances : System.Web.UI.Page
{
    Class1 c = new Class1();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindDataList();
        }
    }
    private void BindDataList()
    {
        try
        {
            dtlPdf.DataSource = c.Display("EXEC AddUpdateGetPdfPage 'Get_By_Master_Id',0,22");
            dtlPdf.DataBind();
        }
        catch (Exception ex)
        {
 
        }
    }
}
