﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TestingPage.aspx.cs" Inherits="TestingPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Button ID="btnExport" runat="server" Text="Export" 
        onclick="btnExport_Click" />
    <br />
    <div>


    
    <asp:GridView ID="gv" runat="server" AutoGenerateColumns="False" 
                    BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px" 
                    CellPadding="4" AllowPaging="True" >
                    <RowStyle BackColor="White" ForeColor="#330099" />
                    <Columns>
                        <asp:BoundField DataField="Inquiry_Id_bint" HeaderText="Id" />
                        <asp:BoundField DataField="Name_vcr" HeaderText="Name" />
                        <asp:BoundField DataField="Email_vcr" HeaderText="Email Id" />
                        <asp:BoundField DataField="Company_Name_vcr" HeaderText="Company Name" />
                        <asp:BoundField DataField="Inquiry_Type_vcr" HeaderText="Inquiry Type" />
                        <asp:BoundField DataField="Message_vcr" HeaderText="Message" />
                        <asp:CommandField ButtonType="Button" HeaderText="Delete" 
                            ShowDeleteButton="True" />
                    </Columns>
                    
                    <EmptyDataTemplate>
                    <table cellspacing="0" cellpadding="4" rules="all" border="1" style="width:600px;background-color:White;border-color:#3366CC;border-width:1px;border-style:None;border-collapse:collapse;">
		            <tbody><tr style="color:#CCCCFF;background-color:#003399;font-weight:bold;">
			            <th scope="col">No Record Found</th>
		            </tr>
		            <tr style="color:#003399;background-color:White;">
			        <td>
			        
			        </td>
		             </tr>
	</tbody></table>
                    
                    </EmptyDataTemplate>
                    <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                    <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
                </asp:GridView>
    </div>
    </form>
</body>
</html>
