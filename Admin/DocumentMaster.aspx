﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="DocumentMaster.aspx.cs" Inherits="Admin_DocumentMaster" Title="Aegis Logistics Limited - Documents" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register Src="~/Admin/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <link href="StyleSheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <div>
&nbsp;<table class="style1">
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <uc1:MyMessageBox ID="Msg" runat="server" ShowCloseButton="true" 
                    Visible="False" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td valign="top">
                <asp:Label ID="Label1" runat="server" Text="Document Title"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtTitle" runat="server" Height="146px" TextMode="MultiLine" 
                    Width="426px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="Date"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                
                <asp:CalendarExtender TargetControlID="txtDate" Format="dd/MM/yyyy" ID="CalendarExtender1" runat="server">
                </asp:CalendarExtender>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label3" runat="server" Text="Upload Document"></asp:Label>
                
                </td>
            <td>
                <asp:FileUpload ID="FileUpload1" runat="server" /><asp:HiddenField ID="hdnPdf" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:Button ID="btnSubmit" runat="server" Height="26px" Text="Submit" 
                    onclick="btnSubmit_Click" />
                                         <asp:Button ID="btnCancel" runat="server" onclick="btnCancel_Click" 
                    Text="Cancel" Visible="False" />
            
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:GridView ID="dg" runat="server" AllowPaging="True" 
                    AutoGenerateColumns="False" BackColor="White" BorderColor="#CC9966" 
                    BorderStyle="None" BorderWidth="1px" CellPadding="4" 
                    onpageindexchanging="dg_PageIndexChanging" PageSize="15" 
                    onrowdeleting="dg_RowDeleting" 
                    onselectedindexchanging="dg_SelectedIndexChanging">
                    <RowStyle BackColor="White" ForeColor="#330099" />
                    <Columns>
                        <asp:BoundField DataField="Doc_Id_bint" HeaderText="Id" />
                        <asp:BoundField DataField="Title_vcr" HeaderText="Title" />
                        <asp:BoundField DataField="Document_Date_dtm" HeaderText="Date" />
                        <asp:TemplateField HeaderText="Download">
                        <ItemTemplate>
                            <a href='<%# string.Format("Documents/{0}.pdf",Eval("Doc_Path_vcr")) %>' target="_blank">Download</a>
                        </ItemTemplate>                        
                        </asp:TemplateField>
                         <asp:CommandField ButtonType="Button" HeaderText="Edit" SelectText="Edit" 
                             ShowSelectButton="True" />
                        <asp:CommandField ButtonType="Button" HeaderText="Delete" 
                            ShowDeleteButton="True" />
                    </Columns>
                    <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                    <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</div>
</asp:Content>

