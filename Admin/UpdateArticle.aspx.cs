﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

public partial class Admin_UpdateArticle : System.Web.UI.Page
{
    Class1 c = new Class1();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["AdminUserName"].Equals("") || Session["AdminLoginId"].Equals(""))
        {
            Response.Redirect("AdminLogin.aspx");
        }
        else
        {
            if (!Page.IsPostBack)
            {

               
               
                if (Request.QueryString.Count.Equals(1))
                {
                    string qs = Request.QueryString.Keys[0].ToString();
                    string id=ExtractNumbers(qs);
                    if (id.Length > 0)
                    {
                        lblMId.Text = id;
                        DataTable dt = new DataTable();
                        dt = GetPageDetails(int.Parse(id));
                        if (dt.Rows.Count > 0)
                        {
                            lblTitle.Text = dt.Rows[0]["Page_Name_vcr"].ToString();
                            lblPageTitle.Text = dt.Rows[0]["Title_vcr"].ToString();
                            txtDesc.Content = dt.Rows[0]["Desc_vcr"].ToString();
                            lblId.Text = dt.Rows[0]["ID_bint"].ToString();
                           
                        }
                        else
                        {
                            Response.Redirect("Default.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("Default.aspx");
                    }
                }
                else
                {
                    Response.Redirect("Default.aspx");
                }
            }
        }
    }
    private DataTable GetPageDetails(int Id)
    {
        DataTable dt = new DataTable();
        try
        {
            dt = c.Display("EXEC AddUpdateGetCMS 'Get_By_Id',"+Id);

        }
        catch (Exception ex)
        {
            Msg.ShowError("Error Occurred while fetching Details.");
        }
        return dt;
    }

       public static string ExtractNumbers( string expr )
       {
           return string.Join( null,System.Text.RegularExpressions.Regex.Split( expr, "[^\\d]" ) );
       }
    protected void btn1_Click(object sender, EventArgs e)
    {
        string blank = "";       
        if (txtDesc.Content.Trim().Length < 1)
        {
            blank = "Please Type Description of page.";
            Msg.ShowError(blank);
        }       
        else
        {
            UpdateArticle();
            Reset();
        }
    }
    private void UpdateArticle()
    {
        try
        {
            using (SqlCommand cmd = new SqlCommand("AddUpdategetCMS", c.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para_vcr","Update");
                cmd.Parameters.AddWithValue("@ID_bint",int.Parse(lblId.Text));
                cmd.Parameters.AddWithValue("@Desc_vcr", txtDesc.Content.Trim());                
                if (c.con.State == ConnectionState.Closed)
                {
                    c.con.Open();
                }
                cmd.ExecuteNonQuery();
                Session["CMSName"] = lblTitle.Text.Trim();
                Response.Redirect("Default.aspx");
            }
        }
        catch (Exception ex)
        {
            Msg.ShowError("Error Occurred.");
        }
    }
    private void Reset()
    {       
        txtDesc.Content = "";
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Default.aspx");
    }
}
