﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class Admin_TendorMaster : System.Web.UI.Page
{
    Class1 c = new Class1();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["AdminUserName"].Equals("") || Session["AdminLoginId"].Equals(""))
        {
            Response.Redirect("AdminLogin.aspx");

        }
        else
        {
            if (!Page.IsPostBack)
            {
                BindGrid();
            }
        }
    }
    private void BindGrid()
    {
        try
        {
            gd.Columns[0].Visible = true;
            gd.DataSource = c.Display("EXEC AddUpdateGetTendorMaster 'Get'");
            gd.DataBind();
            gd.Columns[0].Visible = false;
        }
        catch (Exception ex)
        {
            Msg.ShowError("Error occurred while fetching record.");
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (txtTitle.Text.Trim().Length < 1)
        {
            Msg.ShowError("Please Type Title.");
        }
        if (FileUpload1.FileName.Equals(""))
        {
            Msg.ShowError("Please Upload Pdf.");
        }

        else
        {
            AddPdf();
        }
    }
    private void AddPdf()
    {
        string opt = btnSubmit.Text;
        try
        {
            int DId = 0;
            string ext = "";

            using (SqlCommand cmd = new SqlCommand("AddUpdateGetTendorMaster", c.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para_vcr", "Add");

                cmd.Parameters.AddWithValue("@Tendor_path_vcr", "Tendor_Pdf");
               // cmd.Parameters.AddWithValue("@Added_Updated_By_Id_bint", int.Parse(Session["AdminLoginId"].ToString()));
                cmd.Parameters.AddWithValue("@Tendor_Title_vcr", txtTitle.Text.Trim());

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                DId = int.Parse(dt.Rows[0][0].ToString());
                string fileName = dt.Rows[0][1].ToString();
                ext = SaveFile(fileName);
            }
            using (SqlCommand cmd1 = new SqlCommand("AddUpdateGetTendorMaster", c.con))
            {
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.AddWithValue("@Para_vcr", "UpdateDocumentExtension");
                cmd1.Parameters.AddWithValue("@Tendor_path_vcr", ext);
                cmd1.Parameters.AddWithValue("@Tendor_Id_int", DId);
                if (c.con.State == ConnectionState.Closed)
                {
                    c.con.Open();
                }
                cmd1.ExecuteNonQuery();
            }

            txtTitle.Text = "";
            txtTitle.Focus();
            BindGrid();

            if (opt.Equals("Submit"))
            {
                Msg.ShowSuccess("Record Added Successfully.");
            }
            else if (opt.Equals("Update"))
            {
                Msg.ShowSuccess("Record Updated Successfully.");
            }

        }
        catch (Exception ex)
        {
            if (opt.Equals("Submit"))
            {
                Msg.ShowError("Some Error Occurred, can not save record" + ex.Message);
            }
            else if (opt.Equals("Update"))
            {
                Msg.ShowError("Some Error Occurred, can not update record");
            }
        }
    }
    private string SaveFile(string fl)
    {
        string extensionI1 = System.IO.Path.GetExtension(FileUpload1.FileName);

        if (extensionI1 != "")
        {
            FileUpload1.SaveAs(Server.MapPath("Tendor\\" + fl + extensionI1 + ""));
            // FilePath = Server.MapPath("Admin\\Resume\\" + fl + extensionI1);
        }
        return extensionI1;
    }
    protected void gd_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        try
        {
            string FileName = gd.Rows[e.NewSelectedIndex].Cells[1].Text;
            Download(FileName);
        }
        catch (Exception ex)
        {
            Msg.ShowError("Can not Download File, Some Error Occurred.");
        }
    }
    private void Download(string fileName)
    {
        try
        {
            // string filename = "Connectivity.doc";
            if (fileName != "")
            {
                string strPath = Server.MapPath("Admin\\Tendor\\" + fileName);

                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
                Response.Charset = "";
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                //Response.ContentType = "image/Jpeg";
                Response.ContentType = "Application/pdf";
                //Response.AppendHeader("Content-Disposition", "attachment; filename=" + strFileName);
                Response.TransmitFile(strPath);
                Response.End();

            }
        }
        catch (Exception ex)
        {
            Msg.ShowError("Some Error Occurred.");
        }
    }
    protected void gd_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            int Id = int.Parse(gd.Rows[e.RowIndex].Cells[0].Text);
            using (SqlCommand cmd = new SqlCommand("AddUpdateGetTendorMaster", c.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para_vcr", "Delete");
                cmd.Parameters.AddWithValue("@Tendor_Id_int", Id);
               // cmd.Parameters.AddWithValue("@Added_Updated_By_Id_bint", int.Parse(Session["AdminLoginId"].ToString()));
                if (!c.con.State.Equals(ConnectionState.Open))
                {
                    c.con.Open();
                }

                cmd.ExecuteNonQuery();
                c.con.Close();
                BindGrid();
                Msg.ShowSuccess("Record deleted successfully.");
            }
        }
        catch (Exception ex)
        {
            Msg.ShowError("Error occurred,can not delete record");
        }
    }
}
