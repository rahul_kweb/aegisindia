﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class Admin_TestLogin : System.Web.UI.Page
{
    Class1 c = new Class1();
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    private string checkblank()
    {
        string blank="";
        if (txtName.Text.Equals(""))
        {
            blank += "type,";
        }
        if (!blank.Equals(""))
        {
            blank = blank.Remove(blank.Length - 1);
        }
        return blank;
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        if(Button1.Text.Equals("Submit"))
        {
            
        }
    }
    private int UpdateArticle()
    {
        int a = 0;
        try
        {
            using (SqlCommand cmd = new SqlCommand("AddUpdategetCMS", c.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                if (Button1.Text.Equals("Submit"))
                {
                    cmd.Parameters.AddWithValue("@Para_vcr", "Update");
                }
                else if (Button1.Text.Equals("Update"))
                {
                    //btnCancel.Visible = true;
                    cmd.Parameters.AddWithValue("@Para_vcr", "Update");

                }

                cmd.Parameters.AddWithValue("@ID_bint", 10);
                cmd.Parameters.AddWithValue("@Desc_vcr", txtSupport.Text.Trim());
                if (c.con.State == ConnectionState.Closed)
                {
                    c.con.Open();
                }

                cmd.ExecuteNonQuery();
                c.con.Close();
                a = 1;

            }
        }
        catch (Exception ex)
        {
            Msg.ShowError("Error Occurred.");
        }
        return a;
    }
}
