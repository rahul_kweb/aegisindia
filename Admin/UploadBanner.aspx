﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="UploadBanner.aspx.cs" Inherits="Admin_UploadBanner" Title="Aegis Logistics Limited || Upload Banner" %>
<%@ Register Src="~/Admin/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="StyleSheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
         .Req
        {
        	color:Red;
        	vertical-align:top;
        }
        .style2
        {
            height: 161px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <div>
   <table class="style1">
        <tr>
            <td width="120">
                <asp:Label ID="lblMId" runat="server" Text="" Visible="false"></asp:Label></td>
            <td>
                <h3><asp:Label ID="lblTitle" runat="server" Text="" ></asp:Label></h3></td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <uc1:MyMessageBox ID="Msg" runat="server" ShowCloseButton="true" 
                    Visible="False" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text=" Name"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlSubMenu" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="ddlSubMenu_SelectedIndexChanged">
                </asp:DropDownList>
                <span class="Req">*<asp:RequiredFieldValidator ID="ReqTitle" runat="server" 
                    ControlToValidate="ddlSubMenu" ErrorMessage="Please Type Title For Image" 
                    SetFocusOnError="True"></asp:RequiredFieldValidator></span></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="Upload Image"></asp:Label>
            </td>
            <td>
                <asp:FileUpload ID="FileUpload1" runat="server" />
                <span class="Req">*</span>&nbsp;
                 
               <%-- <asp:Image ID="ImgPartner"  runat="server" Height="150px" Width="150px" />--%>
                </td>
        </tr>
        <tr >
            <td> &nbsp;
              </td>
            <td>
                <asp:Image runat="server" Visible="false" Height="150px" Width="545px" 
                    ID="ImgPartner" 
                    ImageUrl='<%# string.Format("~/Admin/images/{0}",Eval("Image_vcr")) %>' /></td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" 
                    onclick="btnSubmit_Click" />
            &nbsp;<asp:Button ID="btnCancel" runat="server" onclick="btnCancel_Click" 
                    Text="Cancel" Visible="False" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
      
        <tr>
            <td class="style2">
            
               </td>
            <td class="style2">
                &nbsp;</td>
        </tr>
    </table>

</div>

</asp:Content>


