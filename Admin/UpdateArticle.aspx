﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UpdateArticle.aspx.cs" Inherits="Admin_UpdateArticle" Title="Aegis Logistics Limited - Update Article" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit.HTMLEditor" tagprefix="cc1" %>

<%@ Register Src="~/Admin/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Update Article</title>
    <link href="StyleSheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            width: 90%;
        }
        .req
        {
        	color:Red;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    
 
   
    <div>
    
        <table class="style1">
            <tr>
                <td>
                    <fieldset>
                        <legend>Updating
                            <asp:Label ID="lblTitle" runat="server" />
                            Article</legend>
                        <div style="padding-top: 1px;">
                            <table align="center" border="0" cellpadding="2" cellspacing="2" 
                                style="width: 96%">
                                <tr>
                                    <td></td>
                                    <td width="26%" align="center"  style="width: 100%">
                                        <uc1:MyMessageBox ID="Msg" runat="server" ShowCloseButton="true" 
                                            Visible="False" />
                                    </td>
                                    <td width="74%">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="26%">
                                        &nbsp;</td>
                                    <td width="74%">
                                        <input id="CAT_ID" runat="server" class="textbox" name="CAT_ID" type="hidden" />
                                        <asp:Label ID="lblId" runat="server" Visible="False"></asp:Label>
                                        <asp:Label ID="lblMId" runat="server" Visible="False"></asp:Label>
                                    </td>
                                    <td width="74%">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="26%" align="right">
                                        <span class="content2">Title:</span><span class="req">*</span></td>
                                    <td width="74%">
  
                                        <asp:Label ID="lblPageTitle" runat="server"></asp:Label>
                                    </td>
                                    <td width="74%">
  
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td valign="top" width="26%" align="right">
                                        <span class="content2">Content:</span><span class="req">*</span></td>
                                    <td width="74%">
                                        <cc1:Editor ID="txtDesc" runat="server" Height="500px" /> 
                                        
                                        </td>
                                    <td width="74%">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="26%">
                                    </td>
                                    <td width="74%">
                                        &nbsp;<br />
                                        <asp:Button ID="btn1" runat="server" cssClass="submit" Text="Update" 
                                            onclick="btn1_Click" />
                                        <asp:Button ID="btn2" runat="server" cssClass="submit" 
                                            Text="Preview and Finalize" Visible="false" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" 
                                            onclick="btnCancel_Click" Text="Cancel" />
                                    </td>
                                    <td width="74%">
                                        &nbsp;</td>
                                </tr>
                            </table>
                        </div>
                    </fieldset></td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
