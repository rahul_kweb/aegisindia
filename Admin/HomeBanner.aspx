﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="HomeBanner.aspx.cs" Inherits="Admin_HomeBannner" Title="Aegis Logistics Limited || Home Banner" %>
<%@ Register Src="~/Admin/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>

 

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link href="StyleSheet.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
   <style type="text/css">
        .tbl1
        {
            width: 900px;
            margin:0 auto;            
        }
      
    </style>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
 
   
    <table class="tbl1"  border="0" align="center">
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td colspan="2">
                <uc1:MyMessageBox ID="Msg" runat="server" ShowCloseButton="true" Visible="false" />
            </td>
        </tr>
        <tr runat="server" id="trUpload">
            <td align="right">
                <label for="txtDownloadText"> Banner : </label></td>
            <td valign="top">
                <asp:FileUpload ID="FileUpload1" runat="server" />
                
                <span class="star">*</span>
                
                </td>
                
        </tr>
        <tr runat="server" id="trBanner" >
            <td align="right">
             </td>
            <td valign="top">
           
                <asp:Image ID="imgBanner" runat="server" Height="204px" Width="566px" />
                
                </td>
                
        </tr>
        <tr  runat="server" id="trSubmit" >
            <td></td>
            <td>
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" 
                    onclick="btnSubmit_Click" CssClass="Button"  />
                    &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" Visible="false" 
                    onclick="btnCancel_Click" CssClass="Button" />
            </td>
        </tr>
         <tr>
            <td></td>
            <td>                
            </td>
        </tr>
        <tr>
        <td>
            
        </td>
        <td><asp:GridView ID="gd" runat="server" BackColor="White" BorderColor="#DEDFDE" 
                BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" 
                GridLines="Vertical" AutoGenerateColumns="False" 
                onrowdeleting="gd_RowDeleting" 
                onselectedindexchanging="gd_SelectedIndexChanging">
            <RowStyle BackColor="#F7F7DE" />
            <Columns>
                <asp:BoundField DataField="Banner_Id_int" HeaderText="Id" />
                
               <asp:TemplateField HeaderText="Banner">
               <ItemTemplate>
                  <asp:Image runat="server"  ID="Banner" Height="200px" Width="450px" ImageUrl='<%# string.Format("~/Admin/Banner/{0}",Eval("Banner_vcr")) %>' />
               </ItemTemplate>
               </asp:TemplateField>
                <asp:CommandField ButtonType="Button" HeaderText="Edit" SelectText="Edit" 
                    ShowSelectButton="True" />
                <asp:CommandField ButtonType="Button" HeaderText="Delete" 
                    ShowDeleteButton="True" />
            </Columns>
            <FooterStyle BackColor="#CCCC99" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" />
            </asp:GridView></td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                &nbsp;</td>
        </tr>
    </table>
    

</asp:Content>

