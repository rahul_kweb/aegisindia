﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class Admin_Talent : System.Web.UI.Page
{
    Class1 c = new Class1();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["AdminUserName"].Equals("") || Session["AdminLoginId"].Equals(""))
        {
            Response.Redirect("AdminLogin.aspx");
        }
        else
        {
            if (!Page.IsPostBack)
            {
                string rcheck = Session["Permission"].ToString();
                if (!c.ShowHideAdminMenu(rcheck, this, "UploadImages", "UploadImages"))
                {
                    Response.Redirect("Default.aspx");
                }
                
              
                getData();
                //GetPageDetails(1);
            }
        }
    }
    private void getData()
    {
        try
        {
            DataTable dt = new DataTable();
            dt = c.Display("EXEC AddUpdategetTalentMaster 'Get'");
            if (dt.Rows.Count > 0)
            {
                btnSubmit.Text = "Update";
                txtTalent.Content = dt.Rows[0]["Desc_vcr"].ToString();
            }
        }
        catch (Exception ex)
        {
        }
    }
    private DataTable GetPageDetails(int Id)
    {
        DataTable dt = new DataTable();
        try
        {
            dt = c.Display("EXEC AddUpdategetTalentMaster 'Get_By_Id'," + Id);

        }
        catch (Exception ex)
        {
            Msg.ShowError("Error Occurred while fetching Details.");
        }
        return dt;
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        Msg.Visible = false;
        if (txtTalent.Content.Equals(""))
        {
            Msg.ShowError("Please Type.");
        }
        else
        {
            if (btnSubmit.Text.Equals("Submit"))
            {
              
                UpdateArticle();
            }
            else if (btnSubmit.Text.Equals("Update"))
            {
                
                UpdateArticle();
            }
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        //txtTalent.Text = "";
        //ImgPartner.ImageUrl = null;
        //ImgPartner.Visible = false;
        //gd.SelectedIndex = -1;
        //txtName.Focus();
        btnCancel.Visible = false;
    }
    private void UpdateArticle()
    {
        string opt = btnSubmit.Text;
        try
        {
            using (SqlCommand cmd = new SqlCommand("AddUpdategetTalentMaster", c.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para_vcr", "Update");
                cmd.Parameters.AddWithValue("@ID_bint", 1);
                cmd.Parameters.AddWithValue("@Desc_vcr", txtTalent.Content.Trim());
                cmd.Parameters.AddWithValue("@Added_Updated_By_Id_bint", int.Parse(Session["AdminLoginId"].ToString()));

                if (c.con.State == ConnectionState.Closed)
                {
                    c.con.Open();
                }
                cmd.ExecuteNonQuery();
                //Session["CMSName"] = lblTitle.Text.Trim();
                // Response.Redirect("Default.aspx");
            }
            if (opt.Equals("Submit"))
            {
                Msg.ShowSuccess("Record Added Successfully.");
            }
            else if (opt.Equals("Update"))
            {
                Msg.ShowSuccess("Record Updated Successfully.");
            }
        }
        catch (Exception ex)
        {
            Msg.ShowError("Error Occurred.");
        }
    }
}
