﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Drawing;

public partial class Jobs : System.Web.UI.Page
{
    Class1 c = new Class1();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["AdminLoginId"].Equals(""))
        {
            Response.Redirect("AdminLogin.aspx");
        }
        else
        {
            if (!Page.IsPostBack)
            {

                string rcheck = Session["Permission"].ToString();
                if (!c.ShowHideAdminMenu(rcheck, this, "Career", "Career"))
                {
                    Response.Redirect("Default.aspx");
                }
                BindGrid();
            }
        }
    }
    private void BindGrid()
    {
        try
        {
            gv.Columns[0].Visible = true;
            gv.DataSource = c.Display("EXEC AddUpdateJobMaster 'Get'");
            gv.DataBind();
            gv.Columns[0].Visible = false;

            foreach (GridViewRow gr in gv.Rows)
            {
                string status = gr.Cells[4].Text;
                if (status.Equals("Shown"))
                {
                    ((Button)gr.Cells[6].Controls[0]).Text = "Hide Job";
                    gr.Cells[4].BackColor = System.Drawing.Color.LightGreen;
                    gr.Cells[4].ForeColor = Color.Black;
                }
                else
                {
                    ((Button)gr.Cells[6].Controls[0]).Text = "Show Job";
                    gr.Cells[4].BackColor = Color.LightSlateGray;
                    gr.Cells[4].ForeColor = Color.White;
                }
            }
        }
        catch (Exception ex)
        {
            Msg.ShowError("Some Error Occurred.");
        }
    }
    private string CheckBlank()
    {
        string blank = "";

        if (txtJobTitle.Text.Trim().Equals(""))
        {
            blank = "Job Title,";

        }
        if (txtLocation.Text.Trim().Equals(""))
        {
            blank += "Location,";
        }
        if (txtDesc.Text.Trim().Equals(""))
        {
            blank += "Description,";
        }
        if (blank != "")
        {
            blank = blank.Remove(blank.Length - 1);
        }
        return blank;

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string blank = CheckBlank();
        if (blank != "")
        {
            Msg.ShowWarning("Please Fill Following Details.</br>" + blank);
        }
        else
        {
            try
            {
                int a = AddUpdate();
                if (btnSubmit.Text.Equals("Submit"))
                {
                    if (a.Equals(1))
                    {
                        Msg.ShowSuccess("Record Added Successfully.");
                    }
                }
                else if (btnSubmit.Text.Equals("Update"))
                {
                    if (a.Equals(1))
                    {
                        Msg.ShowSuccess("Record Updated Successfully.");
                    }
                }
                Reset();
                BindGrid();
            }
            catch (Exception ex)
            {
                Msg.ShowError("Some Error Occurred.");
            }
        }
    }

    #region  Add Update Table
    private int AddUpdate()
    {
        int a = 0;
        using (SqlCommand cmd = new SqlCommand("AddUpdateJobMaster", c.con))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            if (btnSubmit.Text.Equals("Submit"))
            {
                cmd.Parameters.AddWithValue("@Para_vcr", "Add");
            }
            else if (btnSubmit.Text.Equals("Update"))
            {
                cmd.Parameters.AddWithValue("@Para_vcr", "Update");
                cmd.Parameters.AddWithValue("@Job_Id_bint", int.Parse(gv.Rows[gv.SelectedIndex].Cells[0].Text));
            }

            cmd.Parameters.AddWithValue("@Title_vcr", txtJobTitle.Text.Trim());
            cmd.Parameters.AddWithValue("@Location_vcr", txtLocation.Text.Trim());
            cmd.Parameters.AddWithValue("@Desc_vcr", txtDesc.Text.Trim());
            if (c.con.State == ConnectionState.Closed)
            {
                c.con.Open();
            }

            cmd.ExecuteNonQuery();
            c.con.Close();
            a = 1;

        }
        return a;

    }
    #endregion
    protected void gv_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        try
        {
            Msg.Visible = false;
            txtJobTitle.Text = gv.Rows[e.NewSelectedIndex].Cells[1].Text;
            txtLocation.Text = gv.Rows[e.NewSelectedIndex].Cells[2].Text;
            txtDesc.Text = gv.Rows[e.NewSelectedIndex].Cells[3].Text;
            btnSubmit.Text = "Update";
            btnReset.Text = "Cancel";
        }
        catch (Exception ex)
        {
            Msg.ShowError("Some Error Occurred.");
        }
    }
    private void Reset()
    {
        Msg.Visible = false;
        txtJobTitle.Text = "";
        txtLocation.Text = "";
        txtDesc.Text = "";
        btnSubmit.Text = "Submit";
        btnReset.Text = "Reset";
        gv.SelectedIndex = -1;
        txtJobTitle.Focus();
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        Msg.Visible = false;

        txtJobTitle.Text = "";
        txtLocation.Text = "";
        txtDesc.Text = "";
        btnSubmit.Text = "Submit";
        btnReset.Text = "Reset";
        gv.SelectedIndex = -1;
        txtJobTitle.Focus();
    }
    protected void gv_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            Msg.Visible = false;
            int Id = int.Parse(gv.Rows[e.RowIndex].Cells[0].Text);
            string opt = ((Button)gv.Rows[e.RowIndex].Cells[6].Controls[0]).Text;
            using (SqlCommand cmd = new SqlCommand("AddUpdateJobMaster", c.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para_vcr", opt);
                cmd.Parameters.AddWithValue("@Job_Id_bint", Id);
                if (c.con.State == ConnectionState.Closed)
                {
                    c.con.Open();
                }
                cmd.ExecuteNonQuery();
                c.con.Close();
                BindGrid();
            }
        }
        catch (Exception ex)
        {
            Msg.ShowError("Some Error Occurred.");
        }
    }
    protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            Msg.Visible = false;
            gv.PageIndex = e.NewPageIndex;
            BindGrid();
        }
        catch (Exception ex)
        {
            Msg.ShowError("Some Error Occurred.");
        }
    }
}
