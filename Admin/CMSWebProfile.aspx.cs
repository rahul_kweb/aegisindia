﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

public partial class Admin_CMSWebProfile : System.Web.UI.Page
{
    Class1 c = new Class1();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["AdminLoginId"].Equals(""))
        {
            Response.Redirect("AdminLogin.aspx");
        }
        else
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString.Count > 0)
                {
                    string a = Request.QueryString[0].ToString();
                    if (c.IsNumeric(a))
                    {
                        lblId.Text = a;
                        BindGrid(a);
                        if (Request.QueryString.Count.Equals(2))
                        {
                            if (Request.QueryString[1].ToString().Equals("upd"))
                            {
                                Msg.ShowSuccess("Record Updated Successfully."); 
                            }
                        }
                    }
                    else
                    {
                        Response.Redirect("Default.aspx");
                    }
                }
                else
                {
                    Response.Redirect("Default.aspx");
                }
            }
        }
    }
    
   
    private void BindGrid(string Id)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = c.Display("EXEC AddUpdategetCMS 'Get_By_Menu_Id',0," + int.Parse(Id));
            if (dt.Rows.Count > 0)
            {
                lblHead.Text = dt.Rows[0].ItemArray[2].ToString();
                dgrd.Columns[0].Visible = true;
                dgrd.DataSource = dt;
                dgrd.DataBind();
                dgrd.Columns[0].Visible = false;
            }
        }
        catch (Exception ex)
        { 

        } 
    }

    protected void dgrd_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName.Equals("edit"))
        {
            Response.Redirect("UpdateArticle.aspx?sdf434zds=" + e.Item.Cells[0].Text+"&mId="+lblId.Text);
        }
    }
}
