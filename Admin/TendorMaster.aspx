﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="TendorMaster.aspx.cs" Inherits="Admin_TendorMaster" Title="Aegis Logistics Limited - Tendor Master" %>
<%@ Register Src="~/Admin/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
  <link href="StyleSheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        
         .Req
        {
        	color:Red;
        	vertical-align:top;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
   
<div>
    <table align=center">
        <tr>
            <td width="100px">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <uc1:MyMessageBox ID="Msg" runat="server" ShowCloseButton="true" 
                    Visible="False" />
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:Label ID="Label1" runat="server" Text="Title"></asp:Label>
            </td>
            <td align="left">
                <asp:TextBox ID="txtTitle" runat="server"></asp:TextBox>
                <span class="Req">*<asp:RequiredFieldValidator ID="ReqTitle" runat="server" 
                    ControlToValidate="txtTitle" ErrorMessage="Please Type Title For Pdf" 
                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                </span>
             </td>
        </tr>
        <tr>
            <td align="left">
                <asp:Label ID="Label2" runat="server" Text="Upload Pdf"></asp:Label>
            </td>
            <td align="left">
                <asp:FileUpload ID="FileUpload1" runat="server" />
                <span class="Req">*</span></td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td align="left">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit"
                    onclick="btnSubmit_Click" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:GridView ID="gd" runat="server" AutoGenerateColumns="False" 
                    BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px" 
                    CellPadding="4" onrowdeleting="gd_RowDeleting" >
                   
                    <RowStyle BackColor="White" ForeColor="#330099" />
                    <Columns>
                        <asp:BoundField DataField="Tendor_Id_int" HeaderText="Id" />
                        <asp:BoundField DataField="Tendor_Title_vcr" HeaderText="Pdf Name" />
                        <asp:TemplateField >
                        <ItemTemplate>
                            <a href='<%# string.Format("Tendor/{0}.pdf",Eval("Tendor_path_vcr")) %>' target="_blank">Download</a>
                        </ItemTemplate>                        
                        </asp:TemplateField>
                       <%-- <asp:CommandField ButtonType="Image" HeaderText="Download" 
                            SelectImageUrl="~/images/download.jpg" ShowSelectButton="True" />--%>
                        <asp:CommandField ButtonType="Button" HeaderText="Delete" 
                            ShowDeleteButton="True" />
                    </Columns>
                    <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                    <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
                </asp:GridView>
            </td>
        </tr>
    </table>

</div>

</asp:Content>

