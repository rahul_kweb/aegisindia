﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class Admin_GenralInformation : System.Web.UI.Page
{
    Class1 c = new Class1();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["AdminUserName"].Equals("") || Session["AdminLoginId"].Equals(""))
        {
            Response.Redirect("AdminLogin.aspx");

        }
        else
        {
            if (!Page.IsPostBack)
            {
                string rcheck = Session["Permission"].ToString();
                if (!c.ShowHideAdminMenu(rcheck, this, "IntranetManager", "IntranetManager"))
                {
                    Response.Redirect("Default.aspx");
                }
                BindGrid();
            }
        }
    }
    private void BindGrid()
    {
        Msg.Visible = false;
        try
        {
            gd.Columns[0].Visible = true;
            gd.DataSource = c.Display("EXEC AddUpdateGetNoticeMaster 'Get'");
            gd.DataBind();
            gd.Columns[0].Visible = false;
        }
        catch (Exception ex)
        {
            Msg.ShowError("Some Error Occurred, can not fetch record");
        }
    }
    protected void gd_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gd.PageIndex = e.NewPageIndex;
        BindGrid();
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        if (txtNotice.Content.Trim().Length < 1)
        {
            Msg.ShowError("Please Type Notice.");
        }
        else
        {
            AddUpdateNotice(); 
        }

    }
    private void AddUpdateNotice()
    {
        string opt = btnSubmit.Text;
        try
        {
            using (SqlCommand cmd = new SqlCommand("AddUpdateGetNoticeMaster", c.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                if (opt.Equals("Save Notice"))
                {
                    cmd.Parameters.AddWithValue("@Para_vcr", "Add");
                }
                else if (opt.Equals("Update Notice"))
                {
                    int Id = int.Parse(gd.Rows[gd.SelectedIndex].Cells[0].Text);
                    cmd.Parameters.AddWithValue("@Para_vcr", "Update");
                    cmd.Parameters.AddWithValue("@Notice_Id_bint", Id);
                }                
                cmd.Parameters.AddWithValue("@Notice_ntxt",txtNotice.Content.Trim());
                cmd.Parameters.AddWithValue("@AddedUpdatedBy_Id_bint", int.Parse(Session["AdminLoginId"].ToString()));
                if (!c.con.State.Equals(ConnectionState.Open))
                {
                    c.con.Open();
                }

                cmd.ExecuteNonQuery();
                c.con.Close();

                txtNotice.Content = "";
                txtNotice.Focus();
                BindGrid();

                if (opt.Equals("Save Notice"))
                {
                    Msg.ShowSuccess("Record Added Successfully.");
                }
                else if (opt.Equals("Update Notice"))
                {
                    Msg.ShowSuccess("Record Updated Successfully.");
                }
               
            }
        }
        catch (Exception ex)
        {
            if (opt.Equals("Save Notice"))
            {
                Msg.ShowError("Some Error Occurred, can not save record");
            }
            else if (opt.Equals("Update Notice"))
            {
                Msg.ShowError("Some Error Occurred, can not update record");
            }
        }
    }
    protected void gd_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {

    }
}
