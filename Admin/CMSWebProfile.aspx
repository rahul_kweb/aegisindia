﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="CMSWebProfile.aspx.cs" Inherits="Admin_CMSWebProfile" Title="Aegis Logistics Limited - Web Profile" %>
<%@ Register Src="~/Admin/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <link href="StyleSheet.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <FIELDSET><LEGEND><SPAN style="FONT-WEIGHT: bold; padding-right:5px; font-size : small; FONT-FAMILY: Verdana">Static Content</SPAN></LEGEND>

<table width="100%">
<tr width="100%">

<td width="100%">

    &nbsp;</td></tr>


<tr width="100%">

<td width="100%">
  <uc1:MyMessageBox ID="Msg" runat="server" ShowCloseButton="true" 
                                            Visible="False" />
</td></tr>


<tr width="100%">

<td width="100%">

 <div class="roundcont2" style="width:100%">
<div class="roundtop" style="width:100%" >
<img src="../images/hleft.gif" height="5" width="5" alt="" class="corner">
<%--<table width="100%">
<tr>

<td align="Center">
<span class="Content2" >
Search By CMS Title/Description :
</span> 
</td>
<td align="left"  >


<asp:TextBox runat="server" id="find" class="textbox" size="50" />
</td>



<td align="left" >
<asp:Button ID="Button1" runat="server" class="submit" tooltip="Go find it!" OnClick="AdminSearch_Click" Text="Search"/>

</td>


</tr>


</table>--%>
    <asp:Label ID="lblId" runat="server" Text="Label" Visible="False"></asp:Label>
</div>
</div>
   
<div class="contentdisplay" style="width:100%">
<div class="contentdis5" style="width:100%">

<%--<table width="100%">
<tr>

<td align="Center">
<span class="Content2" >
Search By CMS Title/Description :
</span> 
</td>
<td align="left"  >


<asp:TextBox runat="server" id="find" class="textbox" size="50" />
</td>



<td align="left" >
<asp:Button ID="Button1" runat="server" class="submit" tooltip="Go find it!" OnClick="AdminSearch_Click" Text="Search"/>

</td>


</tr>


</table>--%>
 <center><strong> <asp:Label ID="lblHead" runat="server" Text="CMS"></asp:Label></strong> </center>  
</div>
</div>



</td></tr>


<tr width="100%"><td>
 <asp:Label ID="lblmsg" runat="server"  CssClass="content2" ></asp:Label><br />
<asp:DataGrid runat="server" id="dgrd" cssclass="hlink" AutoGenerateColumns="False"
     Backcolor="White" BorderStyle="None" BorderColor="#E1EDFF" cellpadding="5" 
        Width="100%" HorizontalAlign="Center" DataKeyField="CMS_Menu_Id_int" 
        onitemcommand="dgrd_ItemCommand"> 
     <headerStyle Font-Bold="True" BackColor="#6898D0" cssclass="header" />
     <AlternatingItemStyle BackColor="White" />                                      
     <Columns>    
     <asp:BoundColumn DataField="ID_bint" HeaderText="ID" 
             SortExpression="ID" />   
     <asp:BoundColumn DataField="Page_Name_vcr" HeaderText="Profile Name" 
             SortExpression="Name" />

         <asp:BoundColumn DataField="Updated_On_dtm" HeaderText="Last updated" 
             DataFormatString="{0:dd MMM yyyy}" SortExpression="Name" />
     <asp:ButtonColumn CommandName="edit" HeaderStyle-ForeColor="white" 
             HeaderStyle-Font-Underline="true" HeaderText="Edit" 
             Text='<img border="0" src="../Admin/images/icon_edit.gif">' >

<HeaderStyle Font-Underline="True" ForeColor="White"></HeaderStyle>
         </asp:ButtonColumn>

     <asp:ButtonColumn Text='<img border="0" src="../images/icon_delete.gif">' Visible="false" HeaderText="Delete" CommandName="Delete" />
     </Columns>
     <PagerStyle Mode="NumericPages" BackColor="#fcfcfc" HorizontalAlign="left" />
    </asp:DataGrid>
    
<!--End display datagrid-->

<!--Begin display pager button-->
    </td></tr>

<tr><td>


<asp:Label ID="LblPageInfoTop" Visible="false" cssClass="content2" runat="server" />
<asp:Label ID="lblrecordperpageTop" Visible="false" cssClass="content2" runat="server" />

<br />

</td></tr>


</table>
<!--End display pager button-->    </FIELDSET>

</asp:Content>

