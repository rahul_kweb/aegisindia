﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true"
    CodeFile="Inquiries.aspx.cs" Inherits="Inquiries" Title="Aegis Logistics Limited - Inquiries" %>

<%@ Register Src="~/Admin/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="StyleSheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .tbl {
            width: 100%;
            border: solid 1px gray;
        }
        .style3 {
        }
        .Req {
            color: Red;
            vertical-align: top;
        }
        .style4 {
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <table class="tbl">
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <uc1:MyMessageBox ID="Msg" runat="server" ShowCloseButton="true" Visible="False" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Button ID="btnExport" runat="server" Text="Export to Excel" OnClick="btnExport_Click"
                    CssClass="log_but" Style="float: right; margin-right: 20px; width: 100px;" />
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:GridView ID="gv" runat="server" AutoGenerateColumns="False" AllowPaging="true"
                    BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px"
                    CellPadding="4" OnPageIndexChanging="gv_PageIndexChanging" OnRowDeleting="gv_RowDeleting"
                    PageSize="10">
                    <RowStyle BackColor="White" ForeColor="#330099" />
                    <Columns>
                        <asp:BoundField DataField="Inquiry_Id_bint" HeaderText="Id" />
                        <asp:BoundField DataField="Name_vcr" HeaderText="Name" />
                        <asp:BoundField DataField="Email_vcr" HeaderText="Email Id" />
                        <asp:BoundField DataField="Company_Name_vcr" HeaderText="Company Name" />
                        <asp:BoundField DataField="City" HeaderText="City" />
                        <asp:BoundField DataField="Telephone_vcr" HeaderText="Telephone" />
                        <asp:BoundField DataField="Inquiry_Type_vcr" HeaderText="Inquiry Type" />
                        <asp:BoundField DataField="Message_vcr" HeaderText="Message" />
                        <asp:CommandField ButtonType="Button" HeaderText="Delete" ShowDeleteButton="True" />
                    </Columns>
                    <EmptyDataTemplate>
                        <table cellspacing="0" cellpadding="4" rules="all" border="1" style="width: 600px;
                            background-color: White; border-color: #3366CC; border-width: 1px; border-style: None;
                            border-collapse: collapse;">
                            <tbody>
                                <tr style="color: #CCCCFF; background-color: #003399; font-weight: bold;">
                                    <th scope="col">
                                        No Record Found
                                    </th>
                                </tr>
                                <tr style="color: #003399; background-color: White;">
                                    <td>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </EmptyDataTemplate>
                    <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                    <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
