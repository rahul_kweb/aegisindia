﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class Awards_Recognition : System.Web.UI.Page
{
    Class1 c = new Class1();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["AdminUserName"].Equals("") || Session["AdminLoginId"].Equals(""))
        {
            Response.Redirect("AdminLogin.aspx");
        }
        else
        {
            if (!Page.IsPostBack)
            {
                BindGrid();
            }
        }
    }
    private void BindGrid()
    {
        try
        {
            gd.Columns[0].Visible = true;
            gd.DataSource = c.Display("EXEC AddUpdateGetImageMaster 'Get_By_Type_Id',0,'','',0,2");
            gd.DataBind();
            gd.Columns[0].Visible = false;
        }
        catch (Exception ex)
        {
            Msg.ShowError("Error occurred while fetching record.");
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        Msg.Visible = false;
       
            if (btnSubmit.Text.Equals("Submit"))
            {
                if (FileUpload1.HasFile)
                {
                    AddImage();
                }
                else
                {
                    Msg.ShowError("Please upload Image.");
                }
            }
            else if (btnSubmit.Text.Equals("Update"))
            {
                UpdateImage();
            }
        
    }
    private void AddImage()
    {
        string opt = btnSubmit.Text;
        try
        {
            int DId = 0;
            string ext = "";

            using (SqlCommand cmd = new SqlCommand("AddUpdateGetImageMaster", c.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para_vcr", "Add");               
                cmd.Parameters.AddWithValue("@Image_Path_vcr", "Awards_Image_");
                cmd.Parameters.AddWithValue("@Added_Updated_By_Id_bint", int.Parse(Session["AdminLoginId"].ToString()));
                cmd.Parameters.AddWithValue("@Image_Type_int", 2);

                //if (!c.con.State.Equals(ConnectionState.Open))
                //{
                //    c.con.Open();
                //}
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                DId = int.Parse(dt.Rows[0][0].ToString());
                string fileName = dt.Rows[0][1].ToString();
                ext = SaveFile(fileName);
            }
            using (SqlCommand cmd1 = new SqlCommand("AddUpdateGetImageMaster", c.con))
            {
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.AddWithValue("@Para_vcr", "UpdateDocumentExtension");
                cmd1.Parameters.AddWithValue("@Image_Path_vcr", ext);
                cmd1.Parameters.AddWithValue("@Image_Id_bint", DId);
                if (c.con.State == ConnectionState.Closed)
                {
                    c.con.Open();
                }
                cmd1.ExecuteNonQuery();
            }
            BindGrid();

            if (opt.Equals("Submit"))
            {
                Msg.ShowSuccess("Record Added Successfully.");
            }
            else if (opt.Equals("Update"))
            {
                Msg.ShowSuccess("Record Updated Successfully.");
            }

        }
        catch (Exception ex)
        {
            if (opt.Equals("Submit"))
            {
                Msg.ShowError("Some Error Occurred, can not save record"+ex.Message);
            }
            else if (opt.Equals("Update"))
            {
                Msg.ShowError("Some Error Occurred, can not update record");
            }
        }
    }

    private void UpdateImage()
    {
        try
        {
            
            string ext = "";
            Uri ur = new Uri("http://www.aegis.com/Admin/" + ImgPartner.ImageUrl);

            using (SqlCommand cmd = new SqlCommand("AddUpdateGetImageMaster", c.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para_vcr", "Update");                
                cmd.Parameters.AddWithValue("@Added_Updated_By_Id_bint", int.Parse(Session["AdminLoginId"].ToString()));

                if (FileUpload1.HasFile)
                {
                    string fileName = ur.Segments.Last().Split('.').ElementAt(0);
                    ext = SaveFile(fileName);
                }
                if (!c.con.State.Equals(ConnectionState.Open))
                {
                    c.con.Open();
                }
                //cmd.ExecuteNonQuery();
            }
            gd.SelectedIndex = -1;
            BindGrid();
          
            ImgPartner.Visible = false;
            btnCancel.Visible = false;
            btnSubmit.Text = "Submit";
            Msg.ShowSuccess("Record Updated Successfully.");
        }
        catch (Exception ex)
        {
            Msg.ShowError("Some error occurred,Can not update record.");
        }
    }
    private string SaveFile(string fl)
    {
        string extensionI1 = System.IO.Path.GetExtension(FileUpload1.FileName);

        if (extensionI1 != "")
        {
            FileUpload1.SaveAs(Server.MapPath("images\\" + fl + extensionI1 + ""));
          
        }
        return fl + extensionI1;
    }
    protected void gd_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        Msg.Visible = false;
        try
        {
            int Id = int.Parse(gd.Rows[e.RowIndex].Cells[0].Text);
            using (SqlCommand cmd = new SqlCommand("AddUpdateGetImageMaster", c.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para_vcr", "Delete");
                cmd.Parameters.AddWithValue("@Image_Id_bint", Id);
                cmd.Parameters.AddWithValue("@Added_Updated_By_Id_bint", int.Parse(Session["AdminLoginId"].ToString()));
                if (!c.con.State.Equals(ConnectionState.Open))
                {
                    c.con.Open();
                }

                cmd.ExecuteNonQuery();
                c.con.Close();
                BindGrid();
                Msg.ShowSuccess("Record deleted successfully.");
            }
        }
        catch (Exception ex)
        {
            Msg.ShowError("Error occurred,can not delete record");
        }
    }
    protected void gd_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Msg.Visible = false;
        try
        {
            gd.PageIndex = e.NewPageIndex;
            BindGrid();
        }
        catch (Exception ex)
        {
 
        }

    }

    protected void gd_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        Msg.Visible = false;
        try
        {
            int Id = int.Parse(gd.Rows[e.NewSelectedIndex].Cells[0].Text);
            DataTable dt = new DataTable();
            dt = c.Display("EXEC AddUpdateGetImageMaster 'Get_By_Image_Id',"+Id);           
            string photo = dt.Rows[0]["Image_Path_vcr"].ToString();
            if (photo.Length > 0)
            {
                ImgPartner.Visible = true;
                ImgPartner.ImageUrl = "~/Admin/images/"+photo;
            }
            else
            {
                ImgPartner.Visible = false;
            }
            btnSubmit.Text = "Update";
            btnCancel.Visible = true;
        }
        catch (Exception ex)
        {
            Msg.ShowError("Some Error Occurred.");
 
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {       
        ImgPartner.ImageUrl = null;
        ImgPartner.Visible = false;
        gd.SelectedIndex = -1;       
        btnCancel.Visible = false;
    }
}
