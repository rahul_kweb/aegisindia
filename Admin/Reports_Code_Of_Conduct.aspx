<%@ Page Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="Reports_Code_Of_Conduct.aspx.cs" Inherits="Reports_Code_Of_Conduct" Title="Aegis Logistics Limited - Code Of Conduct" %>
<%@ Register Src="~/Admin/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link href="StyleSheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
         .Req
        {
        	color:Red;
        	vertical-align:top;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
  <div>
    <table class="style1">
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <uc1:MyMessageBox ID="Msg" runat="server" ShowCloseButton="true" 
                    Visible="False" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Title"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtTitle" runat="server"></asp:TextBox>
                <span class="Req">*</span></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="Upload Pdf"></asp:Label>
            </td>
            <td>
                <asp:FileUpload ID="FileUpload1" runat="server" />
                <span class="Req">*</span><asp:HiddenField ID="hdnPdf" runat="server" /></td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" 
                    onclick="btnSubmit_Click" />
                <asp:Button ID="btnCancel" runat="server" onclick="btnCancel_Click" 
                    Text="Cancel" Visible="False" />
            
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:GridView ID="gd" runat="server" AutoGenerateColumns="False" 
                    BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px" 
                    CellPadding="4" onrowdeleting="gd_RowDeleting" 
                    onselectedindexchanging="gd_SelectedIndexChanging1" >
                  
                    <RowStyle BackColor="White" ForeColor="#330099" />
                    <Columns>
                        <asp:BoundField DataField="Pdf_Page_bint" HeaderText="Id" />
                        <asp:BoundField DataField="Pdf_Title_vcr" HeaderText="Pdf Name" />
                         <asp:TemplateField HeaderText="Download" >
                        <ItemTemplate>
                            <a href='<%# string.Format("Documents/{0}.pdf",Eval("Pdf_Page_Path_vcr")) %>' target="_blank">Download</a>
                        </ItemTemplate>                        
                        </asp:TemplateField>
                        <asp:CommandField ButtonType="Button" HeaderText="Edit" SelectText="Edit" 
                             ShowSelectButton="True" />
                        <asp:CommandField ButtonType="Button" HeaderText="Delete" 
                            ShowDeleteButton="True" />
                    </Columns>
                    <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                    <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
                </asp:GridView>
            </td>
        </tr>
    </table>

</div>
</asp:Content>


