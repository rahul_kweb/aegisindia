﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Drawing;

public partial class Inquiries : System.Web.UI.Page
{
    Class1 c = new Class1();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["AdminLoginId"].Equals(""))
        {
            Response.Redirect("AdminLogin.aspx");
        }
        else
        {
            if (!Page.IsPostBack)
            {

                string rcheck = Session["Permission"].ToString();
                if (!c.ShowHideAdminMenu(rcheck, this, "Inquiries", "Inquiries"))
                {
                    Response.Redirect("Default.aspx");
                }
                BindGrid();
            }
        }
    }
    private void BindGrid()
    {
        try
        {
            gv.Columns[0].Visible = true;
            gv.DataSource = c.Display("EXEC AddUpdateGetInquiryMaster 'Get'");
            gv.DataBind();
            gv.Columns[0].Visible = false;

            
        }
        catch (Exception ex)
        {
            Msg.ShowError("Some Error Occurred.");
        }
    }

    protected void gv_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            Msg.Visible = false;
            int Id = int.Parse(gv.Rows[e.RowIndex].Cells[0].Text);
            
            using (SqlCommand cmd = new SqlCommand("AddUpdateGetInquiryMaster", c.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para_vcr", "Delete");
                cmd.Parameters.AddWithValue("@Inquiry_Id_bint", Id);
                if (c.con.State == ConnectionState.Closed)
                {
                    c.con.Open();
                }
                cmd.ExecuteNonQuery();
                c.con.Close();
                BindGrid();
                Msg.ShowSuccess("Record Deleted Successfully.");
            }
        }
        catch (Exception ex)
        {
            Msg.ShowError("Some Error Occurred.");
        }
    }

    protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gv.PageIndex = e.NewPageIndex;
            BindGrid();
        }
        catch (Exception ex)
        {
            Msg.ShowError("Some Error Occurred.");
        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dt = c.Display("EXEC AddUpdateGetInquiryMaster 'Get'");

            if (dt.Rows.Count > 0)
            {
                DataTable dtOriginal = new DataTable();
                dtOriginal = dt; //Return Table consisting data
                //Create Tempory Table
                DataTable dtTemp = new DataTable();
                //Creating Header Row
                dtTemp.Columns.Add("<b>Sr.No</b>");
                dtTemp.Columns.Add("<b>Name</b>");
                dtTemp.Columns.Add("<b>Email Id</b>");
                dtTemp.Columns.Add("<b>Company Name</b>");
                dtTemp.Columns.Add("<b>Telephone</b>");
                dtTemp.Columns.Add("<b>Inquiry Type</b>");
                dtTemp.Columns.Add("<b>Message </b>");
                dtTemp.Columns.Add("<b>Date </b>");

                DataRow drAddItem;

                for (int i = 0; i < dtOriginal.Rows.Count; i++)
                {
                    drAddItem = dtTemp.NewRow();
                    drAddItem[0] = (i + 1).ToString();
                    drAddItem[1] = dtOriginal.Rows[i]["Name_vcr"].ToString();
                    drAddItem[2] = dtOriginal.Rows[i]["Email_vcr"].ToString();
                    drAddItem[3] = dtOriginal.Rows[i]["Company_Name_vcr"].ToString();
                    drAddItem[4] = dtOriginal.Rows[i]["Telephone_vcr"].ToString();
                    drAddItem[5] = dtOriginal.Rows[i]["Inquiry_Type_vcr"].ToString();
                    drAddItem[6] = dtOriginal.Rows[i]["Message_vcr"].ToString();
                    drAddItem[7] = dtOriginal.Rows[i]["Added_On_dtm"].ToString();
                    dtTemp.Rows.Add(drAddItem);
                }

                //Temp Grid
                DataGrid dg = new DataGrid();
                dg.DataSource = dtTemp;
                dg.DataBind();

                string Cdate = DateTime.Now.Day.ToString() + "_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Year.ToString();
                ExportToExcel("Inquiry_List_" + Cdate + ".xls", dg);
                dg = null;
                dg.Dispose();
            }

        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }
    private void ExportToExcel(string strFileName, DataGrid dg)
    {
        Response.ClearContent();
        Response.AddHeader("content-disposition", "attachment; filename=" + strFileName);
        Response.ContentType = "application/excel";
        Response.ContentType = "application/vnd.ms-excel";
        System.IO.StringWriter sw = new System.IO.StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        dg.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }

}
