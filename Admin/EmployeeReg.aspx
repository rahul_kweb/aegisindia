﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="EmployeeReg.aspx.cs" Inherits="Admin_EmployeeReg" Title="Aegis Logistics Limited - Employee Registration" %>
<%@ Register Src="~/Admin/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="StyleSheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style2
        {
            width: 100%;
        }
        .style3
        {
        }
        .Req
        {
        	color:Red;
        	vertical-align:top;
        }
        .style4
        {
        	
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

    <table class="style2">
        <tr>
            <td class="style3">
                &nbsp;</td>
            <td class="style4" colspan="2">
                <uc1:MyMessageBox ID="Msg" runat="server" ShowCloseButton="true" 
                    Visible="False" />
            </td>
        </tr>
        <tr>
            <td class="style3">
                &nbsp;</td>
            <td valign="top" class="style4">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style3">
                <asp:Label ID="Label1" runat="server" Text="Employee Name"></asp:Label>
            </td>
            <td valign="top" class="style4">
                <asp:TextBox ID="txtEmployeeName" runat="server" MaxLength="200" Width="250px"></asp:TextBox>
                <span class="Req">*</span>
                <asp:RequiredFieldValidator ID="ReqEmpName" runat="server" 
                    ControlToValidate="txtEmployeeName" 
                    ErrorMessage="Please Type Employee Name"></asp:RequiredFieldValidator>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style3">
                <asp:Label ID="Label2" runat="server" Text="Gender"></asp:Label>
            </td>
            <td valign="top" class="style4">
            <div style="float:left;">
                <asp:RadioButtonList ID="RdbGender" runat="server" 
                    RepeatDirection="Horizontal">
                    <asp:ListItem Selected="True">Male</asp:ListItem>
                    <asp:ListItem>Female</asp:ListItem>
                </asp:RadioButtonList>
                </div>
                
                <span class="Req">*</span>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style3">
                &nbsp;</td>
            <td class="style4">
                <asp:Button ID="btnSubmit" runat="server" onclick="btnSubmit_Click" 
                    Text="Submit" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style3">
                &nbsp;</td>
            <td class="style4">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style3" align="center" colspan="3">
                <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Width="750px">
                    <asp:HiddenField ID="hdnEmpId" runat="server" />
                <asp:GridView ID="gd" runat="server" AllowPaging="True" 
                    AutoGenerateColumns="False" BackColor="White" BorderColor="#CC9966" 
                    BorderStyle="None" BorderWidth="1px" CellPadding="4" 
                    onpageindexchanging="gd_PageIndexChanging" 
                    onrowcancelingedit="gd_RowCancelingEdit" onrowdeleting="gd_RowDeleting" 
                    onrowediting="gd_RowEditing" onrowupdating="gd_RowUpdating" PageSize="20">
                    <RowStyle BackColor="White" ForeColor="#330099" />
                    <Columns>
                        <asp:BoundField DataField="Employee_Id_bint" HeaderText="Id" />
                        <asp:BoundField DataField="Employee_Name_vcr" HeaderText="Name" />                        
                        <asp:TemplateField HeaderText="Gender">
                        <ItemTemplate>
                        <asp:Label runat="server" ID="lblGender" Text='<%# Eval("Employee_Gender_vcr") %>'></asp:Label>
                        <asp:Label runat="server" ID="lblGridEmpId" Visible="false" Text='<%# Eval("Employee_Id_bint") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:RadioButtonList ID="rdbGridGender" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True">Male</asp:ListItem>
                                <asp:ListItem>Female</asp:ListItem>                                                            
                            </asp:RadioButtonList>                        
                        </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField ReadOnly="true" DataField="user_Id_vcr" HeaderText="Username" />
                        <asp:BoundField DataField="pwd_vcr" HeaderText="Password" />
                        <asp:CommandField ButtonType="Button" CausesValidation="false" HeaderText="Edit" ShowEditButton="True" />
                        <asp:CommandField ButtonType="Button" CausesValidation="false" HeaderText="Delete" 
                            ShowDeleteButton="True" />
                    </Columns>
                    <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                    <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
                </asp:GridView>
                
                </asp:Panel>
            </td>
        </tr>
    </table>

</asp:Content>

