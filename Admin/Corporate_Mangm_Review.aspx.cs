﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class Corporate_Mangm_Review : System.Web.UI.Page
{
    Class1 c = new Class1();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["AdminUserName"].Equals("") || Session["AdminLoginId"].Equals(""))
        {
            Response.Redirect("AdminLogin.aspx");
        }
        else
        {
            if (!Page.IsPostBack)
            {
                BindGrid();
            }
        }
    }
    private void BindGrid()
    {
        try
        {
            gd.Columns[0].Visible = true;
            gd.DataSource = c.Display("EXEC AddUpdateGetPdfPage 'Get_By_Master_Id',0,8");
            gd.DataBind();
            gd.Columns[0].Visible = false;
        }
        catch (Exception ex)
        {
            Msg.ShowError("Error occurred while fetching record.");
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (txtTitle.Text.Trim().Length < 1)
        {
            Msg.ShowError("Please Type Title.");
        }
        else
        {
            AddPdf();
        }
    }
    private void AddPdf()
    {
        string opt = btnSubmit.Text;
        try
        {
            int DId = 0;
            string ext = "";

            using (SqlCommand cmd = new SqlCommand("AddUpdateGetPdfPage", c.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                if (opt.Equals("Submit"))
                {
                    cmd.Parameters.AddWithValue("@Para_vcr", "Add");
                }
                
                cmd.Parameters.AddWithValue("@Pdf_Master_Id_int", 8);
                cmd.Parameters.AddWithValue("@Pdf_Page_Path_vcr", "Corporate_Mngmt_Review_Pdf");
                cmd.Parameters.AddWithValue("@Added_Updated_By_Id_bint", int.Parse(Session["AdminLoginId"].ToString()));
                cmd.Parameters.AddWithValue("@Pdf_Title_vcr", txtTitle.Text.Trim());

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                DId = int.Parse(dt.Rows[0][0].ToString());
                string fileName = dt.Rows[0][1].ToString();
                ext = SaveFile(fileName);
            }
            using (SqlCommand cmd1 = new SqlCommand("AddUpdateGetPdfPage", c.con))
            {
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.AddWithValue("@Para_vcr", "UpdateDocumentExtension");
                cmd1.Parameters.AddWithValue("@Pdf_Page_Path_vcr", ext);
                cmd1.Parameters.AddWithValue("@Pdf_Page_bint", DId);
                if (c.con.State == ConnectionState.Closed)
                {
                    c.con.Open();
                }
                cmd1.ExecuteNonQuery();
            }

            txtTitle.Text = "";
            txtTitle.Focus();
            BindGrid();

            if (opt.Equals("Submit"))
            {
                Msg.ShowSuccess("Record Added Successfully.");
            }
            else if (opt.Equals("Update"))
            {
                Msg.ShowSuccess("Record Updated Successfully.");
            }

        }
        catch (Exception ex)
        {
            if (opt.Equals("Submit"))
            {
                Msg.ShowError("Some Error Occurred, can not save record");
            }
            else if (opt.Equals("Update"))
            {
                Msg.ShowError("Some Error Occurred, can not update record");
            }
        }
    }
    private string SaveFile(string fl)
    {
        string extensionI1 = System.IO.Path.GetExtension(FileUpload1.FileName);

        if (extensionI1 != "")
        {
            FileUpload1.SaveAs(Server.MapPath("Documents\\" + fl + extensionI1 + ""));
            // FilePath = Server.MapPath("Admin\\Resume\\" + fl + extensionI1);
        }
        return extensionI1;
    }   
    protected void gd_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        try
        {
            string FileName = gd.Rows[e.NewSelectedIndex].Cells[1].Text;
            Download(FileName);
        }
        catch (Exception ex)
        {
            Msg.ShowError("Can not Download File, Some Error Occurred.");
        }
    }
    private void Download(string fileName)
    {
        try
        {
            // string filename = "Connectivity.doc";
            if (fileName != "")
            {
                string strPath = Server.MapPath("Admin\\Documents\\" + fileName);

                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
                Response.Charset = "";
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                //Response.ContentType = "image/Jpeg";
                Response.ContentType = "Application/pdf";
                //Response.AppendHeader("Content-Disposition", "attachment; filename=" + strFileName);
                Response.TransmitFile(strPath);
                Response.End();
                
            }
        }
        catch (Exception ex)
        {
            Msg.ShowError("Some Error Occurred.");
        }
    }
    protected void gd_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            int Id = int.Parse(gd.Rows[e.RowIndex].Cells[0].Text);
            using (SqlCommand cmd = new SqlCommand("AddUpdateGetPdfPage", c.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para_vcr", "Delete");
                cmd.Parameters.AddWithValue("@Pdf_Page_bint", Id);
                cmd.Parameters.AddWithValue("@Added_Updated_By_Id_bint", int.Parse(Session["AdminLoginId"].ToString()));
                if (!c.con.State.Equals(ConnectionState.Open))
                {
                    c.con.Open();
                }

                cmd.ExecuteNonQuery();
                c.con.Close();
                BindGrid();
                Msg.ShowSuccess("Record deleted successfully.");
            }
        }
        catch (Exception ex)
        {
            Msg.ShowError("Error occurred,can not delete record");
        }

    }
}
