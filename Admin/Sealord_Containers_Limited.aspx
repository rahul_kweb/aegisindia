﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="Sealord_Containers_Limited.aspx.cs" Inherits="Admin_Sealord_Containers_Limited" Title="Aegis Logistics Limited - Sealord Containers Limited" %>
<%@ Register Src="~/Admin/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="StyleSheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        
         .Req
        {
        	color:Red;
        	vertical-align:top;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <div>
    <table align=center">
        <tr>
            <td width="100px">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <uc1:MyMessageBox ID="Msg" runat="server" ShowCloseButton="true" 
                    Visible="False" />
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:Label ID="Label1" runat="server" Text="Title"></asp:Label>
            </td>
            <td align="left">
                <asp:TextBox ID="txtTitle" runat="server"></asp:TextBox>
                <span class="Req">*</span></td>
        </tr>
        <tr>
            <td align="left">
                <asp:Label ID="Label2" runat="server" Text="Upload Pdf"></asp:Label>
            </td>
            <td align="left">
                <asp:FileUpload ID="FileUpload1" runat="server" />
                <span class="Req">*</span>
                <asp:HiddenField ID="hdnPdf" runat="server" /></td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td align="left">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit"
                    onclick="btnSubmit_Click" />
                    <asp:Button ID="btnCancel" runat="server" onclick="btnCancel_Click" 
                    Text="Cancel" Visible="False" />
            
                <asp:GridView ID="gd" runat="server" AutoGenerateColumns="False" 
                    BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px" 
                    CellPadding="4" onrowdeleting="gd_RowDeleting" onselectedindexchanging="gd_SelectedIndexChanging1" 
                     >
                   
                    <RowStyle BackColor="White" ForeColor="#330099" />
                    <Columns>
                        <asp:BoundField DataField="Pdf_Page_bint" HeaderText="Id" />
                        <asp:BoundField DataField="Pdf_Title_vcr" HeaderText="Pdf Name" />                        
                   <asp:TemplateField HeaderText="Download" >
                        <ItemTemplate>
                            <a href='<%# string.Format("Documents/{0}.pdf",Eval("Pdf_Page_Path_vcr")) %>' target="_blank">Download</a>
                        </ItemTemplate>                        
                        </asp:TemplateField>
                        <asp:CommandField ButtonType="Button" HeaderText="Edit" SelectText="Edit" ShowSelectButton="true" />
                        <asp:CommandField ButtonType="Button" HeaderText="Delete" 
                            ShowDeleteButton="True" />
                    </Columns>
                    <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                    <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
                </asp:GridView>
            
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>

</div>

</asp:Content>


