﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Admin_Default1 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["AdminUserName"].Equals("") || Session["AdminLoginId"].Equals(""))
        {
            Response.Redirect("AegisLogin.aspx");
        }
        else
        {
            if (!Session["CMSName"].Equals(""))
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Changes successfully applied.";
                Session["CMSName"] = "";
            }
            lblAdmin.Text = "Welcome Admin";
        }

    }
}
