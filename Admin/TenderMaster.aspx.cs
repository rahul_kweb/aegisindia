﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class Admin_TenderMaster : System.Web.UI.Page
{
    Class1 c = new Class1();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["AdminUserName"].Equals("") || Session["AdminLoginId"].Equals(""))
        {
            Response.Redirect("AdminLogin.aspx");

        }
        else
        {
            if (!Page.IsPostBack)
            {
                string rcheck = Session["Permission"].ToString();
                if (!c.ShowHideAdminMenu(rcheck, this, "Tender", "Tender"))
                {
                    Response.Redirect("Default.aspx");
                }
                BindGrid();
                BindTitle();
            }
        }
    }

    private void BindTitle()
    {
        try
        {
            DataTable dt = new DataTable();
            c.BindDropDownList(ddlTitle, "EXEC AddUpdateGetTenderTitle 'Get'", "Tender_Title_Id_int", "Tender_Title_vcr");
            ddlTitle.Items.Insert(0, "Select Title");
        }
        catch (Exception ex)
        { 
        
        }
    }

    private void BindGrid()
    {
        try
        {
            gd.Columns[0].Visible = true;
            gd.DataSource = c.Display("EXEC AddUpdateGetTenderMaster 'Get'");
            gd.DataBind();
            gd.Columns[0].Visible = false;
        }
        catch (Exception ex)
        {
            Msg.ShowError("Error occurred while fetching record.");
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string blank = "";
        if (ddlTitle.SelectedIndex < 1)
        {
            blank +=" Select Title,";
        }
        if (FileUpload1.FileName.Equals(""))
        {
            blank +=" Upload Pdf,";
        }
        if (txtDesc.Text.Trim().Equals(""))
        {
            blank += "Type Desciption";
        }
        if (!blank.Equals(""))
        {
            Msg.ShowError("Please " + blank);
        }
        else
        {
            if (btnSubmit.Text.Equals("Submit"))
            {
                AddPdf();

            }
            else if (btnSubmit.Text.Equals("Update"))
            {
                UpdatePdf();

            }
        }
        
    }
    private void AddPdf()
    {
        string opt = btnSubmit.Text;
        try
        {
            int DId = 0;
            string ext = "";

            using (SqlCommand cmd = new SqlCommand("AddUpdateGetTenderMaster", c.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para_vcr", "Add");

                cmd.Parameters.AddWithValue("@Tender_Title_Id_int", int.Parse(ddlTitle.SelectedValue.ToString()));
                cmd.Parameters.AddWithValue("@Tender_path_vcr", "Tender_Pdf");
               // cmd.Parameters.AddWithValue("@Added_Updated_By_Id_bint", int.Parse(Session["AdminLoginId"].ToString()));
                cmd.Parameters.AddWithValue("@Tender_Desc_vcr", txtDesc.Text.Trim());

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                DId = int.Parse(dt.Rows[0][0].ToString());
                string fileName = dt.Rows[0][1].ToString();
                ext = SaveFile(fileName);
            }
            using (SqlCommand cmd1 = new SqlCommand("AddUpdateGetTenderMaster", c.con))
            {
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.AddWithValue("@Para_vcr", "UpdateDocumentExtension");
                cmd1.Parameters.AddWithValue("@Tender_path_vcr", ext);
                cmd1.Parameters.AddWithValue("@Tender_Id_int", DId);
                if (c.con.State == ConnectionState.Closed)
                {
                    c.con.Open();
                }
                cmd1.ExecuteNonQuery();
            }

            txtDesc.Text = "";
            ddlTitle.SelectedIndex=-1;
            BindGrid();

            if (opt.Equals("Submit"))
            {
                Msg.ShowSuccess("Record Added Successfully.");
            }
            else if (opt.Equals("Update"))
            {
                Msg.ShowSuccess("Record Updated Successfully.");
            }

        }
        catch (Exception ex)
        {
            if (opt.Equals("Submit"))
            {
                Msg.ShowError("Some Error Occurred, can not save record" + ex.Message);
            }
            else if (opt.Equals("Update"))
            {
                Msg.ShowError("Some Error Occurred, can not update record");
            }
        }
    }
    private void UpdatePdf()
    {
        try
        {

            string ext1 = "";
            int Id = int.Parse(gd.Rows[gd.SelectedIndex].Cells[0].Text);
            using (SqlCommand cmd = new SqlCommand("AddUpdateGetTenderMaster", c.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para_vcr", "Update");
                cmd.Parameters.AddWithValue("@Tender_Id_int", Id);
                cmd.Parameters.AddWithValue("@Tender_Title_Id_int", int.Parse(ddlTitle.SelectedValue.ToString()));
                cmd.Parameters.AddWithValue("@Tender_Desc_vcr", txtDesc.Text.Trim());
               // cmd.Parameters.AddWithValue("@Added_Updated_By_Id_bint", int.Parse(Session["AdminLoginId"].ToString()));

                string PdffileName = hdnPdf.Value;
                if (FileUpload1.HasFile)
                {
                    ext1 = SaveFile(PdffileName);
                    cmd.Parameters.AddWithValue("@Tender_path_vcr", PdffileName);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@Tender_path_vcr", PdffileName);
                }

                if (!c.con.State.Equals(ConnectionState.Open))
                {
                    c.con.Open();
                }
                cmd.ExecuteNonQuery();
            }

            Reset();
            BindGrid();
            Msg.ShowSuccess("Record Updated Successfully.");

        }
        catch (Exception ex)
        {
            Msg.ShowError("Some error occurred,Can not update record.");
        }
    }
    private string SaveFile(string fl)
    {
        string extensionI1 = System.IO.Path.GetExtension(FileUpload1.FileName);

        if (extensionI1 != "")
        {
            FileUpload1.SaveAs(Server.MapPath("Tender\\" + fl + extensionI1 + ""));
            // FilePath = Server.MapPath("Admin\\Resume\\" + fl + extensionI1);
        }
        return extensionI1;
    }
  
    protected void gd_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            int Id = int.Parse(gd.Rows[e.RowIndex].Cells[0].Text);
            using (SqlCommand cmd = new SqlCommand("AddUpdateGetTenderMaster", c.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para_vcr", "Delete");
                cmd.Parameters.AddWithValue("@Tender_Id_int", Id);
               // cmd.Parameters.AddWithValue("@Added_Updated_By_Id_bint", int.Parse(Session["AdminLoginId"].ToString()));
                if (!c.con.State.Equals(ConnectionState.Open))
                {
                    c.con.Open();
                }

                cmd.ExecuteNonQuery();
                c.con.Close();
                BindGrid();
                Msg.ShowSuccess("Record deleted successfully.");
            }
        }
        catch (Exception ex)
        {
            Msg.ShowError("Error occurred,can not delete record");
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Reset();
    }
    protected void gd_SelectedIndexChanging1(object sender, GridViewSelectEventArgs e)
    {
        Msg.Visible = false;
        try
        {
            int Id = int.Parse(gd.Rows[e.NewSelectedIndex].Cells[0].Text);
            DataTable dt = new DataTable();
            dt = c.Display("EXEC AddUpdateGetTenderMaster 'Get_By_Id'," + Id);
            ddlTitle.SelectedValue = dt.Rows[0]["Tender_Title_Id_int"].ToString();
            txtDesc.Text = dt.Rows[0]["Tender_Desc_vcr"].ToString();
            hdnPdf.Value = dt.Rows[0]["Tender_path_vcr"].ToString();
            btnSubmit.Text = "Update";
            btnCancel.Visible = true;
           
            foreach (GridViewRow gr in gd.Rows)
            {
                gr.Cells[5].Enabled = true;
            }
            gd.Rows[e.NewSelectedIndex].Cells[5].Enabled = false;
        }
        catch (Exception ex)
        {
            Msg.ShowError("Some Error Occurred.");

        }
    }
    private void Reset()
    {
        ddlTitle.SelectedIndex = -1;
        gd.SelectedIndex = -1;
        txtDesc.Text = "";
        btnCancel.Visible = false;
        btnSubmit.Text = "Submit";
        hdnPdf.Value = "";
        foreach (GridViewRow gr in gd.Rows)
        {
            gr.Cells[5].Enabled = true;
        }
    }
}
