﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AegisLogin.aspx.cs" Inherits="Admin_AegisLogin" Title="Aegis - Admin Login" %>
<%@ Register Src="~/Admin/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

 
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Admin Login</title>
    <link href="Css/aegis.css" rel="stylesheet" type="text/css" />
    
    
    <%--<style type="text/css">
        .tbl {
            width: 100%;
        }
        .CenterDiv
        {
        	margin-left:auto;
        	margin-right:auto;
        	width:350px;
        }
        .Req
        {
        	color:Red;
        	font-size:10px;
        	vertical-align: top;
        }
        .style1
        {
        }
        .style2
        {
            height: 23px;
        }
    </style>--%>
    
    
</head>
<body>
    <form id="form1" runat="server">
   
    
    
    
     <div id="main_container" style="width:100%; height:auto; border-top: 1px solid #e4e4e4;">


            
            <div id="logo_admin" style="margin: 25px auto 0px; width:81px;"> <img src="../images/logo.jpg" /></div>
            
        <div id="login_box">        
        
        <div id="pg_head">Admin Login</div>
        <div class="clear">
            </div>
        
        <div class="log_box">               
        Username &nbsp;&nbsp;
        
            <asp:TextBox ID="txtUsername" CssClass="input_log" runat="server"></asp:TextBox>
       	    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"  Display="Dynamic"
                ErrorMessage="*" ControlToValidate="txtUsername"></asp:RequiredFieldValidator>
       	    
       	</div>
        <div class="clear"></div>
        
        <div class="log_box">               
        Password &nbsp;&nbsp;
        
            <asp:TextBox ID="txtPassword" CssClass="input_log" runat="server" 
                TextMode="Password"></asp:TextBox>
       	    <asp:RequiredFieldValidator ID="ReqPwd" runat="server"  Display="Dynamic"
                ErrorMessage="*" ControlToValidate="txtPassword"></asp:RequiredFieldValidator>
       	</div>
        <div class="clear" style="height:10px;"></div>
        
        <div class="log_box">
            <asp:Button ID="Button1" CssClass="log_but"                
                runat="server" Text="Login" onclick="btnLogin_Click" />
        <%--<input type="submit" value="Login" style="width: 80px; height: 30px; border:1px solid #cecece; background-color:#eeeeee; cursor:pointer;"/>--%>
       	</div>
        <div class="clear"></div>
        
        <div class="log_box" style="color:#e9242d; font-size:12px; font-weight:bold;">
            
            <asp:Label ID="lblMsg" runat="server" Visible="false" Text=""></asp:Label>
        
            <%--<uc1:MyMessageBox ID="Msg" runat="server" ShowCloseButton="true" Visible="False" />--%>
        
        </div>
        
		</div>



<div class="clear"></div>
</div>
    </form>
</body>
</html>
