﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="News.aspx.cs" Inherits="Admin_News" Title="Aegis Logistics Limited - News" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Admin/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="StyleSheet.css" rel="stylesheet" type="text/css" />
   
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
         .Req
        {
        	color:Red;
        	vertical-align:top;
        }
    .style2
    {
        height: 28px;
    }
        .style3
        {
            height: 29px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
<div>
     <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <table class="style1">
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <uc1:MyMessageBox ID="Msg" runat="server" ShowCloseButton="true" 
                    Visible="False" />
            </td>
        </tr>
        <tr>
            <td class="style3">
                <asp:Label ID="lblDate" runat="server" Text="Date"></asp:Label>
            </td>
            <td class="style3">
                <asp:TextBox ID="txtCaleder" runat="server"></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender1" runat="server" 
                    TargetControlID="txtCaleder" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <span class="Req">*</span></td>
            </td>
        </tr>
        <tr>
            <td class="style2">
                <asp:Label ID="Label1" runat="server" Text="Title"></asp:Label>
            </td>
            <td class="style2">
                <asp:TextBox ID="txtTitle" runat="server"></asp:TextBox>
                <span class="Req">*</span></td>
        </tr>
         <tr>
            <td>
                <asp:Label ID="lblSubTitle" runat="server" Text="Sub Title"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtSubTitle" runat="server" Height="67px" TextMode="MultiLine" 
                    Width="205px"></asp:TextBox>
                <span class="Req">*</span>
                
                </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="Upload Pdf"></asp:Label>
            </td>
            <td>
                <asp:FileUpload ID="FileUpload1" runat="server" />
                <span class="Req">*</span><asp:HiddenField ID="hdnPdf" runat="server" /></td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" 
                    onclick="btnSubmit_Click" />
                     <asp:Button ID="btnCancel" runat="server" onclick="btnCancel_Click" 
                    Text="Cancel" Visible="False" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:Panel ID="Panel1" ScrollBars="Auto" runat="server" Width="590px">
              
                <asp:GridView ID="gd" runat="server" AutoGenerateColumns="False" 
                    BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px" 
                    CellPadding="4" onrowdeleting="gd_RowDeleting" 
                    onselectedindexchanging="gd_SelectedIndexChanging" AllowPaging="True" 
                        onpageindexchanging="gd_PageIndexChanging">
                    <RowStyle BackColor="White" ForeColor="#330099" />
                    <Columns>
                        <asp:BoundField DataField="News_Id_bint" HeaderText="Id" />
                        <asp:BoundField DataField="Year_int" HeaderText="Year" />
                        <asp:BoundField DataField="News_Date_dtm" HeaderText="Date" />
                        <asp:BoundField DataField="Title_vcr" HeaderText="Title" />
                        <asp:BoundField DataField="SubTitle_vcr" HeaderText="Sub Title" />
                              <asp:TemplateField HeaderText="Download">
                        <ItemTemplate>
                            <a href='<%# string.Format("Documents/{0}.pdf",Eval("Pdf_Path_vcr")) %>' target="_blank">Download</a>
                        </ItemTemplate>                        
                        </asp:TemplateField>
                         <asp:CommandField ButtonType="Button" HeaderText="Edit" SelectText="Edit" 
                             ShowSelectButton="True" />
                        <asp:CommandField ButtonType="Button" HeaderText="Delete" 
                            ShowDeleteButton="True" />
                    </Columns>
                    <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                    <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
                </asp:GridView>
                
                
                  </asp:Panel>
            </td>
        </tr>
        <tr>
            <td style="height:60px;">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>

</div>
</asp:Content>

