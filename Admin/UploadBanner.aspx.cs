﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class Admin_UploadBanner : System.Web.UI.Page
{
    Class1 c = new Class1();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["AdminUserName"].Equals("") || Session["AdminLoginId"].Equals(""))
        {
            Response.Redirect("AdminLogin.aspx");
        }
        else
        {
            if (!Page.IsPostBack)
            {

               
                if (Request.QueryString.Count.Equals(1))
                {
                    string qs = Request.QueryString.Keys[0].ToString();
                    string id = ExtractNumbers(qs);
                    if (id.Length > 0)
                    {
                        lblMId.Text = id;
                       
                        Bind(int.Parse(id));
                       

                    }
                    else
                    {
                        Response.Redirect("Default.aspx");
                    }
                }
                else
                {
                    Response.Redirect("Default.aspx");
                }
                //Bind(id);
            }
        }
    }
    private DataTable GetPageDetails(int Id)
    {
        DataTable dt = new DataTable();
        try
        {
            dt = c.Display("EXEC AddUpdateGetCMS 'Get_By_Menu_Id',0," + Id);

        }
        catch (Exception ex)
        {
            Msg.ShowError("Error Occurred while fetching Details.");
        }
        return dt;
    }
    public static string ExtractNumbers(string expr)
    {
        return string.Join(null, System.Text.RegularExpressions.Regex.Split(expr, "[^\\d]"));
    }
    private void Bind(int id)
    {
        try
        {
            c.BindDropDownList(ddlSubMenu, "EXEC AddUpdategetCMS 'Get_By_Menu_Id',0," + id, "ID_bint", "Page_Name_vcr");
            ddlSubMenu.Items.Insert(0, "Select");
            DataTable dt = new DataTable();
            dt = GetPageDetails(id);
            if (dt.Rows.Count > 0)
            {
                lblTitle.Text = dt.Rows[0]["CMS_Menu_Name_vcr"].ToString();
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        Msg.Visible = false;
        if (ddlSubMenu.SelectedIndex < 1)
        {
            Msg.ShowError("Please Select.");
        }
        else
        {
            if (btnSubmit.Text.Equals("Submit"))
            {
                AddImage();
            }
            else if (btnSubmit.Text.Equals("Update"))
            {
                UpdateImage();
            }
        }
    }
    private void AddImage()
    {
        string opt = btnSubmit.Text;
        try
        {
            int DId = 0;
            string ext = "";

            using (SqlCommand cmd = new SqlCommand("AddUpdateGetInnerBannerMaster", c.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para_vcr", "Add");
                cmd.Parameters.AddWithValue("@Menu_Id_bint", int.Parse(ddlSubMenu.SelectedValue));
                cmd.Parameters.AddWithValue("@Image_vcr", "Inner_Banner_");
                cmd.Parameters.AddWithValue("@Added_Updated_By_Id_bint", int.Parse(Session["AdminLoginId"].ToString()));
                //cmd.Parameters.AddWithValue("@Image_Type_int", 1);

                //if (!c.con.State.Equals(ConnectionState.Open))
                //{
                //    c.con.Open();
                //}
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                DId = int.Parse(dt.Rows[0][0].ToString());
                string fileName = dt.Rows[0][1].ToString();
                ext = SaveFile(fileName);
            }
            using (SqlCommand cmd1 = new SqlCommand("AddUpdateGetInnerBannerMaster", c.con))
            {
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.AddWithValue("@Para_vcr", "UpdateDocumentExtension");
                cmd1.Parameters.AddWithValue("@Image_vcr", ext);
                cmd1.Parameters.AddWithValue("@Banner_Id_bint", DId);
                if (c.con.State == ConnectionState.Closed)
                {
                    c.con.Open();
                }
                cmd1.ExecuteNonQuery();
            }


            // txtTitle.Text = "";
            // txtTitle.Focus();
            // BindGrid();

            if (opt.Equals("Submit"))
            {
                Msg.ShowSuccess("Record Added Successfully.");
                ddlSubMenu.SelectedIndex = 0;
            }
            else if (opt.Equals("Update"))
            {
                Msg.ShowSuccess("Record Updated Successfully.");
                ddlSubMenu.SelectedIndex = 0;
            }

        }
        catch (Exception ex)
        {
            if (opt.Equals("Submit"))
            {
                Msg.ShowError("Some Error Occurred, can not save record" + ex.Message);
            }
            else if (opt.Equals("Update"))
            {
                Msg.ShowError("Some Error Occurred, can not update record");
            }
        }
    }

    private void UpdateImage()
    {
        try
        {

            string ext = "";
            Uri ur = new Uri("http://www.aegis.com/Admin/" + ImgPartner.ImageUrl);

            using (SqlCommand cmd = new SqlCommand("AddUpdateGetInnerBannerMaster", c.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para_vcr", "Update");
                cmd.Parameters.AddWithValue("@Menu_Id_bint", ddlSubMenu.SelectedValue);

                cmd.Parameters.AddWithValue("@Added_Updated_By_Id_bint", int.Parse(Session["AdminLoginId"].ToString()));

                if (FileUpload1.HasFile)
                {
                    string fileName = ur.Segments.Last().Split('.').ElementAt(0);
                    ext = SaveFile(fileName);
                    cmd.Parameters.AddWithValue("@Image_vcr", ext);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@Image_vcr", ur.Segments.Last());
                }



                if (!c.con.State.Equals(ConnectionState.Open))
                {
                    c.con.Open();
                }
                //cmd.ExecuteNonQuery();
            }
            //gd.SelectedIndex = -1;
            //BindGrid();
            //txtTitle.Text = "";
            ImgPartner.Visible = false;
            btnCancel.Visible = false;
            btnSubmit.Text = "Submit";
            Msg.ShowSuccess("Record Updated Successfully.");
            ddlSubMenu.SelectedIndex = 0;
            //txtTitle.Focus();

        }
        catch (Exception ex)
        {
            Msg.ShowError("Some error occurred,Can not update record.");
        }
    }
    private string SaveFile(string fl)
    {
        string extensionI1 = System.IO.Path.GetExtension(FileUpload1.FileName);

        if (extensionI1 != "")
        {
            FileUpload1.SaveAs(Server.MapPath("images\\" + fl + extensionI1 + ""));

        }
        return fl + extensionI1;
    }


    protected void btnCancel_Click(object sender, EventArgs e)
    {
        //txtTitle.Text = "";
        ImgPartner.ImageUrl = null;
        ImgPartner.Visible = false;
        // gd.SelectedIndex = -1;
        //txtTitle.Focus();
        btnCancel.Visible = false;
    }
    protected void ddlSubMenu_SelectedIndexChanged(object sender, EventArgs e)
    {
        Msg.Visible = false;
        //if (gd.SelectedIndex.Equals(-1))
        //{

        try
        {
           
          
 DataTable dt = new DataTable();
                dt = c.Display("EXEC AddUpdateGetInnerBannerMaster 'Check_By_Category',0,'',0," + int.Parse(ddlSubMenu.SelectedValue));

                if (dt.Rows.Count > 0)
                {
                    btnSubmit.Text = "Update";
                    int Id = int.Parse(ddlSubMenu.SelectedValue);
                    DataTable dt1 = new DataTable();
                    dt1 = c.Display("EXEC AddUpdateGetInnerBannerMaster 'Get_By_Id',0,'',0," + Id);
                    string photo = dt1.Rows[0]["Image_vcr"].ToString();
                    if (dt1.Rows.Count > 0)
                    {
                        //trBannerImg.Attributes["style"] = "";
                        ImgPartner.Visible = true;
                        ImgPartner.ImageUrl = "~/Admin/images/" + photo;
                    }
                    else
                    {
                        //trBannerImg.Attributes["style"] = "display:none;";
                        ImgPartner.Visible = false;
                    }

                }
          
            else
            {
                ImgPartner.Visible = false;
                btnSubmit.Text = "Submit";
                btnSubmit.Enabled = true;
                btnCancel.Visible = false;
            }

        }
        catch (Exception ex)
        {

        }
        //}
    }
}
