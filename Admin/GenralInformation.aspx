﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="GenralInformation.aspx.cs" Inherits="Admin_GenralInformation" Title="Aegis Logistics Limited - General Information" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register Src="~/Admin/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="StyleSheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
   
    <div>
&nbsp;<table class="style1">
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <uc1:MyMessageBox ID="Msg" runat="server" ShowCloseButton="true" 
                    Visible="False" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td valign="top">
                <asp:Label ID="Label1" runat="server" Text="Notice"></asp:Label>
            </td>
            <td>
                <cc1:Editor ID="txtNotice" runat="server" Height="500px" />
              </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:Button ID="btnSubmit" runat="server" Text="Save Notice" 
                    onclick="Button1_Click" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:GridView ID="gd" runat="server" AutoGenerateColumns="False" 
                    BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px" 
                    CellPadding="4" AllowPaging="True" 
                    onpageindexchanging="gd_PageIndexChanging" PageSize="15" 
                    onselectedindexchanging="gd_SelectedIndexChanging">
                    <RowStyle BackColor="White" ForeColor="#330099" />
                    <Columns>
                        <asp:BoundField DataField="Notice_Id_bint" HeaderText="Id" />
                        <asp:TemplateField HeaderText="Notice">
                        <ItemTemplate>
                            <asp:Label ID="lblContent" runat="server" Text='<%#Eval("Notice_ntxt") %>'></asp:Label>                        
                        </ItemTemplate>                        
                        </asp:TemplateField>
                        <asp:CommandField ButtonType="Button" HeaderText="Edit" SelectText="Edit" 
                            ShowSelectButton="True" />
                        <asp:CommandField ButtonType="Button" HeaderText="Delete" 
                            ShowDeleteButton="True" />
                    </Columns>
                    <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                    <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td style="height:60px;">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</div>

</asp:Content>

