﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="Jobs.aspx.cs" Inherits="Jobs" Title="Aegis Logistics Limited - Jobs" %>
<%@ Register Src="~/Admin/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="StyleSheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
          .tbl
        {
            width: 100%;
            border: solid 1px gray;             
        }
        .style3
        {
        }
        .Req
        {
        	color:Red;
        	vertical-align:top;
        }
        .style4
        {
        	
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

    <table class="tbl">
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
               <uc1:MyMessageBox  ID="Msg" runat="server" ShowCloseButton="true" Visible="False" /></td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Job Title"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtJobTitle" runat="server" Width="337px"></asp:TextBox>
                <span class="Req" style="vertical-align:top;">*</span>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="Location"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtLocation" runat="server" Width="337px"></asp:TextBox>
                <span class="Req" style="vertical-align:top;">*</span></td>
        </tr>
        <tr>
            <td valign="top">
                <asp:Label ID="Label3" runat="server" Text="Description"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtDesc" runat="server" Height="184px" TextMode="MultiLine" 
                    Width="337px"></asp:TextBox>
                <span class="Req" style="vertical-align:top;">*</span></td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:Button ID="btnSubmit" runat="server" onclick="btnSubmit_Click" 
                    Text="Submit" />
&nbsp;
                <asp:Button ID="btnReset" runat="server" Text="Reset" 
                    onclick="btnReset_Click" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:GridView ID="gv" runat="server" AutoGenerateColumns="False" 
                    BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px" 
                    CellPadding="4" onpageindexchanging="gv_PageIndexChanging" 
                    onrowdeleting="gv_RowDeleting" 
                    onselectedindexchanging="gv_SelectedIndexChanging">
                    <RowStyle BackColor="White" ForeColor="#330099" />
                    <Columns>
                        <asp:BoundField DataField="Job_Id_bint" HeaderText="Id" />
                        <asp:BoundField DataField="title_vcr" HeaderText="Title" />
                        <asp:BoundField DataField="location_vcr" HeaderText="Location" />
                        <asp:BoundField DataField="Desc_vcr" HeaderText="Description" />
                        <asp:BoundField DataField="Status" HeaderText="Job Status" />
                        <asp:CommandField ButtonType="Button" HeaderText="Edit" SelectText="Edit" 
                            ShowSelectButton="True" />
                        <asp:CommandField ButtonType="Button" DeleteText="Hide Job" HeaderText="Hide" 
                            ShowDeleteButton="True" />
                    </Columns>
                    
                    <EmptyDataTemplate>
                    <table cellspacing="0" cellpadding="4" rules="all" border="1" style="width:600px;background-color:White;border-color:#3366CC;border-width:1px;border-style:None;border-collapse:collapse;">
		            <tbody><tr style="color:#CCCCFF;background-color:#003399;font-weight:bold;">
			            <th scope="col">No Record Found</th>
		            </tr>
		            <tr style="color:#003399;background-color:White;">
			        <td>
			        
			        </td>
		             </tr>
	</tbody></table>
                    
                    </EmptyDataTemplate>
                    <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                    <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
                </asp:GridView>
            </td>
        </tr>
    </table>

</asp:Content>

