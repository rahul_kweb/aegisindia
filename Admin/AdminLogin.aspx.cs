﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Drawing;
using System.Data.SqlClient;

public partial class Admin_AdminLogin : System.Web.UI.Page
{
    Class1 c = new Class1();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtUsername.Focus();
        }
    }
    
    private bool CheckBlank()
    {
        bool blank = false;
        if (txtUsername.Text.Trim().Equals(""))
        {
            blank = true;
        }
        if (txtPassword.Text.Trim().Equals(""))
        {
            blank = true;
        }
        return blank;
    }  
    private int CheckLogin()
    {
        int success = 0;
        try
        {
            using (SqlCommand cmd = new SqlCommand("AddUpdateGetAdminLoginMaster", c.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para_vcr", "Get_By_User_Name");
                cmd.Parameters.AddWithValue("@User_Name_vcr", txtUsername.Text.Trim());
                cmd.Parameters.AddWithValue("@Password_vcr", txtPassword.Text.Trim());
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    Session["AdminLoginId"] = dt.Rows[0][0].ToString();
                    Session["Permission"] = dt.Rows[0][1].ToString();
                    
                    success = 1;
                }
            }
        }
        catch (Exception ex)
        {
            success = -1;           
        }
        return success;
    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        if (CheckBlank())
        {
            lblMsg.Visible = true;
            lblMsg.Text="Please fill required fields.";
        }
        else
        {
            int Valid = CheckLogin();
            if (Valid.Equals(1))
            {
                Session["AdminUserName"] = txtUsername.Text.Trim();
                Response.Redirect("Default.aspx");
            }
            else if (Valid.Equals(0))
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Incorrect Username or Password. Please try again.";
            }
            else
            {
                lblMsg.Visible = true;
                lblMsg.Text="Some Error Occurred.";
            }
        } 
    }
}
