﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class Admin_UploadPdf : System.Web.UI.Page
{
    Class1 c = new Class1();
    protected void Page_Load(object sender, EventArgs e)
    {
       
         if (Session["AdminUserName"].Equals("") || Session["AdminLoginId"].Equals(""))
        {
            Response.Redirect("AdminLogin.aspx");
        }
        else
        {
            if (!Page.IsPostBack)
            {
                string rcheck = Session["Permission"].ToString();
                if (!c.ShowHideAdminMenu(rcheck, this, "CMS", "CMS"))
                {
                    Response.Redirect("Default.aspx");
                }
                 Bind();
            }
        }
    }
        
       
    

    private void Bind()
    {
        DataTable dt = new DataTable();
        dt = c.Display("EXEC AddUpdateGetPdfPage 'Get_By_Master_Id',0,16");
        HyperLink1.NavigateUrl="Documents/"+dt.Rows[0]["Pdf_Page_Path_vcr"].ToString()+".pdf";
        hdnPdf.Value =  dt.Rows[0]["Pdf_Page_Path_vcr"].ToString() ;

        //<%# string.Format("Admin/Documents/{0}.pdf",Eval("Pdf_Page_Path_vcr")) %>'
    }
    private void UpdatePdf()
    {
        try
        {

            string ext1 = "";
            // Uri ur = new Uri("http://www.Horizon.com/Admin/" + ImgPartner.ImageUrl);
            //int Id = int.Parse(gd.Rows[gd.SelectedIndex].Cells[0].Text);

            //int Id = int.Parse(gd.Rows[gd.SelectedIndex].Cells[0].Text);
            using (SqlCommand cmd = new SqlCommand("AddUpdateGetPdfPage", c.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para_vcr", "Update");
                cmd.Parameters.AddWithValue("@Pdf_Page_bint", 121);
                cmd.Parameters.AddWithValue("@Pdf_Title_vcr", "HSF Policy");
                cmd.Parameters.AddWithValue("@Added_Updated_By_Id_bint", int.Parse(Session["AdminLoginId"].ToString()));

                string PdffileName = hdnPdf.Value;
                if (FileUpload1.HasFile)
                {
                    ext1 = SaveFile(PdffileName);
                    cmd.Parameters.AddWithValue("@Pdf_Page_Path_vcr", PdffileName);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@Pdf_Page_Path_vcr", PdffileName);
                }

                if (!c.con.State.Equals(ConnectionState.Open))
                {
                    c.con.Open();
                }
                cmd.ExecuteNonQuery();
            }

            //Reset();
            //BindGrid();
            Msg.ShowSuccess("Record Updated Successfully.");

        }
        catch (Exception ex)
        {
            Msg.ShowError("Some error occurred,Can not update record.");
        }
    }
    private string SaveFile(string fl)
    {
        string extensionI1 = System.IO.Path.GetExtension(FileUpload1.FileName);

        if (extensionI1 != "")
        {
            FileUpload1.SaveAs(Server.MapPath("Documents\\" + fl + extensionI1 + ""));
            // FilePath = Server.MapPath("Admin\\Resume\\" + fl + extensionI1);
        }
        return extensionI1;
    }
    protected void btnSubmit_Click1(object sender, EventArgs e)
    {
        if (FileUpload1.FileName.Equals(""))
        {
            Msg.ShowError("Please Upload File");
        }
        else
        {
            UpdatePdf();
        }
    }
}
