﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="ContactMaster.aspx.cs" Inherits="ContactMaster" Title="Aegis Logistics Limited - Contact Page" %>
<%@ Register Src="~/Admin/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit.HTMLEditor" tagprefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="StyleSheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style2
        {
            width: 100%;
        }
        .style3
        {
        }
        .Req
        {
        	color:Red;
        	vertical-align:top;
        }
        .style4
        {
        	
        }
        textarea
        {
        	resize:none;
       	}
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    
    <table class="style2">
        <tr>
            <td class="style3" colspan="3">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style3">
                &nbsp;</td>
            <td valign="top" class="style4">
                <uc1:MyMessageBox ID="Msg" runat="server" ShowCloseButton="true" 
                    Visible="False" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        
        <tr>
            <td class="style3" valign="top" align="center" colspan="3">
                <asp:Label ID="Label10" runat="server" Font-Size="13pt" 
                    Text="Registered Office Details"></asp:Label>
            </td>
        </tr>
        
        <tr>
            <td class="style3" valign="top">
                <asp:Label ID="Label1" runat="server" Text="Address"></asp:Label>
            </td>
            <td valign="top" class="style4">
                <asp:TextBox ID="txtRegOffice" runat="server" Height="93px" 
                    TextMode="MultiLine" Width="254px" MaxLength="800"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ErrorMessage="Type Registered Office Address" 
                    ControlToValidate="txtRegOffice"></asp:RequiredFieldValidator>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        
        <tr>
            <td class="style3" valign="top">
                <asp:Label ID="Label3" runat="server" Text="Phone No."></asp:Label>
            </td>
            <td valign="top" class="style4">
                <asp:TextBox ID="txtRegPhone" runat="server" MaxLength="15"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        
        <tr>
            <td class="style3" valign="top">
                <asp:Label ID="Label4" runat="server" Text="Fax  No."></asp:Label>
            </td>
            <td valign="top" class="style4">
                <asp:TextBox ID="txtRegFaxNo" runat="server" MaxLength="15"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        
        <tr>
            <td class="style3" colspan="3">
                <hr />
                </td>
           
            
        </tr>
        
        <tr>
            <td class="style3" valign="top" align="center" colspan="3">
                <asp:Label ID="Label11" runat="server" Font-Size="13pt" 
                    Text="Corporate Office Details"></asp:Label>
            </td>
        </tr>
        
        <tr>
            <td class="style3" valign="top">
                <asp:Label ID="Label5" runat="server" Text="Address"></asp:Label>
            </td>
            <td valign="top" class="style4">
                <asp:TextBox ID="txtCorpOffice" runat="server" Height="93px" TextMode="MultiLine" 
                    Width="254px"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        
        <tr>
            <td class="style3">
                <asp:Label ID="Label6" runat="server" Text="Phone No."></asp:Label>
            </td>
            <td valign="top" class="style4">
                <asp:TextBox ID="txtCorpPhone" runat="server"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        
        <tr>
            <td class="style3">
                <asp:Label ID="Label7" runat="server" Text="Fax No."></asp:Label>
            </td>
            <td valign="top" class="style4">
                <asp:TextBox ID="txtCorpFaxNo" runat="server"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        
        <tr>
            <td class="style3" colspan="3">
            <hr /></td>
           
        </tr>
        
        <tr>
            <td class="style3" valign="top" align="center" colspan="3">
                <asp:Label ID="Label12" runat="server" Font-Size="13pt" 
                    Text="Terminal Office Details"></asp:Label>
            </td>
        </tr>
        
        <tr>
            <td class="style3" valign="top">
                <asp:Label ID="Label8" runat="server" Text="Address"></asp:Label>
            </td>
            <td valign="top" class="style4">
                <asp:TextBox ID="txtTerminalAddress" runat="server" Height="93px" TextMode="MultiLine" 
                    Width="254px"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        
        <tr>
            <td class="style3" valign="top">
                <asp:Label ID="Label9" runat="server" Text="Phone No."></asp:Label>
            </td>
            <td valign="top" class="style4">
                <asp:TextBox ID="txtTerminalPhone" runat="server"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        
        <tr>
            <td class="style3">
                <asp:Label ID="Label2" runat="server" Text="Fax No."></asp:Label>
            </td>
            <td valign="top" class="style4">
                <asp:TextBox ID="txtTerminalFax" runat="server"></asp:TextBox>
                
            </td>
            <td>
                &nbsp;</td>
        </tr>
        
        
        <tr>
            <td class="style3" colspan="3">
            <hr /></td>
           
        </tr>
        
        <tr>
            <td class="style3" valign="top" align="center" colspan="3">
                <asp:Label ID="Label13" runat="server" Font-Size="13pt" 
                    Text="Investor corner"></asp:Label>
            </td>
        </tr>
        
        <tr>
            <td class="style3" valign="top">
                <asp:Label ID="Label14" runat="server" Text="Content"></asp:Label>
            </td>
            <td valign="top" class="style4">
            <asp:Label ID="lblId" runat="server" Visible="False"></asp:Label>
               <cc1:Editor ID="txtDesc" runat="server" Height="350px" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        
       
        <tr>
            <td class="style3">
                &nbsp;</td>
            <td class="style4">
                <asp:Button ID="btnSubmit" runat="server" style="width: 60px; height: 25px; border:1px solid #cecece; background-color:#eeeeee;" onclick="btnSubmit_Click" 
                    Text="Submit" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        
        <tr>
            <td class="style3">
                &nbsp;</td>
            <td class="style4">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        
        <tr>
            <td class="style3">
                &nbsp;</td>
            <td class="style4">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        
        <tr>
            <td class="style3">
                &nbsp;</td>
            <td class="style4">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        
        <tr>
            <td class="style3" align="center" colspan="3">
                <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Width="750px">
                    <asp:HiddenField ID="hdnContactId" runat="server" />
                
                </asp:Panel>
            </td>
        </tr>
        
    </table>

</asp:Content>

