﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

public partial class Admin_Admin : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            binduser();
        }
    }

    protected void btnLogout_Click(object sender, EventArgs e)
    {
        Session["AdminUserName"] = "";
        Session["AdminLoginId"] = "";
        Response.Redirect("AdminLogin.aspx");
    }

    private void binduser()
    {
        string body = string.Empty;

        string rcheck = Session["Permission"].ToString();
        //cms
        if ((rcheck.Contains("CMS") || rcheck.Contains("ADMIN")))
        {
            CMS.Visible = true;

        }
        else
        {

            CMS.Visible = false;
        }
        //Contact
        if ((rcheck.Contains("Contact") || rcheck.Contains("ADMIN")))
        {
            Contact.Visible = true;

        }
        else
        {

            Contact.Visible = false;
        }
        //Career
        if ((rcheck.Contains("Career") || rcheck.Contains("ADMIN")))
        {
            Career.Visible = true;
            

        }
        else
        {

            Career.Visible = false;
           

        }
        // ContactusBanner
        if ((rcheck.Contains("ContactusBanner") || rcheck.Contains("ADMIN")))
        {
            ContactusBanner.Visible = true;

        }
        else
        {

            ContactusBanner.Visible = false;

        }
       
        //News
        if ((rcheck.Contains("News") || rcheck.Contains("ADMIN")))
        {
            News.Visible = true;

        }
        else
        {

            News.Visible = false;
        }
        //Inquiries
        if ((rcheck.Contains("Inquiries") || rcheck.Contains("ADMIN")))
        {
            Inquiries.Visible = true;

        }
        else
        {
            Inquiries.Visible = false;
        }
        //UploadImages
        if ((rcheck.Contains("UploadImages") || rcheck.Contains("ADMIN")))
        {
            UploadImages.Visible = true;

        }
        else
        {

            UploadImages.Visible = false;
        }
        //IntranetManager
        if ((rcheck.Contains("IntranetManager") || rcheck.Contains("ADMIN")))
        {
            IntranetManager.Visible = true;

        }
        else
        {

            IntranetManager.Visible = false;
        }
        //Tender
        if ((rcheck.Contains("Tender") || rcheck.Contains("ADMIN")))
        {
            Tender.Visible = true;

        }
        else
        {

            Tender.Visible = false;
        }
        //ChangePwd
        if ((rcheck.Contains("ChangePwd") || rcheck.Contains("ADMIN")))
        {
            ChangePwd.Visible = true;

        }
        else
        {

            ChangePwd.Visible = false;
        }


        //if (rcheck.Contains("ADMIN"))
        //{
        //    Admin.Visible = true;

        //}
        //else
        //{

        //    Admin.Visible = false;
        //}



    }
}
