﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="TenderMaster.aspx.cs" Inherits="Admin_TenderMaster" Title="Aegis Logistics Limited - Tender Master" %>
<%@ Register Src="~/Admin/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
  <link href="StyleSheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        
         .Req
        {
        	color:Red;
        	vertical-align:top;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
   
<div>
    <table align=center">
        <tr>
            <td width="100px">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <uc1:MyMessageBox ID="Msg" runat="server" ShowCloseButton="true" 
                    Visible="False" />
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:Label ID="Label1" runat="server" Text="Title"></asp:Label>
            </td>
            <td align="left">
                <%--<asp:TextBox ID="txtTitle" runat="server"></asp:TextBox>--%>
                <asp:DropDownList ID="ddlTitle" runat="server" AutoPostBack="true">
                </asp:DropDownList>
                <span class="Req">*<%--<asp:RequiredFieldValidator ID="ReqTitle" runat="server" 
                    ControlToValidate="txtTitle" ErrorMessage="Please Type Title For Pdf" 
                    SetFocusOnError="True"></asp:RequiredFieldValidator>--%></span></td>
        </tr>
        <tr>
            <td align="left">
                <asp:Label ID="Label2" runat="server" Text="Upload Pdf"></asp:Label>
            </td>
            <td align="left">
                <asp:FileUpload ID="FileUpload1" runat="server" />
                <span class="Req">*</span><asp:HiddenField ID="hdnPdf" runat="server" /></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label3" runat="server" Text="Description"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtDesc" runat="server" TextMode="MultiLine" Height="135px" 
                    Width="645px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td align="left">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit"
                    onclick="btnSubmit_Click" />
                    <asp:Button ID="btnCancel" runat="server"
                    Text="Cancel" Visible="False" onclick="btnCancel_Click" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:GridView ID="gd" runat="server" AutoGenerateColumns="False" 
                    BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px" 
                    CellPadding="4" onrowdeleting="gd_RowDeleting" 
                    onselectedindexchanging="gd_SelectedIndexChanging1" >
                   
                    <RowStyle BackColor="White" ForeColor="#330099" />
                    <Columns>
                        <asp:BoundField DataField="Tender_Id_int" HeaderText="Id" />
                        <asp:BoundField DataField="Tender_Title_vcr" HeaderText="Title" />
                        <asp:BoundField DataField="Tender_Desc_vcr" HeaderText="Description" />
                        <asp:TemplateField HeaderText="Download" >
                        <ItemTemplate>
                            <a href='<%# string.Format("Tender/{0}.pdf",Eval("Tender_path_vcr")) %>' target="_blank">Download</a>
                        </ItemTemplate>                        
                        </asp:TemplateField>
                      <asp:CommandField ButtonType="Button" HeaderText="Edit" SelectText="Edit" 
                             ShowSelectButton="True" />
                        <asp:CommandField ButtonType="Button" HeaderText="Delete" 
                            ShowDeleteButton="True" />
                    </Columns>
                    <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                    <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
                </asp:GridView>
            </td>
        </tr>
    </table>

</div>

</asp:Content>

