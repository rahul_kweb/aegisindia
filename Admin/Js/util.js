﻿

function onlyNumbers(evt)
{
    var e = event || evt; // for trans-browser compatibility
    var charCode = e.which || e.keyCode;
   
    if (charCode > 31 && (charCode < 48 || charCode > 57))
    
        return false;

    return true;
  

}

function onlyNumbersAndSplChar(evt)
{
    var e = event || evt; // for trans-browser compatibility
    var charCode = e.which || e.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57))
    {
        if((charCode == 45) || (charCode == 40) || (charCode == 41))
        {
             return true;
        }
        else
        {
            return false;
        }
    }
else
{
    return true;
    }

}

 function dateSelectionChanged(sender, args)
    {
        if(sender.get_selectedDate().getDay() == 0)
        {
          sender._textbox.set_Value('');
          alert('You can not select sunday.');
        }
    }