﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class Admin_EmployeeReg : System.Web.UI.Page
{
    Class1 c = new Class1();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["AdminUserName"].Equals("") || Session["AdminLoginId"].Equals(""))
        {
            Response.Redirect("AdminLogin.aspx");
        }
        else
        {
            if (!Page.IsPostBack)
            {
                string rcheck = Session["Permission"].ToString();
                if (!c.ShowHideAdminMenu(rcheck, this, "IntranetManager", "IntranetManager"))
                {
                    Response.Redirect("Default.aspx");
                }
                BindGrid();
            }
        } 
    }
    private void BindGrid()
    {
        Msg.Visible = false;
        try
        {
            gd.Columns[0].Visible = true;
            gd.DataSource = c.Display("EXEC AddUpdateGetEmployeeMaster 'Get'");
            gd.DataBind();
            gd.Columns[0].Visible = false;

        }
        catch (Exception ex)
        {
            Msg.ShowError("Error occurred while fetching record.");
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        Msg.Visible = false;
        if (!txtEmployeeName.Text.Trim().Equals(""))
        {
            if (AddEmployee().Equals(1))
            {
                BindGrid();
                txtEmployeeName.Text = "";
                RdbGender.SelectedIndex = 0;
                Msg.ShowSuccess("Record Added Successfully.");               
            }
            else
            {
                Msg.ShowError("Some error occurred,can not add record.");
            }
        }        
    }
    private int AddEmployee()
    {
        int Added = 0;
        try
        {
            using (SqlCommand cmd = new SqlCommand("AddUpdateGetEmployeeMaster", c.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para_vcr", "Add");                
                cmd.Parameters.AddWithValue("@Employee_Name_vcr",txtEmployeeName.Text.Trim());
                cmd.Parameters.AddWithValue("@Employee_Gender_vcr",RdbGender.SelectedItem.Text);
                cmd.Parameters.AddWithValue("@user_Id_vcr","");
                cmd.Parameters.AddWithValue("@pwd_vcr", "");
                cmd.Parameters.AddWithValue("@Added_Updated_By_Id_int",int.Parse(Session["AdminLoginId"].ToString()));
                if (!c.con.State.Equals(ConnectionState.Open))
                {
                    c.con.Open();
                }

              Added = cmd.ExecuteNonQuery();
              c.con.Close();

            }

        }
        catch (Exception ex)
        {
            Added = 0;
        }
        return Added;
    }
    protected void gd_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Msg.Visible = false;
        try
        {
            gd.PageIndex = e.NewPageIndex;
            BindGrid();
        }
        catch (Exception ex)
        {
            Msg.ShowError("Error occurred.");
        }
    }
    protected void gd_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            using (SqlCommand cmd = new SqlCommand("AddUpdateGetEmployeeMaster", c.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para_vcr","Delete");
                cmd.Parameters.AddWithValue("@Employee_Id_bint",int.Parse(gd.Rows[e.RowIndex].Cells[0].Text));
                cmd.Parameters.AddWithValue("@Added_Updated_By_Id_int",int.Parse(Session["AdminLoginId"].ToString()));
                if (!c.con.State.Equals(ConnectionState.Open))
                {
                    c.con.Open();
                }
                cmd.ExecuteNonQuery();
                c.con.Close();
                BindGrid();
                Msg.ShowSuccess("Record Deleted Successfully.");
            }
        }
        catch (Exception ex)
        {
            Msg.ShowError("Error occurred.");
        }
    }
    protected void gd_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gd.EditIndex = -1;
        BindGrid();
    }
    protected void gd_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            gd.EditIndex = e.NewEditIndex;
            string gender = ((Label)gd.Rows[e.NewEditIndex].Cells[2].Controls[1]).Text;
            hdnEmpId.Value = ((Label)gd.Rows[e.NewEditIndex].Cells[2].Controls[3]).Text;
            BindGrid();
            if (gender.Equals("Male"))
            {
                ((RadioButtonList)gd.Rows[e.NewEditIndex].Cells[2].FindControl("rdbGridGender")).SelectedIndex = 0;
            }
            else
            {
                ((RadioButtonList)gd.Rows[e.NewEditIndex].Cells[2].FindControl("rdbGridGender")).SelectedIndex = 1;
            }
        }
        catch (Exception ex)
        {
            Msg.ShowError("Some error occurred.Do not update record,Contact to your administrator.");

        }
    }
    protected void gd_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            using (SqlCommand cmd = new SqlCommand("AddUpdateGetEmployeeMaster", c.con))
            {
                string name=((TextBox)gd.Rows[e.RowIndex].Cells[1].Controls[0]).Text;
                string pwd = ((TextBox)gd.Rows[e.RowIndex].Cells[4].Controls[0]).Text;
                string Gender =((RadioButtonList) gd.Rows[e.RowIndex].Cells[2].FindControl("rdbGridGender")).SelectedItem.Text;
                int Id=int.Parse(hdnEmpId.Value);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para_vcr", "Update");
                cmd.Parameters.AddWithValue("@Employee_Id_bint", Id);
                cmd.Parameters.AddWithValue("@Employee_Name_vcr", name);
                cmd.Parameters.AddWithValue("@Employee_Gender_vcr", Gender);
                cmd.Parameters.AddWithValue("@pwd_vcr", pwd);

                cmd.Parameters.AddWithValue("@Added_Updated_By_Id_int", int.Parse(Session["AdminLoginId"].ToString()));                
                if (!c.con.State.Equals(ConnectionState.Open))
                {
                    c.con.Open();
                }
                cmd.ExecuteNonQuery();
                c.con.Close();
                hdnEmpId.Value = "0";                
                gd.EditIndex = -1;                
                BindGrid();
                Msg.ShowSuccess("Record Updated Successfully.");
            }
        }
        catch (Exception ex)
        {
            Msg.ShowError("Some error occurred,can not update record.");

        }
            
    }
}
