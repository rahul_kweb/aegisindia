﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class Admin_News : System.Web.UI.Page
{
    Class1 c = new Class1();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["AdminUserName"].Equals("") || Session["AdminLoginId"].Equals(""))
        {
            Response.Redirect("AdminLogin.aspx");

        }
        else
        {
            if (!Page.IsPostBack)
            {
                string rcheck = Session["Permission"].ToString();
                if (!c.ShowHideAdminMenu(rcheck, this, "News", "News"))
                {
                    Response.Redirect("Default.aspx");
                }
                BindGrid();
            }
        }
    }
    private void BindGrid()
    {
        try
        {
            gd.Columns[0].Visible = true;
            gd.DataSource = c.Display("EXEC AddUpdateGetNewsMaster 'Get'");
            gd.DataBind();
            gd.Columns[0].Visible = false;
        }
        catch (Exception ex)
        {
            Msg.ShowError("Error occurred while fetching record.");
        }
    }
    private string CheckBlank()
    {
        string blank = "";
        if (txtCaleder.Text.Trim().Equals(""))
        {
            blank = " Type Date ,";
        }
        if (txtTitle.Text.Trim().Equals(""))
        {
            blank += " Type Title ,";
        }
        if (txtSubTitle.Text.Trim().Equals(""))
        {
            blank += " Type SubTitle ,";
        }
        if (!FileUpload1.HasFile)
        {
            blank += " Upload Pdf ";
        }
        if (!blank.Equals(""))
        {
            blank = blank.Remove(blank.Length - 1);
        }
        return blank;
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string blank = CheckBlank();
        if (blank != "")
        {
            Msg.Visible = true;
            Msg.ShowError("Please" + blank);
        }
        else
        {
            if (btnSubmit.Text.Equals("Submit"))
            {
                AddPdf();

            }
            else if (btnSubmit.Text.Equals("Update"))
            {
                UpdatePdf();

            }
        }
    }
    private void AddPdf()
    {
        string opt = btnSubmit.Text;
        try
        {
            int DId = 0;
            string ext = "";
            string year = DateTime.ParseExact(txtCaleder.Text, "dd/MM/yyyy", null).Year.ToString();
            using (SqlCommand cmd = new SqlCommand("AddUpdateGetNewsMaster", c.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para_vcr", "Add");
                cmd.Parameters.AddWithValue("@News_Date_dtm", DateTime.ParseExact(txtCaleder.Text, "dd/MM/yyyy", null).ToString("MM/dd/yyyy"));
                cmd.Parameters.AddWithValue("@Title_vcr", txtTitle.Text.Trim());
                cmd.Parameters.AddWithValue("@SubTitle_vcr", txtSubTitle.Text.Trim());
                cmd.Parameters.AddWithValue("@Pdf_Path_vcr", "News_"+txtCaleder.Text.Replace('/','_'));
                cmd.Parameters.AddWithValue("@Added_Updated_By_Id_int", int.Parse(Session["AdminLoginId"].ToString()));
                
                

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                DId = int.Parse(dt.Rows[0][0].ToString());
                string fileName = dt.Rows[0][1].ToString();
                ext = SaveFile(fileName);
            }
            using (SqlCommand cmd1 = new SqlCommand("AddUpdateGetNewsMaster", c.con))
            {
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.AddWithValue("@Para_vcr", "UpdateDocumentExtension");
                cmd1.Parameters.AddWithValue("@Pdf_Path_vcr", ext);
                cmd1.Parameters.AddWithValue("@News_Id_bint", DId);
                if (c.con.State == ConnectionState.Closed)
                {
                    c.con.Open();
                }
                cmd1.ExecuteNonQuery();
            }

            Reset();
            BindGrid();

            if (opt.Equals("Submit"))
            {
                Msg.ShowSuccess("Record Added Successfully.");
            }
            else if (opt.Equals("Update"))
            {
                Msg.ShowSuccess("Record Updated Successfully.");
            }

        }
        catch (Exception ex)
        {
            if (opt.Equals("Submit"))
            {
                Msg.ShowError("Some Error Occurred, can not save record");
            }
            else if (opt.Equals("Update"))
            {
                Msg.ShowError("Some Error Occurred, can not update record");
            }
        }
    }
    private void UpdatePdf()
    {
        try
        {

            string ext1 = "";
            // Uri ur = new Uri("http://www.Horizon.com/Admin/" + ImgPartner.ImageUrl);

            int Id = int.Parse(gd.Rows[gd.SelectedIndex].Cells[0].Text);
            using (SqlCommand cmd = new SqlCommand("AddUpdateGetNewsMaster", c.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para_vcr", "Update");
                cmd.Parameters.AddWithValue("@News_Id_bint", Id);
                cmd.Parameters.AddWithValue("@News_Date_dtm", DateTime.ParseExact(txtCaleder.Text, "dd/MM/yyyy", null).ToString("MM/dd/yyyy"));
                cmd.Parameters.AddWithValue("@Title_vcr", txtTitle.Text.Trim());
                cmd.Parameters.AddWithValue("@SubTitle_vcr", txtSubTitle.Text.Trim());
                cmd.Parameters.AddWithValue("@Added_Updated_By_Id_int", int.Parse(Session["AdminLoginId"].ToString()));

                string PdffileName = hdnPdf.Value;
                if (FileUpload1.HasFile)
                {
                    ext1 = SaveFile(PdffileName);
                    cmd.Parameters.AddWithValue("@Pdf_Path_vcr", PdffileName);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@Pdf_Path_vcr", PdffileName);
                }

                if (!c.con.State.Equals(ConnectionState.Open))
                {
                    c.con.Open();
                }
                cmd.ExecuteNonQuery();
            }

            Reset();
            BindGrid();
            Msg.ShowSuccess("Record Updated Successfully.");

        }
        catch (Exception ex)
        {
            Msg.ShowError("Some error occurred,Can not update record.");
        }
    }
    private void Reset()
    {
        txtTitle.Focus();
        gd.SelectedIndex = -1;
        txtCaleder.Text = "";
        txtTitle.Text = "";
        txtSubTitle.Text = "";
        btnCancel.Visible = false;
        btnSubmit.Text = "Submit";
        hdnPdf.Value = "";
        foreach (GridViewRow gr in gd.Rows)
        {
            gr.Cells[7].Enabled = true;
        }
    }
    private string SaveFile(string fl)
    {
        string extensionI1 = System.IO.Path.GetExtension(FileUpload1.FileName);

        if (extensionI1 != "")
        {
            FileUpload1.SaveAs(Server.MapPath("Documents\\" + fl + extensionI1 + ""));
            // FilePath = Server.MapPath("Admin\\Resume\\" + fl + extensionI1);
        }
        return extensionI1;
    }
    protected void gd_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        Msg.Visible = false;
        try
        {
            int Id = int.Parse(gd.Rows[e.NewSelectedIndex].Cells[0].Text);
            DataTable dt = new DataTable();
            dt = c.Display("EXEC AddUpdateGetNewsMaster 'Get_By_Id'," + Id);
            txtCaleder.Text = dt.Rows[0]["News_Date_dtm"].ToString();
            txtTitle.Text = dt.Rows[0]["Title_vcr"].ToString();
            txtSubTitle.Text = dt.Rows[0]["SubTitle_vcr"].ToString();
            hdnPdf.Value = dt.Rows[0]["Pdf_Path_vcr"].ToString();
            btnSubmit.Text = "Update";
            btnCancel.Visible = true;
            txtTitle.Focus();
            foreach (GridViewRow gr in gd.Rows)
            {
                gr.Cells[7].Enabled = true;
            }
            gd.Rows[e.NewSelectedIndex].Cells[7].Enabled = false;
        }
        catch (Exception ex)
        {
            Msg.ShowError("Some Error Occurred.");

        }
    }
    private void Download(string fileName)
    {
        try
        {
            // string filename = "Connectivity.doc";
            if (fileName != "")
            {
                string strPath = Server.MapPath("Admin\\Documents\\" + fileName);

                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
                Response.Charset = "";
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                //Response.ContentType = "image/Jpeg";
                Response.ContentType = "Application/pdf";
                //Response.AppendHeader("Content-Disposition", "attachment; filename=" + strFileName);
                Response.TransmitFile(strPath);
                Response.End();

            }
        }
        catch (Exception ex)
        {
            Msg.ShowError("Some Error Occurred.");
        }
    }
    protected void gd_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            int Id = int.Parse(gd.Rows[e.RowIndex].Cells[0].Text);
            using (SqlCommand cmd = new SqlCommand("AddUpdateGetNewsMaster", c.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para_vcr", "Delete");
                cmd.Parameters.AddWithValue("@News_Id_bint", Id);
                cmd.Parameters.AddWithValue("@Added_Updated_By_Id_int", int.Parse(Session["AdminLoginId"].ToString()));
                if (!c.con.State.Equals(ConnectionState.Open))
                {
                    c.con.Open();
                }

                cmd.ExecuteNonQuery();
                c.con.Close();
                BindGrid();
                Msg.ShowSuccess("Record deleted successfully.");
            }
        }
        catch (Exception ex)
        {
            Msg.ShowError("Error occurred,can not delete record");
        }
    }
    protected void gd_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Msg.Visible = false;
        try
        {
            gd.PageIndex = e.NewPageIndex;
            BindGrid();

        }
        catch(Exception ex)
        {
            Msg.ShowError("Some Error Occurred.");
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Reset();
    }
}
