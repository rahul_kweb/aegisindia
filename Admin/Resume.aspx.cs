﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Drawing;

public partial class Resume : System.Web.UI.Page
{

    Class1 c = new Class1();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["AdminLoginId"].Equals(""))
        {
            Response.Redirect("AdminLogin.aspx");
        }
        else
        {
            if (!Page.IsPostBack)
            {
                BindGrid();
            }
        }
    }
    private void BindGrid()
    {
        try
        {
            gv.Columns[0].Visible = true;
            gv.DataSource = c.Display("EXEC AddUpdateResumeMaster 'Get'");
            gv.DataBind();
            gv.Columns[0].Visible = false;
        }
        catch (Exception ex)
        {
            Msg.ShowError("Some Error Occurred.");
        }
    }
    private void Download(string fileName)
    {
        try
        {
            // string filename = "Connectivity.doc";
            if (fileName != "")
            {
                string strPath = Server.MapPath("Resume\\" + fileName);

                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
                Response.Charset = "";
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                //Response.ContentType = "image/Jpeg";
                Response.ContentType = "Application/msword";
                //Response.AppendHeader("Content-Disposition", "attachment; filename=" + strFileName);
                Response.TransmitFile(strPath);
                Response.End();
                Msg.ShowSuccess("Resume Downloaded Successfully.");
            }
        }
        catch (Exception ex)
        {
            Msg.ShowError("Some Error Occurred.");
        }
    }
    protected void gv_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        try
        {
            string FileName = gv.Rows[e.NewSelectedIndex].Cells[4].Text;
            Download(FileName);
        }
        catch (Exception ex)
        {
            Msg.ShowError("Can not Download File, Some Error Occurred.");
        }
    }
    protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gv.PageIndex = e.NewPageIndex;
        BindGrid();
    }
    protected void gv_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            int id = int.Parse(gv.Rows[e.RowIndex].Cells[0].Text);
            using (SqlCommand cmd = new SqlCommand("AddUpdateResumeMaster", c.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para_vcr", "Delete");
                cmd.Parameters.AddWithValue("@Resume_Id_bint", id);

                if (c.con.State == ConnectionState.Closed)
                {
                    c.con.Open();
                }
                cmd.ExecuteNonQuery();
                Msg.ShowSuccess("Record Deleted Successfully.");
                BindGrid();
            }
        }
        catch (Exception ex)
        {
            Msg.ShowError("Some Error Occurred.");
        }
    }
}
