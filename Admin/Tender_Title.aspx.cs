﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class Admin_Tender_Title : System.Web.UI.Page
{
    Class1 c = new Class1();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["AdminUserName"].Equals("") || Session["AdminLoginId"].Equals(""))
        {
            Response.Redirect("AdminLogin.aspx");
        }
        else
        {
            if (!Page.IsPostBack)
            {
                string rcheck = Session["Permission"].ToString();
                if (!c.ShowHideAdminMenu(rcheck, this, "Tender", "Tender"))
                {
                    Response.Redirect("Default.aspx");
                }
                BindGrid();
                txtTitle.Focus();
            }
        }
    }
    private void BindGrid()
    {
        try
        {
            gd.Columns[0].Visible = true;
            gd.DataSource = c.Display("EXEC AddUpdateGetTenderTitle 'Get'");
            gd.DataBind();
            gd.Columns[0].Visible = false;
        }
        catch (Exception ex)
        {
            Msg.ShowError("Error occurred while fetching record.");
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (txtTitle.Text.Length<1)
        {
            Msg.ShowError("Please type Tender title.");
        }
        else
        {
            AddTitle();
        }
    }
    private void AddTitle()
    {
        string opt = btnSubmit.Text;
        try
        {
            int Added = 0;
            using (SqlCommand cmd = new SqlCommand("AddUpdateGetTenderTitle", c.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                if (opt.Equals("Submit"))
                {
                    cmd.Parameters.AddWithValue("@Para_vcr", "Add");
                }
                else
                {
                    int Id = int.Parse(gd.Rows[gd.SelectedIndex].Cells[0].Text);
                    cmd.Parameters.AddWithValue("@Para_vcr", "Update");
                    cmd.Parameters.AddWithValue("@Tender_Title_Id_int", Id);
                }               
                cmd.Parameters.AddWithValue("@Tender_Title_vcr", txtTitle.Text.Trim());
                cmd.Parameters.AddWithValue("@AddedUpdatedBy_Id_int", int.Parse(Session["AdminLoginId"].ToString()));
                
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                Added =Convert.ToInt32(dt.Rows[0].ItemArray[0].ToString());
               
            }
            if (Added.Equals(1))
            {
                if (opt.Equals("Submit"))
                {
                    Msg.ShowSuccess("Record Added Successfully.");
                }
                else if (opt.Equals("Update"))
                {
                    Msg.ShowSuccess("Record Updated Successfully.");
                }
                Reset();
                BindGrid();                
            }
            else
            {
                Msg.ShowError("Title already exists.");
                txtTitle.Focus();
            }
        }
        catch (Exception ex)
        {
            if (opt.Equals("Submit"))
            {
                Msg.ShowError("Some Error Occurred, can not save record.");
            }
            else if (opt.Equals("Update"))
            {
                Msg.ShowError("Some Error Occurred, can not update record."+ex.Message);
            }
            
        }
    }
    private void Reset()
    {
        txtTitle.Text = "";
        txtTitle.Focus();
        btnSubmit.Text = "Submit";
        gd.SelectedIndex = -1;
        foreach (GridViewRow gr in gd.Rows)
        {
            ((Button)gr.Cells[3].Controls[0]).Enabled = true;
        }
        btnCancel.Visible = false;
    }
    protected void gd_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        Msg.Visible = false;
        try
        {
            int Id = int.Parse(gd.Rows[e.NewSelectedIndex].Cells[0].Text);
           
            DataTable dt = new DataTable();
            dt = c.Display("EXEC AddUpdateGetTenderTitle 'Get_By_Id',"+Id);
            txtTitle.Text = dt.Rows[0]["Tender_Title_vcr"].ToString();
            btnSubmit.Text = "Update";
            btnCancel.Visible = true;
            foreach (GridViewRow gr in gd.Rows)
            {
                ((Button)gr.Cells[3].Controls[0]).Enabled = true;
            }
            ((Button)gd.Rows[e.NewSelectedIndex].Cells[3].Controls[0]).Enabled = false;
        }
        catch (Exception ex)
        {
            Msg.ShowError("Can not Download File, Some Error Occurred.");
        }
    }
   
    protected void gd_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        Msg.Visible = false;
        try
        {
            int Id = int.Parse(gd.Rows[e.RowIndex].Cells[0].Text);
            using (SqlCommand cmd = new SqlCommand("AddUpdateGetTenderTitle", c.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para_vcr", "Delete");
                cmd.Parameters.AddWithValue("@Tender_Title_Id_int", Id);
                cmd.Parameters.AddWithValue("@AddedUpdatedBy_Id_int", int.Parse(Session["AdminLoginId"].ToString()));
                if (!c.con.State.Equals(ConnectionState.Open))
                {
                    c.con.Open();
                }

                cmd.ExecuteNonQuery();
                c.con.Close();
                BindGrid();
                Msg.ShowSuccess("Record deleted successfully.");
            }
        }
        catch (Exception ex)
        {
            Msg.ShowError("Error occurred,can not delete record");
        }
    }
    protected void gd_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Msg.Visible = false;
        try
        {
            gd.PageIndex = e.NewPageIndex;
            BindGrid();
        }
        catch(Exception ex)
        {
            Msg.ShowError("Some Error Occurred.");
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Reset();
    }
}
