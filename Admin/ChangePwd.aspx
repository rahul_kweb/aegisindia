<%@ Page Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="ChangePwd.aspx.cs" Inherits="Admin_ChangePwd" Title="Aegis Logistics Limited - Change Password" %>
<%@ Register Src="~/Admin/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="StyleSheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style2
        {
            width: 100%;
        }
        .style3
        {
            width: 136px;
        }
        .style4
        {
            width: 136px;
            height: 30px;
        }
        .style5
        {
            height: 30px;
        }
    </style>

    <script src="jscripts/jquery-1.4.1.min.js" type="text/javascript"></script>
  <%--  <script type="text/javascript">
    function CheckBlank()
    {
    
    var a=false;
    
            if ($('#<%=txtOldPwd.ClientID %>').val().length<1) 
            { 
                $('#<%=lblOld.ClientID %>').show();           
                a=true;
            }
            else
            {
                $('#<%=lblOld.ClientID %>').hide();    
                a=false;       
            }
            if($('#<%=txtNewPwd.ClientID %>').val().length<1)
            {
                $('#<%=lblNew.ClientID %>').show();
                a=true;
            }
            else
            {
                $('#<%=lblNew.ClientID %>').hide();
                a=false;
            }
            if($('#<%=txtConPwd.ClientID %>').val().length<1)
            {
             $('#<%=lblConf.ClientID %>').val('Please type conform password.');
                    $('#<%=lblConf.ClientID %>').show();
                    a=true;
            }
            else
            {        $('#<%=lblConf.ClientID %>').hide();
                    a=false;
            }
             alert(a);
        if(a=true)
        {
     
            return false;   
             
        }else
        {
            if($('#<%=txtNewPwd.ClientID %>').val() == $('#<%=txtConPwd.ClientID %>').val())
            {            
                 return true;
            }
            else
            {
               
            
                 $('#<%=lblConf.ClientID %>').val('Please check confirmation password.');
                 
                 $('#<%=lblConf.ClientID %>').show();
                 return false;
            }
        }
       
    }
    </script>--%>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


    


</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
<table class="style2">
        <tr>
            <td colspan="2">
                <uc1:MyMessageBox ID="Msg" runat="server" ShowCloseButton="true" 
                    Visible="False" />
            </td>
        </tr>
        <tr>
            <td class="style3">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style3">
                <asp:Label ID="Label1" runat="server" Text="Old Password :-"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtOldPwd" runat="server" TextMode="Password"></asp:TextBox><span class="Req">*</span>
                
                <asp:RequiredFieldValidator ID="ReqOld" runat="server" 
                    ControlToValidate="txtOldPwd" ErrorMessage="Type old password" 
                    SetFocusOnError="True"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style4">
                <asp:Label ID="Label2" runat="server" Text="New Password :-"></asp:Label>
            </td>
            <td class="style5">
                <asp:TextBox ID="txtNewPwd" runat="server" TextMode="Password"></asp:TextBox>
                <span class="Req">*</span> 
                
                <asp:RequiredFieldValidator ID="ReqNew" runat="server" 
                    ErrorMessage="Type new password" ControlToValidate="txtNewPwd"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style3">
                <asp:Label ID="Label3" runat="server" Text="Confirm Password :-"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtConPwd" runat="server" TextMode="Password"></asp:TextBox>
                <span class="Req">* </span>
                
                <asp:RequiredFieldValidator ID="ReqConf" runat="server" 
                    ErrorMessage="Type confirm password" ControlToValidate="txtConPwd"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidator1" runat="server" 
                    ErrorMessage="Please Check Confirm Password" ControlToCompare="txtNewPwd" 
                    ControlToValidate="txtConPwd" SetFocusOnError="True"></asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td class="style3">
                &nbsp;</td>
            <td>
                <asp:Button ID="btnChange" runat="server" onclick="btnChange_Click" 
                    Text="Change Password" />
            </td>
        </tr>
    </table>
</asp:Content>
