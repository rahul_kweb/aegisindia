﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class Admin_HomeBannner : System.Web.UI.Page
{
    Class1 c = new Class1();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["AdminUserName"].Equals("") || Session["AdminLoginId"].Equals(""))
        {
            Response.Redirect("AdminLogin.aspx");
        }
        else
        {
            if (!Page.IsPostBack)
            {
                string rcheck = Session["Permission"].ToString();
                if (!c.ShowHideAdminMenu(rcheck, this, "CMS", "CMS"))
                {
                    Response.Redirect("Default.aspx");
                }
                GetData();
            }
        }
    }
    //private void BindProduct()
    //{
    //    try
    //    {
    //        c.FillDropDownList( "EXEC AddUpdateGetProductMaster 'Get'", "Product_Name_vcr", "Product_Id_int");
    //        //ddlProduct.Items.Insert(0, "Select Product");
    //    }
    //    catch (Exception ex)
    //    {

    //    }
    //}
    private string SaveFile(string fl)
    {
        string extensionI1 = System.IO.Path.GetExtension(FileUpload1.FileName);

        if (extensionI1 != "")
        {
            FileUpload1.SaveAs(Server.MapPath("Banner/" + fl + extensionI1 + ""));
        }
        return fl + extensionI1;
    }
    private void AddBanner()
    {
        try
        {
            int BannerId = 0;
            string ext = "";
            using (SqlCommand cmd = new SqlCommand("AddUpdateGetHomePageBanner", c.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para_vcr", "Add");
                cmd.Parameters.AddWithValue("@Banner_vcr", "Home_Page_Banner_");
                //cmd.Parameters.AddWithValue("@Product_Id_int", int.Parse(ddlProduct.SelectedValue));
                cmd.Parameters.AddWithValue("@AddedBy_Id_int", int.Parse(Session["AdminLoginId"].ToString()));
               // cmd.Parameters.AddWithValue("@Page_Url", "");
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                BannerId = int.Parse(dt.Rows[0][0].ToString());
                string fileName = dt.Rows[0][1].ToString();
                ext = SaveFile(fileName);

            }
            using (SqlCommand cmd1 = new SqlCommand("AddUpdateGetHomePageBanner", c.con))
            {
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.AddWithValue("@Para_vcr", "UpdateBannerExtension");
                cmd1.Parameters.AddWithValue("@Banner_vcr", ext);
                cmd1.Parameters.AddWithValue("@Banner_Id_int", BannerId);
                if (c.con.State == ConnectionState.Closed)
                {
                    c.con.Open();
                }
                cmd1.ExecuteNonQuery();
            }
            GetData();
            //ddlProduct.ClearSelection();
            SetVisibility("display:none");
            Msg.ShowSuccess("Record Added Successfully.");
        }
        catch (Exception ex)
        {
            Msg.ShowError("Some Error Occurred, can not save record.");
        }

    }
    private void UpdateBanner()
    {

        try
        {

            string ext = "";
            Uri ur = new Uri("http://www.Horizon.com/Admin/" + imgBanner.ImageUrl);


            using (SqlCommand cmd = new SqlCommand("AddUpdateGetHomePageBanner", c.con))
            {
                int Id = int.Parse(gd.Rows[gd.SelectedIndex].Cells[0].Text);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para_vcr", "Update");
                cmd.Parameters.AddWithValue("@Banner_Id_int", Id);
                cmd.Parameters.AddWithValue("@AddedBy_Id_int", int.Parse(Session["AdminLoginId"].ToString()));
                //cmd.Parameters.AddWithValue("@Product_Id_int", int.Parse(ddlProduct.SelectedValue));
                if (FileUpload1.HasFile)
                {
                    string fileName = ur.Segments.Last().Split('.').ElementAt(0);
                    ext = SaveFile(fileName);
                    cmd.Parameters.AddWithValue("@Banner_vcr", ext);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@Banner_vcr", ur.Segments.Last());
                }


                if (!c.con.State.Equals(ConnectionState.Open))
                {
                    c.con.Open();
                }
                cmd.ExecuteNonQuery();
            }
            GetData();
           // SetVisibility("display:none;");
            Msg.ShowSuccess("Record Updated Successfully.");


        }
        catch (Exception ex)
        {
            Msg.ShowError("Some error occurred,Can not update record.");
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        Msg.Visible = false;

        bool valid = true;
        if (!FileUpload1.HasFile)
        {
            if (btnSubmit.Text.Equals("Submit"))
            {
                valid = false;
            }
        }
        if (!valid)
        {
            Msg.ShowError("Please Upload Banner.");
        }
        else
        {
            try
            {
                if (btnSubmit.Text.Equals("Submit"))
                {
                    if (check() == true)
                    {
                        AddBanner();
                    }
                    else
                    {
                        Msg.ShowError("Please Remove one to add new");
                    }
                }
                else if (btnSubmit.Text.Equals("Update"))
                {
                    UpdateBanner();
                    Reset();
                }
            }
            catch (Exception ex)
            {
                Msg.ShowError("Some Error Occurred.");
            }
        }

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Reset();
        //ddlProduct.ClearSelection();
        SetVisibility("display:none;"); GetData();
    }
    //protected void ddlProduct_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    Msg.Visible = false;
    //    if (ddlProduct.SelectedIndex > 0)
    //    {

    //        try
    //        {
    //            DataTable dt = new DataTable();
    //            dt = c.Display("EXEC AddUpdateGetHomePageBanner 'Get_By_Product_Id',0," + int.Parse(ddlProduct.SelectedValue));

    //            if (dt.Rows.Count > 0)
    //            {
    //                SetVisibility("display:none;");
    //                Msg.ShowInfo("This Product already has a banner.");
    //            }
    //            else
    //            {
    //                SetVisibility("");
    //            }
    //        }
    //        catch (Exception ex)
    //        {

    //        }
    //    }
    //    else
    //    {
    //        SetVisibility("display:none;");
    //    }

    //}

    private void GetData()
    {
        try
        {
            DataTable dt = new DataTable();
            dt = c.Display("EXEC AddUpdateGetHomePageBanner 'Get'");
            gd.SelectedIndex = -1;
            btnSubmit.Text = "Submit";
            btnCancel.Visible = false;
            imgBanner.Visible = false;

            gd.Columns[0].Visible = true;
            gd.DataSource = dt;
            gd.DataBind();
            gd.Columns[0].Visible = false;

        }
        catch (Exception ex)
        {
            Msg.ShowError("Some error occurred.");
        }
    }
    private void Reset()
    {
        btnSubmit.Text = "Submit";
        imgBanner.Visible = false;
       // ddlProduct.Enabled = true;
        btnCancel.Visible = false;
        //foreach (GridViewRow gr in gd.Rows)
        //{
        //    gr.Cells[4].Enabled = true;
        //}



    }
    protected void gd_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        Msg.Visible = false;
        try
        {
            DataTable dt = new DataTable();
            int Id = int.Parse(gd.Rows[e.NewSelectedIndex].Cells[0].Text);
            dt = c.Display("EXEC AddUpdateGetHomePageBanner 'Get_By_Id'," + Id);
          //  ddlProduct.SelectedValue = dt.Rows[0]["Product_Id_int"].ToString();
            imgBanner.ImageUrl = "Banner/" + dt.Rows[0]["Banner_vcr"].ToString();
            imgBanner.Visible = true;
            btnSubmit.Text = "Update";
            btnCancel.Visible = true;
            SetVisibility("");
            //ddlProduct.Enabled = false;
            //gd.Rows[e.NewSelectedIndex].Cells[4].Enabled = false;
            FileUpload1.Focus();

        }
        catch (Exception ex)
        {
            Msg.ShowError("Some error occcurred.");
        }
    }
    protected void gd_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        Msg.Visible = false;
        try
        {
            using (SqlCommand cmd = new SqlCommand("AddUpdateGetHomePageBanner", c.con))
            {
                int Id = int.Parse(gd.Rows[e.RowIndex].Cells[0].Text);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Para_vcr", "Delete");
                cmd.Parameters.Add("@Banner_Id_int", Id);
                cmd.Parameters.Add("@AddedBy_Id_int", int.Parse(Session["AdminLoginId"].ToString()));
                if (!c.con.State.Equals(ConnectionState.Open))
                {
                    c.con.Open();
                }
                cmd.ExecuteNonQuery();
                c.con.Close();
                GetData();
                Msg.ShowSuccess("Record deleted Successfully.");
            }
        }
        catch (Exception ex)
        {
            Msg.ShowError("Some error occurred,");
        }
    }
    private void SetVisibility(string set)
    {
        //trUpload.Attributes["style"] = set;
        //trBanner.Attributes["style"] = set;
        //trSubmit.Attributes["style"] = set;
    }
    public bool check()
    {   bool a=true;
         bool b=false;
        DataTable dt = new DataTable();
        string s = "select * from HomePageBanner where Is_Active_bit=1";
        dt = c.Display(s);
        if (dt.Rows.Count < 4)
        {
            return a;
        }
        else
        {
            return b;
        }
    }

}
