﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class Admin_Chairman : System.Web.UI.Page
{

    Class1 c = new Class1();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["AdminUserName"].Equals("") || Session["AdminLoginId"].Equals(""))
        {
            Response.Redirect("AdminLogin.aspx");
        }
        else
        {
            if (!Page.IsPostBack)
            {
                string rcheck = Session["Permission"].ToString();
                if (!c.ShowHideAdminMenu(rcheck, this, "UploadImages", "UploadImages"))
                {
                    Response.Redirect("Default.aspx");
                }
                
                BindGrid();
            }
        }
    }
 
    private void BindGrid()
    {
        try
        {
            gd.Columns[0].Visible = true;
            //gd.DataSource = c.Display("EXEC AddUpdateGetKeyMgmtMaster 'Get_By_Type_Id',0,'','',0,1");
            gd.DataSource = c.Display("EXEC AddUpdateGetChairmanMaster 'Get'");
            gd.DataBind();
            gd.Columns[0].Visible = false;
        }
        catch (Exception ex)
        {
            Msg.ShowError("Error occurred while fetching record.");
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        Msg.Visible = false;
        if (txtName.Text.Trim().Length < 1)
        {
            Msg.ShowError("Please Type.");
        }
        if (txtPost.Text.Trim().Length < 1)
        {
            Msg.ShowError("Please Type Post.");
        }
        else
        {
            if (btnSubmit.Text.Equals("Submit"))
            {
                AddImage();
               
            }
            else if (btnSubmit.Text.Equals("Update"))
            {
                UpdateImage();
                
            }
        }
    }
  
    private void AddImage()
    {
        string opt = btnSubmit.Text;
        try
        {
            int DId = 0;
            string ext = "";

            using (SqlCommand cmd = new SqlCommand("AddUpdateGetChairmanMaster", c.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para_vcr", "Add");
                cmd.Parameters.AddWithValue("@Name_vcr", txtName.Text.Trim());
                cmd.Parameters.AddWithValue("@Desg_vcr", txtPost.Text.Trim());
                cmd.Parameters.AddWithValue("@Image_vcr", "Mgmt_Team_");
                //cmd.Parameters.AddWithValue("@Talent_vcr",txtTalent.Text.Trim());
                cmd.Parameters.AddWithValue("@Added_Updated_By_Id_bint", int.Parse(Session["AdminLoginId"].ToString()));
                
               

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                
                DId = int.Parse(dt.Rows[0][0].ToString());
                string fileName = dt.Rows[0][1].ToString();
                ext = SaveFile(fileName);
                //if (DId < 1)
                //{
                //    Msg.ShowError("Chariman  only one");
                //}
                //else
                //{
                //    string fileName = dt.Rows[0][1].ToString();
                //    ext = SaveFile(fileName);
                //}
            }
            using (SqlCommand cmd1 = new SqlCommand("AddUpdateGetChairmanMaster", c.con))
            {
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.AddWithValue("@Para_vcr", "UpdateDocumentExtension");
                cmd1.Parameters.AddWithValue("@Image_vcr", ext);
                cmd1.Parameters.AddWithValue("@Id_bint", DId);
                if (c.con.State == ConnectionState.Closed)
                {
                    c.con.Open();
                }
                cmd1.ExecuteNonQuery();
            }


            txtName.Text = "";
            txtPost.Text = "";
            txtName.Focus();
            BindGrid();

            if (opt.Equals("Submit"))
            {
                Msg.ShowSuccess("Record Added Successfully.");
            }
            else if (opt.Equals("Update"))
            {
                Msg.ShowSuccess("Record Updated Successfully.");
            }

        }
        catch (Exception ex)
        {
            if (opt.Equals("Submit"))
            {
                Msg.ShowError("Some Error Occurred, can not save record" + ex.Message);
            }
            else if (opt.Equals("Update"))
            {
                Msg.ShowError("Some Error Occurred, can not update record");
            }
        }
    }

    private void UpdateImage()
    {
        try
        {

            string ext = "";
            Uri ur = new Uri("http://www.aegis.com/Admin/" + ImgPartner.ImageUrl);

            using (SqlCommand cmd = new SqlCommand("AddUpdateGetChairmanMaster", c.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para_vcr", "Update");
                cmd.Parameters.AddWithValue("@Name_vcr", txtName.Text.Trim());
                cmd.Parameters.AddWithValue("@Desg_vcr", txtPost.Text.Trim());
                //if (txtPost.Text.Equals("Chairman"))
                //{
                //    cmd.Parameters.AddWithValue("@Type_Id",1);

                //}
                //else 
                //{
                //    cmd.Parameters.AddWithValue("@Type_Id", 2);
                //}
                cmd.Parameters.AddWithValue("@Added_Updated_By_Id_bint", int.Parse(Session["AdminLoginId"].ToString()));

                int Id = int.Parse(gd.Rows[gd.SelectedIndex].Cells[0].Text);
                
                cmd.Parameters.AddWithValue("@Id_bint", Id);
                if (FileUpload1.HasFile)
                {
                    string fileName = ur.Segments.Last().Split('.').ElementAt(0);
                    ext = SaveFile(fileName);
                    cmd.Parameters.AddWithValue("@Image_vcr", ext);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@Image_vcr", ur.Segments.Last());
                }
               
                if (!c.con.State.Equals(ConnectionState.Open))
                {
                    c.con.Open();
                }
                cmd.ExecuteNonQuery();
            }
            gd.SelectedIndex = -1;
            BindGrid();
            txtName.Text = "";
            txtPost.Text = "";
            ImgPartner.Visible = false;
            btnCancel.Visible = false;
            btnSubmit.Text = "Submit";
            Msg.ShowSuccess("Record Updated Successfully.");
            txtName.Focus();

        }
        catch (Exception ex)
        {
            Msg.ShowError("Some error occurred,Can not update record." + ex.Message);
        }
    }
    private string SaveFile(string fl)
    {
        string extensionI1 = System.IO.Path.GetExtension(FileUpload1.FileName);

        if (extensionI1 != "")
        {
            FileUpload1.SaveAs(Server.MapPath("images\\" + fl + extensionI1 + ""));

        }
        return fl + extensionI1;
    }
    protected void gd_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        Msg.Visible = false;
        try
        {
            int Id = int.Parse(gd.Rows[e.RowIndex].Cells[0].Text);
            using (SqlCommand cmd = new SqlCommand("AddUpdateGetChairmanMaster", c.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para_vcr", "Delete");
                cmd.Parameters.AddWithValue("@Id_bint", Id);
                cmd.Parameters.AddWithValue("@Added_Updated_By_Id_bint", int.Parse(Session["AdminLoginId"].ToString()));
                if (!c.con.State.Equals(ConnectionState.Open))
                {
                    c.con.Open();
                }

                cmd.ExecuteNonQuery();
                c.con.Close();
                BindGrid();
                Msg.ShowSuccess("Record deleted successfully.");
            }
        }
        catch (Exception ex)
        {
            Msg.ShowError("Error occurred,can not delete record");
        }
    }
    protected void gd_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Msg.Visible = false;
        try
        {
            gd.PageIndex = e.NewPageIndex;
            BindGrid();
        }
        catch (Exception ex)
        {

        }

    }

    protected void gd_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        Msg.Visible = false;
        try
        {
            int Id = int.Parse(gd.Rows[e.NewSelectedIndex].Cells[0].Text);
            DataTable dt = new DataTable();
            dt = c.Display("EXEC AddUpdateGetChairmanMaster 'Get_By_Id'," + Id);
            txtName.Text = dt.Rows[0]["Name_vcr"].ToString();
            txtPost.Text = dt.Rows[0]["Desg_vcr"].ToString();
            string photo = dt.Rows[0]["Image_vcr"].ToString();
            if (photo.Length > 0)
            {
                ImgPartner.Visible = true;
                ImgPartner.ImageUrl = "~/Admin/images/" + photo;
            }
            else
            {
                ImgPartner.Visible = false;
            }

          
            btnSubmit.Text = "Update";
            btnCancel.Visible = true;
        }
        catch (Exception ex)
        {
            Msg.ShowError("Some Error Occurred.");

        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtName.Text = "";
        ImgPartner.ImageUrl = null;
        ImgPartner.Visible = false;
        gd.SelectedIndex = -1;
        txtName.Focus();
        btnCancel.Visible = false;
    }
}
