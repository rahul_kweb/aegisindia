﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="Awards_Recognition.aspx.cs" Inherits="Awards_Recognition" Title="Aegis Logistics Limited - Awards & Recognition" %>
<%@ Register Src="~/Admin/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="StyleSheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
         .Req
        {
        	color:Red;
        	vertical-align:top;
        }
        .style2
        {
            height: 161px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <div>
    <table class="style1">
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <uc1:MyMessageBox ID="Msg" runat="server" ShowCloseButton="true" 
                    Visible="False" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="Upload Image"></asp:Label>
            </td>
            <td>
                <asp:FileUpload ID="FileUpload1" runat="server" />
                <span class="Req">*</span>&nbsp;
                <asp:Image ID="ImgPartner" Visible="false" runat="server" Height="150px" Width="150px" />
                </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" 
                    onclick="btnSubmit_Click" />
            &nbsp;<asp:Button ID="btnCancel" runat="server" onclick="btnCancel_Click" 
                    Text="Cancel" Visible="False" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style2">
                </td>
            <td class="style2">
                <asp:GridView ID="gd" runat="server" AutoGenerateColumns="False" 
                    BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px" 
                    CellPadding="4" onrowdeleting="gd_RowDeleting" AllowPaging="True" 
                    onpageindexchanging="gd_PageIndexChanging" 
                    onselectedindexchanging="gd_SelectedIndexChanging">
                    <RowStyle BackColor="White" ForeColor="#330099" />
                    <Columns>
                        <asp:BoundField DataField="Image_Id_bint" HeaderText="Id" />
                         <asp:TemplateField HeaderText="Photo">
                        <ItemTemplate>
                            <asp:Image runat="server" Height="150px" Width="150px" ID="ImgDoc" ImageUrl='<%# string.Format("~/Admin/images/{0}",Eval("Image_Path_vcr")) %>' />
                        </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField ButtonType="Button" HeaderText="Edit" SelectText="Edit" 
                            ShowSelectButton="True" />
                        <asp:CommandField ButtonType="Button" HeaderText="Delete" 
                            ShowDeleteButton="True" />
                    </Columns>
                    <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                    <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td class="style2">
                &nbsp;</td>
        </tr>
    </table>

</div>

</asp:Content>


