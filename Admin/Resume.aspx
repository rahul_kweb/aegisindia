﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="Resume.aspx.cs" Inherits="Resume" Title="Aegis Logistics Limited - Resumes" %>
<%@ Register Src="~/Admin/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="StyleSheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
          .tbl
        {
            width: 100%;
            border: solid 1px gray;             
        }
        .Req
        {
        	color:Red;
        	vertical-align:top;
        }
        .style5
    {
        height: 22px;
    }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

    <table class="tbl">
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <uc1:MyMessageBox ID="Msg" runat="server" ShowCloseButton="true" 
                    Visible="False" />
            </td>
        </tr>
        <tr>
            <td class="style5">
                </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="gv" runat="server" AutoGenerateColumns="False"  AllowPaging="true"
                    BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px" 
                    CellPadding="4" onselectedindexchanging="gv_SelectedIndexChanging" 
                    onpageindexchanging="gv_PageIndexChanging" PageSize="20" 
                    onrowdeleting="gv_RowDeleting">
                    <RowStyle BackColor="White" ForeColor="#330099" />
                    <Columns>
                        <asp:BoundField DataField="Resume_Id_bint" HeaderText="Id" />
                        <asp:BoundField DataField="JOb_Title" HeaderText="Applying For" />
                        <asp:BoundField DataField="Name_vcr" HeaderText="Name" />
                        <asp:BoundField DataField="Email_vcr" HeaderText="Email Id" />
                        <asp:BoundField DataField="File_Name_vcr" HeaderText="Resume" />
                           <asp:BoundField DataField="Added_On_dtm" HeaderText="Application Date" />
                        <asp:CommandField ButtonType="Image" HeaderText="Download" 
                            SelectImageUrl="~/images/download.jpg" SelectText="Download" 
                            ShowSelectButton="True" />
                         <asp:CommandField ButtonType="Button" HeaderText="Delete" 
                           ShowDeleteButton="true" />    
                            
                    </Columns>
                    <EmptyDataTemplate>
                    <table cellspacing="0" cellpadding="4" rules="all" border="1" id="ctl00_ContentPlaceHolder1_gv" style="background-color:White;border-color:#3366CC;border-width:1px;border-style:None;border-collapse:collapse;">
		<tbody><tr style="color:#CCCCFF;background-color:#003399;font-weight:bold;">
			<th scope="col">Name</th><th scope="col">Email Id</th><th scope="col">Resume</th><th scope="col">Download</th>
		</tr><tr style="color:#003399;background-color:White;">
			<td colspan="4" align="center">No Record Found</td>
		</tr>
	</tbody></table>
                    
                    </EmptyDataTemplate>
                    <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                    <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
                </asp:GridView>
            </td>
        </tr>
    </table>

</asp:Content>

