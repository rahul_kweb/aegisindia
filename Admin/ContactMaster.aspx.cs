﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class ContactMaster : System.Web.UI.Page
{
    Class1 c = new Class1();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["AdminUserName"].Equals("") || Session["AdminLoginId"].Equals(""))
        {
            Response.Redirect("AdminLogin.aspx");
        }
        else
        {
            if (!Page.IsPostBack)
            {

                string rcheck = Session["Permission"].ToString();
                if (!c.ShowHideAdminMenu(rcheck, this, "Contact", "Contact"))
                {
                    Response.Redirect("Default.aspx");
                }
                txtRegOffice.Focus();
                DataTable dt1 = new DataTable();
                dt1 = GetPageDetails(21);
                if (dt1.Rows.Count > 0)
                {
                    txtDesc.Content = dt1.Rows[0]["Desc_vcr"].ToString();
                    lblId.Text = dt1.Rows[0]["ID_bint"].ToString();

                }
                BindGrid();
            }
        } 
    }
    private DataTable GetPageDetails(int Id)
    {
        DataTable dt = new DataTable();
        try
        {
            dt = c.Display("EXEC AddUpdateGetCMS 'Get_By_Id'," + Id);

        }
        catch (Exception ex)
        {
            Msg.ShowError("Error Occurred while fetching Details.");
        }
        return dt;
    }
    private void BindGrid()
    {
        Msg.Visible = false;
        try
        {

            DataTable dt = new DataTable();
            dt = c.Display("EXEC AddUpdateContactMaster 'Get'");

            int Count = dt.Rows.Count;
            if (Count > 0)
            {
                btnSubmit.Text = "Update";
                hdnContactId.Value = dt.Rows[0]["Contact_Id_bint"].ToString();
                txtRegOffice.Text = dt.Rows[0]["Reg_Office_Address_vcr"].ToString();
                txtCorpOffice.Text = dt.Rows[0]["Corp_Office_Address_vcr"].ToString();
                txtTerminalAddress.Text = dt.Rows[0]["Terminal_Address_vcr"].ToString();
                txtRegPhone.Text = dt.Rows[0]["Reg_Office_Phone_vcr"].ToString();
                txtRegFaxNo.Text = dt.Rows[0]["Reg_Office_Fax_vcr"].ToString();                 
                txtCorpPhone.Text = dt.Rows[0]["Corp_Office_Phone_vcr"].ToString();
                txtCorpFaxNo.Text = dt.Rows[0]["Corp_Office_Fax_vcr"].ToString();
                txtTerminalPhone.Text = dt.Rows[0]["Terminal_Phone_vcr"].ToString();
                txtTerminalFax.Text = dt.Rows[0]["Terminal_Fax_vcr"].ToString();
                txtRegOffice.Focus();

            }

           
            
        }
        catch (Exception ex)
        {
            Msg.ShowError("Error occurred while fetching record.");
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        Msg.Visible = false;

        if (btnSubmit.Text.Equals("Submit"))
        {
            if (AddEmployee().Equals(1))
            {
                BindGrid();
                UpdateArticle();
                Msg.ShowSuccess("Record Added Successfully.");
            }
            else
            {
                Msg.ShowError("Some error occurred,can not add record.");
            }
        }
        else
        {
            if (AddEmployee().Equals(1))
            {
                BindGrid();
                UpdateArticle();
                Msg.ShowSuccess("Record Updated Successfully.");
            }
            else
            {
                Msg.ShowError("Some error occurred,can not update record.");
            }
        }

          
           
                
    }
    private void UpdateArticle()
    {
        try
        {
            using (SqlCommand cmd = new SqlCommand("AddUpdategetCMS", c.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para_vcr", "Update");
                cmd.Parameters.AddWithValue("@ID_bint", int.Parse(lblId.Text));
                cmd.Parameters.AddWithValue("@Desc_vcr", txtDesc.Content.Trim());
                if (c.con.State == ConnectionState.Closed)
                {
                    c.con.Open();
                }
                cmd.ExecuteNonQuery();
               //Session["CMSName"] = lblTitle.Text.Trim();
               // Response.Redirect("Default.aspx");
            }
        }
        catch (Exception ex)
        {
            Msg.ShowError("Error Occurred.");
        }
    }
    private int AddEmployee()
    {
        int Added = 0;
        try
        {
            using (SqlCommand cmd = new SqlCommand("AddUpdateContactMaster", c.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                if (btnSubmit.Text.Equals("Submit"))
                {
                    cmd.Parameters.AddWithValue("@Para_vcr", "Add");                
                }
                else
                {
                    cmd.Parameters.AddWithValue("@Para_vcr", "Update");
                    cmd.Parameters.AddWithValue("@Contact_Id_bint", int.Parse(hdnContactId.Value));
 
                }
                cmd.Parameters.AddWithValue("@Reg_Office_Address_vcr", txtRegOffice.Text.Trim());
                cmd.Parameters.AddWithValue("@Corp_Office_Address_vcr", txtCorpOffice.Text.Trim());
                cmd.Parameters.AddWithValue("@Terminal_Address_vcr", txtTerminalAddress.Text.Trim());
                cmd.Parameters.AddWithValue("@Reg_Office_Phone_vcr", txtRegPhone.Text.Trim());
                cmd.Parameters.AddWithValue("@Reg_Office_Fax_vcr", txtRegFaxNo.Text.Trim());
                cmd.Parameters.AddWithValue("@Corp_Office_Phone_vcr",txtCorpPhone.Text.Trim() );
                cmd.Parameters.AddWithValue("@Corp_Office_Fax_vcr", txtCorpFaxNo.Text.Trim());
                cmd.Parameters.AddWithValue("@Terminal_Phone_vcr", txtTerminalPhone.Text.Trim());
                cmd.Parameters.AddWithValue("@Terminal_Fax_vcr", txtTerminalFax.Text.Trim());
                
                if (!c.con.State.Equals(ConnectionState.Open))
                {
                    c.con.Open();
                }

              Added = cmd.ExecuteNonQuery();
              c.con.Close();
              
            }

        }
        catch (Exception ex)
        {
            Added = 0;
        }
        return Added;
    }
}
