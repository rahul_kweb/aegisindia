﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="AddYear.aspx.cs" Inherits="Admin_AddYear" Title="Aegis Logistics Limited - Add Year" %>
<%@ Register Src="~/Admin/controls/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="StyleSheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
         .Req
        {
        	color:Red;
        	vertical-align:top;
        }
    .style2
    {
        height: 10px;
    }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <div>
    <table class="style1">
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <uc1:MyMessageBox ID="Msg" runat="server" ShowCloseButton="true" 
                    Visible="False" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Year"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtYear" runat="server"></asp:TextBox>
                <span class="Req">*<asp:RequiredFieldValidator ID="ReqTitle" runat="server" 
                    ControlToValidate="txtYear" ErrorMessage="Please Type Year" 
                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                </span>
             </td>
        </tr>
        <tr>
            <td class="style2">
                </td>
            <td class="style2">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" 
                    onclick="btnSubmit_Click" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:HiddenField ID="HdnYearId" runat="server" />
                <asp:GridView ID="gd" runat="server" AutoGenerateColumns="False" 
                    BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px" 
                    CellPadding="4" AllowPaging="True" 
                    onpageindexchanging="gd_PageIndexChanging" 
                    onrowcancelingedit="gd_RowCancelingEdit" 
                    onrowediting="gd_RowEditing" onrowupdating="gd_RowUpdating" PageSize="20">
                    <RowStyle BackColor="White" ForeColor="#330099" />
                    <Columns>
                        <asp:BoundField DataField="Pdf_Master_Id_int" HeaderText="Id" />
                        <asp:BoundField DataField="Pdf_Master_Name_vcr" HeaderText="Year"  />
                        <asp:CommandField ButtonType="Button" HeaderText="Edit" CausesValidation="false" ShowEditButton="True" />
                    </Columns>
                    <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                    <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
                </asp:GridView>
            </td>
        </tr>
    </table>

</div>
</asp:Content>


