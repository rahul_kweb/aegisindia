﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class Admin_AddYear : System.Web.UI.Page
{
    Class1 c = new Class1();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["AdminUserName"].Equals("") || Session["AdminLoginId"].Equals(""))
        {
            Response.Redirect("AdminLogin.aspx");
        }
        else
        {
            if (!Page.IsPostBack)
            {
                string rcheck = Session["Permission"].ToString();
                if (!c.ShowHideAdminMenu(rcheck, this, "CMS", "CMS"))
                {
                    Response.Redirect("Default.aspx");
                }
                BindGrid();
            }
        }
    }
    private void BindGrid()
    {
        try
        {
            gd.Columns[0].Visible = true;
            gd.DataSource = c.Display("EXEC AddUpdateGetPdfMaster 'Get_Years'");
            gd.DataBind();
            gd.Columns[0].Visible = false;
        }
        catch (Exception ex)
        {
            Msg.ShowError("Error occurred while fetching record.");
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        Msg.Visible = false;
        if (!txtYear.Text.Trim().Equals(""))
        {
            if (AddYear().Equals(1))
            {
                BindGrid();
                txtYear.Text = "";

                Msg.ShowSuccess("Record Added Successfully.");
            }
            else
            {
                Msg.ShowError("Some error occurred,can not add record.");
            }
        }
        else
        {
            Msg.ShowError("Please Type Year.");
            txtYear.Focus();
        }
    }
    private int AddYear()
    {
        int Added = 0;
        try
        {
            using (SqlCommand cmd = new SqlCommand("AddUpdateGetPdfMaster", c.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para_vcr", "Add_Year");
                cmd.Parameters.AddWithValue("@Pdf_Master_Name_vcr", txtYear.Text.Trim());
                //cmd.Parameters.AddWithValue("@Added_Updated_By_Id_int", int.Parse(Session["AdminLoginId"].ToString()));
                if (!c.con.State.Equals(ConnectionState.Open))
                {
                    c.con.Open();
                }

                Added = cmd.ExecuteNonQuery();
                c.con.Close();
            }

        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
            Added = 0;
        }
        return Added;
    }
    protected void gd_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gd.PageIndex = e.NewPageIndex;
        BindGrid();
    }
    protected void gd_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gd.EditIndex = -1;
        BindGrid();
    }
    
    protected void gd_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gd.EditIndex = e.NewEditIndex;
        HdnYearId.Value = gd.Rows[e.NewEditIndex].Cells[0].Text;
        BindGrid();
    }
    protected void gd_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            using (SqlCommand cmd = new SqlCommand("AddUpdateGetPdfMaster", c.con))
            {
                string year = ((TextBox)gd.Rows[e.RowIndex].Cells[1].Controls[0]).Text;

                int Id = int.Parse(HdnYearId.Value);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para_vcr", "Update_Year");
                cmd.Parameters.AddWithValue("@Pdf_Master_Id_int", Id);
                cmd.Parameters.AddWithValue("@Pdf_Master_Name_vcr", year);                                
                if (!c.con.State.Equals(ConnectionState.Open))
                {
                    c.con.Open();
                }
                cmd.ExecuteNonQuery();
                c.con.Close();
                HdnYearId.Value = "0";
                gd.EditIndex = -1;
                BindGrid();
                Msg.ShowSuccess("Record Updated Successfully.");

            }
        }
        catch (Exception ex)
        {
            Msg.ShowError("Some error occurred, can not update record.");
        }
    }
}
