﻿<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true" CodeFile="initiatives.aspx.cs" Inherits="initiatives" Title="Aegis Logistics Limited - About Us - Continuous Improvement" %>

<%@ Register src="about_top.ascx" tagname="about_top" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <%--<!--  scroll to top  -->

<script src="js/jquery.localscroll-1.2.6-min.js" type="text/javascript"></script>
<script src="js/jquery.scrollTo-1.4.0-min.js" type="text/javascript"></script>
<script type="text/javascript">

$(document).ready(function () {
	$.localScroll();	
});

</script>

<!--  scroll to top  -->--%>

<!--  tabs  -->

<%--<script type="text/javascript" src="js/tabber.js"></script>
<link rel="stylesheet" href="css/example.css" type="text/css" media="screen" />

<script type="text/javascript">

/* Optional: Temporarily hide the "tabber" class so it does not "flash"
   on the page as plain HTML. After tabber runs, the class is changed
   to "tabberlive" and it will appear. */

document.write('<style type="text/css">.tabber{display:none;}<\/style>');
</script>--%>

<!--  tabs  -->

<!-- colorbox -->

<%--<link media="screen" rel="stylesheet" href="colorbox/colorbox.css" />	
<script type="text/javascript" src="colorbox/jquery.colorbox.js"></script>
<script type="text/javascript" src="colorbox/id_nos.js"></script>--%>

<!-- colorbox -->

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:about_top ID="about_top1" runat="server" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

    <div id="main_container">


	<div id="banner">
	<%--<img src="images/banner/general.jpg" border="0" alt="Continuous Improvement" title="Continuous Improvement" runat="server" />--%>
	<asp:Image runat="server" Visible="false"  border="0"  ID="ImgBanner" alt="Continuous Improvement" title="Continuous Improvement" ImageUrl='<%# string.Format("~/Admin/images/{0}",Eval("Image_vcr")) %>' />
	</div>
		
	<div class="clear"></div>
        
        
    <!--  Cont-left  -->  
		
	<div id="cont_left">
    
    <div id="pg_head"><a href="company_profile.aspx">About Us</a></div>
    
    <div id="rel_box_link">
    <ul>
    <%--<li><a href="company_profile.aspx">Company Profile</a></li>
    <li><a href="background_parent_company.aspx">Background</a></li>--%>
    <li><a href="key_management_team.aspx">Key Management &amp; Team</a></li>
    <li><a href="partners.aspx">Industry Partners</a></li>
    <li><a href="associations_credentials.aspx">Credentials</a></li>
    <li class="selec">Continuous Improvement</li>
  	<%--<li><a href="infrastructure.aspx">Infrastructure</a></li>
  	<li><a href="awards_recognition.aspx">Awards &amp; Recognition</a></li>--%>
    </ul>
    </div>
    
    </div>
        
    <!--  Cont-left  -->  
    
    <div id="gray_line" style="height:500px;"></div>
    
    <!--  Cont-right  -->
    
    <div id="cont_right">
    
    <div id="cont_right_box">
    
    
    <asp:Label ID="lblContent" runat="server" Text=""></asp:Label> 
    
    
    <%--<p>Since 2008, Aegis Group has adopted a policy of striving for continuous improvement in its operations. The use of 
    Lean Six Sigma as well as “5S” techniques has led to significant improvements in operational efficiency, improvements 
    in environmental standards, quicker turnaround times for tanker deliveries, and significantly improved quality 
    performance.</p> 

    <p>Aegis is committed to deepening the culture of excellence in operations throughout all its sites, and encourages its 
    employees to undergo training in Lean Six Sigma techniques.</p>--%>

    
    </div>
    
    
       
        
        
        <%-- <div class="tabber">
               
              
        <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
    
         <div class="tabbertab">
          <h2>Six Sigma</h2>
          
          <p>Implemented to improve the quality of process outputs by identifying and removing the causes of defects (errors) and minimizing variability in manufacturing and business process.</p>
          
          <div class="green_heading">Project Charter</div>
          
          <ul class="list">
              <li>Control of Polymerization in Styrene</li>
              <li>Quality control</li>
              <li>Material Loss control</li>
              <li>Tanker Overflow incident reduction</li>
              <li>To strive towards zero OHSE incidents</li>
              <li>LTD tanker cycle time reduction</li>
              <li>To strive towards Zero spillage</li>
              <li>Pipeline cleaning time reduction</li>
              <li>Minimization of shipment activity</li>
              <li>Pig Management - <a href="images/pig_mngmt.jpg" class="anylink" rel="example1" title="Pig Management System Involves recycling of foam pigs by resizing">View details</a></li>
              <li>Tank cleaning time reduction</li>
              <li>Operation Manpower requirement</li>
              <li>Procurement</li>
          </ul>
          
          <div class="clear"></div>
         </div>
        
        <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
    
    
         <div class="tabbertab">
          <h2>5 'S'</h2>
          
        <p>5 'S' is an integrated Japanese concept for “Workplace Management”</p>
        
        <div class="green_heading">Main Purpose of 5'S' implementation</div>
        
          <ul class="list">
              <li>To keep our workplace clean</li>
              <li>Provides nice environment to work</li>
              <li>Increase productivity</li>
              <li>Saving time</li>
              <li>Reducing the cost</li>
              <li>Provides safety</li>
          </ul>
          
          <p>Click images to zoom</p>
          
          <div style="float:left; width:100%;">
          <a href="images/initiative/img_1.jpg" rel="example2"><img src="images/initiative/img_1.jpg" border="0" width="250px" height="117px" class="init_img" /></a> 
          <a href="images/initiative/img_2.jpg" rel="example2"><img src="images/initiative/img_2.jpg" border="0" width="250px" height="117px" class="init_img" /></a>
          </div>
          
          <div style="float:left; width:100%;">
          <a href="images/initiative/img_3.jpg" rel="example2"><img src="images/initiative/img_3.jpg" border="0" width="250px" height="117px" class="init_img" /></a> 
          <a href="images/initiative/img_4.jpg" rel="example2"><img src="images/initiative/img_4.jpg" border="0" width="250px" height="117px" class="init_img" /></a>
          </div>
                  
          <div class="clear"></div>
          
         </div>
        
        <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
    
    
         <div class="tabbertab">
          <h2>Responsible Care</h2>
          
          <ul class="list">
          <li>Responsible Care is a chemical industry initiative which calls on companies to voluntarily demonstrate their commitment to improve all aspects of performance which relate to protection of health, safety and the environment.</li>
          <li>The first terminalling company to become signatory and is authorized to use the Responsible Care Logo by ICC <br />
          <a href="images/initiative/icc_cert.jpg" rel="example3" class="anylink"><img src="images/initiative/icc_cert.jpg" border="0" width="120px" height="114px" style="vertical-align: bottom; border:1px solid #c8c8c8;" /> click to zoom</a>
          <br /><br />
          </li>
          <li>Codes Of management practices -
          		<ul class="list_in">
                <li>Process Safety</li>
				<li>Employee Health and Safety</li>
				<li>Pollution Prevention</li>
				<li>Emergency response</li>
				<li>Distribution</li>
				<li>Product stewardship</li>
                </ul>
          </li>
          <li>Ensuring efficiency and accuracy using up to date technology viz. Radar Gauges, Mass Flow Meters, Integrated DCS system<br />
          <a href="images/initiative/dcs.jpg" rel="example4" title="Distributed Control System" class="anylink"><img src="images/initiative/dcs.jpg" border="0" width="120px" style="vertical-align: bottom; border:1px solid #c8c8c8;" /> click to zoom</a>
          <br /><br />
          </li>
          <li>Tanks Design ensures Zero dead stock</li>
          <li>Internal Floating roof tanks to ensure minimum vapour loss Only</li>
          <li>Private Sector terminalling company to have successfully implemented SAP R/3 IS-OIL solution<br />
          <a href="images/initiative/sap.jpg" rel="example5" title="SAP R/3 IS-OIL" class="anylink"><img src="images/initiative/sap.jpg" border="0" width="120px" style="vertical-align: bottom; border:1px solid #c8c8c8;" /> click to zoom</a>
          <br /><br />
          </li>
          <li>Maximized asset utilization, minimized downtime by engaging advanced cleaning techniques, preventive maintenance techniques</li>
          </ul>
          
          <div class="clear"></div>
          
         </div>
        
        <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
    
        </div> --%>
    
    </div>
    
    <!--  Cont-right  -->

<div class="clear" style="height:40px;"></div>

<!--  Breadcrum  -->
        
    <div id="bread_box">
    <a href="index.aspx" onmouseover="home.src='images/home_ic_h.jpg'" onmouseout="home.src='images/home_ic.jpg'" style="padding:7px 16px 10px 8px;"><img src="images/home_ic.jpg" border="0" name="home" title="Home" alt="" runat="server" /></a>
    <a href="company_profile.aspx">About us</a>
    <span style="margin-left:7px; margin-right:10px;">Continuous Improvement</span>
    </div>
        
    <!--  Breadcrum  -->

<%--<div style="float:right; margin-top: 9px;"><a href="#top" onmouseover="tp.src='images/top_h.jpg'" onmouseout="tp.src='images/top.jpg'"><img src="images/top.jpg" border="0" name="tp" alt="" runat="server" /></a></div>--%>
</div>

</asp:Content>

