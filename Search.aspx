﻿<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true" CodeFile="Search.aspx.cs" Inherits="Search" Title="Aegis - Search" %>

<%@ Register src="general_top.ascx" tagname="general_top" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <%--<!--  scroll to top  -->

<script src="js/jquery.localscroll-1.2.6-min.js" type="text/javascript"></script>
<script src="js/jquery.scrollTo-1.4.0-min.js" type="text/javascript"></script>
<script type="text/javascript">

$(document).ready(function () {
	$.localScroll();	
});

</script>

<!--  scroll to top  -->--%>

<!-- colorbox -->

<link media="screen" rel="stylesheet" href="colorbox/colorbox.css" />	
<script type="text/javascript" src="colorbox/jquery.colorbox.js"></script>
<script type="text/javascript" src="colorbox/id_nos.js"></script>

<!-- colorbox -->

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<uc1:general_top ID="general_top1" runat="server" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

    <div id="main_container">


	<div id="banner"><img src="images/banner/liquid_logistic.jpg" border="0" alt="Liquid Logistics" title="Liquid logistics" /></div>
		
	<div class="clear"></div>
    
    <div id="cont_full">
    <div id="pg_head">
     Results found for '<asp:Label ID="lblSearch" runat="server" Text=""></asp:Label>'</div>
    
  
    <asp:Label ID="lblMsg" runat="server" Text="No results found. Please try your search again." Visible="False"></asp:Label>
    <ul class="list">
          <asp:DataList ID="DataList1" runat="server" Width="750px">
        <ItemTemplate>
        <li style="border-bottom: 1px solid #eeeeee;">
          <strong><a class="anc" href='<%# Eval("Page_Url_vcr") %>'> <%# Eval("Page_Name_vcr")%> </a></strong>
          <br />
        
          <span style='color:#7C7C7C;'>
              <asp:Label ID="lblDetails" runat="server" Text='<%# Eval("Desc_vcr")%>'></asp:Label>
                 
          </span>
            </li>
        </ItemTemplate>        
        </asp:DataList>
        </ul>
        
   <%-- <div id="pg_head">Corporate Social Responsibility</div>
    
    <p>ANaRDe FOUNDATION was set up in 1979 with a mission to eradicate poverty through an integrated rural development approach addressing the multifaceted complexity of rural development. Constant approach in improving the quality of life of the villagers by providing water at the door step in villages and alleviating poverty are the major projects.</p>
    
    <div class="green_heading">Activities :</div>
    
    <ul class="list">
    <li>Economic Programmes</li>
    <li>Education</li>
    <li>Afforestation</li>
    <li>Health &amp; Sanitation</li>
    <li>Social &amp; Cultural</li>
    <li>Community Needs</li>
    </ul>
    
    <div class="csr_img">
    <a href="images/csr/img_1.jpg" rel="example1" title="Pure Water Station for Rural People">
    <img src="images/csr/img_1.jpg" width="200px" height="130px" border="0" alt="" /></a>
    <br />Pure Water Station for Rural People
    </div>
    
    <div class="csr_img">
    <a href="images/csr/img_2.jpg" rel="example2" title="Skill Development Programme">
    <img src="images/csr/img_2.jpg" width="200px" height="130px" border="0" alt="" /></a>
    <br />Skill Development Programme
    </div>
    
    <div class="csr_img">
    <a href="images/csr/img_3.jpg" rel="example3" title="Check Dams in Rural Area">
    <img src="images/csr/img_3.jpg" width="200px" height="130px" border="0" alt="" /></a>
    <br />Check Dams in Rural Area
    </div>
    
    <div class="csr_img">
    <a href="images/csr/img_4.jpg" rel="example4" title="Computer Training Programmes">
    <img src="images/csr/img_4.jpg" width="200px" height="130px" border="0" alt="" /></a>
    <br />Computer Training Programmes
    </div>--%>
    
    </div>
        

<div class="clear" style="height:40px;"></div>

<!--  Breadcrum  -->
        
    <div id="bread_box">
    <a href="index.aspx" onmouseover="home.src='images/home_ic_h.jpg'" onmouseout="home.src='images/home_ic.jpg'" style="padding:7px 16px 10px 8px;"><img src="images/home_ic.jpg" border="0" name="home" title="Home" alt="Home" /></a>
    <!--<a href="#">About Us</a>-->
    <span style="margin-left:7px; margin-right:10px;">Search</span>
    </div>
        
    <!--  Breadcrum  -->

<%--<div style="float:right; margin-top: 9px;"><a href="#top" onmouseover="tp.src='images/top_h.jpg'" onmouseout="tp.src='images/top.jpg'"><img src="images/top.jpg" border="0" name="tp" alt="Top" /></a></div>--%>
</div>

</asp:Content>

