﻿<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true" CodeFile="partners.aspx.cs" Inherits="partners" Title="Aegis Logistics Limited - About Us - Industry Partners" %>

<%@ Register src="about_top.ascx" tagname="about_top" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<%-- <!--  scroll to top  -->

<script src="js/jquery.localscroll-1.2.6-min.js" type="text/javascript"></script>
<script src="js/jquery.scrollTo-1.4.0-min.js" type="text/javascript"></script>
<script type="text/javascript">

$(document).ready(function () {
	$.localScroll();	
});

</script>

<!--  scroll to top  --> --%>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<uc1:about_top ID="about_top1" runat="server" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

<div id="main_container">


	<div id="banner">
<%--	<img src="images/banner/general.jpg" border="0" alt="Industry Partners" title="Industry Partners" runat="server" />--%>
<asp:Image runat="server" Visible="false"  border="0"  ID="ImgBanner" alt="Industry Partners" title="Industry Partners"  ImageUrl='<%# string.Format("~/Admin/images/{0}",Eval("Image_vcr")) %>' />
	</div>
		
	<div class="clear"></div>
        
        
    <!--  Cont-left  -->  
		
	<div id="cont_left">
    
    <div id="pg_head"><a href="company_profile.aspx">About Us</a></div>
    
    <div id="rel_box_link">
    <ul>
    <%--<li><a href="company_profile.aspx">Company Profile</a></li>
    <li><a href="background_parent_company.aspx">Background</a></li>--%>
    <li><a href="key_management_team.aspx">Key Management &amp; Team</a></li>
    <li class="selec">Industry Partners</li>
  	<li><a href="associations_credentials.aspx">Credentials</a></li>
  	<li><a href="initiatives.aspx">Continuous Improvement</a></li>
  	<%--<li><a href="infrastructure.aspx">Infrastructure</a></li>
  	<li><a href="awards_recognition.aspx">Awards &amp; Recognition</a></li>--%>
    </ul>
    </div>
    
    
    </div>
        
    <!--  Cont-left  -->  
    
    <div id="gray_line" style="height:500px;"></div>
    
    <!--  Cont-right  -->
    
    <div id="cont_right">
    
    <div id="cont_right_box">
        <asp:DataList ID="dtlImages" runat="server" RepeatColumns="4" 
            RepeatDirection="Horizontal">
        <ItemTemplate>        
            <div class="p_logo_box">
                <asp:Image ID="Img1"  runat="server" ToolTip='<%#Eval("Title_vcr") %>' style="border:0;" ImageUrl='<%# string.Format("~/Admin/images/{0}",Eval("Image_Path_vcr")) %>' />
            </div>
<%--            <img id="Img1" src="images/partners_logo/bharat_petroleum.jpg" border="0" title="Bharat Petroleum" alt="" runat="server" /></div>--%>
        </ItemTemplate>
        
        </asp:DataList>
    
   <%-- <div class="p_logo_box"><img src="images/partners_logo/bharat_petroleum.jpg" border="0" title="Bharat Petroleum" alt="" runat="server" /></div>
    
    <div class="p_logo_box"><img src="images/partners_logo/hindustan_petroleum.jpg" border="0" title="Hindustan Petroleum" alt="" runat="server" /></div>
    
    <div class="p_logo_box"><img src="images/partners_logo/shell.jpg" border="0" title="Shell" alt="" runat="server" /></div>
    
    <div class="p_logo_box"><img src="images/partners_logo/reliance.jpg" border="0" title="Reliance Industries Limited" alt="" runat="server" /></div>
    
    <div class="p_logo_box"><img src="images/partners_logo/essar_steel.jpg" border="0" title="Essar Steel" alt="" runat="server" /></div>
    
    <div class="p_logo_box"><img src="images/partners_logo/shv_gas.jpg" border="0" title="SHV Gas" alt="" runat="server" /></div>
    
    <div class="p_logo_box"><img src="images/partners_logo/tata_steel.jpg" border="0" title="TATA Steel" alt="" runat="server" /></div>
    
    <div class="p_logo_box"><img src="images/partners_logo/valvoline_cummins.jpg" border="0" title="Valvoline Cummins" alt="" runat="server" /></div>
    
    <div class="p_logo_box"><img src="images/partners_logo/bharat_oman.jpg" border="0" title="Bharat Oman" alt="" runat="server" /></div>
    
    <div class="p_logo_box"><img src="images/partners_logo/ongc_mrpl.jpg" border="0" title="ONGC MRPL" alt="" runat="server" /></div>
    
    <div class="p_logo_box"><img src="images/partners_logo/mahindra.jpg" border="0" title="Mahindra" alt="" runat="server" /></div>
    
    <div class="p_logo_box"><img src="images/partners_logo/unilever.jpg" border="0" title="Hindustan Unilever Limited" alt="" runat="server" /></div>
    
    <div class="p_logo_box"><img src="images/partners_logo/bajaj.jpg" border="0" title="Bajaj Electrical Limited" alt="" runat="server" /></div>
    
    <div class="p_logo_box"><img src="images/partners_logo/jubilant.jpg" border="0" title="Jubilant Life Sciences" alt="" runat="server" /></div>
    
    <div class="p_logo_box"><img src="images/partners_logo/bombay_dyeing.jpg" border="0" title="Bombay Dyeing" alt="" runat="server" /></div>
    
    <div class="p_logo_box"><img src="images/partners_logo/piaggio.jpg" border="0" title="Piaggio" alt="" runat="server" /></div>
    
    <div class="p_logo_box"><img src="images/partners_logo/spl.jpg" border="0" title="SPL" alt="" runat="server" /></div>--%>
    
    
    <div class="clear"></div>
    </div> 
    
    
    </div>
    
    <!--  Cont-right  -->

<div class="clear" style="height:40px;"></div>

<!--  Breadcrum  -->
        
    <div id="bread_box">
    <a href="index.aspx" onmouseover="home.src='images/home_ic_h.jpg'" onmouseout="home.src='images/home_ic.jpg'" style="padding:7px 16px 10px 8px;"><img src="images/home_ic.jpg" border="0" name="home" title="Home" alt="" runat="server" /></a>
    <a href="company_profile.aspx">About Us</a>
    <span style="margin-left:7px; margin-right:10px;">Industry Partners</span>
    </div>
        
    <!--  Breadcrum  -->

<%--<div style="float:right; margin-top: 9px;"><a href="#top" onmouseover="tp.src='images/top_h.jpg'" onmouseout="tp.src='images/top.jpg'"><img src="images/top.jpg" border="0" name="tp" alt="" runat="server" /></a></div>--%>
</div>


</asp:Content>

