﻿<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true" CodeFile="Tendor.aspx.cs" Inherits="Tendor" Title="Aegis Logistics Limited - Tendor" %>
<%@ Register src="general_top.ascx" tagname="general_top" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" src="js/mootools.js"></script>    
    <link rel="stylesheet" type="text/css" media="screen" href="css/screen.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  <uc1:general_top ID="general_top1" runat="server" />

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
 <div id="main_container" style="border-top: 1px solid #e4e4e4; padding-top:20px;">
 
 <div class="clear"></div>
        
     
    
    <div id="cont_full">    
    
    <div id="pg_head">Tendor</div>
    
    <div class="clear"></div>
    
    <div id="accordion" style="width:950px;">
     <div class="element atStart">                          
                          <ul class="list_news">  
            <asp:DataList ID="dtlInner" runat="server">
            <ItemTemplate>
                                            
                            <li><p><strong><%# Eval("Tendor_Title_vcr")%><br />
                       
                            </strong>
                           
                            </p>
                             <p><a href='<%# string.Format("Admin/Tendor/{0}.pdf",Eval("Tendor_path_vcr")) %>' target="_blank" class="v_pdf" >PDF</a></p>
                            
                             <%--<p><a href='<%# string.Format("Admin/Documents/{0}.pdf",Eval("Pdf_Path_vcr")) %>' target="_blank"><img src="images/pdf.jpg" /></a></p>--%>
                            </li>
                          
                
            </ItemTemplate>            
            </asp:DataList>
             </ul>
           </div>
    </div>
    </div>
 </div>
</asp:Content>

