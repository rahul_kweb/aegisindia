﻿<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true" CodeFile="hse_training_development.aspx.cs" Inherits="hse_training_development" Title="Aegis - HSE - Training & Development" %>

<%@ Register src="hse_top.ascx" tagname="hse_top" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<%--<!--  scroll to top  -->

<script src="js/jquery.localscroll-1.2.6-min.js" type="text/javascript"></script>
<script src="js/jquery.scrollTo-1.4.0-min.js" type="text/javascript"></script>
<script>

$(document).ready(function () {
	$.localScroll();	
});

</script>

<!--  scroll to top  -->--%>

<!-- colorbox -->

<link media="screen" rel="stylesheet" href="colorbox/colorbox.css" />	
<script type="text/javascript" src="colorbox/jquery.colorbox.js"></script>
<script type="text/javascript" src="colorbox/id_nos.js"></script>

<!-- colorbox -->

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:hse_top ID="hse_top1" runat="server" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
<div id="main_container">


	<div id="banner"><img src="images/banner/liquid_logistic.jpg" border="0" alt="Liquid Logistics" title="Liquid logistics" /></div>
		
	<div class="clear"></div>
        
        
    <!--  Cont-left  -->  
		
	<div id="cont_left">
    
    <div id="pg_head">HSE</div>
    
    <div id="rel_box_link">
    <ul>
    <li><a href="hse_highlights.aspx">Highlights</a></li>
  	<li class="selec">Training  &amp; Development</li>
    </ul>
    
    </div>
   
    
    <div style="float:left; margin-top:10px;"><a href="#"><img src="images/hse/dw_hse_policy.jpg" alt="" /></a></div>
    
        
    
    </div>
        
    <!--  Cont-left  -->  
    
    <div id="gray_line" style="height:500px;"></div>
    
    <!--  Cont-right  -->
    
    <div id="cont_right">
    
    <div id="cont_right_box">
    
     <asp:Label ID="lblContent" runat="server"></asp:Label>
    
    <%--
    <ul class="list">
    <li>Competence Development as a corner stone of HSE Management</li>
    <li>Senior Leaders as Change Agents</li>
    <li>Intensive In-House Training Program</li>
    <li>Value adding External Help in Training</li>
    <li>Participation in Industry level interaction</li>
    <li>Behavioral Safety Tools like NM and PI reporting at every level of organization</li>
    </ul>
    
    <div class="green_heading">Emergency Response Drills &nbsp;<span style="font-weight:normal; font-size:12px;">(Click images to zoom)</span></div>
    
    <div style="margin-bottom:20px;">
    <a href="images/hse/tm_1.jpg" rel="example1"><img src="images/hse/tm_1.jpg" width="150px" height="107px" border="0" alt="" class="hse_img" /></a>
    <a href="images/hse/tm_2.jpg" rel="example1"><img src="images/hse/tm_2.jpg" width="150px" height="107px" border="0" alt="" class="hse_img" /></a>
    <a href="images/hse/tm_3.jpg" rel="example1"><img src="images/hse/tm_3.jpg" width="150px" height="107px" border="0" alt="" class="hse_img" /></a>
    <a href="images/hse/tm_4.jpg" rel="example1"><img src="images/hse/tm_4.jpg" width="150px" height="107px" border="0" alt="" class="hse_img" /></a>
    </div>
    
    <div class="clear"></div>
    
    <div class="green_heading">Fire Fighting &amp; Evacuation Training &nbsp;<span style="font-weight:normal; font-size:12px;">(Click images to zoom)</span></div>
    
    <div>
    <a href="images/hse/tm_5.jpg" rel="example2"><img src="images/hse/tm_5.jpg" width="150px" height="107px" border="0" alt="" class="hse_img" /></a>
    <a href="images/hse/tm_6.jpg" rel="example2"><img src="images/hse/tm_6.jpg" width="150px" height="107px" border="0" alt="" class="hse_img" /></a>
    <a href="images/hse/tm_7.jpg" rel="example2"><img src="images/hse/tm_7.jpg" width="150px" height="107px" border="0" alt="" class="hse_img" /></a>
    <a href="images/hse/tm_8.jpg" rel="example2"><img src="images/hse/tm_8.jpg" width="150px" height="107px" border="0" alt="" class="hse_img" /></a>
    </div>
    
    --%>
    
    </div>        
    
    </div>
    
    <!--  Cont-right  -->

<div class="clear" style="height:40px;"></div>

<!--  Breadcrum  -->
        
    <div id="bread_box">
    <a href="index.aspx" onmouseover="home.src='images/home_ic_h.jpg'" onmouseout="home.src='images/home_ic.jpg'" style="padding:7px 16px 10px 8px;"><img src="images/home_ic.jpg" border="0" name="home" title="Home" /></a>
    <a href="#">HSE</a>
    <span style="margin-left:7px; margin-right:10px;">Training &amp; Development</span>
    </div>
        
    <!--  Breadcrum  -->

<%--<div style="float:right; margin-top: 9px;"><a href="#top" onmouseover="tp.src='images/top_h.jpg'" onmouseout="tp.src='images/top.jpg'"><img src="images/top.jpg" border="0" name="tp" /></a></div>--%>
</div>
</asp:Content>

