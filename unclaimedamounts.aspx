﻿<%@ Page Language="C#" MasterPageFile="~/aegis_master.master" AutoEventWireup="true"
    CodeFile="unclaimedamounts.aspx.cs" Inherits="unclaimedamounts" Title="Aegis Logistics Limited - Investor Relations - Unpaid & Unclaimed Amounts" %>

<%@ Register Src="investor_top.ascx" TagName="investor_top" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%--<!--  scroll to top  -->

<script src="js/jquery.localscroll-1.2.6-min.js" type="text/javascript"></script>
<script src="js/jquery.scrollTo-1.4.0-min.js" type="text/javascript"></script>
<script>

$(document).ready(function () {
	$.localScroll();	
});

</script>

<!--  scroll to top  -->--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:investor_top ID="investor_top1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div id="main_container" style="border-top: 1px solid #e4e4e4; padding-top: 20px;">
        <%--<div id="banner"><img src="images/banner/liquid_logistic.jpg" border="0" alt="Liquid Logistics" title="Liquid logistics" /></div>--%>
        <div class="clear">
        </div>
        <!--  Cont-left  -->
        <div id="cont_left">
            <div id="pg_head">
                Investor Relations
            </div>
            <div id="rel_box_link">
                <ul>
                    <li><a href="Corporate_Governances.aspx">Corporate Governance</a></li>
                    <li><a href="key_financial_data.aspx">Financial Information</a></li>
                    <%--<li><a href="key_financial_data.aspx">Key Financial Data</a></li>--%>
                    <li><a href="shares.aspx">Stock Information</a></li>
                    <li><a href="Debentures.aspx">Debentures</a></li>
                    <li><a href="reports_filings.aspx">Stock Exchange Communication</a></li>
                    <li><a href="events_presentations.aspx">Presentations</a></li>
                    <%--<li><a href="acquisitions.aspx">Acquisitions</a></li>
    <li><a href="corporate_governance.aspx">Corporate Governance</a></li>
    <li><a href="investor_contact.aspx">Investor Contact</a></li>--%>
                    <%--<li><a href="quarterly_results.aspx">Financial Results</a></li>--%>
                    <li><a href="news.aspx">News &amp; Events</a></li>
                    <li class="selec">Unpaid & Unclaimed Amounts</li>
                    <%--<li><a href="sealord_containers_limited.aspx">Sea Lord Containers Limited (Subsidiary)</a></li>--%>
                    <li><a href="Aegis_Gas_Lpg_Pvt_Ltd.aspx">Aegis Gas (LPG) Pvt. Ltd. (Subsidiary)</a></li>
                </ul>
            </div>
        </div>
        <!--  Cont-left  -->
        <div id="gray_line" style="height: 525px;">
        </div>
        <!--  Cont-right Admin/Documents/Quarterly_Results_2006-07_Pdf1.pdf" -->
        <div id="cont_right">
            <div id="cont_right_box">
                <div style="display: none">
                    <iframe src="http://203.115.112.109/aegisiepf/default.aspx" frameborder="0" width="685px"
                        height="450px"></iframe>
                </div>
                 <ul class="list">                    
                     <li><a href="Admin/unclaimedamounts/Unclaimed_Dividend_from_2009-10(Interim)-to-2015-16-(3rd Interim)-As-on-5-08-2016.pdf" target="_blank">Unclaimed Dividend from 2009-10 (Interim) to 2015-16 (3rd Interim) – Status as on 05-08-2016</a></li>
                     <li><a href="Admin/unclaimedamounts/Unclaimed_Bonus_Fraction_Amount-As-on-05-08-2016.pdf" target="_blank">Unclaimed Bonus Fraction Amount – Status as on 05-08-2016</a></li>
                     <li><a href="Admin/unclaimedamounts/Unclaimed_interest_on_matured_Deposits_and_unclaimed_amount_of_matured_Deposits.pdf" target="_blank">Unclaimed interest on matured Deposits and unclaimed amount of matured Deposits – Status as on 05-08-2016</a></li>
                </ul>
            </div>
        </div>
        <!--  Cont-right  -->
        <div class="clear" style="height: 40px;">
        </div>
        <!--  Breadcrum  -->
        <div id="bread_box">
            <a href="index.aspx" onmouseover="home.src='images/home_ic_h.jpg'" onmouseout="home.src='images/home_ic.jpg'"
                style="padding: 7px 16px 10px 8px;">
                <img src="images/home_ic.jpg" border="0" name="home" title="Home" alt="" /></a>
            <a href="#">Investor Relations</a> <span style="margin-left: 7px; margin-right: 10px;">Unpaid & Unclaimed Amounts</span>
        </div>
        <!--  Breadcrum  -->
        <%--<div style="float:right; margin-top: 9px;"><a href="#top" onmouseover="tp.src='images/top_h.jpg'" onmouseout="tp.src='images/top.jpg'"><img src="images/top.jpg" border="0" name="tp" alt="" /></a></div>--%>
    </div>
</asp:Content>
