﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class TestingPage : System.Web.UI.Page
{
    Class1 c = new Class1();

    protected void Page_Load(object sender, EventArgs e)
    {
        BindGrid();
    }
    private void SendReceiptEmail()
    {
        try
        {
            Label lbl = new Label();
            string receiptBody = string.Empty;
            receiptBody += "<table><tr><td><h3>Auto generated receipt for Retail LPG Inquiries </h3><br /> </td></tr><tr><td><label style='color:#4b4b4b;font-family: Arial, Helvetica, sans-serif;font-size: 14px;'>Thank you for your interest in our company . Our sales and marketing team shall get in touch with you soon.<br /> To learn more about Aegis India please log onto our website </label><a href='http://www.aegisindia.com' style='color:#848484;'>www.aegisindia .com</a><br />";
            receiptBody += "<br /></td></tr><tr><td><img src='http://www.aegisindia.com/images/logo.jpg' title='Aegis Logo' alt='Aegis Logo' style='height: 50px; width: 50px'></td></tr><tr><td><label style='color:#4b4b4b;font-family: Arial, Helvetica, sans-serif;font-size: 13px;'><b> AEGIS LOGISTICS LIMITED </b></label><br /><label style='color:#4b4b4b;font-family: Arial, Helvetica, sans-serif;font-size: 12px;'>403, Peninsula Chambers<br /> Peninsula Corporate Park<br /> G.K.Marg,<br /> Lower Parel(W)<br /> Mumbai - 400 013 <br />";
            receiptBody += "T : 022-6666 3666 <br />F : 022-6666 3777 <br />w : <a href='http://www.aegisindia.com' style='color:#848484;'>www.aegisindia .com</a></label></td></tr></table>";

            bool MailSentToClient = c.fn_SendMail("sumit@kwebmaker.com", "bd@aegisindia.com", "Receipt for Retail LPG Inquiries", receiptBody, lbl);
            if (MailSentToClient)
                Response.Write("Email Sent");
            else
                Response.Write("Email not Sent");
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    private void BindGrid()
    {
        try
        {
            gv.Columns[0].Visible = true;
            gv.DataSource = c.Display("EXEC AddUpdateGetInquiryMaster 'Get'");
            gv.DataBind();
            gv.Columns[0].Visible = false;

        }
        catch (Exception ex)
        {

            Response.Redirect(ex.Message);
        }
    }

  
    protected void btnExport_Click(object sender, EventArgs e)
    {
        Response.Clear();

        Response.AddHeader("content-disposition", "attachment;filename=FileName.xls");


        Response.ContentType = "application/Aegis_inquiries.xls";

        System.IO.StringWriter stringWrite = new System.IO.StringWriter();

        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

        gv.RenderControl(htmlWrite);

        Response.Write(stringWrite.ToString());

        Response.End();
    }
}