﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class hse : System.Web.UI.Page
{
    Class1 c = new Class1();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            c.BindPageContent(33, lblContent);
            c.BindInnerPageBanner(33, ImgBanner);

            DataTable dt = new DataTable();
            dt = c.Display("EXEC AddUpdateGetPdfPage 'Get_By_Master_Id',0,16");
            if (dt.Rows.Count > 0)
            {
                link.HRef = "Admin/Documents/" + dt.Rows[0]["Pdf_Page_Path_vcr"].ToString()+".pdf";
            }
            else
            {
                link.HRef = "";
            }

            
        }
    }
}
