﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class general_information : System.Web.UI.Page
{
    Class1 c = new Class1();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["EmployeeName"].Equals("") || Session["EmployeeLoginId"].Equals(""))
        {
            Response.Redirect("employee_login.aspx");
        }
        else
        {
            if (!Page.IsPostBack)
            {
                lblEmpName.Text = "Welcome, " + Session["EmployeeName"].ToString();
                getDetatils();
                c.BindInnerPageBanner(44, ImgBanner);
            }
        }
    }
    private void getDetatils()
    {
        try
        {
            DataTable dt = new DataTable();
            dt = c.Display("EXEC AddUpdateGetNoticeMaster 'Get'");
            dtlNotice.DataSource = dt;
            dtlNotice.DataBind();

        }
        catch (Exception ex)
        {
            Msg.ShowError("Some error occurred.");

        }

    }

    protected void lnkLogOut_Click(object sender, EventArgs e)
    {
        Session["EmployeeName"] = "";
        Session["EmployeeLoginId"] = "";
        Response.Redirect("employee_login.aspx", true);
    }


}
