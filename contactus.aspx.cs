﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Drawing;

public partial class contactus : System.Web.UI.Page
{
    Class1 c = new Class1();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GetData();
           // c.BindPageContent(21, lblContent);
        }
    }
    private void GetData()
    {
        try
        {
            DataTable dt = new DataTable();
            dt = c.Display("EXEC AddUpdateContactMaster 'Get'");
            if (dt.Rows.Count > 0)
            {
                lblRegOffice.Text = dt.Rows[0]["Reg_Office_Address_vcr"].ToString();
                lblCorpOffice.Text = dt.Rows[0]["Corp_Office_Address_vcr"].ToString();
                //lblTerminalOffice.Text = dt.Rows[0]["Terminal_Address_vcr"].ToString();
                lblRegPhone.Text = dt.Rows[0]["Reg_Office_Phone_vcr"].ToString();
                lblRegFax.Text = dt.Rows[0]["Reg_Office_Fax_vcr"].ToString();

                lblCorpPhone.Text = dt.Rows[0]["Corp_Office_Phone_vcr"].ToString();
                lblCorpFax.Text = dt.Rows[0]["Corp_Office_Fax_vcr"].ToString();
                //lblTerminalPhone.Text = dt.Rows[0]["Terminal_Phone_vcr"].ToString();
                //lblTerminalFax.Text = dt.Rows[0]["Terminal_Fax_vcr"].ToString();
            }
            else
            {

            }
        }
        catch (Exception ex)
        { }
    }

    private string CheckBlank()
    {
        string blank = "";
        if (txtName.Value.Equals("") || txtName.Value.Equals("Name"))
        {
            blank = "Name,";

        }
        if (txtEmail.Value.Equals("") || txtEmail.Value.Equals("Email"))
        {
            blank += "Email,";
        }
        if (txtEmail.Value.Length > 0)
        {
            var atpos = txtEmail.Value.IndexOf("@");
            var dotpos = txtEmail.Value.LastIndexOf(".");
            if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= txtEmail.Value.Length)
            {
                blank += " Email id is not valid,";
            }
        }

        if (txtCompanyName.Value.Equals("") || txtCompanyName.Value.Equals("Company Name"))
        {
            blank += "Company Name,";
        }
        if (txtTelephone.Value.Equals("") || txtTelephone.Value.Equals("Telephone"))
        {
            blank += "Telephone,";
        }
        if (txtMessage.Text.Equals("") || txtMessage.Text.Equals("Message"))
        {
            blank += "Message ";
        }
        if (blank.Trim() != "")
        {
            blank = blank.Remove(blank.Length - 1);
        }
        return blank.Trim();


    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        //lblError.Visible = false;
        string blank = CheckBlank();
        if (!blank.Equals(""))
        {
            lblError.ForeColor = Color.Red;
            lblError.Text = "Please Fill " + blank;
        }



        else
        {

            //if (txtName.Value.Trim().Length > 0 && txtCompanyName.Value.Trim().Length > 0 && txtMessage.Text.Trim().Length > 0)
            //{
            AddInquiry();

        }
    }
    private void AddInquiry()
    {
        try
        {
            using (SqlCommand cmd = new SqlCommand("AddUpdateGetInquiryMaster", c.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para_vcr", "Add");
                cmd.Parameters.AddWithValue("@Name_vcr", txtName.Value.Trim());
                cmd.Parameters.AddWithValue("@Email_vcr", txtEmail.Value.Trim());
                cmd.Parameters.AddWithValue("@Company_Name_vcr", txtCompanyName.Value.Trim());
                cmd.Parameters.AddWithValue("@Telephone_vcr", txtTelephone.Value.Trim());
                cmd.Parameters.AddWithValue("@Inquiry_Type_vcr", ddlType.SelectedItem.Text);
                cmd.Parameters.AddWithValue("@Message_vcr", txtMessage.Text.Trim());
                cmd.Parameters.AddWithValue("@City", ddlcity.SelectedValue);

                if (!c.con.State.Equals(ConnectionState.Open))
                {
                    c.con.Open();
                }
                cmd.ExecuteNonQuery();
                string body = "";
                string receiptBody = string.Empty;

                body = body + "<table><tr><td>Name</td><td>" + txtName.Value.Trim() + "</td></tr>";
                body = body + " <tr><td>Email</td><td>" + txtEmail.Value.Trim() + "</td></tr>";
                body = body + " <tr><td>Company</td><td>" + txtCompanyName.Value.Trim() + "</td></tr>";
                body = body + " <tr><td>City</td><td>" + ddlcity.SelectedValue.Trim() + "</td></tr>";
                body = body + " <tr><td>Telephone</td><td>" + txtTelephone.Value.Trim() + "</td></tr>";
                body = body + " <tr><td>Inquiry Type</td><td>" + ddlType.SelectedItem.Text + "</td></tr>";
                body = body + "<tr><td>Message</td><td>" + txtMessage.Text.Trim() + "</td></tr>";
                body = body + "</table>";



                receiptBody += "<table><tr><td><h3>Auto generated receipt for Retail LPG Inquiries </h3><br /> </td></tr><tr><td><label style='color:#4b4b4b;font-family: Arial, Helvetica, sans-serif;font-size: 14px;'>Thank you for your interest in our company . Our sales and marketing team shall get in touch with you soon.<br /> To learn more about Aegis India please log onto our website </label><a href='http://www.aegisindia.com' style='color:#848484;'>www.aegisindia .com</a><br />";
                receiptBody += "<br /></td></tr><tr><td><img src='http://www.aegisindia.com/images/logo.jpg' title='Aegis Logo' alt='Aegis Logo' style='height: 50px; width: 50px'></td></tr><tr><td><label style='color:#4b4b4b;font-family: Arial, Helvetica, sans-serif;font-size: 13px;'><b> AEGIS LOGISTICS LIMITED </b></label><br /><label style='color:#4b4b4b;font-family: Arial, Helvetica, sans-serif;font-size: 12px;'>403, Peninsula Chambers<br /> Peninsula Corporate Park<br /> G.K.Marg,<br /> Lower Parel(W)<br /> Mumbai - 400 013 <br />";
                receiptBody += "T : 022-6666 3666 <br />F : 022-6666 3777 <br />w : <a href='http://www.aegisindia.com' style='color:#848484;'>www.aegisindia .com</a></label></td></tr></table>";

                //        <asp:ListItem Value="1">General Inquiries</asp:ListItem>
                //<asp:ListItem Value="2">Liquid Logistics Inquiries</asp:ListItem>
                //<asp:ListItem Value="3">Gas Logistics Inquiries</asp:ListItem>
                //<asp:ListItem Value="4">EPC Services Inquiries</asp:ListItem>
                // <asp:ListItem Value="5">Retail LPG Inquiries</asp:ListItem>
                // <asp:ListItem Value="6">Marine Inquiries</asp:ListItem>




                if (ddlType.SelectedItem.Value.Equals("1"))//General Inquiries
                {
                    //int Mail = c.SendEMail("Inquiry Details for General Inquiries", body, "rohan.singh@aegisindia.com", "rohan.singh@aegisindia.com");
                    //if (Mail.Equals("1"))
                    //{
                    //    this.Title = "SENT";
                    //}
                    //else
                    //{
                    //    this.Title = "Not SENT";
                    //}
                    //int Mail = c.SendEMail("Inquiry Details for General Inquiries", body, txtEmail.Value, "saloni@kwebmaker.com");




                    Label lbl = new Label();
                    bool MailSent = c.fn_SendMail("bd@aegisindia.com", "aegis@aegisindia.com", "Inquiry Details for General Inquiries", body, lbl);



                    bool MailSentToClient = c.fn_SendMail(txtEmail.Value.Trim(), "bd@aegisindia.com", "Receipt for General Inquiries", receiptBody, lbl);

                    if (!MailSent)
                    {
                        this.Title = lbl.Text;
                    }



                }
                else if (ddlType.SelectedItem.Value.Equals("2"))//Value="2">Liquid Logistics Inquiries
                {
                    //Liquid Logistics : ltdmktg@aegisindia.com
                    //  int Mail = c.SendEMail("Inquiry Details for General Inquiries", body, "sunil.t@kwebmaker.com", "sunil.t@kwebmaker.com");

                    //int Mail = c.SendEMail("Inquiry Details for Liquid Logistics Inquiries", body, "ltdmktg@aegisindia.com", "ltdmktg@aegisindia.com");
                    //if (Mail.Equals("1"))
                    //{
                    //    this.Title = "SENT";
                    //}
                    //else
                    //{
                    //    this.Title = "Not SENT";
                    //}


                    Label lbl = new Label();
                    bool MailSent = c.fn_SendMail("ltdmktg@aegisindia.com", "aegis@aegisindia.com", "Inquiry Details for Liquid Logistics", body, lbl);



                    bool MailSentToClient = c.fn_SendMail(txtEmail.Value.Trim(), "bd@aegisindia.com", "Receipt for Liquid Logistics", receiptBody, lbl);

                    if (!MailSent)
                    {
                        this.Title = lbl.Text;
                    }

                }
                else if (ddlType.SelectedItem.Value.Equals("3"))//Gas Logistics Inquiries
                {
                    //Gas Logistics : sanjitmondal@aegisindia.com
                    //int Mail = c.SendEMail("Inquiry Details for Gas Logistics Inquiries", body, "sanjitmondal@aegisindia.com", "sanjitmondal@aegisindia.com");
                    //if (Mail.Equals("1"))
                    //{
                    //    this.Title = "SENT";
                    //}
                    //else
                    //{
                    //    this.Title = "Not SENT";
                    //}


                    Label lbl = new Label();
                    bool MailSent = c.fn_SendMail("sanjitmondal@aegisindia.com", "aegis@aegisindia.com", "Inquiry Details for Gas Logistics", body, lbl);



                    bool MailSentToClient = c.fn_SendMail(txtEmail.Value.Trim(), "bd@aegisindia.com", "Receipt for Gas Logistics", receiptBody, lbl);

                    if (!MailSent)
                    {
                        this.Title = lbl.Text;
                    }



                }
                else if (ddlType.SelectedItem.Value.Equals("4"))//EPC Services Inquiries
                {
                    //EPC Services: rohan.singh@aegisindia.com
                    //    int Mail = c.SendEMail("Inquiry Details for EPC Services Inquiries", body, "rohan.singh@aegisindia.com", "rohan.singh@aegisindia.com");
                    //    if (Mail.Equals("1"))
                    //    {
                    //        this.Title = "SENT";
                    //    }
                    //    else
                    //    {
                    //        this.Title = "Not SENT";
                    //    }


                    Label lbl = new Label();
                    bool MailSent = c.fn_SendMail("rohan.singh@aegisindia.com", "aegis@aegisindia.com", "Inquiry Details for EPC Services  Logistics", body, lbl);



                    bool MailSentToClient = c.fn_SendMail(txtEmail.Value.Trim(), "bd@aegisindia.com", "Receipt for EPC Services  Logistics", receiptBody, lbl);

                    if (!MailSent)
                    {
                        this.Title = lbl.Text;
                    }




                }

                else if (ddlType.SelectedItem.Value.Equals("5"))//Retail LPG Inquiries
                {
                    //Retail LPG (Packed and Auto LPG) : ag.ravindra@aegisindia.com
                    //bool MailSent = c.fn_SendMail(("Inquiry Details for Retail LPG Inquiries", body, "ag.ravindra@aegisindia.com", "ag.ravindra@aegisindia.com");

                    // int Mail = c.SendEMail("Inquiry Details for Retail LPG Inquiries", body, "sumitbhoir16@rediffmail.com", "sumitbhoir16@rediffmail.com");


                    //int Mail1 = c.SendEMail("thanks", body, txtEmail.Value, "sumitbhoir16@rediffmail.com");

                    Label lbl = new Label();
                    bool MailSent = c.fn_SendMail("sales.support@aegisindia.com,n.padmanabhan@aegisindia.com,hs.mann@aegisindia.com", "aegis@aegisindia.com", "Inquiry Details for Retail LPG Inquiries", body, lbl);



                    bool MailSentToClient = c.fn_SendMail(txtEmail.Value.Trim(), "bd@aegisindia.com", "Receipt for Retail LPG Inquiries", receiptBody, lbl);

                    if (!MailSent)
                    {
                        this.Title = lbl.Text;
                    }

                }
                else if (ddlType.SelectedItem.Value.Equals("6"))//Marine Inquiries
                {
                    //Marine: marine@aegisindia.com
                    string Mail = c.SendEMail("Inquiry Details for Marine Inquiries ", body, "marine@aegisindia.com", "marine@aegisindia.com");
                    if (Mail.Equals(""))
                    {
                        this.Title = "SENT";
                    }
                    else
                    {
                        this.Title = Mail;
                    }
                }

                txtName.Value = "Name";
                txtEmail.Value = "Email";
                txtCompanyName.Value = "Company Name";
                txtTelephone.Value = "Telephone";
                lblError.ForeColor = Color.Green;
                lblError.Text = "Inquiry Submitted Sucessfully.";
                txtMessage.Text = "Message";
                ddlType.SelectedIndex = 0;
                ddlcity.SelectedIndex = 0;
            }
        }
        catch (Exception ex)
        {

        }
    }
}
